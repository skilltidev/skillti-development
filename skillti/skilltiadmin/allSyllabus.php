
<?php
	include('adminheader.php'); 
	protect_admin_page();
	
	$sid = $_GET['edit'];
	$UpdQuery = "select * from syllabus where ID='$sid'";
	$UpdResult = $dbObject->query($UpdQuery);
	$UpdRow = mysqli_fetch_assoc($UpdResult);
	$catID = $UpdRow['Category'];
	
	if(isset($_POST['UpdateSyllabus'])) {
		
		$catID = $_POST['cat'];
		$SubcatID = $_POST['subcat'];
		$other = $_POST['other'];
		$key1 = str_replace("'", "", $_POST['key1']);
		$key2 = str_replace("'", "", $_POST['key2']);
		$key3 = str_replace("'", "", $_POST['key3']);
		$key4 = str_replace("'", "", $_POST['key4']);
		$key5 = str_replace("'", "", $_POST['key5']);
		$key6 = str_replace("'", "", $_POST['key6']);
		$key7 = str_replace("'", "", $_POST['key7']);
		$key8 = str_replace("'", "", $_POST['key8']);
		$key9 = str_replace("'", "", $_POST['key9']);
		$key10 = str_replace("'", "", $_POST['key10']);
		$description = str_replace("'", "", $_POST['description']);
		
		$Query = "update syllabus set Category='".$catID."', SubCategory='".$SubcatID."', OtherSubCat='".$other."', KeyPoint1='".$key1."', KeyPoint2='".$key2."', KeyPoint3='".$key3."', KeyPoint4='".$key4."', KeyPoint5='".$key5."', KeyPoint6='".$key6."', KeyPoint7='".$key7."', KeyPoint8='".$key8."', KeyPoint9='".$key9."', KeyPoint10='".$key10."', Description='".$description."' where ID='".$sid."'";
		$Update = $dbObject->query($Query);
		if($Update == true) {
		    print "<script>setTimeout(\"location.href = '".$base_url."skilltiadmin/allSyllabus?edit=".$sid."';\",500);</script>";
	        session_start();
			$_SESSION['SESS_MSG'] = "Syllabus has beed updated.";
		}
	}
?>

  <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper"><!-- HTML (DOM) sourced data -->
			<section id="html">
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body ">
							
							<?php if($sid == '') { ?>
								<form class="form px-3 mt-2">
									<div class="form-body">
										<h4 class="form-section"><i class="fa fa-crosshairs"></i> Syllabus List</h4>
									</div>
								</form>
								<div class="card-block card-dashboard">
								
								<table class="table table-striped table-bordered sourced-data">
										<thead>
											<tr>
												<th>SNO</th>
												<th>User Email</th>
												<th>Category</th>
												<th>Sub Category</th>
												<th>Created Date</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										<?php
										$Query = "select * from syllabus";
										$Result = $dbObject->query($Query);
										$i=1;
										while($Row = mysqli_fetch_assoc($Result)) { 
										$userID = $Row['UserID'];
										$CatID = $Row['Category'];
										$SubCatID = $Row['SubCategory'];
										
										$UQuery = "select EmailID from users where UserID='$userID'";
										$UResult = $dbObject->query($UQuery);
										$URow = mysqli_fetch_assoc($UResult);
										
										$CQuery = "select CatName from category where CatID='$CatID'";
										$CResult = $dbObject->query($CQuery);
										$CRow = mysqli_fetch_assoc($CResult);
										
										$SCQuery = "select CatName from category where CatID='$SubCatID'";
										$SCResult = $dbObject->query($SCQuery);
										$SCRow = mysqli_fetch_assoc($SCResult);
										?>
											<tr >
												<td><?php echo $i;?></td>
												<td><?php echo $URow['EmailID'];?></td>
												<td><?php echo $CRow['CatName'];?></td>
												<td><?php echo $SCRow['CatName'];?></td>
												<td><?php echo date('d M, Y', strtotime($Row['CreatedDate']));?></td>
												<td class="text-center"><a title="Edit" href="allSyllabus?edit=<?php echo $Row['ID'];?>"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size: 20px !important;"></i></a>
												&nbsp;&nbsp;<a title="Delete" id="<?php echo $Row['ID'];?>" class="delete"><i class="fa fa-times" aria-hidden="true" style="font-size: 20px !important;color:red"></i></a>
												</td>
												
											</tr>
										<?php $i++; } ?>
											
										</tbody>
										
									</table>
								</div>
							
							<?php } else { ?>
							<div class="px-3 mt-2">
							<form class="form form-horizontal striped-rows form-bordered" action="" method="post" enctype="multipart/form-data">
        	                    	<div class="form-body">
									  
                                        <?php include("../include/error.msg.inc.php"); ?>
									    <h4 class="form-section"><i class="fa fa-plus"></i> Edit Syllabus</h4>
									        
        		                        <div class="form-group row">
        									<label class="col-md-3 label-control" for="projectinput5">Category</label>
        									<div class="col-md-9">
        		                            <select class="form-control" name="cat" id="cat" required>
												<option value="">Select Category</option>
												 <?php
													$Query = "select * from category where parent=0 and Status='ACTIVE'";
													$Result =  $dbObject->query($Query);
													while($Row = mysqli_fetch_assoc($Result)) {
												?>
												<option value="<?php echo $Row['CatID'];?>"><?php echo $Row['CatName'];?></option>
												<?php } ?>
											</select>																			
        		                            </div>
        		                        </div>
										
										<div class="form-group row">
        									<label class="col-md-3 label-control" >Sub Category</label>
        									<div class="col-md-9">
        		                            <select class="form-control" name="subcat" id="subcat" style="display:none;">
												<option value="">Select Sub Category</option>
											</select>	
											<select class="form-control" name="subcat" id="subcat2" >
												<option value="">Select Category</option>
												 <?php
													$Query = "select * from category where Parent='$catID' and Status='ACTIVE'";
													$Result =  $dbObject->query($Query);
													while($Row = mysqli_fetch_assoc($Result)) {
												?>
												<option value="<?php echo $Row['CatID'];?>"><?php echo $Row['CatName'];?></option>
												<?php } ?>
												<option value="999999">Other</option>
											</select>	
        		                            </div>
        		                        </div>
        
        		                       <div class="form-group row" style="display:none;" id="other">
        									<label class="col-md-3 label-control">Other Sub Category</label>
        									<div class="col-md-9">
        										<input type="text" class="form-control attr" placeholder="Enter Sub Category" value="<?php echo $UpdRow['OtherSubCat'];?>" name="other" />
											</div>
        								</div>
        
        								<div class="form-group row">
        									<label class="col-md-3 label-control" >Key Point 1</label>
        									<div class="col-md-9">
												<input type="text" class="form-control" placeholder="Key Point 1" value="<?php echo $UpdRow['KeyPoint1'];?>" name="key1" required />
        									</div>
        								</div>
										
										<div class="form-group row">
        									<label class="col-md-3 label-control" >Key Point 2</label>
        									<div class="col-md-9">
												<input type="text" class="form-control" placeholder="Key Point 2" value="<?php echo $UpdRow['KeyPoint2'];?>" name="key2" required />
        									</div>
        								</div>
										
										<div class="form-group row">
        									<label class="col-md-3 label-control" >Key Point 3</label>
        									<div class="col-md-9">
												<input type="text" class="form-control" placeholder="Key Point 3" value="<?php echo $UpdRow['KeyPoint3'];?>" name="key3" required />
        									</div>
        								</div>
										
										<div class="form-group row">
        									<label class="col-md-3 label-control" >Key Point 4</label>
        									<div class="col-md-9">
												<input type="text" class="form-control" placeholder="Key Point 4" value="<?php echo $UpdRow['KeyPoint4'];?>" name="key4" required />
        									</div>
        								</div>
										
										<div class="form-group row">
        									<label class="col-md-3 label-control" >Key Point 5</label>
        									<div class="col-md-9">
												<input type="text" class="form-control" placeholder="Key Point 5" value="<?php echo $UpdRow['KeyPoint5'];?>" name="key5" required />
        									</div>
        								</div>
										
										<div class="form-group row">
        									<label class="col-md-3 label-control" >Key Point 6</label>
        									<div class="col-md-9">
												<input type="text" class="form-control" placeholder="Key Point 6" value="<?php echo $UpdRow['KeyPoint6'];?>" name="key6"  />
        									</div>
        								</div>
										
										<div class="form-group row">
        									<label class="col-md-3 label-control" >Key Point 7</label>
        									<div class="col-md-9">
												<input type="text" class="form-control" placeholder="Key Point 7" value="<?php echo $UpdRow['KeyPoint7'];?>" name="key7"  />
        									</div>
        								</div>
										
										<div class="form-group row">
        									<label class="col-md-3 label-control" >Key Point 8</label>
        									<div class="col-md-9">
												<input type="text" class="form-control" placeholder="Key Point 8" value="<?php echo $UpdRow['KeyPoint8'];?>" name="key8"  />
        									</div>
        								</div>
										
										<div class="form-group row">
        									<label class="col-md-3 label-control" >Key Point 9</label>
        									<div class="col-md-9">
												<input type="text" class="form-control" placeholder="Key Point 9" value="<?php echo $UpdRow['KeyPoint9'];?>" name="key9"  />
        									</div>
        								</div>
										
										<div class="form-group row">
        									<label class="col-md-3 label-control" >Key Point 10</label>
        									<div class="col-md-9">
												<input type="text" class="form-control" placeholder="Key Point 10" value="<?php echo $UpdRow['KeyPoint10'];?>" name="key10"  />
        									</div>
        								</div>
										
										<div class="form-group row last">
        									<label class="col-md-3 label-control" >Description</label>
        									<div class="col-md-9">
												<textarea class="form-control" placeholder="About your profile" name="description" ><?php echo $UpdRow['Description'];?></textarea>
        									</div>
        								</div>
										
										
										<div class="form-actions text-center">
											<a href="allSyllabus"> <button type="button" class="btn btn-raised btn-warning mr-1">
												<i class="ft-x"></i> Cancel
											</button></a>
											<button type="submit" name="UpdateSyllabus" class="btn btn-raised btn-primary">
												<i class="fa fa-check-square-o"></i> Update
											</button>
										</div>
																						
        							</div>
        
        	                        
        	                    </form>
							</div>
							<?php } ?>
							
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--/ HTML (DOM) sourced data -->
			</div>
        </div>

   

<?php include('adminfooter.php'); ?>
<script>
   
 $('.delete').click(function(){
  var el = this;
  var id = this.id;
  
  var syllabusid = id;
  
  // AJAX Request
  $.ajax({
   url: 'deleteCat.php',
   type: 'POST',
   data: { sid:syllabusid },
   success: function(response){

    $(el).closest('tr').css('background','tomato');
    $(el).closest('tr').fadeOut(800, function(){ 
     $(this).remove();
    });

   }
  });

 });


</script>
<script>
$(document).ready(function(){
    $(document).on('change','#cat',function(){
        var Cid = $(this).val();
		
        $.ajax({
            url:'../getSubCat.php',
            type:'post',
            data:{'Cid':Cid},
            success:function(data){
				$('#subcat2').hide();
				$('#subcat').show();
                $('#subcat').html(data);
            }
        })
    });
    
     $(document).on('change','#subcat',function(){
        var subcat = $(this).val();
        if(subcat == '999999') {
            $("#other").show();
            $('.attr').attr("required", "required");
        } else {
             $("#other").hide();
             $('.attr').removeAttr("required", "required");
        }
        
     });
});


	var cat = '<?php echo $UpdRow['Category'];?>';
    document.getElementById("cat").value = cat;
	
	var subcat = '<?php echo $UpdRow['SubCategory'];?>';
    document.getElementById("subcat2").value = subcat;
	
	
</script>