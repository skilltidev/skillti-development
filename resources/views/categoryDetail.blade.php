@extends('header.home_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")

@section('maincontent')
    <section class="mid_content">
        <div class="container">
            <div class="row">
                @if($selectedCategory != '')
                <div class="col-xs-12 col-sm-3">
                    <div id="" data-spy="">
                        <div class="filter_bx">
                            @foreach($menus as $k => $cat)
                                @if($selectedCategory == $cat->name)
                            <h3>{{$cat->name}}</h3>
                            <ul>
                                @if(!empty($cat->subCategory))
                                    @foreach($cat->subCategory as $j => $subCat)
                                        <li>
                                            <div onclick="location='<?php echo url('category/'.$subCat->link); ?>'">
                                                <input {{($selected == $subCat->link)?"checked":""}} type="checkbox" name="category[]" id="category{{$k.$j}}"/>
                                                <label for="category{{$k.$j}}">{{$subCat->name}}</label>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif
                @if(!empty($mentors) && count($mentors) > 0)
                    <div class="col-xs-12 col-sm-9">
                        <div class="profile_list">
                            @foreach($mentors as $mentor)
                                {{--{{debug($mentor)}}--}}
                                <ul>
                                    <li><img src="{{profile_image(asset('profile-photo/'.$mentor->profilePic))}}" alt="{{$mentor->profilePic}}"></li>
                                    <li>
                                        <div class="profile_list_bx">
                                            <h4><a href="{{url('profile-detail/'.custom_encode($mentor->userId))}}">{{$mentor->firstName." ".$mentor->lastName}}</a></h4>
                                            @if($mentor->tagline != '')
                                                <h4>{{$mentor->tagline}}</h4>
                                            @endif
                                            @if($mentor->shortBio != '')
                                                <div class="bio_disc">{{$mentor->shortBio}}</div>
                                            @endif
                                            <div class="pills">
                                                <ul class="list-inline">
                                                    <li class="head_main">Skills</li>
                                                    @foreach(json_decode($mentor->skills) as $k => $cat)
                                                        @if($k < 5)
                                                            <li>{{$cat}}</li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <div class="test">
                                                <ul class="list-inline">
                                                    <li class="head_main">Location : </li>
                                                    <li>{{$mentor->state.", ".$mentor->country}}</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            @endforeach
                        </div>
                    </div>
                @else
                    <div class="col-xs-12 col-sm-9">
                        <div class="profile_list">
                            <h3>Sorry, we couldn't find any results for "{{str_replace('-', ' ', $selected)}}" category</h3><br>
                            <h4>Try adjusting your search. Here are some ideas: </h4><h4>
                                <ul style="line-height: 39px;color: #727272;margin-top: 20px;margin-left: -18px;">
                                    <li>Make sure all words are spelled correctly.</li>
                                    <li>Try different search terms.</li>
                                    <li>Try more general search terms.</li>
                                </ul>
                            </h4>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('scripting')
@endsection

@section('footer')
    @include('footer.footer_home')
@endsection
