<?php include('include/config.php');?>
<?php include('include/setting.php');

$PQuery = "select profilePic from users where UserID='".$_SESSION['USER_ID']."'";
$PResult =  $dbObject->query($PQuery);
$PRow = mysqli_fetch_assoc($PResult);
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link rel="icon" href="assets/images/skillti_logo_4_update-fav_icon.png" type="image/x-icon" />
<?php $pageName = explode(".",$page) ?>
<title><?php echo ucwords($pageName[0]);?> | Skillti</title>
<!--font-awesome.min-css-->
<link href="assets/css/font-awesome.min.css" type="text/css" rel="stylesheet"/>
<!--bootstrap-css-->
<link href="assets/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
<!--custom-css-->
<link href="assets/css/style.css" type="text/css" rel="stylesheet" />
<!--media-query-css-->
<link href="assets/css/media.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="assets/css/animate.css">

<link rel="stylesheet" href="assets/css/jquery-ui.css">
</head>

<body>
	<header>
		<div class="top_header">
			<div class="container">
				<div class="row">
					<div class="col-xs-5 col-sm-3">
						<div class="logo">
						    <?php IF($_SESSION['USER_ROLE']==2) { ?>
							<a href="<?php echo $base_url;?>dashboard"><img src="assets/images/skillti_update.png" alt="SkillTI" ></a>
							<?php } else { ?>
							<a href="<?php echo $base_url;?>"><img src="assets/images/skillti_update.png" alt="SkillTI" ></a>
							<?php } ?>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6  mobile_search">
						<div class="search_bx">
							<!--<select>
								<option>Library</option>
								<option>Lorem</option>
								<option>Dolor</option>
							</select>-->
							<form action="category" method="get">
							<div class="search_inr">
								<input type="text" name="search" id="search" placeholder="Find Your Mentor,Technology...." autocomplete="off" />
								<button type="submit"><i class="fa fa-search"></i></button>
							</div>
							</form>
						</div>
					</div>
					<div class="col-xs-7 col-sm-3 mobile_float">
						<div class="action">
							<button type="submit" id="search_toggle"><i class="fa fa-search"></i></button>
							<ul class="list-inline">
							    <?php if(isset($_SESSION['USER_ID'])) { ?>
								<div class="dropdown dash_btn">
							  <button class="dropdown-toggle" type="button" data-toggle="dropdown"><img src="assets/profile-photo/<?php //echo $PRow['profilePic']  ;?>" alt="">Welcome <?php echo $_SESSION['USER_FULL_NAME'];?>
							  <span class="caret"></span></button>
							  
                
							  <ul class="dropdown-menu">
							      <?php IF($_SESSION['USER_ROLE']==1) {?>
							      <li><a href="skilltiadmin/dashboard">Dashboard</a></li>
							      <?php } else { ?>
							      <li><a href="dashboard">Dashboard</a></li>
							      <?php } ?>
								<li><a href="viewProfile">View Profile </a></li>
								<!--<li><a href="#">Change Password</a></li>-->
								<li><a href="logout">Log out</a></li>
							  </ul>
							</div>
								<?php } else { ?>
								<li><a href="signin">Sign In</a></li>
								<li><a href="signup">Sign Up</a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
			<nav class="navbar navbar-inverse" id="my_menu">
		  <div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>                        
			  </button>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
			  <ul class="nav navbar-nav">
			  <?php
					$Query = "select * from category where Parent=0 and Status='ACTIVE' limit 7";
					$Result =  $dbObject->query($Query);
					while($Row = mysqli_fetch_assoc($Result)) {
					$CatID = $Row['CatID'];
				?>
				<?php
					$SCQuery = "select count(*) as totalchild from category where Parent=$CatID and Status='ACTIVE'";
					$SCResult =  $dbObject->query($SCQuery);
					$SCRow = mysqli_fetch_assoc($SCResult);
					if($SCRow['totalchild'] > 0) {
				?>
				<li class="dropdown">
				  <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo trim($Row['CatName']);?></a>
				  <ul class="dropdown-menu">
					<?php
						$SQuery = "select * from category where Parent=$CatID and Status='ACTIVE'";
						$SResult =  $dbObject->query($SQuery);
						while($SRow = mysqli_fetch_assoc($SResult)) {
						    $catName = trim($SRow['CatName']);
						    
						    
						    
					?>
					<li><a href="category?cat=<?php echo strtolower($catName);?>"><?php echo trim($SRow['CatName']);?></a></li>
					<?php } ?>
				  </ul>
				</li>
				<?php } } ?>
				
				<li><a href="ViewAllCategory">All Categories </a></li>
			  </ul>
			</div>
		  </div>
		</nav>
	
	</header>