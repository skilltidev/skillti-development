<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div>
            <div class="emailer" style="max-width: 500px; width: 100%; margin: 0 auto; border: 1px solid #ccc; padding: 0 0 20px; box-shadow: 0 1px 2px rgba(0,0,0,0.5), 0 5px 2px rgba(0,0,0,0.2);font-size:16px;line-height:24px">
                <h1 style=" padding: 20px 0; margin:0; width: auto;  font-size: 20px; text-align: center; border-bottom: 1px solid #ccc; background: #f6f9fc; ">SKILLTI APPLICATION </h1>
                <p style="margin: 30px 0;padding: 0 20px">Thanks for registering in our application! Now you have to verify your account. We will reach out if we need additional information</p>

                <a class="btn" href="{{ URL::to('register/verify/' . $confirmation_code) }}"   style="background: #666ee8;display: block;padding: 10px;border-radius: 4px;max-width: 300px; display: block; margin: 0 auto;text-align: center; color: #fff;text-transform: uppercase;
						box-shadow: 0 2px 2px rgba(0,0,0,0.5);text-decoration: none; transition: all .5s; ">Account Verification</a>
                <p style="margin: 30px 0;padding: 0 20px" ><span style="display:block; margin:10px 0;">Username: <b>{{$email}}</b></span>
                    <span style="display:block; margin:10px 0;">Password: <b>{{$password}}</b></span></p>
                <p style="margin: 30px 0;padding: 0 20px">We will be here to help you with any step along the way. You can find answers to most questions and get in touch with us on our <a href="#">support site</a>.</p>
            </div>
        </div>
    </body>
</html>