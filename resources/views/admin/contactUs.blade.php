@extends('header.admin_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")
<style>
    .modal-backdrop {
        position: relative !important;
</style>
@section('maincontent')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper"><!-- HTML (DOM) sourced data -->
                <section id="html">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body ">
                                    <form class="form px-3 mt-2">
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="fa fa-th-list"></i>Contact-us page messages</h4>
                                        </div>
                                    </form>
                                    <div class="card-block card-dashboard table-responsive">
                                        <table class="table table-striped table-bordered sourced-data">
                                            <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Subject</th>
                                                <th>Message</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($contacts))
                                                @foreach($contacts as $k => $contact)
                                                    <tr>
                                                        <td>{{($k + 1)}}</td>
                                                        <td  class="nowrap">
                                                            {{$contact->name}}
                                                        </td>
                                                        <td  class="nowrap">
                                                            {{$contact->email}}
                                                        </td>
                                                        <td class="nowrap">
                                                            {{$contact->subject}}
                                                        </td>
                                                        <td>
                                                            {{$contact->message}}
                                                        </td>
                                                        <td class="nowrap">{{format_datetime($contact->created_at)}}</td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ HTML (DOM) sourced data -->
            </div>
        </div>
    </div>



    <!-- Modal -->

@endsection
@section('scripting')
@endsection
@section('footer')
    @include('footer.footer_admin')
@endsection
