<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Skills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('skills', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('userId')->unsigned();
		    $table->integer('catId')->unsigned();
		    $table->integer('subCatId')->unsigned();
		    $table->mediumText('skills');
		    $table->text('description');
		    $table->text('courseType')->nullable();
		    $table->integer('coursefee')->default(0);
		    $table->text('coursefeeType')->nullable();
		    $table->tinyInteger('status')->default('0');
		    $table->timestamps();
	    });
	    Schema::table('skills', function($table) {
		    $table->foreign('catId')->references('id')->on('categories');
	    });
	    Schema::table('skills', function($table) {
		    $table->foreign('subCatId')->references('id')->on('subCategories');
	    });
	    Schema::table('skills', function($table) {
		    $table->foreign('userId')->references('id')->on('users');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('skills');
    }
}
