<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Categories extends Model
{
	/**
	 * Indicates mysql connection index
	 * @var string
	 */
	protected $connection = 'mysql';
	/**
	 * Indicates mysql table name
	 * @var string
	 */
	protected $table = 'categories';
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;
}
