<?php
	include('adminheader.php'); 
	protect_admin_page();
	
	if(isset($_POST['Submit'])) {
		$fname = $_POST['fname'];
		$lname = $_POST['lname'];
		$email = $_POST['email'];
		$password	= $_POST['password'];
		$cpassword	= $_POST['Cpassword'];
		$role	= $_POST['role'];
		$country	= $_POST['country'];
		$uniqKey = randomPassword(20);
		$email_en= base64_encode($email);
		
		$pass = md5($password);
		if(strlen($cpassword) >= 8) {
		if(strlen($cpassword) == strlen($password)) {
			
		$Query = "insert into users (RoleID,FirstName,LastName,EmailID,Password,Country,Status,UniqueKey) values ('$role','$fname','$lname','$email','$pass','$country','INACTIVE','$uniqKey')";
		$Result = $dbObject->query($Query);
		if($Result) {
			$fromEmail = "info@skillti.com";
			    $to = $email;  
                $headers = "From:SkillTI Team <".$fromEmail.">\r\n";
        		$headers .= 'MIME-Version: 1.0' . "\r\n";
        		$headers .= "Content-type: text/html; charset=iso-8859-1";
				$subject = "SkillTI Welcome Email";
				$body = '<p style="margin-left:20px;"></p>';
				$body .= '<div class="emailer" style="max-width: 500px; width: 100%; margin: 0 auto; border: 1px solid #ccc; padding: 0 0 20px; box-shadow: 0 1px 2px rgba(0,0,0,0.5), 0 5px 2px rgba(0,0,0,0.2);font-size:16px;line-height:24px">
		<h1 style=" padding: 20px 0; margin:0; width: auto;  font-size: 20px; text-align: center; border-bottom: 1px solid #ccc; background: #f6f9fc; ">SKILLTI APPLICATION </h1>
		<p style="margin: 30px 0;padding: 0 20px">Thanks for registering in our application! Now you have to verify your account. We will reach out if we need additional information</p>
		
		<a class="btn" href="https://www.jeetso.com/skillti/dev/accountVerify?key='.$uniqKey.'&email='.$email_en.'"   style="background: #666ee8;display: block;padding: 10px;border-radius: 4px;max-width: 300px; display: block; margin: 0 auto;text-align: center; color: #fff;text-transform: uppercase;
						box-shadow: 0 2px 2px rgba(0,0,0,0.5);text-decoration: none; transition: all .5s; ">Account Verification</a>
							<p style="margin: 30px 0;padding: 0 20px" ><span style="display:block; margin:10px 0;">Username: <b>'.$email.'</b></span>
		<span style="display:block; margin:10px 0;">Password: <b>'.$password.'</b></span></p>	
		<p style="margin: 30px 0;padding: 0 20px">We will be here to help you with any step along the way. You can find answers to most questions and get in touch with us on our <a href="#">support site</a>.</p>';
				
		
				$mail = mail($to, $subject, $body, $headers);
				if(!$mail)
				{
				   $_SESSION['SESS_MSG'] = "Cannot send password to your e-mail address due to some error";
				} else {

			    	$_SESSION['SESS_MSG'] = "Please check your email and verify your account.";
				}
		}
		
		} else {
			$_SESSION['SESS_MSG']="Please put the correct password";
		}
		
		} else {
			$_SESSION['SESS_MSG']="Password Must be of 8 Alpha-Numeric characters";
		}
		
		
		
	}

	
?>
<style>
.mform{
	    margin: 0 auto;
    border: 1px solid #ccc;
    padding: 30px;
    -webkit-box-shadow: 3px 3px 7px 4px #ccc;
    -moz-box-shadow: 3px 3px 7px 4px #ccc;
    box-shadow: 3px 3px 7px 4px #ccc;
}
</style>
<div class="main-panel">
      <div class="main-content">
        <div class="content-wrapper">
          <!-- Basic form layout section start -->
          <section id="basic-form-layouts">
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <div class="px-3 mt-3">
                      <form class="form" method="post" action="" >
                        <div class="form-body">
						
                          <h4 class="form-section">
                            <i class="ft-user"></i> Add New User</h4><br>
							<?php include("../include/error.msg.inc.php"); ?>
							<div class="col-md-5 mform pt-2 mt3"><br>
                          <div class="form-group">
						  <input type="text" class="form-control" id="email" placeholder="FirstName" name="fname" required />
						</div>
						 <div class="form-group">
						  <input type="text" class="form-control" id="" placeholder="LastName" name="lname" required />
						</div>
						<div class="form-group">
						  <input type="email" class="form-control" id="" placeholder="Email ID" name="email" required />
						</div>
						<div class="form-group">
						  <input type="password" class="form-control" id="" placeholder="Password" name="password" required />
						</div>
						<div class="form-group">
						 <input type="password" class="form-control" id="" placeholder="Confirm Password" name="Cpassword" required />
						</div>
						<div class="form-group">
						<select class="form-control" name="role" required>
							<option value="" >Select User Type</option>
							<option value="2" >Mentor</option>
							<option value="3" >Mentee</option>
						 </select>
						 </div>
						<div class="form-group" >
						 
							 <select class="form-control" name="country" >
							 	<option value="" title="">Select Country</option>
							 	<option value="Afghanistan" title="Afghanistan">Afghanistan</option>
								<option value="Åland Islands" title="Åland Islands">Åland Islands</option>
								<option value="Albania" title="Albania">Albania</option>
								<option value="Algeria" title="Algeria">Algeria</option>
								<option value="American Samoa" title="American Samoa">American Samoa</option>
								<option value="Andorra" title="Andorra">Andorra</option>
								<option value="Angola" title="Angola">Angola</option>
								<option value="Anguilla" title="Anguilla">Anguilla</option>
								<option value="Antarctica" title="Antarctica">Antarctica</option>
								<option value="Antigua and Barbuda" title="Antigua and Barbuda">Antigua and Barbuda</option>
								<option value="Argentina" title="Argentina">Argentina</option>
								<option value="Armenia" title="Armenia">Armenia</option>
								<option value="Aruba" title="Aruba">Aruba</option>
								<option value="Australia" title="Australia">Australia</option>
								<option value="Austria" title="Austria">Austria</option>
								<option value="Azerbaijan" title="Azerbaijan">Azerbaijan</option>
								<option value="Bahamas" title="Bahamas">Bahamas</option>
								<option value="Bahrain" title="Bahrain">Bahrain</option>
								<option value="Bangladesh" title="Bangladesh">Bangladesh</option>
								<option value="Barbados" title="Barbados">Barbados</option>
								<option value="Belarus" title="Belarus">Belarus</option>
								<option value="Belgium" title="Belgium">Belgium</option>
								<option value="Belize" title="Belize">Belize</option>
								<option value="Benin" title="Benin">Benin</option>
								<option value="Bermuda" title="Bermuda">Bermuda</option>
								<option value="Bhutan" title="Bhutan">Bhutan</option>
								<option value="Bolivia, Plurinational State of" title="Bolivia, Plurinational State of">Bolivia, Plurinational State of</option>
								<option value="Bonaire, Sint Eustatius and Saba" title="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option>
								<option value="Bosnia and Herzegovina" title="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
								<option value="Botswana" title="Botswana">Botswana</option>
								<option value="Bouvet Island" title="Bouvet Island">Bouvet Island</option>
								<option value="Brazil" title="Brazil">Brazil</option>
								<option value="British Indian Ocean Territory" title="British Indian Ocean Territory">British Indian Ocean Territory</option>
								<option value="Brunei Darussalam" title="Brunei Darussalam">Brunei Darussalam</option>
								<option value="Bulgaria" title="Bulgaria">Bulgaria</option>
								<option value="Burkina Faso" title="Burkina Faso">Burkina Faso</option>
								<option value="Burundi" title="Burundi">Burundi</option>
								<option value="Cambodia" title="Cambodia">Cambodia</option>
								<option value="Cameroon" title="Cameroon">Cameroon</option>
								<option value="Canada" title="Canada">Canada</option>
								<option value="Cape Verde" title="Cape Verde">Cape Verde</option>
								<option value="Cayman Islands" title="Cayman Islands">Cayman Islands</option>
								<option value="Central African Republic" title="Central African Republic">Central African Republic</option>
								<option value="Chad" title="Chad">Chad</option>
								<option value="Chile" title="Chile">Chile</option>
								<option value="China" title="China">China</option>
								<option value="Christmas Island" title="Christmas Island">Christmas Island</option>
								<option value="Cocos (Keeling) Islands" title="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
								<option value="Colombia" title="Colombia">Colombia</option>
								<option value="Comoros" title="Comoros">Comoros</option>
								<option value="Congo" title="Congo">Congo</option>
								<option value="Congo, the Democratic Republic of the" title="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option>
								<option value="Cook Islands" title="Cook Islands">Cook Islands</option>
								<option value="Costa Rica" title="Costa Rica">Costa Rica</option>
								<option value="Côte d'Ivoire" title="Côte d'Ivoire">Côte d'Ivoire</option>
								<option value="Croatia" title="Croatia">Croatia</option>
								<option value="Cuba" title="Cuba">Cuba</option>
								<option value="Curaçao" title="Curaçao">Curaçao</option>
								<option value="Cyprus" title="Cyprus">Cyprus</option>
								<option value="Czech Republic" title="Czech Republic">Czech Republic</option>
								<option value="Denmark" title="Denmark">Denmark</option>
								<option value="Djibouti" title="Djibouti">Djibouti</option>
								<option value="Dominica" title="Dominica">Dominica</option>
								<option value="Dominican Republic" title="Dominican Republic">Dominican Republic</option>
								<option value="Ecuador" title="Ecuador">Ecuador</option>
								<option value="Egypt" title="Egypt">Egypt</option>
								<option value="El Salvador" title="El Salvador">El Salvador</option>
								<option value="Equatorial Guinea" title="Equatorial Guinea">Equatorial Guinea</option>
								<option value="Eritrea" title="Eritrea">Eritrea</option>
								<option value="Estonia" title="Estonia">Estonia</option>
								<option value="Ethiopia" title="Ethiopia">Ethiopia</option>
								<option value="Falkland Islands (Malvinas)" title="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
								<option value="Faroe Islands" title="Faroe Islands">Faroe Islands</option>
								<option value="Fiji" title="Fiji">Fiji</option>
								<option value="Finland" title="Finland">Finland</option>
								<option value="France" title="France">France</option>
								<option value="French Guiana" title="French Guiana">French Guiana</option>
								<option value="French Polynesia" title="French Polynesia">French Polynesia</option>
								<option value="French Southern Territories" title="French Southern Territories">French Southern Territories</option>
								<option value="Gabon" title="Gabon">Gabon</option>
								<option value="Gambia" title="Gambia">Gambia</option>
								<option value="Georgia" title="Georgia">Georgia</option>
								<option value="Germany" title="Germany">Germany</option>
								<option value="Ghana" title="Ghana">Ghana</option>
								<option value="Gibraltar" title="Gibraltar">Gibraltar</option>
								<option value="Greece" title="Greece">Greece</option>
								<option value="Greenland" title="Greenland">Greenland</option>
								<option value="Grenada" title="Grenada">Grenada</option>
								<option value="Guadeloupe" title="Guadeloupe">Guadeloupe</option>
								<option value="Guam" title="Guam">Guam</option>
								<option value="Guatemala" title="Guatemala">Guatemala</option>
								<option value="Guernsey" title="Guernsey">Guernsey</option>
								<option value="Guinea" title="Guinea">Guinea</option>
								<option value="Guinea-Bissau" title="Guinea-Bissau">Guinea-Bissau</option>
								<option value="Guyana" title="Guyana">Guyana</option>
								<option value="Haiti" title="Haiti">Haiti</option>
								<option value="Heard Island and McDonald Islands" title="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>
								<option value="Holy See (Vatican City State)" title="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
								<option value="Honduras" title="Honduras">Honduras</option>
								<option value="Hong Kong" title="Hong Kong">Hong Kong</option>
								<option value="Hungary" title="Hungary">Hungary</option>
								<option value="Iceland" title="Iceland">Iceland</option>
								<option value="India" title="India">India</option>
								<option value="Indonesia" title="Indonesia">Indonesia</option>
								<option value="Iran, Islamic Republic of" title="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
								<option value="Iraq" title="Iraq">Iraq</option>
								<option value="Ireland" title="Ireland">Ireland</option>
								<option value="Isle of Man" title="Isle of Man">Isle of Man</option>
								<option value="Israel" title="Israel">Israel</option>
								<option value="Italy" title="Italy">Italy</option>
								<option value="Jamaica" title="Jamaica">Jamaica</option>
								<option value="Japan" title="Japan">Japan</option>
								<option value="Jersey" title="Jersey">Jersey</option>
								<option value="Jordan" title="Jordan">Jordan</option>
								<option value="Kazakhstan" title="Kazakhstan">Kazakhstan</option>
								<option value="Kenya" title="Kenya">Kenya</option>
								<option value="Kiribati" title="Kiribati">Kiribati</option>
								<option value="Korea, Democratic People's Republic of" title="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
								<option value="Korea, Republic of" title="Korea, Republic of">Korea, Republic of</option>
								<option value="Kuwait" title="Kuwait">Kuwait</option>
								<option value="Kyrgyzstan" title="Kyrgyzstan">Kyrgyzstan</option>
								<option value="Lao People's Democratic Republic" title="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
								<option value="Latvia" title="Latvia">Latvia</option>
								<option value="Lebanon" title="Lebanon">Lebanon</option>
								<option value="Lesotho" title="Lesotho">Lesotho</option>
								<option value="Liberia" title="Liberia">Liberia</option>
								<option value="Libya" title="Libya">Libya</option>
								<option value="Liechtenstein" title="Liechtenstein">Liechtenstein</option>
								<option value="Lithuania" title="Lithuania">Lithuania</option>
								<option value="Luxembourg" title="Luxembourg">Luxembourg</option>
								<option value="Macao" title="Macao">Macao</option>
								<option value="Macedonia, the former Yugoslav Republic of" title="Macedonia, the former Yugoslav Republic of">Macedonia, the former Yugoslav Republic of</option>
								<option value="Madagascar" title="Madagascar">Madagascar</option>
								<option value="Malawi" title="Malawi">Malawi</option>
								<option value="Malaysia" title="Malaysia">Malaysia</option>
								<option value="Maldives" title="Maldives">Maldives</option>
								<option value="Mali" title="Mali">Mali</option>
								<option value="Malta" title="Malta">Malta</option>
								<option value="Marshall Islands" title="Marshall Islands">Marshall Islands</option>
								<option value="Martinique" title="Martinique">Martinique</option>
								<option value="Mauritania" title="Mauritania">Mauritania</option>
								<option value="Mauritius" title="Mauritius">Mauritius</option>
								<option value="Mayotte" title="Mayotte">Mayotte</option>
								<option value="Mexico" title="Mexico">Mexico</option>
								<option value="Micronesia, Federated States of" title="Micronesia, Federated States of">Micronesia, Federated States of</option>
								<option value="Moldova, Republic of" title="Moldova, Republic of">Moldova, Republic of</option>
								<option value="Monaco" title="Monaco">Monaco</option>
								<option value="Mongolia" title="Mongolia">Mongolia</option>
								<option value="Montenegro" title="Montenegro">Montenegro</option>
								<option value="Montserrat" title="Montserrat">Montserrat</option>
								<option value="Morocco" title="Morocco">Morocco</option>
								<option value="Mozambique" title="Mozambique">Mozambique</option>
								<option value="Myanmar" title="Myanmar">Myanmar</option>
								<option value="Namibia" title="Namibia">Namibia</option>
								<option value="Nauru" title="Nauru">Nauru</option>
								<option value="Nepal" title="Nepal">Nepal</option>
								<option value="Netherlands" title="Netherlands">Netherlands</option>
								<option value="New Caledonia" title="New Caledonia">New Caledonia</option>
								<option value="New Zealand" title="New Zealand">New Zealand</option>
								<option value="Nicaragua" title="Nicaragua">Nicaragua</option>
								<option value="Niger" title="Niger">Niger</option>
								<option value="Nigeria" title="Nigeria">Nigeria</option>
								<option value="Niue" title="Niue">Niue</option>
								<option value="Norfolk Island" title="Norfolk Island">Norfolk Island</option>
								<option value="Northern Mariana Islands" title="Northern Mariana Islands">Northern Mariana Islands</option>
								<option value="Norway" title="Norway">Norway</option>
								<option value="Oman" title="Oman">Oman</option>
								<option value="Pakistan" title="Pakistan">Pakistan</option>
								<option value="Palau" title="Palau">Palau</option>
								<option value="Palestinian Territory, Occupied" title="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
								<option value="Panama" title="Panama">Panama</option>
								<option value="Papua New Guinea" title="Papua New Guinea">Papua New Guinea</option>
								<option value="Paraguay" title="Paraguay">Paraguay</option>
								<option value="Peru" title="Peru">Peru</option>
								<option value="Philippines" title="Philippines">Philippines</option>
								<option value="Pitcairn" title="Pitcairn">Pitcairn</option>
								<option value="Poland" title="Poland">Poland</option>
								<option value="Portugal" title="Portugal">Portugal</option>
								<option value="Puerto Rico" title="Puerto Rico">Puerto Rico</option>
								<option value="Qatar" title="Qatar">Qatar</option>
								<option value="Réunion" title="Réunion">Réunion</option>
								<option value="Romania" title="Romania">Romania</option>
								<option value="Russian Federation" title="Russian Federation">Russian Federation</option>
								<option value="Rwanda" title="Rwanda">Rwanda</option>
								<option value="Saint Barthélemy" title="Saint Barthélemy">Saint Barthélemy</option>
								<option value="Saint Helena, Ascension and Tristan da Cunha" title="Saint Helena, Ascension and Tristan da Cunha">Saint Helena, Ascension and Tristan da Cunha</option>
								<option value="Saint Kitts and Nevis" title="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
								<option value="Saint Lucia" title="Saint Lucia">Saint Lucia</option>
								<option value="Saint Martin (French part)" title="Saint Martin (French part)">Saint Martin (French part)</option>
								<option value="Saint Pierre and Miquelon" title="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
								<option value="Saint Vincent and the Grenadines" title="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
								<option value="Samoa" title="Samoa">Samoa</option>
								<option value="San Marino" title="San Marino">San Marino</option>
								<option value="Sao Tome and Principe" title="Sao Tome and Principe">Sao Tome and Principe</option>
								<option value="Saudi Arabia" title="Saudi Arabia">Saudi Arabia</option>
								<option value="Senegal" title="Senegal">Senegal</option>
								<option value="Serbia" title="Serbia">Serbia</option>
								<option value="Seychelles" title="Seychelles">Seychelles</option>
								<option value="Sierra Leone" title="Sierra Leone">Sierra Leone</option>
								<option value="Singapore" title="Singapore">Singapore</option>
								<option value="Sint Maarten (Dutch part)" title="Sint Maarten (Dutch part)">Sint Maarten (Dutch part)</option>
								<option value="Slovakia" title="Slovakia">Slovakia</option>
								<option value="Slovenia" title="Slovenia">Slovenia</option>
								<option value="Solomon Islands" title="Solomon Islands">Solomon Islands</option>
								<option value="Somalia" title="Somalia">Somalia</option>
								<option value="South Africa" title="South Africa">South Africa</option>
								<option value="South Georgia and the South Sandwich Islands" title="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
								<option value="South Sudan" title="South Sudan">South Sudan</option>
								<option value="Spain" title="Spain">Spain</option>
								<option value="Sri Lanka" title="Sri Lanka">Sri Lanka</option>
								<option value="Sudan" title="Sudan">Sudan</option>
								<option value="Suriname" title="Suriname">Suriname</option>
								<option value="Svalbard and Jan Mayen" title="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
								<option value="Swaziland" title="Swaziland">Swaziland</option>
								<option value="Sweden" title="Sweden">Sweden</option>
								<option value="Switzerland" title="Switzerland">Switzerland</option>
								<option value="Syrian Arab Republic" title="Syrian Arab Republic">Syrian Arab Republic</option>
								<option value="Taiwan, Province of China" title="Taiwan, Province of China">Taiwan, Province of China</option>
								<option value="Tajikistan" title="Tajikistan">Tajikistan</option>
								<option value="Tanzania, United Republic of" title="Tanzania, United Republic of">Tanzania, United Republic of</option>
								<option value="Thailand" title="Thailand">Thailand</option>
								<option value="Timor-Leste" title="Timor-Leste">Timor-Leste</option>
								<option value="Togo" title="Togo">Togo</option>
								<option value="Tokelau" title="Tokelau">Tokelau</option>
								<option value="Tonga" title="Tonga">Tonga</option>
								<option value="Trinidad and Tobago" title="Trinidad and Tobago">Trinidad and Tobago</option>
								<option value="Tunisia" title="Tunisia">Tunisia</option>
								<option value="Turkey" title="Turkey">Turkey</option>
								<option value="Turkmenistan" title="Turkmenistan">Turkmenistan</option>
								<option value="Turks and Caicos Islands" title="Turks and Caicos Islands">Turks and Caicos Islands</option>
								<option value="Tuvalu" title="Tuvalu">Tuvalu</option>
								<option value="Uganda" title="Uganda">Uganda</option>
								<option value="Ukraine" title="Ukraine">Ukraine</option>
								<option value="United Arab Emirates" title="United Arab Emirates">United Arab Emirates</option>
								<option value="United Kingdom" title="United Kingdom">United Kingdom</option>
								<option value="United States" title="United States">United States</option>
								<option value="United States Minor Outlying Islands" title="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
								<option value="Uruguay" title="Uruguay">Uruguay</option>
								<option value="Uzbekistan" title="Uzbekistan">Uzbekistan</option>
								<option value="Vanuatu" title="Vanuatu">Vanuatu</option>
								<option value="Venezuela, Bolivarian Republic of" title="Venezuela, Bolivarian Republic of">Venezuela, Bolivarian Republic of</option>
								<option value="Viet Nam" title="Viet Nam">Viet Nam</option>
								<option value="Virgin Islands, British" title="Virgin Islands, British">Virgin Islands, British</option>
								<option value="Virgin Islands, U.S." title="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
								<option value="Wallis and Futuna" title="Wallis and Futuna">Wallis and Futuna</option>
								<option value="Western Sahara" title="Western Sahara">Western Sahara</option>
								<option value="Yemen" title="Yemen">Yemen</option>
								<option value="Zambia" title="Zambia">Zambia</option>
								<option value="Zimbabwe" title="Zimbabwe">Zimbabwe</option>			
							 </select>
						 </div>
                        

                        <div class="form-actions">
                          <button type="submit" name="Submit" class="btn btn-raised btn-raised btn-primary">
                            <i class="fa fa-check-square-o"></i> Save
                          </button>
                        </div>
						</div>
                        </div>
						</div>
                      </form>
                    <br><br>
                  </div>
                </div>
              </div>
			</div>

		  </section>
          <!-- // Basic form layout section end -->
	  </div>
	</div>

 

<?php include('adminfooter.php'); ?>

