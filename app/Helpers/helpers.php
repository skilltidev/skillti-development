<?php
/**
 * Created by PhpStorm.
 * User: vidooly
 * Date: 24/11/17
 * Time: 3:40 PM
 */
if (! function_exists('fooBar')) {
	function fooBar() {
		return \App\Helpers\CustomHelper::fooBar();
	}
}

if (! function_exists('array_add')) {
	function array_add($array, $key, $value) {
		return Arr::add($array, $key, $value);
	}
}

if (! function_exists('print_myname')) {
	function print_myname() {
		echo "Aditya Kumar";
	}
}