<?php

namespace App\Http\Controllers;

use App\Models\categories;
use App\Models\ContactUs;
use App\Models\Course;
use App\Models\Messages;
use App\Models\Ratting;
use App\Models\Skills;
use App\Models\SubCategories;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use tests\Mockery\Adapter\Phpunit\EmptyTestCase;
use Validator;
use Illuminate\Support\Facades\Redirect;
use DB;
use Auth;

class HomeController extends Controller
{
	public function index()
	{
		$topCategories = SubCategories::where('isHome', 1)->where('status', 1)->limit(8)->get();
		debug($topCategories);
		return view('home', compact('topCategories'));
	}

	public function commingShoon() {
        return view('commigShoon');
    }

    public function aboutUs() {
        return view('aboutUs');
    }

	public function forgotPassword() {
		return view('forgotPassword');
	}

	public function home(Request $request)
	{
		/*$subCategory = syllabus::topSubcategory();
		$topCategories = array();
		if(!empty($subCategory)) {
			foreach ($subCategory as $cat) {
				$categories = categories::getActiveById($cat->SubCategory);
				$topCategories[] = $categories;
			}
		}*/
		$topCategories = SubCategories::where('isHome', 1)->where('status', 1)->limit(8)->get();
		//debug($topCategories);
		return view('home', compact('topCategories'));
	}

	public function apiSuggestion(Request $request) {
		$searchKey = $request->input('term');
		$searchData = array();
		$k = 0;
		$mentors = Skills::select('users.firstName', 'users.lastName', 'users.id')
			->leftJoin('users', 'users.id', 'skills.userId')
			->where('skills.status', 1)
			->where('users.status', 1)
			->where('users.certified', 1)
			->where('skills.skills', 'like', '%' . $searchKey . '%')->limit(5)->get();
		if(!empty($mentors) && count($mentors) > 0) {
			foreach ($mentors as $mentor) {
				$searchData[$k]['label'] = $mentor->firstName." ".$mentor->lastName;
				$searchData[$k]['value'] = url('profile-detail/'.custom_encode($mentor->id));
				$k++;
			}
		}

		$category = SubCategories::select('name', 'link')->where('status', 1)
			->where('name', 'like', '%' . $searchKey . '%')->limit(5)->get();
		if(!empty($category) && count($category) > 0) {
			foreach ($category as $value) {
				$searchData[$k]['label'] = $value->name;
				$searchData[$k]['value'] = url('category/'.$value->link);
				$k++;
			}
		}

		return json_encode($searchData);
	}

	public function search(Request $request) {
		$searchKey = trim($request->input('term'));
		if($searchKey == '') {
			return redirect('404');
		}
		//DB::connection()->enableQueryLog();
		$mentorsData = Skills::select('skills.*', 'categories.name as catname', 'subCategories.name as subCatName',
			'users.name', 'users.firstName', 'users.lastName', 'users.gender', 'users.qualification', 'users.expertise',
			'users.email', 'users.addressOne', 'users.addressTwo', 'users.city', 'users.state', 'users.country', 'users.zip',
			'users.varificationDoc', 'users.experience', 'users.certified', 'users.about', 'users.tagline', 'users.interestToLearn',
			'users.profilePic', 'users.avgRatting')
			->leftJoin('categories', 'categories.id', 'skills.catId')
			->leftJoin('subCategories', 'subCategories.id', 'skills.subCatId')
			->leftJoin('users', 'users.id', 'skills.userId')
			->where('skills.status', 1)
			->where('users.status', 1)
			->where('users.certified', 1)
			->where(function ($query) use ($searchKey) {
				$query->where('skills.skills', 'like', '%' . $searchKey . '%')
					->orWhere('categories.name', 'like', '%' . $searchKey . '%')
					->orWhere('subCategories.name', 'like', '%' . $searchKey . '%');
			})
			->limit(10)
			->get();
		//debug(DB::getQueryLog(), false);
		//debug($mentorsData);
		return view('searchData', compact('searchKey', 'mentorsData'));
	}



	public function categories(Request $request)
	{
		$selected = $request->categoryname;
		$users = array();
		$category = SubCategories::select('id', 'catId')->where('link', $selected)->first();
		$mentors = null;
		if (isset($category->id)) {
			$selectedCategoryGroup = Categories::select('name')->where('id', $category->catId)->first();
			try {
				$mentors = Skills::select('skills.*', 'categories.name as catname', 'subCategories.name as subCatName',
					'users.name', 'users.firstName', 'users.lastName', 'users.gender', 'users.qualification', 'users.expertise',
					'users.email', 'users.addressOne', 'users.addressTwo', 'users.city', 'users.state', 'users.country', 'users.zip',
					'users.varificationDoc', 'users.experience', 'users.certified', 'users.about', 'users.tagline', 'users.interestToLearn',
					'users.profilePic', 'users.avgRatting')
					->leftJoin('categories', 'categories.id', 'skills.catId')
					->leftJoin('subCategories', 'subCategories.id', 'skills.subCatId')
					->leftJoin('users', 'users.id', 'skills.userId')
					->where('skills.subCatId', $category->id)
					->where('skills.status', 1)
					->where('users.status', 1)
					->where('users.certified', 1)
					->get();
			} catch (\Exception $e) {}
		}
		$selectedCategory = isset($selectedCategoryGroup['name'])?$selectedCategoryGroup['name']:'';
		return view('categoryDetail', compact('selected', 'mentors', 'selectedCategory'));
	}

	public function profileDetails(Request $request)
	{
		$userId = $request->userid;
		$selected = custom_decode($userId);
		$mentor = User::where('id', $selected)->first();
		$rattings = $courses = array();
		if(!empty($mentor)) {
			$mentor->ratting = Ratting::where('mentorId', $mentor->id)->count();
			$mentor->course = Skills::where('userId', $mentor->id)->where('status', 1)->count();
			$mentor->students = Course::where('mentorId', $mentor->id)->where('courseStatus', 1)->groupBy('menteeId')->count();
			$rattings = Ratting::select('ratting.*', 'users.name', 'users.profilePic')->where('ratting.mentorId', $mentor->id)
				->leftJoin('users', 'users.id', 'ratting.mentorId')
				->where('ratting.status', 1)
				->get();
			$courses = Skills::select('skills.*', 'categories.name as catname', 'subCategories.name as subCatName', 'categories.logo')
					->leftJoin('categories', 'categories.id', 'skills.catId')
					->leftJoin('subCategories', 'subCategories.id', 'skills.subCatId')
					->where('skills.userId', $mentor->id)->where('skills.status', 1)
					->get();
		}
		//debug($courses, false);
        return view('profileDetails', compact('mentor', 'rattings', 'courses'));
    }

	public function allCategories(Request $request)
	{
		$categories = Categories::where('status', 1)->get();
		if (!empty($categories)) {
			foreach ($categories as $k => $cat) {
				$subCategory = SubCategories::where('catId', $cat->id)->where('status', 1)->get();
				if (!empty($subCategory) && count($subCategory) > 0) {
					$cat->subCategory = $subCategory;
				} else {
					$cat->subCategory = [];
				}
			}
		}
		return view('category', compact('categories'));
	}

	function sort($a, $b)
	{
		return strlen($b) - strlen($a);
	}

	public function contactUs() {
		return view('contactUs');
	}

	public function contactUsPost(Request $request) {
		$input = $request->input();
		$rules = array(
			'name' => 'required',
			'email' => 'required|email',
			'subject' => 'required|max:150',
			'comment' => 'required|max:1500',
		);
		$v = Validator::make($input, $rules);
		if ($v->passes()) {
			$message = new ContactUs();
			$message->name = $input['name'];
			$message->email = $input['email'];
			$message->subject = $input['subject'];
			$message->message = $input['comment'];
			$message->save();
			Mail::send('email.contactUs', compact('input'), function ($m) {
				$m->to('sonu.tyagi2229@gmail.com', 'Sonu Tyagi')
					->subject('Contact Us Request');
			});
			Mail::send('email.contactUs', compact('input'), function ($m) use($input) {
				$m->to($input['email'], $input['name'])
					->subject('Contact Us Request');
			});
			return Redirect::back()->with('message', 'Thanks for contact Us, Our team contact you shortlly.');
		}else{
			return Redirect::back()->withInput()->withErrors($v);
		}
	}

	public function sendMessage(Request $request) {
		$inputs = $request->input();
		$rules = array(
			'mentorId' => 'required',
			'msgtext' => 'required|max:500',
		);
		$v = Validator::make($inputs, $rules);
		if ($v->passes()) {
			$messages = new Messages();
			$messages->mentorId = custom_decode($inputs['mentorId']);
			$messages->menteeId = Auth::user()->id;
			$messages->message = $inputs['msgtext'];
			$messages->sender = 3;
			if($messages->save()) {
				$mentorData = User::where('id', $messages->mentorId)->first();
				if(!empty($mentorData)) {
					Mail::send('email.message', compact('inputs', 'mentorData'), function ($m) use($mentorData) {
						$m->to($mentorData->email, $mentorData->firstName." ".$mentorData->lastName)
							->subject('Skillti Notification - You have a new message');
					});
				}
				return Redirect::back()->with('message', 'Your message successfully sent.');
			}
		}else{
			return Redirect::back()->withInput()->withErrors($v);
		}
	}
}
