@extends('header.admin_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")

@section('maincontent')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">
                <section id="striped-row-form-layouts">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    @if(Session::get('message'))
                                        <div class="alert alert-success">
                                            <a href="#" class="close" data-dismiss="alert"
                                               aria-label="close">&times;</a>
                                            <strong>Success!</strong> {{ Session::get('message') }}{{ Session::forget('message') }}
                                        </div>
                                    @endif
                                    @if(Session::get('errorMessage'))
                                        <div class="alert alert-danger">
                                            <a href="#" class="close" data-dismiss="alert"
                                               aria-label="close">&times;</a>
                                            <strong>Message
                                                !</strong> {{ Session::get('errorMessage') }}{{ Session::forget('errorMessage') }}
                                        </div>
                                    @endif
                                    <div class="px-3 mt-2">
                                        <form class="form form-horizontal  form-bordered"
                                              action="{{url('admin/add-edit-category')}}" method="post"
                                              enctype="multipart/form-data">
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="fa fa-pencil-square-o"></i>Subject
                                                </h4>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Category
                                                        Name</label>
                                                    <div class="col-md-9">
                                                        <strong>{{ $mentorCourse->first('catname') }}</strong>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control">Subcategory Name</label>
                                                    <div class="col-md-9">
                                                        <strong>{{$mentorCourse->first('subcatname') }}</strong>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control"
                                                           for="projectinput9">Skill</label>
                                                    <div class="col-md-9">
                                                        <textarea id="projectinput9" rows="5" class="form-control"
                                                                  name="description"
                                                                  placeholder="Category Description">{{isset($categoryInfo->description)?$categoryInfo->description:old('description')}}</textarea>

                                                        <strong>{{ $mentorCourse->first('skills') }}</strong>
                                                        </span>

                                                    </div>
                                                </div>


                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('scripting')
@endsection
@section('footer')
    @include('footer.footer_admin')
@endsection
