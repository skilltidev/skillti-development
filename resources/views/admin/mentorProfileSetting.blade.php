@extends('header.admin_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")


@section('maincontent')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">
                <section id="striped-row-form-layouts">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    @if(Session::get('message'))
                                        <div class="alert alert-success">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>Success!</strong> {{ Session::get('message') }}{{ Session::forget('message') }}
                                        </div>
                                    @endif
                                    @if(Session::get('errorMessage'))
                                        <div class="alert alert-danger">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>Message !</strong> {{ Session::get('errorMessage') }}{{ Session::forget('errorMessage') }}
                                        </div>
                                    @endif
                                    <div class="px-3 mt-2">
                                        <form id="form" method="post" action="{{url('admin/mentor-profile-edit')}}" enctype="multipart/form-data" class="form-horizontal">
                                            @csrf
                                            <div class="form-group row">
                                                <label for="avatar" class="col-sm-3 col-form-label form-label">Profile Pic</label>
                                                <div class="col-sm-9">
                                                    <div class="media align-items-center">
                                                        <div class="media-left">

                                                                <img src="{{ profile_image(asset('profile-photo/'.(isset($profile->profilePic)?$profile->profilePic:'')))}}" alt="profile pic" class="img-responsive img-thumbnail" style="width: 50px;">

                                                        </div>
                                                        <div class="media-body">
                                                            <div class="custom-file" style="width: auto;">
                                                                <input accept=".jpeg,.jpg,.png" type="file" id="profilePic" name="profilePic" class="custom-file-input">
                                                                <label for="avatar" class="custom-file-label">Choose file</label>
                                                            </div>
                                                            @if ($errors->has('profilePic'))
                                                                <span style="display: block;" class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('profilePic') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="mentorId" value="{{$profile->id}}">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-3 col-form-label form-label">Full Name</label>
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input id="name" name="firstName" type="text" class="form-control" placeholder="First Name" value="{{old('firstName')?old('firstName'):$profile->firstName}}">
                                                            @if ($errors->has('firstName'))
                                                                <span style="display: block;" class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('firstName') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" name="lastName" class="form-control" placeholder="Last Name" value="{{old('lastName')?old('lastName'):$profile->lastName}}">
                                                            @if ($errors->has('lastName'))
                                                                <span style="display: block;" class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('lastName') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="avatar" class="col-sm-3 col-form-label form-label">Upload your documents</label>
                                                <div class="col-sm-9">
                                                    <div class="media align-items-center">
                                                        <div class="media-body">
                                                            <div class="custom-file" style="width: auto;">
                                                                <input accept=".pdf" type="file" id="documents" name="documents" class="custom-file-input">
                                                                <label for="avatar" class="custom-file-label">Choose file</label>
                                                            </div>
                                                            @if ($errors->has('documents'))
                                                                <span style="display: block;" class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('documents') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                        @if($profile->varificationDoc != '')
                                                            <div class="media-right">
                                                                <a class="dropdown-item" target="_blank" href="{{url(asset('profile-doc/'.$profile->varificationDoc))}}">​​​​​​​​​​​​​​​​​<i class="material-icons md-18 text-muted">attachment</i> {{$profile->varificationDoc}}</a>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label form-label">Email</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        <input type="email" id="email" class="form-control" placeholder="Email Address" value="{{$profile->email}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label form-label">Qualification</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        <input type="text" id="qualification" name="qualification" class="form-control" placeholder="Qualification" value="{{old('qualification')?old('qualification'):$profile->qualification}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label form-label">Exprience</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        <input type="number" id="exprience" name="exprience" class="form-control" placeholder="Exprience" value="{{old('exprience')?old('exprience'):$profile->experience}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label form-label">Your Tag Line</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        <input type="text" id="tagLine" name="tagLine" class="form-control" placeholder="Profile Tagline" value="{{old('tagLine')?old('tagLine'):$profile->tagline}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label form-label">Describe your self</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        <textarea name="about" id="about" placeholder="Describe your self" cols="30" rows="2" class="form-control">{{old('about')?old('about'):$profile->about}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--<div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label form-label">Expertise</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="material-icons md-18 text-muted">textsms</i>
                                                            </div>
                                                        </div>
                                                        <textarea name="expertise" id="expertise" placeholder="Your Expertise" cols="30" rows="2" class="form-control">{{old('expertise')?old('expertise'):$profile->expertise}}</textarea>
                                                    </div>
                                                </div>
                                            </div>--}}
                                            <div class="row form-group">
                                                <label for="expertise" class="col-sm-3 col-form-label form-label">Full Description</label>
                                                <div class="col-sm-6">
                                                    <div id="expertise" name="description" class="input-group" style="height: 150px;" data-toggle="quill" data-quill-placeholder="Quill WYSIWYG editor">
                                                        {!! old('discription')?old('discription'):(isset($profile->description)?$profile->description:'') !!}
                                                    </div>
                                                    @if ($errors->has('description'))
                                                        <span style="display: block;" class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <textarea name="discription" id="discription" style="display: none;"></textarea>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label form-label">Address 1</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        <textarea name="addressOne" id="addressOne" placeholder="Address" cols="30" rows="2" class="form-control">{{old('addressOne')?old('addressOne'):$profile->addressOne}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label form-label">Address 2</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        <textarea name="addressTwo" id="addressTwo" placeholder="Address" cols="30" rows="2" class="form-control">{{old('addressTwo')?old('addressTwo'):$profile->addressTwo}}</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label form-label">Country</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        {!! countrySelector(old('country')?old('country'):$profile->country, "country", "country"); !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label form-label">State</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        <input type="text" id="state" name="state" class="form-control" placeholder="State" value="{{old('state')?old('state'):$profile->state}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label form-label">City</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        <input type="text" id="city" name="city" class="form-control" placeholder="City" value="{{old('city')?old('city'):$profile->city}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label form-label">ZIP Code</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        <input type="text" id="zip" name="zip" class="form-control" placeholder="ZIP Code" value="{{old('zip')?old('zip'):$profile->zip}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="website" class="col-sm-3 col-form-label form-label">Facebook url</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        <input type="text" id="facebook" name="facebook" class="form-control" placeholder="https://facebook.com/page-name" value="{{old('facebook')?old('facebook'):$profile->facebook}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="website" class="col-sm-3 col-form-label form-label">twitter url</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="twitter" name="twitter" class="form-control" placeholder="https://twitter.com/page-name" value="{{old('twitter')?old('twitter'):$profile->twitter}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="website" class="col-sm-3 col-form-label form-label">linkedIn url</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        <input type="text" id="linkedIn" name="linkedIn" class="form-control" placeholder="https://linkedIn.com/page-name" value="{{old('linkedIn')?old('linkedIn'):$profile->linkedIn}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="website" class="col-sm-3 col-form-label form-label">github url</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="github" name="github" class="form-control" placeholder="https://github.com/page-name" value="{{old('github')?old('github'):$profile->github}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="website" class="col-sm-3 col-form-label form-label">Other</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        <input type="text" id="other" name="other" class="form-control" placeholder="www." value="{{old('other')?old('other'):$profile->other}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label form-label">Avg Ratting</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">

                                                        <input type="number" id="avgRatting" maxlength="5" name="avgRatting" class="form-control" placeholder="" value="{{old('avgRatting')?old('zip'):$profile->avgRatting}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="website" class="col-sm-3 col-form-label form-label">Certified</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">
                                                        <select name="certified" id="status" class="form-control">
                                                            <option value="1" {{($profile->certified==1)?'selected':''}}>Active</option>
                                                            <option value="0" {{($profile->certified==0)?'selected':''}}>Inactive</option>
                                                        </select>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="website" class="col-sm-3 col-form-label form-label">Status</label>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="input-group">
                                                        <select name="status" id="status" class="form-control">
                                                            <option value="1" {{($profile->status==1)?'selected':''}}>Active</option>
                                                            <option value="0" {{($profile->status==0)?'selected':''}}>Inactive</option>
                                                        </select>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-8 offset-sm-3">
                                                    <div class="media align-items-center">
                                                        <div class="media-left">
                                                            <button type="submit" class="btn btn-success">Save Changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('scripting')
    <!-- Quill -->
    <script src="{{asset('assets/vendor/quill.min.js')}}"></script>
    {{--<script src="{{asset('assets/js/quill.js')}}"></script>--}}
    <script>
        var editor = new Quill('#expertise', {
            modules: {
                'toolbar': {},
            },
            theme: "snow",
            placeholder: Quill.DEFAULTS.placeholder,
            readOnly: Quill.DEFAULTS.readOnly,
            debug: Quill.DEFAULTS.debug,
            formats: Quill.DEFAULTS.formats
        });

        $('#form').submit(function (e) {
            $('#discription').val(document.querySelector(".ql-editor").innerHTML);
        });
    </script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.img-thumbnail').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profilePic").change(function(){
            readURL(this);
            var name = $(this)[0].files[0].name;
            $(this).siblings('label').text(name);
        });
        $("#documents").change(function() {
            var name = $(this)[0].files[0].name;
            $(this).siblings('label').text(name);
        });
    </script>
@endsection
@section('footer')
    @include('footer.footer_admin')
@endsection
