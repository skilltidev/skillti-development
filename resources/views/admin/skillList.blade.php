@extends('header.admin_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")
<style>
    .modal-backdrop {
        position: relative !important;
</style>
@section('maincontent')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper"><!-- HTML (DOM) sourced data -->
                <section id="html">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body ">
                                    <form class="form px-3 mt-2">
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="fa fa-th-list"></i> All Course List

                                            </h4>
                                        </div>
                                    </form>
                                    <div class="card-block card-dashboard table-responsive">
                                        <table class="table table-striped table-bordered sourced-data">
                                            <thead>
                                            <tr>
                                                <th>SNO</th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>SubCat</th>
                                                <th>Skills</th>
                                                <th>Fee</th>
                                                <th>Status</th>
                                                <th>Action</th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(!empty($skillList))
                                                @foreach($skillList as $k => $val)
                                                    <tr>
                                                        <td>{{($k + 1)}}</td>
                                                        <td class="nowrap">{{$val->username}}</td>
                                                        <td class="nowrap">{{$val->catname}}</td>
                                                        <td class="nowrap">{{$val->subcatname}}</td>
                                                        <td>
                                                            @foreach(json_decode($val->skills, true) as $skill)
                                                                <span class="text text-bold">{{$skill}},</span>&nbsp;
                                                                &nbsp
                                                            @endforeach
                                                        </td>
                                                        <td>{{$val->coursefee}}</td>
                                                        <td class="text-center">
                                                            <a onclick="return confirm('Do you want to change course sattus?');" id="changeStatus" href="{{url('admin/changes-status-skill/'.custom_encode($val->id))}}">{{($val->status==1)?'Active':'InActive'}}</a>
                                                        </td>
                                                        <td class="text-center">
                                                            <a title="Details"
                                                               href="{{url('admin/all-skill/'.custom_encode($val->id))}}">
                                                                <i class="fa fa-eye" aria-hidden="true"
                                                                   style="font-size: 20px !important;"></i>
                                                            </a>&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ HTML (DOM) sourced data -->
            </div>
        </div>
    </div>



    <!-- Modal -->

@endsection
@section('scripting')
    <script>
        $('.delete').click(function () {
            if (confirm("Are you sure!")) {
                var el = this;
                var id = this.id;
                var deleteid = id;
                $.ajax({
                    url: '{{url('admin/delete-mentee')}}',
                    type: 'GET',
                    data: {id: deleteid},
                    postType: "JSON",
                    success: function (response) {
                        if (response.error == false) {
                            $(el).closest('tr').css('background', 'tomato');
                            $(el).closest('tr').fadeOut(800, function () {
                                $(this).remove();
                            });
                        }
                    }
                });
            }
        });
    </script>
@endsection
@section('footer')
    @include('footer.footer_admin')
@endsection
