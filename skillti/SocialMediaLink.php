<?php 
	include('template/header.php'); 
	protect_admin_page();
	
    $UserID = $_SESSION['USER_ID'];
    $CQuery = "select * from socialmedialink where UserID='$UserID'";
	$CResult = $dbObject->query($CQuery);
	$CRow = mysqli_fetch_assoc($CResult);
	
	
	if(isset($_POST['Submit'])) {
		$facebook = $_POST['facebook'];
		$twitter = $_POST['twitter'];
		$linkedin = $_POST['linkedin'];
		$github = $_POST['github'];
		$other = $_POST['other'];
		
		$ChkQuery = "select * from socialmedialink where UserID='$UserID'";
		$ChkResult = $dbObject->query($ChkQuery);
		$ChkRowCount = mysqli_num_rows($ChkResult);
		if($ChkRowCount > 0) {
		    $Query = "update socialmedialink set Facebook='".$facebook."', Twitter='".$twitter."', LinkedIn='".$linkedin."', Github='".$github."', Other='".$other."' where UserID='".$UserID."'";
		    $result = $dbObject->query($Query);
		    if($result == true) {
		        print "<script>setTimeout(\"location.href = '".$base_url."SocialMediaLink';\",500);</script>";
    	        session_start();
                $_SESSION['SESS_MSG'] = "Links has been updated.";
		    }
		} else {
	    	$Query = "insert into socialmedialink (UserID,Facebook,Twitter,LinkedIn,Github,Other) values ('$UserID','$facebook','$twitter','$linkedin','$github','$other')";
		    $result = $dbObject->query($Query);
		    if($result == true) {
		        print "<script>setTimeout(\"location.href = '".$base_url."SocialMediaLink';\",500);</script>";
    	        session_start();
                $_SESSION['SESS_MSG'] = "Links has been submitted.";
		    }
		}
		
		
		
		
	}

?>

<style>
    .heading.text-center {
        margin-bottom: 30px !important;
    }
</style>
<section class="mid_content dashboard_content">
	<div class="container-fluid pl-0">
		<div class="row">
			<div class="col-xs-12 col-sm-3">
			<?php include ('template/sidebar.php');?>
			</div>
			<div class="col-xs-12 col-sm-9 right_side">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			<div class="heading text-center">
					<h2>Social Media Linking</h2>
					<p>Please add your social media links</p>
				</div>
				<div class="custom_form">
				<?php include("include/error.msg.inc.php"); ?>
					 <form method="post" action="" enctype="multipart/form-data" >
						<div class="form-group">
						  <label for="">Facebook</label>
						  <input type="text" class="form-control" placeholder="" value="<?php echo $CRow['Facebook'];?>" name="facebook"  />
						</div>
						 <div class="form-group">
						  <label for="">Twitter</label>
						  <input type="text" class="form-control"  placeholder="" value="<?php echo $CRow['Twitter'];?>" name="twitter"  />
						</div>
						 <div class="form-group">
						  <label for="">LinkedIn</label>
						  <input type="text" class="form-control"  placeholder="" value="<?php echo $CRow['LinkedIn'];?>"  name="linkedin" />
						</div>
						 <div class="form-group">
						  <label for="">Github</label>
						  <input type="text" class="form-control"  placeholder="" value="<?php echo $CRow['Github'];?>"  name="github" />
						</div>
						<div class="form-group">
						  <label for="">Other</label>
						  <input type="text" class="form-control"  placeholder="" value="<?php echo $CRow['Other'];?>"  name="other" />
						  <p style="float:right;padding-top:10px;color:#939292">Your Other Platform Profile (Udemy,Skillshare,Pluralsight etc)</p>
						</div>
						<div class="text-right"><button type="submit" name="Submit" class="see_all">Submit</button></div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include('template/footer.php'); ?>
