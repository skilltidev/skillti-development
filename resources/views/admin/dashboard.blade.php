@extends('header.admin_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")

@section('maincontent')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper"><!--Statistics cards Starts-->
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
                        <div class="card gradient-blackberry">
                            <div class="card-body">
                                <div class="card-block pt-2 pb-0">
                                    <a href="{{url('admin/all-mentors')}}">
                                        <div class="media">
                                            <div class="media-body white text-left">
                                                <h3 class="font-large-1 mb-0"># {{$totalMentor}}</h3>
                                                <span>No. of Mentor</span>
                                            </div>
                                            <div class="media-right white text-right">
                                                <i class="icon-pie-chart font-large-1"></i>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div id="Widget-line-chart" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
                        <div class="card gradient-ibiza-sunset">
                            <div class="card-body">
                                <div class="card-block pt-2 pb-0">
                                    <a href="{{url('admin/all-mentee')}}">
                                        <div class="media">
                                            <div class="media-body white text-left">
                                                <h3 class="font-large-1 mb-0"># {{$totalMentee}}</h3>
                                                <span>No. of Mentee</span>
                                            </div>
                                            <div class="media-right white text-right">
                                                <i class="icon-pie-chart font-large-1"></i>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div id="Widget-line-chart1" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
                        <div class="card gradient-green-tea">
                            <div class="card-body">
                                <div class="card-block pt-2 pb-0">
                                    <a href="{{url('admin/all-syllabus')}}">
                                        <div class="media">
                                            <div class="media-body white text-left">
                                                <h3 class="font-large-1 mb-0"># {{$totalSkills}}</h3>
                                                <span>No. of Syllabus</span>
                                            </div>
                                            <div class="media-right white text-right">
                                                <i class="icon-graph font-large-1"></i>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div id="Widget-line-chart2" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
                        <div class="card gradient-pomegranate">
                            <div class="card-body">
                                <div class="card-block pt-2 pb-0">
                                    <a href="{{url('admin/purchase-courses')}}">
                                        <div class="media">
                                            <div class="media-body white text-left">
                                                <h3 class="font-large-1 mb-0"># {{$totalCourse}}</h3>
                                                <span>Total Course</span>
                                            </div>
                                            <div class="media-right white text-right">
                                                <i class="icon-book-open font-large-1"></i>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div id="Widget-line-chart3" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6 col-12">
                        <div class="card gradient-amber-amber">
                            <div class="card-body">
                                <div class="card-block pt-2 pb-0">
                                    <div class="media">
                                        <div class="media-body white text-left">
                                            <h3 class="font-large-1 mb-0">{!! get_price_html($totalAmount) !!}</h3>
                                            <span>Total Income</span>
                                        </div>
                                        <div class="media-right white text-right">
                                            <i class="icon-wallet font-large-1"></i>
                                        </div>
                                    </div>
                                </div>
                                <div id="Widget-line-chart3" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Statistics cards Ends-->
            </div>
        </div>
    </div>
@endsection
@section('scripting')
@endsection
@section('footer')
    @include('footer.footer_admin')
@endsection
