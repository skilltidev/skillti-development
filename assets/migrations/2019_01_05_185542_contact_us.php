<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContactUs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('contactUs', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('name');
		    $table->string('email');
		    $table->string('contact')->nullable();
		    $table->string('subject', 100);
		    $table->string('message', 1000);
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('contactUs');
    }
}
