<?php

function protect_admin_page() 
{

	if (!isset($_SESSION['USER_ID'])) 
	{
		echo 'You have no previlage to access<br> without login.';
		print "<script>window.location.href='https://www.jeetso.com/skillti/dev/';</script>";
		exit;
	}
}

 function checkExists($val=''){
	if(is_object($val) && in_array(strval(get_class($val)),array('stdClass')))
		$val = get_object_vars($val);
	
	if(is_string($val))
		$val=trim($val);
	
	if(empty($val) || is_null($val))
		return false;
	else
		return true;
 }
 

function randomPassword( $length = 8 ) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $password = substr( str_shuffle( $chars ), 0, $length );
    return $password;
}


?>