<?php

namespace App\Http\Controllers;

use App\Models\Chattings;
use App\Models\Course;
use App\Models\Categories;
use App\Models\Messages;
use App\Models\Ratting;
use App\Models\Skills;
use App\Models\SocialLinks;
use App\Models\SubCategories;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth,Hash,DB;
use Validator;

class MenteeController extends Controller
{
	public function dashboard() {
		$userId = Auth::user()->id;
		$coureses = Course::select('course.id', 'course.courseId', 'courseStatus', 'skills.skills','skills.shortBio','skills.description', 'categories.name as catName', 'categories.logo', 'categories.link as catlink', 'subCategories.name as subCatName', 'subCategories.link as subCatlink')
			->join('skills', 'skills.id', 'course.courseId')
			->join('categories', 'categories.id', 'skills.catId')
			->join('subCategories', 'subCategories.id', 'skills.subCatId')
			->where('course.menteeId', $userId)
			->limit(5)
			->get();
		$chattings = Chattings::select('chattings.courseId', 'chattings.message', 'chattings.sender', 'chattings.attachment', 'chattings.created_at', 'users.firstName', 'users.lastName', 'users.profilePic')
			->leftJoin('users', 'users.id', 'chattings.mentorId')
			->where('chattings.menteeId', Auth::user()->id)
			->where('chattings.status', 1)
			->where('chattings.sender', 2)
			->orderBy('chattings.id', 'desc')
			->limit(5)
			->get();
		//debug($chattings);
		return view('student.dashboard', compact('coureses', 'chattings'));
	}

	public function addCourse(Request $request) {
		$skill = Skills::select('skills.*', 'categories.name as catname', 'subCategories.name as subCatName')
			->leftJoin('categories', 'categories.id', 'skills.catId')
			->leftJoin('subCategories', 'subCategories.id', 'skills.subCatId')
			->where('skills.id', custom_decode($request->courseId))->first();
		if(!empty($skill)) {
			$message = array(
				'error' => false,
				'message' => array(
					'userId' => custom_encode(Auth::user()->id),
					'name' => Auth::user()->name,
					'email' => Auth::user()->email,
					'paymentAmount' => $skill->coursefee,
					'packageName' => $skill->catname." (".$skill->subCatName.")",
					'courseId' => $request->courseId
				)
			);
		}else{
			$message = array(
				'error' => true,
				'message' => "Something went wrong please try again!"
			);
		}
		return response()->json($message);
	}

	public function confirmPayment(Request $request) {
		$paymentResponse = $request->input('payment_response');
		$username = env('RAZZOR_PAY_KEY', 'rzp_test_TyMscKTEEZKLY6');
		$password = env('RAZZOR_PAY_SECURET_KEY', '25IdONDKQsiEJGgIG9OinIDn');
		if(isset($paymentResponse['razorpay_payment_id'])) {
			$URL = 'https://api.razorpay.com/v1/payments/' . $paymentResponse['razorpay_payment_id'];
			$result = $this->chechRazzorPay($URL, $username, $password);
			if (!isset($result['error'])) {
				$userId = custom_decode($result['notes']['userId']);
				$skillId = custom_decode($result['notes']['courseId']);
				$skill = Skills::select('id', 'userId as mentorId', 'coursefee')->where('id', $skillId)->first();
				$amount = isset($skill->coursefee)?($skill->coursefee * 100):0;
				$URL = 'https://api.razorpay.com/v1/payments/' . $paymentResponse['razorpay_payment_id'] . '/capture?amount=' . $amount;
				$resultCapture = $this->chechRazzorPay($URL, $username, $password, 'POST');
				if (!isset($resultCapture['error'])) {
					if ($resultCapture['captured']) {
						$course = new Course();
						$course->courseId = $skillId;
						$course->mentorId = $skill->mentorId;
						$course->menteeId = $userId;
						$course->courseDuration = 'No limit';
						$course->paymentStatus = 1;
						$course->courseStatus = 0;
						$course->paymentid = $resultCapture['id'];
						$course->paymentAmount = $amount/100;
						$course->save();
						$message = array(
							'error' => false,
							'message' => array(
								'id' => custom_encode($course->id)
							)
						);
					} else {
						$message = array(
							'error' => true,
							'message' => "Something went wrong if your payment deducted from bank then please contact us"
						);
					}
				} else if ($resultCapture['error']['description'] == 'Capture amount must be equal to the amount authorized') {
					$message = array(
						'error' => true,
						'message' => "Payment capture failed if your payment deducted from bank then please contact us"
					);
				} else {
					$message = array(
						'error' => true,
						'message' => "Payment Failed " . isset($resultCapture['error']['description']) ? $resultCapture['error']['description'] : ''
					);
				}
			} else {
				$message = array(
					'error' => true,
					'message' => "Payment Failed " . isset($result['error']['description']) ? $result['error']['description'] : ''
				);
			}
		}else{
			$message = array(
				'error' => true,
				'message' => "Something went wrong if your payment deducted from bank then please contact us"
			);
		}
		return response()->json($message);
	}

	private function chechRazzorPay($URL,$username, $password, $method = 'GET') {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$URL);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		$result = curl_exec ($ch);
		$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
		curl_close ($ch);
		return $resultArray = json_decode($result, true);;
	}

	public function courseRatting(Request $request) {
		$inputs = $request->input();
		$ratting = Ratting::where('menteeId', Auth::user()->id)
			->where('mentorId', custom_decode($inputs['mentorId']))
			->where('courseId', custom_decode($inputs['courseId']))
			->first();
		if(empty($ratting)) {
			$ratting = new Ratting();
		}
		$ratting->mentorId = custom_decode($inputs['mentorId']);
		$ratting->comment = $inputs['message'];
		$ratting->rating = $inputs['rate'];
		$ratting->menteeId = Auth::user()->id;
		$ratting->status = 1;
		$ratting->courseId = custom_decode($inputs['courseId']);
		$ratting->save();
		return Redirect::to('student-dashboard/my-course');
	}

    public function menteeProfileEdit(Request $request) {
        $profile = User::select('users.*')
          //  ->leftjoin('socialLinks', 'socialLinks.userId', 'users.id')
            ->where('users.id', Auth::user()->id)->first();
        if(isset($profile->ppassword)) {
            unset($profile->ppassword);
        }
        return view('student.menteeProfileSetting', compact('profile'));
    }

    public function menteeProfile(Request $request) {
        $profile = Auth::user();
        $course = Skills::getAllSkills(Auth::user()->id);
        return view('student.menteeProfile', compact('profile', 'course'));
    }

    public function menteeProfileEditSave(Request $request) {
        $inputs = $request->input();
        $this->validate($request, array(
            'firstName' => 'required|max:100',
            'lastName' => 'required|max:100',
            'profilePic' => 'image|mimes:jpeg,png,jpg|max:2048',
            'documents' => 'mimes:pdf|max:4096',
        ));
        //debug($inputs, false);
        $image = $request->file('profilePic');
        $document = $request->file('documents');

        $inputs['profilePic'] = '';
        $inputs['varificationDoc'] = '';


        $profile = User::where('id', Auth::user()->id)->first();
        //$social = SocialLinks::where('userId', Auth::user()->id)->first();

        if($inputs['firstName'] != '') {
            $profile->firstName = $inputs['firstName'];
        }

	    if (!empty($image)) {
		    $inputs['imagename'] = time().str_replace(' ', '-', $profile->firstName).".".$image->getClientOriginalExtension();
		    $destinationPath = public_path('/profile-photo');
		    $image->move($destinationPath, $inputs['imagename']);
		    $inputs['profilePic'] = $inputs['imagename'];
	    }

	    if($inputs['profilePic'] != '') {
		    $profile->profilePic = $inputs['profilePic'];
	    }

	    if (!empty($document)) {
		    $inputs['imagename'] = time().str_replace(' ', '-', $profile->firstName).".".$document->getClientOriginalExtension();
		    $destinationPath = public_path('/profile-doc');
		    $document->move($destinationPath, $inputs['imagename']);
		    $inputs['varificationDoc'] = $inputs['imagename'];
	    }

	    if($inputs['varificationDoc'] != '') {
		    $profile->varificationDoc = $inputs['varificationDoc'];
	    }


        if($inputs['lastName'] != '') {
            $profile->lastName = $inputs['lastName'];
        }
        if($inputs['qualification'] != '') {
            $profile->qualification = $inputs['qualification'];
        }

        if($inputs['about'] != '') {
            $profile->about = $inputs['about'];
        }

        if($inputs['addressOne'] != '') {
            $profile->addressOne = $inputs['addressOne'];
        }
        if($inputs['addressTwo'] != '') {
            $profile->addressTwo = $inputs['addressTwo'];
        }
        if($inputs['country'] != '') {
            $profile->country = $inputs['country'];
        }
        if($inputs['state'] != '') {
            $profile->state = $inputs['state'];
        }
        if($inputs['city'] != '') {
            $profile->city = $inputs['city'];
        }
        if($inputs['zip'] != '') {
            $profile->zip = $inputs['zip'];
        }

        if($profile->save()) {
            return back()->with('message', 'Profile successfully update.');
        }else{
            return Redirect::back()->withInput()->with('errorMessage', 'Something went wrong please try again!');
        }
    }

    public function passwordSetting() {
        return view('student.menteePassword');
    }

    public function passwordSettingPost(Request $request) {
        $inputs = $request->input();
        $this->validate($request, array(
            'oldPassword' =>  [function ($attribute, $value, $fail) {
                if(Auth::user()->ppassword != $value) {
                    $fail('Old password wrong or not matched!');
                }
            }],
            'password' =>  ['required',
                'min:6',
                'regex:/^([a-zA-Z0-9@*#]{6,15})$/',
                'confirmed'
            ],
        ));
        $ppassword = $inputs['password'];
        $password = Hash::make($ppassword);
        $user = User::where('id', Auth::user()->id)->first();
        $user->password = $password;
        $user->ppassword = $ppassword;
        if ($user->save()) {
            Auth::logout();
            return Redirect::to('signin')->with('errorMessage', 'Your password have been changed login again with new password!');
        } else {
            return back()->with('errorMessage', 'Sum error found please try again..')->withInput();
        }
    }


    public function myCourse() {
	    $userId = Auth::user()->id;
		$coureses = Course::select('course.id', 'course.courseId','course.mentorId', 'courseStatus', 'skills.skills','skills.shortBio','skills.description', 'categories.name as catName', 'categories.logo', 'categories.link as catlink', 'subCategories.name as subCatName', 'subCategories.link as subCatlink')
			->join('skills', 'skills.id', 'course.courseId')
			->join('categories', 'categories.id', 'skills.catId')
			->join('subCategories', 'subCategories.id', 'skills.subCatId')
			->where('course.menteeId', $userId)
			->get();
		if(!empty($coureses) && count($coureses) > 0) {
			foreach ($coureses as $key => $course) {
				$ratting = Ratting::where('mentorId', $course->mentorId)->where('menteeId', $userId)->where('courseId', $course->courseId)->first();
				if(empty($ratting)) {
					$ratting = new Ratting();
				}
				$coureses[$key]['ratting'] = $ratting;
			}
		}
        return view('student.myCourse', compact('coureses'));
    }

    public function message(Request $request) {
		$courseId = $request->courseId;
		$courseId = custom_decode($courseId);
	    $userId = Auth::user()->id;
	    $coureses = Course::select('course.courseStatus', 'users.firstName', 'users.lastName', 'course.id', 'course.courseId', 'categories.name as catName', 'categories.logo', 'subCategories.name as subCatName')
		    ->join('skills', 'skills.id', 'course.courseId')
		    ->join('categories', 'categories.id', 'skills.catId')
		    ->join('subCategories', 'subCategories.id', 'skills.subCatId')
		    ->join('users', 'users.id', 'course.mentorId')
		    ->where('course.menteeId', $userId)
		    ->orderBy('course.courseStatus')
		    ->get();
		if($courseId != '') {
			$chattings = Chattings::select('chattings.message', 'chattings.sender', 'chattings.attachment', 'chattings.created_at', 'users.firstName', 'users.lastName', 'users.profilePic')
				->leftJoin('users', 'users.id', 'chattings.mentorId')
				->where('chattings.menteeId', Auth::user()->id)
				->where('chattings.status', 1)
				->where('chattings.courseId', $courseId)
				->orderBy('chattings.id', 'desc')
				->limit(50)
				->get();
		}else{
			$chattings = Chattings::select('chattings.message', 'chattings.sender', 'chattings.attachment', 'chattings.created_at', 'users.firstName', 'users.lastName', 'users.profilePic')
				->leftJoin('users', 'users.id', 'chattings.mentorId')
				->where('chattings.menteeId', Auth::user()->id)
				->where('chattings.status', 1)
				->orderBy('chattings.id', 'desc')
				->limit(20)
				->get();
		}

	    if(!empty($chattings) && count($chattings) > 0) {
		    $chattings = $chattings->reverse();
	    }
	    return view('student.chatBot', compact( 'chattings', 'coureses', 'courseId'));
    }

    public function mailbox() {
		$mails = Messages::select('messages.*')
			->where('sender', 3)
			->orderBy('isRead')->orderBy('id', 'desc')
			->limit(5)->get();
		if(!empty($mails) && count($mails) > 0) {
			foreach ($mails as $key => $mail) {
				$replay = Messages::select(DB::raw('CONCAT(users.firstName, " ", users.lastName) AS mentorName'),'users.profilePic', 'messages.*')
					->join('users', 'users.id', 'messages.mentorId')
					->where('menteeId', Auth::user()->id)
					->where('sender', 2)
					->where('messageId', $mail->id)
					->get();
				if(!empty($replay) && count($replay) > 0) {
					$mail->replay = $replay;
				}else{
					$mail->replay = array();
				}
			}
		}
	    $messages = $mails;
		//debug($mails, false);
	    return view('student.mailbox', compact( 'messages'));
    }




    /********Chat-Bot**********/
    public function chatBot(){

        $mentee = User::select('users.profilePic')
            ->where('users.id', Auth::user()->id)->first();

        return view('student.chatBot', compact('mentee'));
    }

	public function saveToken(Request $request) {
		$inputs = $request->input();
		$user = User::where('id', Auth::user()->id)->first();
		$user->fcm_tocken = $inputs['token'];
		if($user->save()) {
			echo 'true'; die;
		}
		echo 'false'; die;
	}

	public function sendMessage(Request $request) {
		$inputs = $request->input();
		$courseId = custom_decode($inputs['courseId']);
		$message = $inputs['message'];
		$file = $request->file('file');
		$inputs['attachment'] = '';
		if (!empty($file)) {
			$inputs['imagename'] = time().$file->getClientOriginalName();
			$destinationPath = public_path('/chatt');
			$file->move($destinationPath, $inputs['imagename']);
			$inputs['attachment'] = $inputs['imagename'];
		}
		$course = Course::select('mentorId', 'menteeId')->where('id', $courseId)->first();
		if(!empty($course)) {
			$chatting = new Chattings();
			$chatting->mentorId = $course->mentorId;
			$chatting->courseId = $courseId;
			$chatting->menteeId = Auth::user()->id;
			$chatting->message = $message;
			$chatting->attachment = $inputs['attachment'];
			$chatting->sender = 3;
			$chatting->status = 1;
			if($chatting->save()) {
				$mentor = User::select('fcm_tocken', 'id')->where('id', $chatting->mentorId)->first();
				$senderName = Auth::user()->firstName." ".Auth::user()->lastName;
				if(!empty($mentor) && $mentor->fcm_tocken != '') {
					$payload = [
						'registration_ids' 	=> [$mentor->fcm_tocken],
						'data'				=> [
							'title' => $senderName. " Send a message",
							'body' => $chatting->message,
							'icon' => asset('images/skillti_logo_4_update-fav_icon.png'),
							'image' => asset('images/skillti_update.png'),
							'attachment' => asset('chatt/'.$chatting->attachment),
							'attachmentName' => $chatting->attachment,
							'courseId' => $chatting->courseId,
							'menteeId' => $chatting->menteeId,
							'sender' => 3,
							'mentorId' => $chatting->mentorId,
							'sendorNeme' => $senderName,
							'profilePic' => asset('profile-photo/'.Auth::user()->profilePic),
							'date' => posted_ago($chatting->created_at),
							'click_action' => url('dashboard/chat-bot/'.custom_encode($chatting->courseId)),
						],
						'notification' => [
							'click_action' => url('dashboard/chat-bot/'.custom_encode($chatting->courseId)),
							'title' => $senderName. " Send a message",
							'body' => $chatting->message,
							'icon' => asset('images/skillti_logo_4_update-fav_icon.png'),
							'image' => asset('images/skillti_update.png'),
						]
					];
					$response = $this->fcmMessage($payload);
				}
				return view('dashboard.message', compact('chatting'));
			}
		}
		return "";
	}

	private function fcmMessage($payload) {
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode( $payload ),
			CURLOPT_HTTPHEADER => [
				'Authorization: Key=' . env('SERVER_API_KEY'),
				'Content-Type: Application/json'
			]
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
			return false;
		} else {
			return $response;
		}
	}
    /********Chat-Bot**********/


}
