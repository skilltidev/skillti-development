<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
	        $table->string('firstName');
	        $table->string('lastName');
	        $table->string('email')->unique();
	        $table->tinyInteger('gender')->default('1');
	        $table->timestamp('email_verified_at')->nullable();
	        $table->string('ppassword');
	        $table->string('password');
	        $table->rememberToken();
	        $table->string('qualification')->nullable();
	        $table->integer('expertise')->nullable();
	        $table->string('addressOne', 500)->nullable();
	        $table->string('addressTwo', 500)->nullable();
	        $table->string('city')->nullable();
	        $table->string('state')->nullable();
	        $table->string('country')->nullable();
	        $table->string('zip')->nullable();
	        $table->string('varificationDoc')->nullable();
	        $table->integer('experience')->default('0');
	        $table->tinyInteger('certified')->default('0');
	        $table->string('about')->nullable();
	        $table->string('tagline')->nullable();
	        $table->string('interestToLearn')->nullable();
	        $table->string('msgurl')->nullable();
	        $table->tinyInteger('isFirstTimeLogin')->default('1');
	        $table->dateTime('lastLoggedIn')->nullable();
	        $table->integer('roleId')->unsigned();
	        $table->tinyInteger('status')->default('0');
            $table->timestamps();
        });
	    Schema::table('users', function($table) {
		    $table->foreign('roleId')->references('id')->on('roles');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('users');
    }
}
