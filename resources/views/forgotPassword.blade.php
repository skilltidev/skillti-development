@extends('header.home_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")

@section('maincontent')
    <section class="mid_content">
        <div class="gradient-background"></div>
        <div class="container">
            <div class="col-xs-12">
                <div class="heading text-center">
                    <h2>Forget Password</h2>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-xs-offset-0">
                @if(Session::get('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> {{ Session::get('message') }}{{ Session::forget('message') }}
                    </div>
                @endif
                @if(Session::get('errorMessage'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Message !</strong> {{ Session::get('errorMessage') }}{{ Session::forget('errorMessage') }}
                    </div>
                @endif
                <div class="custom_form">
                    <form method="post" action="{{url('forgotPassword')}}">
                        @csrf
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Your Email " value="{{ old('email')??'' }}" required="">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" name="Reset" class="blue_btn custom_btn">Send Reset Link</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripting')
@endsection

@section('footer')
    @include('footer.footer_home')
@endsection
