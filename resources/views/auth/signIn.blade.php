@extends('header.home_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")

@section('maincontent')
    <section class="mid_content">
        <div class="gradient-background"></div>
        <div class="container">
            <div class="col-xs-12">
                <div class="heading text-center">
                    <h2>Log In</h2>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-xs-offset-0">


                <div class="custom_form">
                    @if(Session::get('message'))
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ Session::get('message') }}{{ Session::forget('message') }}
                        </div>
                    @endif
                    @if(Session::get('errorMessage'))
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Message !</strong> {{ Session::get('errorMessage') }}{{ Session::forget('errorMessage') }}
                        </div>
                    @endif
                    <form method="post" action="{{url('signin')}}">
                        @csrf
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="username" class="form-control" placeholder="Username" value="" required="">
                            @if ($errors->has('username'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Password" value="" autocomplete="off" required="">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" name="Login" class="blue_btn custom_btn">Login</button>
                        </div>
                        <div class="text-center">
                            <a href="forgotPassword" class="ForgetPwd">Forget Password?</a>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('scripting')
@endsection

@section('footer')
    @include('footer.footer_home')
@endsection
