@extends('header.student_header')
@section('headtitle', "Dashboard")
@section('headdesc', "")
@section('maincontent')
    <div class="mdk-drawer-layout__content">
        <div class="page">
            <div class="container page__container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url('student-dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Edit Account</li>
                </ol>
                <h1 class="h2">Edit Account</h1>

                <div class="card">
                    <ul class="nav nav-tabs nav-tabs-card">
                        <li class="nav-item">
                            <a class="nav-link active" href="#first" data-toggle="tab">Account</a>
                        </li>
                    </ul>
                    <div class="tab-content card-body">
                        <div class="tab-pane active" id="first">
                            <form method="post" action="{{url('student-dashboard/mentee-profile-edit')}}" enctype="multipart/form-data" class="form-horizontal">
                                @csrf
                                <div class="form-group row">
                                    <label for="avatar" class="col-sm-3 col-form-label form-label">Profile Pic</label>
                                    <div class="col-sm-9">
                                        <div class="media align-items-center">
                                            <div class="media-left">
                                                <div class="icon-block rounded">
                                                    <img src="{{profile_image(asset('profile-photo/'.$profile->profilePic))}}" alt="profile pic" class="img-thumbnail material-icons text-muted-light md-36">
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <div class="custom-file" style="width: auto;">
                                                    <input accept=".jpeg,.jpg,.png" type="file" id="profilePic" name="profilePic" class="custom-file-input">
                                                    <label for="avatar" class="custom-file-label">Choose file</label>
                                                </div>
                                                @if ($errors->has('profilePic'))
                                                    <span style="display: block;" class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('profilePic') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-sm-3 col-form-label form-label">Full Name</label>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input id="name" name="firstName" type="text" class="form-control" placeholder="First Name" value="{{old('firstName')?old('firstName'):$profile->firstName}}">
                                                @if ($errors->has('firstName'))
                                                    <span style="display: block;" class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('firstName') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="lastName" class="form-control" placeholder="Last Name" value="{{old('lastName')?old('lastName'):$profile->lastName}}">
                                                @if ($errors->has('lastName'))
                                                    <span style="display: block;" class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('lastName') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="avatar" class="col-sm-3 col-form-label form-label">Upload your documents</label>
                                    <div class="col-sm-9">
                                        <div class="media align-items-center">
                                            <div class="media-body">
                                                <div class="custom-file" style="width: auto;">
                                                    <input accept=".pdf" type="file" id="documents" name="documents" class="custom-file-input">
                                                    <label for="avatar" class="custom-file-label">Choose file</label>
                                                </div>
                                                @if ($errors->has('documents'))
                                                    <span style="display: block;" class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('documents') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            @if($profile->varificationDoc != '')
                                            <div class="media-right">
                                                <a class="dropdown-item" href="javascript: w=window.open('{{url(asset('profile-doc/'.$profile->varificationDoc))}}'); w.print(); w.close(); ">​​​​​​​​​​​​​​​​​<i class="material-icons md-18 text-muted">attachment</i> {{$profile->varificationDoc}}</a>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label form-label">Email</label>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="material-icons md-18 text-muted">mail</i>
                                                </div>
                                            </div>
                                            <input type="email" id="email" class="form-control" placeholder="Email Address" value="{{$profile->email}}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label form-label">Qualification</label>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="material-icons md-18 text-muted">school</i>
                                                </div>
                                            </div>
                                            <input type="text" id="qualification" name="qualification" class="form-control" placeholder="Qualification" value="{{old('qualification')?old('qualification'):$profile->qualification}}">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label form-label">Descrive your self</label>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="material-icons md-18 text-muted">comment</i>
                                                </div>
                                            </div>
                                            <textarea name="about" id="about" placeholder="Descrive your self" cols="30" rows="2" class="form-control">{{old('about')?old('about'):$profile->about}}</textarea>
                                        </div>
                                    </div>
                                </div>
                              {{--  <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label form-label">Expertise</label>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="material-icons md-18 text-muted">textsms</i>
                                                </div>
                                            </div>
                                            <textarea name="expertise" id="expertise" placeholder="Your Expertise" cols="30" rows="2" class="form-control">{{old('expertise')?old('expertise'):$profile->expertise}}</textarea>
                                        </div>
                                    </div>
                                </div>--}}
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label form-label">Address 1</label>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="material-icons md-18 text-muted">location_on</i>
                                                </div>
                                            </div>
                                            <textarea name="addressOne" id="addressOne" placeholder="Address" cols="30" rows="2" class="form-control">{{old('addressOne')?old('addressOne'):$profile->addressOne}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label form-label">Address 2</label>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="material-icons md-18 text-muted">location_on</i>
                                                </div>
                                            </div>
                                            <textarea name="addressTwo" id="addressTwo" placeholder="Address" cols="30" rows="2" class="form-control">{{old('addressTwo')?old('addressTwo'):$profile->addressTwo}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label form-label">Country</label>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="material-icons md-18 text-muted">flag</i>
                                                </div>
                                            </div>
                                            {!! countrySelector(old('country')?old('country'):$profile->country, "country", "country"); !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label form-label">State</label>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="material-icons md-18 text-muted">edit_location</i>
                                                </div>
                                            </div>
                                            <input type="text" id="state" name="state" class="form-control" placeholder="State" value="{{old('state')?old('state'):$profile->state}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label form-label">City</label>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="material-icons md-18 text-muted">edit_location</i>
                                                </div>
                                            </div>
                                            <input type="text" id="city" name="city" class="form-control" placeholder="City" value="{{old('city')?old('city'):$profile->city}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label form-label">ZIP Code</label>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="material-icons md-18 text-muted">local_shipping</i>
                                                </div>
                                            </div>
                                            <input type="text" id="zip" name="zip" class="form-control" placeholder="ZIP Code" value="{{old('zip')?old('zip'):$profile->zip}}">
                                        </div>
                                    </div>
                                </div>





                                <div class="form-group row">
                                    <div class="col-sm-8 offset-sm-3">
                                        <div class="media align-items-center">
                                            <div class="media-left">
                                                <button type="submit" class="btn btn-success">Save Changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('menu')
    @include('student.menu')
@endsection
@section('scripting')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.img-thumbnail').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profilePic").change(function(){
            readURL(this);
            var name = $(this)[0].files[0].name;
            $(this).siblings('label').text(name);
        });
        $("#documents").change(function() {
            var name = $(this)[0].files[0].name;
            $(this).siblings('label').text(name);
        });
    </script>
@endsection
@section('footer')
    @include('footer.footer_dashboard')
@endsection
