@extends('header.home_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")

@section('maincontent')
    <style>
        .why-stripe .bullet-points li {
            color: #000;
        }
        .why-stripe .intro-text {
            color: #000;
        }

    </style>
    <section class="mid_content">
        <!--<div class="gradient-background" style="background-image:url('assets/images/map.PNG');"></div>-->
        <div class="container">
            <div class="col-xs-12">
                <div class="heading text-center">
                    <h2>Contact Us</h2>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7">
                @if(Session::get('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> {{ Session::get('message') }}{{ Session::forget('message') }}
                    </div>
                @endif
                @if(Session::get('errorMessage'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Message !</strong> {{ Session::get('errorMessage') }}{{ Session::forget('errorMessage') }}
                    </div>
                @endif
                <div class="custom_form">
                    <form method="post" action="{{url('contact-us')}}">
                        @csrf
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" class="form-control" placeholder="Jhon" name="name" required="" value="{{old('name')}}">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" class="form-control" id="" placeholder="test@ymail.com" name="email" required="" value="{{old('email')}}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Subject</label>
                            <input type="text" class="form-control" id="" placeholder="Subject" name="subject" required="" value="{{old('subject')}}">
                            @if ($errors->has('subject'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('subject') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Comment</label>
                            <textarea name="comment" placeholder="comment" class="form-control" cols="30" rows="5">{{old('comment')}}</textarea>
                            @if ($errors->has('comment'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('comment') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="text-right"><button type="submit" name="Submit" class="blue_btn custom_btn">Submit</button></div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-sm-offset-0">
                <div class="why-stripe" style="padding-top:0px;">
                    <p class="intro-text">With Skillti you can:</p>
                    <ul class="bullet-points">
                        <li>Email: hello@skillti.com</li>
                        <li>Address: Noida</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripting')
@endsection

@section('footer')
    @include('footer.footer_home')
@endsection
