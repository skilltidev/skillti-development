@extends('header.home_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")

@section('maincontent')
    <section class="mid_content">
        <div class="gradient-background"></div>
        <div class="container">
            <div class="col-xs-12">
                <div class="heading text-center">
                    <h2>Reset Password</h2>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-xs-offset-0">
                @if(Session::get('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> {{ Session::get('message') }}{{ Session::forget('message') }}
                    </div>
                @endif
                @if(Session::get('errorMessage'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Message !</strong> {{ Session::get('errorMessage') }}{{ Session::forget('errorMessage') }}
                    </div>
                @endif
                <div class="custom_form">
                    <form method="post" action="{{url('resetPassword')}}">
                        @csrf
                        <input type="hidden" name="email" value="{{$email}}">
                        <input type="hidden" name="token" value="{{$token}}">
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" placeholder="New password " value="{{ old('password')??'' }}" required="">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm password " value="{{ old('password_confirmation')??'' }}" required="">
                            @if ($errors->has('password_confirmation'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" name="Reset" class="blue_btn custom_btn">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripting')
@endsection

@section('footer')
    @include('footer.footer_home')
@endsection
