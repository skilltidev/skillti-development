<?php

use Illuminate\Database\Eloquent\Model;

if(!function_exists('sendmail')) {
	function sendmail($data)
	{
		require_once(FCPATH . 'assets/phpmailer/class-phpmailer.php');
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->Username = SMTP_USER;
		$mail->Password = SMTP_PASS;
		$mail->SMTPSecure = 'STARTTLS';
		$mail->SMTPAutoTLS = true;
		$mail->Host = SMTP_HOST;
		$mail->Port = SMTP_PORT;
		$from_email = isset($data['from']) ? $data['from'] : SMTP_EMAIL;
		$from_name = isset($data['from_name']) ? $data['from_name'] : SMTP_NAME;
		$mail->SetFrom($from_email, $from_name);
		$mail->isHTML(true);
		$mail->Subject = $data['subject'];
		$mail->MsgHTML($data['message']);
		$mail->AddAddress($data['to']);
		$mail->SMTPDebug = 0;
		
		/* Send mail and return result */
		if (!$mail->Send()) {
			$errors = $mail->ErrorInfo;
			
			return false;
		} else {
			$mail->ClearAddresses();
			$mail->ClearAllRecipients();
			
			return true;
		}
	}
}


function nice_number($n)
{
    // first strip any formatting;
    $n = (0 + str_replace(",", "", $n));
    // is this a number?
    if (!is_numeric($n)) return false;

    // now filter it;
    if ($n > 1000000000000) return number_format(($n / 1000000000000), 2) . ' T';
    else if ($n > 1000000000) return number_format(($n / 1000000000), 2) . ' B';
    else if ($n > 1000000) return number_format(($n / 1000000), 2) . ' M';
    else if ($n > 1000) return number_format(($n / 1000), 2) . ' K';
    else return number_format($n, 0);
    return $n;
}

function nice_number_space($n)
{
    // first strip any formatting;
    $n = (0 + str_replace(",", "", $n));
    // is this a number?
    if (!is_numeric($n)) return false;

    // now filter it;
    if ($n > 1000000000000) return number_format(($n / 1000000000000), 2) . 'T';
    else if ($n > 1000000000) return number_format(($n / 1000000000), 2) . 'B';
    else if ($n > 1000000) return number_format(($n / 1000000), 2) . 'M';
    else if ($n > 1000) return number_format(($n / 1000), 2) . 'K';
    else return number_format($n, 0);
    return $n;
}


if(!function_exists('get_title')) {
	function get_title($title, $trailing = true)
	{
		if ($trailing)
			$title .= ' - ' . SITE_NAME;
		
		return $title;
	}
}

if(!function_exists('compare_datetime')) {
	function compare_datetime($a, $b)
	{
		$ad = strtotime($a['exact_date']);
		$bd = strtotime($b['exact_date']);
		if ($ad == $bd) {
			return 0;
		}
		
		return $ad > $bd ? 1 : -1;
	}
}

if(!function_exists('get_extention')) {
	function get_extention($file)
	{
		return pathinfo($file['name'], PATHINFO_EXTENSION);
	}
}

if(!function_exists('custom_encode')) {
	function custom_encode($string)
	{
		$key = "cYbErClINicAdItYa";
		$string = base64_encode($string);
		$string = str_replace('=', '', $string);
		$main_arr = str_split($string);
		$output = array();
		$count = 0;
		for ($i = 0; $i < strlen($string); $i++) {
			$output[] = $main_arr[ $i ];
			if ($i % 2 == 1) {
				$output[] = substr($key, $count, 1);
				$count++;
			}
		}
		$string = implode('', $output);
		
		return $string;
	}
}

if(!function_exists('custom_decode')) {
	function custom_decode($string)
	{
		$key = "cYbErClINicAdItYa";
		$arr = str_split($string);
		$count = 0;
		for ($i = 0; $i < strlen($string); $i++) {
			if ($count < strlen($key)) {
				if ($i % 3 == 2) {
					unset($arr[ $i ]);
					$count++;
				}
			}
		}
		$string = implode('', $arr);
		$string = base64_decode($string);
		
		return $string;
	}
}

if(!function_exists('getFriendlyURL')) {
	function getFriendlyURL($string)
	{
		setlocale(LC_CTYPE, 'en_US.UTF8');
		$string = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $string);
		$patterns = array("/\s+/", "~[^\-\pL\pN\s]+~u");
		$replacer = array("-", "$1");
		$string = preg_replace($patterns, $replacer, $string);
		$string = trim($string, "-");
		$string = strtolower($string);
		
		return $string;
	}
}

if(!function_exists('json_readable_encode')) {
	function json_readable_encode($in, $indent = 0, $from_array = false)
	{
		$_myself = __FUNCTION__;
		$_escape = function ($str) {
			return preg_replace("!([\b\t\n\r\f\"\\'])!", "\\\\\\1", $str);
		};
		
		$out = '';
		
		foreach ($in as $key => $value) {
			$out .= str_repeat("\t", $indent + 1);
			$out .= "\"" . $_escape((string)$key) . "\": ";
			
			if (is_object($value) || is_array($value)) {
				$out .= "\n";
				$out .= $_myself($value, $indent + 1);
			} else if (is_bool($value)) {
				$out .= $value ? 'true' : 'false';
			} else if (is_null($value)) {
				$out .= 'null';
			} else if (is_string($value)) {
				$out .= "\"" . $_escape($value) . "\"";
			} else {
				$out .= $value;
			}
			
			$out .= ",\n";
		}
		
		if (!empty($out)) {
			$out = substr($out, 0, -2);
		}
		
		$out = str_repeat("\t", $indent) . "{\n" . $out;
		$out .= "\n" . str_repeat("\t", $indent) . "}";
		
		return $out;
	}
}

// include scripts and css
if(!function_exists('inclusions')) {
	function inclusions($values = array())
	{
		$options = array(
			'wow'             => array(
				array(
					'type'  => 'css',
					'value' => 'assets/css/wow.min'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/js/wow.min'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/js/wow_init'
				)
			),
			'validate'        => array(
				array(
					'type'  => 'header_js',
					'value' => 'assets/js/validator',
				),
				array(
					'type'  => 'header_js',
					'value' => 'assets/js/validator-methods',
				),
			),
			'ajax_form'       => array(
				array(
					'type'  => 'header_js',
					'value' => 'assets/js/jquery.form'
				),
			),
			'notification'    => array(
				array(
					'type'  => 'js',
					'value' => 'assets/js/jquery.notification'
				),
				array(
					'type'  => 'css',
					'value' => 'assets/css/notification'
				),
			),
			'check_email_url' => array(
				array(
					'type'  => 'php_scripts',
					'value' => 'assets/php_scripts/check_email_url.php'
				)
			),
			'clipboard'       => array(
				array(
					'type'  => 'header_js',
					'value' => 'assets/clipboard/ZeroClipboard'
				),
				array(
					'type'  => 'php_scripts',
					'value' => 'assets/clipboard/ZeroClipboard.php'
				)
			),
			'dropdown'        => array(
				array(
					'type'  => 'css',
					'value' => 'assets/select2/select2.min'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/select2/select2.full.min'
				)
			),
			'twilio'          => array(
				array(
					'type'  => 'header_js',
					'value' => 'assets/twilio-video.js/src/twilio-video'
				)
			),
			'input_mask'      => array(
				array(
					'type'  => 'js',
					'value' => 'assets/input-mask/jquery.inputmask'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/input-mask/jquery.inputmask.date.extensions'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/input-mask/jquery.inputmask.extensions'
				)
			),
			'daterangepicker' => array(
				array(
					'type'  => 'css',
					'value' => 'assets/daterangepicker/daterangepicker'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/datepicker/moment.min'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/daterangepicker/daterangepicker'
				)
			),
			'ga_jsapi'        => array(
				array(
					'type'  => 'header_js',
					'value' => 'assets/js/ga_jsapi'
				)
			),
			'jquery-ui'       => array(
				array(
					'type'  => 'js',
					'value' => 'assets/jQueryUI/jquery-ui.min'
				),
				array(
					'type'  => 'css',
					'value' => 'assets/jQueryUI/jquery-ui.min'
				),
			),
			'fileupload'      => array(
				array(
					'type'  => 'js',
					'value' => 'assets/js/jquery.fileupload'
				),
			),
			'imagecrop'       => array(
				array(
					'type'  => 'css',
					'value' => 'assets/crop/imgareaselect-animated'
				),
				array(
					'type'  => 'header_js',
					'value' => 'assets/crop/jquery.imgareaselect.pack'
				)
			),
			'jquery-browser'  => array(
				array(
					'type'  => 'header_js',
					'value' => 'assets/js/jquery-browser'
				)
			),
			'summernote'      => array(
				array(
					'type'  => 'css',
					'value' => 'assets/summernote/summernote'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/summernote/summernote.min'
				),
			),
			'jscolor'         => array(
				array(
					'type'  => 'js',
					'value' => 'assets/js/jscolor'
				)
			),
			'tabs'            => array(
				array(
					'type'  => 'js',
					'value' => 'assets/js/responsive-tabs'
				)
			),
			'last_tab'        => array(
				array(
					'type'  => 'js',
					'value' => 'assets/js/jquery-cookie.min'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/js/last_tab'
				)
			),
			'fancybox'        => array(
				array(
					'type'  => 'js',
					'value' => 'assets/fancybox/jquery.fancybox'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/js/jquery-browser'
				),
				array(
					'type'  => 'css',
					'value' => 'assets/fancybox/jquery.fancybox'
				),
			),
			'media'           => array(
				array(
					'type'  => 'js',
					'value' => 'assets/media/media'
				),
				array(
					'type'  => 'css',
					'value' => 'assets/media/media'
				),
			),
			'iCheck'          => array(
				array(
					'type'  => 'header_js',
					'value' => 'assets/iCheck/icheck.min'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/iCheck/icheck'
				),
				array(
					'type'  => 'css',
					'value' => 'assets/iCheck/all'
				),
			),
			'googlemap'       => array(
				array(
					'type'  => 'header_js',
					'value' => '//maps.googleapis.com/maps/api/js?signed_in=false&v=3.exp&libraries=geometry&libraries=places&key=AIzaSyByQPehTQPMAy3osZK0EwS6DBbv2CXAR98'
				),
			),
			'datatable'       => array(
				array(
					'type'  => 'header_js',
					'value' => 'assets/datatables/jquery.dataTables.min'
				),
				array(
					'type'  => 'header_js',
					'value' => 'assets/datatables/dataTables.bootstrap.min'
				),
				array(
					'type'  => 'css',
					'value' => 'assets/datatables/dataTables.bootstrap'
				),
			),
			'datepicker'      => array(
				array(
					'type'  => 'css',
					'value' => 'assets/datepicker/datetimepicker.min'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/datepicker/moment.min'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/datepicker/datetimepicker.min'
				)
			),
			'calendar'        => array(
				array(
					'type'  => 'css',
					'value' => 'assets/fullcalendar/fullcalendar.min'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/fullcalendar/moments'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/fullcalendar/fullcalendar.min'
				),
			),
			'videojs'         => array(
				array(
					'type'  => 'css',
					'value' => 'assets/video/bigvideo'
				), array(
					'type'  => 'js',
					'value' => 'assets/video/video'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/video/bigvideo'
				),
			),
			'bgvideo'         => array(
				array(
					'type'  => 'header_js',
					'value' => 'assets/video/jquery.bgvideo.min'
				),
				array(
					'type'  => 'header_js',
					'value' => 'assets/video/modernizr-video'
				)
			),
			'slider'          => array(
				array(
					'type'  => 'js',
					'value' => 'assets/js/frontend/slider'
				),
				array(
					'type'  => 'css',
					'value' => 'assets/css/frontend/slider'
				)
			),
			'chosen'          => array(
				array(
					'type'  => 'css',
					'value' => 'assets/chosen/chosen'
				),
				array(
					'type'  => 'js',
					'value' => 'assets/chosen/chosen'
				),
			),
			'bootbox'         => array(
				array(
					'type'  => 'header_js',
					'value' => 'assets/js/bootbox.min'
				)
			)
		);
		
		$output['header_js'] = array(
			'assets/js/jquery-3.1.1.min'
		);
		
		foreach ($values as $value) {
			$inputs = $options[ $value ];
			foreach ($inputs as $input) {
				$output[ $input['type'] ][] = $input['value'];
			}
		}
		
		return $output;
	}
}

if(!function_exists('format_datetime')) {
	function format_datetime($datetime)
	{
		return date('j M, Y - h:i A', strtotime($datetime));
	}
}

if(!function_exists('format_date')) {
	function format_date($date)
	{
		return date('j M, Y', strtotime($date));
	}
}

if(!function_exists('format_time')) {
	function format_time($time)
	{
		return date('h:i A', $time);
	}
}

if(!function_exists('timezone_datetime')) {
	function timezone_datetime($datetime = '')
	{
		$timezone_datetime = new DateTime($datetime, new DateTimeZone('Asia/Kolkata'));
		
		return $timezone_datetime;
	}
}

if(!function_exists('posted_ago')) {
	function posted_ago($datetime, $full = false)
	{
		$now = timezone_datetime();
		
		$ago = $datetime;//timezone_datetime($datetime);
		
		$diff = $now->diff($ago);
		
		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;
		
		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[ $k ]);
			}
		}
		
		if (!$full)
			$string = array_slice($string, 0, 1);
		
		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
}


if(!function_exists('getTimeInBetween')) {
	function getTimeInBetween($longtime)
	{
		$future_date = date('Y-m-d H:i:s', $longtime);
		$next = posted_ago($future_date);
		
		return $next;
	}
}

if(!function_exists('debug')) {
	function debug($item = array(), $die = true, $display = true)
	{
		if (is_array($item) || is_object($item)) {
			echo "<pre " . ($display ? '' : 'style="display:none"') . ">";
			if(is_object($item)) {
				print_r($item->toArray());
			}else{
				print_r($item);
			}
			echo "</pre>";
		} else {
			echo $item;
		}
		
		if ($die) {
			die();
		}
	}
}

if(!function_exists('sendSMS')) {
	function sendSMS($mobile, $text)
	{
		$data = array(
			"APIKey"   => "pLOoQ3BVuEyhv9shaVKUKA",
			"senderid" => "WEBSMS",
			"channel"  => "2",
			"DCS"      => "0",
			"flashsms" => "0",
			"number"   => $mobile,
			"text"     => urlencode($text),
			"route"    => "11",
		);
		
		$fields = '';
		foreach ($data as $key => $value) {
			$fields .= $key . '=' . $value . '&';
		}
		
		$fields = rtrim($fields, '&');
		$url = 'http://login.smsgatewayhub.com/api/mt/SendSMS?' . $fields;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 2);
		curl_setopt($ch, CURLOPT_URL, $url);
		$result = curl_exec($ch);
		curl_close($ch);
	}
}

if(!function_exists('sendOTP')) {
	function sendOTP($mobile)
	{
		$digits = "012345678901234567890123456789012345678901234567890123456789";
		$otp = substr(str_shuffle($digits), 0, 6);
		$text = "Your Activation Code for Vcanship is " . $otp . ". Please use this code to complete your signup process.";
		set_session('register_otp', $otp);
		sendSMS($mobile, $text);
	}
}

if(!function_exists('random_code')) {
	function random_code($length = 16)
	{
		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		$code = substr(str_shuffle($chars), 0, $length);
		
		return $code;
	}
}


function set_flashdata($name, $message, $class = '') {
	$CI = & get_instance();
	$data = array(
		'message' => '<div class="' . TOGGLE_CLOSE_CLASS . ' alert alert-' . $class . '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $message . '</div>',
		'type' => $class
	);
	$CI->session->set_flashdata($name, $data);
}

function get_flashdata($name) {
	$CI = & get_instance();
	$data = $CI->session->flashdata($name);
	return $data['message'];
}

function set_flash_session($name, $value){
	$CI = & get_instance();
	$CI->session->set_flashdata($name, $value);
}

function get_flash_session($name) {
	$CI = & get_instance();
	return $CI->session->flashdata($name);
}

function set_notification($message, $class) {
	set_flashdata('notification', $message, $class);
}

function get_notification() {
	$data = get_flashdata('notification');
	return $data;
}

function set_login_sessions($userData) {
	$data = array(
		'logged_in' => 1,
		'user_id' => $userData['id'],
		'name' => $userData['firstname'].' '.$userData['lastname'],
		'email' => $userData['email'],
		'profile_pic' => $userData['profile_pic'],
	);
	set_sessions($data);
}

function unset_login_sessions() {
	$data = array(
		'logged_in',
		'user_id',
		'name',
		'email',
		'profile_pic',
	);
	foreach ($data as $value) {
		unset_session($value);
	}
}

function set_userLogin_sessions($userData) {
	$data = array(
		'user_logged_in' => 1,
		'user_id' => $userData['id'],
		'name' => $userData['name'],
		'email' => $userData['email'],
		'profile_pic' => $userData['profile_pic'],
	);
	set_sessions($data);
}
function generateRandomString($length = 10) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

function unset_userLogin_sessions() {
	$data = array(
		'user_logged_in',
		'user_id',
		'name',
		'email',
		'profile_pic',
	);
	foreach ($data as $value) {
		unset_session($value);
	}
}


function page_title($page_title) {
	echo '
		<div class="page_title">
			<h1>' . $page_title . '</h1>
		</div>
	';
}

function truncate($string, $word_count = 10) {
	$string = htmlspecialchars_decode(strip_tags($string));
	$words = explode(' ', $string);

	$output = '';
	foreach ($words as $key => $word) {
		if ($key < $word_count) {
			$output .= $word . ' ';
		}
	}

	if (sizeof($words) > $word_count) {
		return $output . '...';
	}
	return $output;
}

// =============================================
// =============================================
// ================= ADMIN PANEL ===============
// =============================================
// =============================================

function set_admin_sessions($admin) {
	$data = array(
		'admin_logged_in' => '1',
		'admin_fullname' => $admin['name'],
		'admin_id' => $admin['id'],
		'image' => $admin['pic']
	);
	set_sessions($data);
}

function unset_admin_sessions() {
	$data = array(
		'admin_logged_in',
		'admin_fullname',
		'admin_role',
		'image',
	);
	foreach ($data as $value) {
		unset_session($value);
	}
}

function admin_access() {
	if (get_session('admin_frontend_login') == 1) {
		unset_login_session();
		$data['admin_logged_in'] = '1';
		set_sessions($data);
	}
}

function check_admin_logged_in() {
	if (!admin_logged_in()) {
		redirect('admin');
	}
}

function admin_logged_in() {
	if (get_session('admin_logged_in') == "1") {
		return true;
	}
	return false;
}

function get_admin($name = "") {
	$data = array(
		'fullname' => get_session('admin_fullname'),
		'role' => get_session('admin_role')
	);

	if (empty($name)) {
		return $data;
	}

	if (isset($data[$name])) {
		return $data[$name];
	}
	return false;
}

function get_youtube_link($video_id, $controls = 0) {
	return '//youtube.com/embed/' . $video_id . '?VQ=HD720&autoplay=1&controls=' . $controls . '&showinfo=0&rel=0&iv_load_policy=3';
}

function year_filter($year_from = '', $year_to = '') {
	if ($year_from == '') {
		return ' ( ' . $year_to . ' ) ';
	} else if ($year_to == '') {
		return ' ( ' . $year_from . ' ) ';
	} else if ($year_from == '' && $year_to == '') {
		return '';
	} else {
		return ' ( ' . $year_from . ' - ' . $year_to . ' ) ';
	}
}

function google_url($file) {
	return "https://docs.google.com/gview?url=" . $file . "&embedded=true";
}

function check_image_type($file, $ext = array()) {

	$type = pathinfo($file, PATHINFO_EXTENSION);

	if (empty($ext)) {
		$ext = ['jpg', 'png', 'jpeg'];
	}

	if (in_array($type, $ext)) {
		return true;
	} else {
		return false;
	}
}

function get_price_html($price) {
	return get_currency() . ' ' . number_format($price, 2);
}

function get_currency() {
	return ' <i class="fa fa-inr"></i> ';
}

function get_payment_status($type){
	if(strtoupper($type) == 'CREDIT'){
		$output = '<span class="label label-success">'.$type.'</span>';
	}else{
		$output = '<span class="label label-warning">'.$type.'</span>';
	}
	return $output;
}

function appointment_date($time) {
	$date = date('d M, Y', $time/1000);
	return $date;
}

function appointment_slot($start, $end, $date = false) {
	$start_time = date('h:i A', $start/1000);
	$end_time = date('h:i A', $end/1000);
	if($date != false){
		$response = date('d M Y', $date/1000).', '.$start_time;
	}else{
		$response = $start_time.' to '.$end_time;
	}
	return $response;
}

function json_output($json) {
	$CI = & get_instance();
	$CI->output->set_content_type('application/xml');
	$CI->output->set_output($json);
}

function profile_image($url = '') {
	if ($url == '' || empty($url)) {
		return asset('profile-photo/User-dummy.png');
	} else {
		if($url != asset('profile-photo')) {
			if(file_exists(public_path(str_replace(url(''), '', $url)))){
				return $url;
			}else{
				return asset('profile-photo/User-dummy.png');
			}
		} else {
			return asset('profile-photo/User-dummy.png');
		}
	}
}

function data_output_datatable($columns, $data) {
	$out = array();
	for ($i = 0, $ien = count($data); $i < $ien; $i++) {
		$row = array();
		for ($j = 0, $jen = count($columns); $j < $jen; $j++) {
			$column = $columns[$j];
			// Is there a formatter?
			if (isset($column['formatter'])) {
				$row[$column['dt']] = $column['formatter']($data[$i][$column['db']], $data[$i]);
			} else {
				$row[$column['dt']] = $data[$i][$columns[$j]['db']];
			}
		}
		$out[] = $row;
	}
	return $out;
}

function rating($rate = 0) {
	$output = '<span class="rating">';
	for($i=0; $i < 5; $i++) {
		$rates = ($rate > $i)?'rate':'';
		$output .= "<i class='fa fa-star pull-left ".$rates."'></i>";
	}
	$output .= '</span>';
	return $output;
}

function ajax_alert($message, $class){
	$msg = '<div class="alert alert-'.$class.' alert-dismissable">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong> Message:  </strong> '.$message.'
			</div>';
	return $msg;
}

function readNotification($id) {
	$response = getParamRecord(READ_NOTIFICATIONS, array(
		'id' => custom_decode($id)
	));

	if($response['status'] == 'success') {
		$output = array(
			'error' => false,
			'data' => '',
		);
	} else {
		$output = array(
			'error' => true,
			'data' => '',
		);
	}
	return $output;
}

function booking_status($status) {
	if($status == 'BOOKED') {
		$output = '<span class="label label-primary">Pending</span>';
	} elseif( $status == 'ACCEPTED') {
		$output = '<span class="label label-success">Accepted</span>';
	} elseif( $status == 'REJECTED') {
		$output = '<span class="label label-danger">Rejected</span>';
	} elseif( $status == 'CANCELLED' || $status == 'CANCELLED') {
		$output = '<span class="label label-warning">Cancelled</span>';
	} elseif( $status == 'COMPLETE' || $status == 'COMPLETED') {
		$output = '<span class="label label-success">Completed</span>';
	}elseif( $status == 'GENERAL' || $status == 'general') {
		$output = '<span class="label label-info">General</span>';
	}elseif( $status == 'BOOKING' || $status == 'booking') {
		$output = '<span class="label label-success">Booking</span>';
	}else {
		$output = '<span class="label label-default">'.$status.'</span>';
	}
	return $output;
}

function transaction_status($status) {
	if($status == 'COMPLETE') {
		$output = '<span class="label label-success">Completed</span>';
	} elseif( $status == 'FAILED') {
		$output = '<span class="label label-warning">Failed</span>';
	}elseif( $status == 'REFUND') {
		$output = '<span class="label label-danger">Refund</span>';
	} else {
		$output = '<span class="label label-info">'.$status.'</span>';
	}
	return $output;
}

function remove_zero($time) {
    return str_replace(':00 ', ' ', $time);
}
function split_on($string, $num) {
    $length = strlen($string);
    $output[0] = substr($string, 0, $num);
    $output[1] = substr($string, $num, $length );
    return $output;
}

function question_status($astrologer, $admin) {
    if(!empty($admin)) {
        return '<span class="text text-success fontweight">Answered</span>';
    } else if (!empty($astrologer)) {
        return '<span class="text text-orange fontweight">Draft</span>';
    } else {
        return '<span class="text text-danger fontweight" >In Queue</span>';
    }
}
function question_status_label($astrologer, $admin) {
    if(!empty($admin)) {
        return '<span class="label label-success fontweight">Answered</span>';
    } else if (!empty($astrologer)) {
        return '<span class="label label-warning fontweight">Draft</span>';
    } else {
        return '<span class="label label-danger fontweight" >In Queue</span>';
    }
}

if(!function_exists('image_path')) {
    function image_path($image) {
        $imagePath = Config('constant.image_path');
        return $imagePath.'/'.$image;
    }
}

if (!function_exists('countrySelector')){

    function countrySelector($defaultCountry = "", $id = "", $name = "", $classes = "" , $requires = false){

        $countryArray = array(
            'IN'=>array('name'=>'INDIA','code'=>'91'),
            'AD'=>array('name'=>'ANDORRA','code'=>'376'),
            'AE'=>array('name'=>'UNITED ARAB EMIRATES','code'=>'971'),
            'AF'=>array('name'=>'AFGHANISTAN','code'=>'93'),
            'AG'=>array('name'=>'ANTIGUA AND BARBUDA','code'=>'1268'),
            'AI'=>array('name'=>'ANGUILLA','code'=>'1264'),
            'AL'=>array('name'=>'ALBANIA','code'=>'355'),
            'AM'=>array('name'=>'ARMENIA','code'=>'374'),
            'AN'=>array('name'=>'NETHERLANDS ANTILLES','code'=>'599'),
            'AO'=>array('name'=>'ANGOLA','code'=>'244'),
            'AQ'=>array('name'=>'ANTARCTICA','code'=>'672'),
            'AR'=>array('name'=>'ARGENTINA','code'=>'54'),
            'AS'=>array('name'=>'AMERICAN SAMOA','code'=>'1684'),
            'AT'=>array('name'=>'AUSTRIA','code'=>'43'),
            'AU'=>array('name'=>'AUSTRALIA','code'=>'61'),
            'AW'=>array('name'=>'ARUBA','code'=>'297'),
            'AZ'=>array('name'=>'AZERBAIJAN','code'=>'994'),
            'BA'=>array('name'=>'BOSNIA AND HERZEGOVINA','code'=>'387'),
            'BB'=>array('name'=>'BARBADOS','code'=>'1246'),
            'BD'=>array('name'=>'BANGLADESH','code'=>'880'),
            'BE'=>array('name'=>'BELGIUM','code'=>'32'),
            'BF'=>array('name'=>'BURKINA FASO','code'=>'226'),
            'BG'=>array('name'=>'BULGARIA','code'=>'359'),
            'BH'=>array('name'=>'BAHRAIN','code'=>'973'),
            'BI'=>array('name'=>'BURUNDI','code'=>'257'),
            'BJ'=>array('name'=>'BENIN','code'=>'229'),
            'BL'=>array('name'=>'SAINT BARTHELEMY','code'=>'590'),
            'BM'=>array('name'=>'BERMUDA','code'=>'1441'),
            'BN'=>array('name'=>'BRUNEI DARUSSALAM','code'=>'673'),
            'BO'=>array('name'=>'BOLIVIA','code'=>'591'),
            'BR'=>array('name'=>'BRAZIL','code'=>'55'),
            'BS'=>array('name'=>'BAHAMAS','code'=>'1242'),
            'BT'=>array('name'=>'BHUTAN','code'=>'975'),
            'BW'=>array('name'=>'BOTSWANA','code'=>'267'),
            'BY'=>array('name'=>'BELARUS','code'=>'375'),
            'BZ'=>array('name'=>'BELIZE','code'=>'501'),
            'CA'=>array('name'=>'CANADA','code'=>'1'),
            'CC'=>array('name'=>'COCOS (KEELING) ISLANDS','code'=>'61'),
            'CD'=>array('name'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE','code'=>'243'),
            'CF'=>array('name'=>'CENTRAL AFRICAN REPUBLIC','code'=>'236'),
            'CG'=>array('name'=>'CONGO','code'=>'242'),
            'CH'=>array('name'=>'SWITZERLAND','code'=>'41'),
            'CI'=>array('name'=>'COTE D IVOIRE','code'=>'225'),
            'CK'=>array('name'=>'COOK ISLANDS','code'=>'682'),
            'CL'=>array('name'=>'CHILE','code'=>'56'),
            'CM'=>array('name'=>'CAMEROON','code'=>'237'),
            'CN'=>array('name'=>'CHINA','code'=>'86'),
            'CO'=>array('name'=>'COLOMBIA','code'=>'57'),
            'CR'=>array('name'=>'COSTA RICA','code'=>'506'),
            'CU'=>array('name'=>'CUBA','code'=>'53'),
            'CV'=>array('name'=>'CAPE VERDE','code'=>'238'),
            'CX'=>array('name'=>'CHRISTMAS ISLAND','code'=>'61'),
            'CY'=>array('name'=>'CYPRUS','code'=>'357'),
            'CZ'=>array('name'=>'CZECH REPUBLIC','code'=>'420'),
            'DE'=>array('name'=>'GERMANY','code'=>'49'),
            'DJ'=>array('name'=>'DJIBOUTI','code'=>'253'),
            'DK'=>array('name'=>'DENMARK','code'=>'45'),
            'DM'=>array('name'=>'DOMINICA','code'=>'1767'),
            'DO'=>array('name'=>'DOMINICAN REPUBLIC','code'=>'1809'),
            'DZ'=>array('name'=>'ALGERIA','code'=>'213'),
            'EC'=>array('name'=>'ECUADOR','code'=>'593'),
            'EE'=>array('name'=>'ESTONIA','code'=>'372'),
            'EG'=>array('name'=>'EGYPT','code'=>'20'),
            'ER'=>array('name'=>'ERITREA','code'=>'291'),
            'ES'=>array('name'=>'SPAIN','code'=>'34'),
            'ET'=>array('name'=>'ETHIOPIA','code'=>'251'),
            'FI'=>array('name'=>'FINLAND','code'=>'358'),
            'FJ'=>array('name'=>'FIJI','code'=>'679'),
            'FK'=>array('name'=>'FALKLAND ISLANDS (MALVINAS)','code'=>'500'),
            'FM'=>array('name'=>'MICRONESIA, FEDERATED STATES OF','code'=>'691'),
            'FO'=>array('name'=>'FAROE ISLANDS','code'=>'298'),
            'FR'=>array('name'=>'FRANCE','code'=>'33'),
            'GA'=>array('name'=>'GABON','code'=>'241'),
            'GB'=>array('name'=>'UNITED KINGDOM','code'=>'44'),
            'GD'=>array('name'=>'GRENADA','code'=>'1473'),
            'GE'=>array('name'=>'GEORGIA','code'=>'995'),
            'GH'=>array('name'=>'GHANA','code'=>'233'),
            'GI'=>array('name'=>'GIBRALTAR','code'=>'350'),
            'GL'=>array('name'=>'GREENLAND','code'=>'299'),
            'GM'=>array('name'=>'GAMBIA','code'=>'220'),
            'GN'=>array('name'=>'GUINEA','code'=>'224'),
            'GQ'=>array('name'=>'EQUATORIAL GUINEA','code'=>'240'),
            'GR'=>array('name'=>'GREECE','code'=>'30'),
            'GT'=>array('name'=>'GUATEMALA','code'=>'502'),
            'GU'=>array('name'=>'GUAM','code'=>'1671'),
            'GW'=>array('name'=>'GUINEA-BISSAU','code'=>'245'),
            'GY'=>array('name'=>'GUYANA','code'=>'592'),
            'HK'=>array('name'=>'HONG KONG','code'=>'852'),
            'HN'=>array('name'=>'HONDURAS','code'=>'504'),
            'HR'=>array('name'=>'CROATIA','code'=>'385'),
            'HT'=>array('name'=>'HAITI','code'=>'509'),
            'HU'=>array('name'=>'HUNGARY','code'=>'36'),
            'ID'=>array('name'=>'INDONESIA','code'=>'62'),
            'IE'=>array('name'=>'IRELAND','code'=>'353'),
            'IL'=>array('name'=>'ISRAEL','code'=>'972'),
            'IM'=>array('name'=>'ISLE OF MAN','code'=>'44'),
            'IQ'=>array('name'=>'IRAQ','code'=>'964'),
            'IR'=>array('name'=>'IRAN, ISLAMIC REPUBLIC OF','code'=>'98'),
            'IS'=>array('name'=>'ICELAND','code'=>'354'),
            'IT'=>array('name'=>'ITALY','code'=>'39'),
            'JM'=>array('name'=>'JAMAICA','code'=>'1876'),
            'JO'=>array('name'=>'JORDAN','code'=>'962'),
            'JP'=>array('name'=>'JAPAN','code'=>'81'),
            'KE'=>array('name'=>'KENYA','code'=>'254'),
            'KG'=>array('name'=>'KYRGYZSTAN','code'=>'996'),
            'KH'=>array('name'=>'CAMBODIA','code'=>'855'),
            'KI'=>array('name'=>'KIRIBATI','code'=>'686'),
            'KM'=>array('name'=>'COMOROS','code'=>'269'),
            'KN'=>array('name'=>'SAINT KITTS AND NEVIS','code'=>'1869'),
            'KP'=>array('name'=>'KOREA DEMOCRATIC PEOPLES REPUBLIC OF','code'=>'850'),
            'KR'=>array('name'=>'KOREA REPUBLIC OF','code'=>'82'),
            'KW'=>array('name'=>'KUWAIT','code'=>'965'),
            'KY'=>array('name'=>'CAYMAN ISLANDS','code'=>'1345'),
            'KZ'=>array('name'=>'KAZAKSTAN','code'=>'7'),
            'LA'=>array('name'=>'LAO PEOPLES DEMOCRATIC REPUBLIC','code'=>'856'),
            'LB'=>array('name'=>'LEBANON','code'=>'961'),
            'LC'=>array('name'=>'SAINT LUCIA','code'=>'1758'),
            'LI'=>array('name'=>'LIECHTENSTEIN','code'=>'423'),
            'LK'=>array('name'=>'SRI LANKA','code'=>'94'),
            'LR'=>array('name'=>'LIBERIA','code'=>'231'),
            'LS'=>array('name'=>'LESOTHO','code'=>'266'),
            'LT'=>array('name'=>'LITHUANIA','code'=>'370'),
            'LU'=>array('name'=>'LUXEMBOURG','code'=>'352'),
            'LV'=>array('name'=>'LATVIA','code'=>'371'),
            'LY'=>array('name'=>'LIBYAN ARAB JAMAHIRIYA','code'=>'218'),
            'MA'=>array('name'=>'MOROCCO','code'=>'212'),
            'MC'=>array('name'=>'MONACO','code'=>'377'),
            'MD'=>array('name'=>'MOLDOVA, REPUBLIC OF','code'=>'373'),
            'ME'=>array('name'=>'MONTENEGRO','code'=>'382'),
            'MF'=>array('name'=>'SAINT MARTIN','code'=>'1599'),
            'MG'=>array('name'=>'MADAGASCAR','code'=>'261'),
            'MH'=>array('name'=>'MARSHALL ISLANDS','code'=>'692'),
            'MK'=>array('name'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','code'=>'389'),
            'ML'=>array('name'=>'MALI','code'=>'223'),
            'MM'=>array('name'=>'MYANMAR','code'=>'95'),
            'MN'=>array('name'=>'MONGOLIA','code'=>'976'),
            'MO'=>array('name'=>'MACAU','code'=>'853'),
            'MP'=>array('name'=>'NORTHERN MARIANA ISLANDS','code'=>'1670'),
            'MR'=>array('name'=>'MAURITANIA','code'=>'222'),
            'MS'=>array('name'=>'MONTSERRAT','code'=>'1664'),
            'MT'=>array('name'=>'MALTA','code'=>'356'),
            'MU'=>array('name'=>'MAURITIUS','code'=>'230'),
            'MV'=>array('name'=>'MALDIVES','code'=>'960'),
            'MW'=>array('name'=>'MALAWI','code'=>'265'),
            'MX'=>array('name'=>'MEXICO','code'=>'52'),
            'MY'=>array('name'=>'MALAYSIA','code'=>'60'),
            'MZ'=>array('name'=>'MOZAMBIQUE','code'=>'258'),
            'NA'=>array('name'=>'NAMIBIA','code'=>'264'),
            'NC'=>array('name'=>'NEW CALEDONIA','code'=>'687'),
            'NE'=>array('name'=>'NIGER','code'=>'227'),
            'NG'=>array('name'=>'NIGERIA','code'=>'234'),
            'NI'=>array('name'=>'NICARAGUA','code'=>'505'),
            'NL'=>array('name'=>'NETHERLANDS','code'=>'31'),
            'NO'=>array('name'=>'NORWAY','code'=>'47'),
            'NP'=>array('name'=>'NEPAL','code'=>'977'),
            'NR'=>array('name'=>'NAURU','code'=>'674'),
            'NU'=>array('name'=>'NIUE','code'=>'683'),
            'NZ'=>array('name'=>'NEW ZEALAND','code'=>'64'),
            'OM'=>array('name'=>'OMAN','code'=>'968'),
            'PA'=>array('name'=>'PANAMA','code'=>'507'),
            'PE'=>array('name'=>'PERU','code'=>'51'),
            'PF'=>array('name'=>'FRENCH POLYNESIA','code'=>'689'),
            'PG'=>array('name'=>'PAPUA NEW GUINEA','code'=>'675'),
            'PH'=>array('name'=>'PHILIPPINES','code'=>'63'),
            'PK'=>array('name'=>'PAKISTAN','code'=>'92'),
            'PL'=>array('name'=>'POLAND','code'=>'48'),
            'PM'=>array('name'=>'SAINT PIERRE AND MIQUELON','code'=>'508'),
            'PN'=>array('name'=>'PITCAIRN','code'=>'870'),
            'PR'=>array('name'=>'PUERTO RICO','code'=>'1'),
            'PT'=>array('name'=>'PORTUGAL','code'=>'351'),
            'PW'=>array('name'=>'PALAU','code'=>'680'),
            'PY'=>array('name'=>'PARAGUAY','code'=>'595'),
            'QA'=>array('name'=>'QATAR','code'=>'974'),
            'RO'=>array('name'=>'ROMANIA','code'=>'40'),
            'RS'=>array('name'=>'SERBIA','code'=>'381'),
            'RU'=>array('name'=>'RUSSIAN FEDERATION','code'=>'7'),
            'RW'=>array('name'=>'RWANDA','code'=>'250'),
            'SA'=>array('name'=>'SAUDI ARABIA','code'=>'966'),
            'SB'=>array('name'=>'SOLOMON ISLANDS','code'=>'677'),
            'SC'=>array('name'=>'SEYCHELLES','code'=>'248'),
            'SD'=>array('name'=>'SUDAN','code'=>'249'),
            'SE'=>array('name'=>'SWEDEN','code'=>'46'),
            'SG'=>array('name'=>'SINGAPORE','code'=>'65'),
            'SH'=>array('name'=>'SAINT HELENA','code'=>'290'),
            'SI'=>array('name'=>'SLOVENIA','code'=>'386'),
            'SK'=>array('name'=>'SLOVAKIA','code'=>'421'),
            'SL'=>array('name'=>'SIERRA LEONE','code'=>'232'),
            'SM'=>array('name'=>'SAN MARINO','code'=>'378'),
            'SN'=>array('name'=>'SENEGAL','code'=>'221'),
            'SO'=>array('name'=>'SOMALIA','code'=>'252'),
            'SR'=>array('name'=>'SURINAME','code'=>'597'),
            'ST'=>array('name'=>'SAO TOME AND PRINCIPE','code'=>'239'),
            'SV'=>array('name'=>'EL SALVADOR','code'=>'503'),
            'SY'=>array('name'=>'SYRIAN ARAB REPUBLIC','code'=>'963'),
            'SZ'=>array('name'=>'SWAZILAND','code'=>'268'),
            'TC'=>array('name'=>'TURKS AND CAICOS ISLANDS','code'=>'1649'),
            'TD'=>array('name'=>'CHAD','code'=>'235'),
            'TG'=>array('name'=>'TOGO','code'=>'228'),
            'TH'=>array('name'=>'THAILAND','code'=>'66'),
            'TJ'=>array('name'=>'TAJIKISTAN','code'=>'992'),
            'TK'=>array('name'=>'TOKELAU','code'=>'690'),
            'TL'=>array('name'=>'TIMOR-LESTE','code'=>'670'),
            'TM'=>array('name'=>'TURKMENISTAN','code'=>'993'),
            'TN'=>array('name'=>'TUNISIA','code'=>'216'),
            'TO'=>array('name'=>'TONGA','code'=>'676'),
            'TR'=>array('name'=>'TURKEY','code'=>'90'),
            'TT'=>array('name'=>'TRINIDAD AND TOBAGO','code'=>'1868'),
            'TV'=>array('name'=>'TUVALU','code'=>'688'),
            'TW'=>array('name'=>'TAIWAN, PROVINCE OF CHINA','code'=>'886'),
            'TZ'=>array('name'=>'TANZANIA, UNITED REPUBLIC OF','code'=>'255'),
            'UA'=>array('name'=>'UKRAINE','code'=>'380'),
            'UG'=>array('name'=>'UGANDA','code'=>'256'),
            'US'=>array('name'=>'UNITED STATES','code'=>'1'),
            'UY'=>array('name'=>'URUGUAY','code'=>'598'),
            'UZ'=>array('name'=>'UZBEKISTAN','code'=>'998'),
            'VA'=>array('name'=>'HOLY SEE (VATICAN CITY STATE)','code'=>'39'),
            'VC'=>array('name'=>'SAINT VINCENT AND THE GRENADINES','code'=>'1784'),
            'VE'=>array('name'=>'VENEZUELA','code'=>'58'),
            'VG'=>array('name'=>'VIRGIN ISLANDS, BRITISH','code'=>'1284'),
            'VI'=>array('name'=>'VIRGIN ISLANDS, U.S.','code'=>'1340'),
            'VN'=>array('name'=>'VIET NAM','code'=>'84'),
            'VU'=>array('name'=>'VANUATU','code'=>'678'),
            'WF'=>array('name'=>'WALLIS AND FUTUNA','code'=>'681'),
            'WS'=>array('name'=>'SAMOA','code'=>'685'),
            'XK'=>array('name'=>'KOSOVO','code'=>'381'),
            'YE'=>array('name'=>'YEMEN','code'=>'967'),
            'YT'=>array('name'=>'MAYOTTE','code'=>'262'),
            'ZA'=>array('name'=>'SOUTH AFRICA','code'=>'27'),
            'ZM'=>array('name'=>'ZAMBIA','code'=>'260'),
            'ZW'=>array('name'=>'ZIMBABWE','code'=>'263')
        );
        $output = "<select id='".$id."' name='".$name."' class='form-control'>";
        if($requires) {
            $output = "<select id='".$id."' name='".$name."' class='form-control' required='required'>";
        }

        $output .= "<option value=''>Select Country</option>";
        foreach($countryArray as $code => $country) {
            $countryName = ucwords(strtolower($country["name"])); // Making it look good
            $output .= "<option value='".$countryName."' ".(($countryName==$defaultCountry)?"selected":"").">".$countryName."</option>";
        }
        $output .= "</select>";
        return $output; // or echo $output; to print directly
    }
}



