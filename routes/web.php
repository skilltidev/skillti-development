<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('register/verify/{tocken}', 'AuthController@verify');

Route::group(['middleware' => ['menu']], function () {
    Route::get('/', 'HomeController@home');
    Route::get('business', 'HomeController@commingShoon');
    Route::get('projects', 'HomeController@commingShoon');
    Route::get('jobs', 'HomeController@commingShoon');
    Route::get('about-us', 'HomeController@aboutUs');


    Route::get('/all-categories', 'HomeController@allCategories');
    Route::get('/category/{categoryname}', 'HomeController@categories');
    Route::get('/profile-detail/{userid}', 'HomeController@profileDetails');
    Route::get('/signup', 'AuthController@signUp')->name('users.register');
    Route::get('/signin', 'AuthController@signIn')->name('users.login');;
	Route::get('search', 'HomeController@search');
	Route::get('forgotPassword', 'HomeController@forgotPassword');
	Route::get('register/password-reset/{token}/{email}', 'AuthController@resetPassword');
	Route::get('contact-us', 'HomeController@contactUs');
	Route::post('contact-us', 'HomeController@contactUsPost');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {
    Route::get('/', 'AdminController@dashboard')->name('admin.dashboard');
    Route::get('dashboard', 'AdminController@dashboard')->name('admin.dashboard');
    Route::get('all-categories', 'AdminController@categories')->name('admin.categories');
    Route::get('add-edit-category/{catId?}', 'AdminController@addCategories')->name('admin.categories');
    Route::post('add-edit-category', 'AdminController@postCategoryInfo');
    Route::get('delete-category', 'AdminController@deleteCategories');
    Route::get('all-subCategories/{catId?}', 'AdminController@subCategories')->name('admin.subCategories');
    Route::get('add-edit-subCategory/{catId?}', 'AdminController@addSubCategories')->name('admin.subCategories');
    Route::post('add-edit-subCategory', 'AdminController@postSubCategoryInfo');
    Route::get('delete-subCategory', 'AdminController@deleteSubCategories');

    Route::get('all-mentors', 'AdminController@mentorList')->name('admin.mentorList');
    Route::get('all-mentor-course/{mentorId}', 'AdminController@allMentorCourse');
    Route::get('all-mentee-course/{menteeId}', 'AdminController@allMenteeCourse');
    Route::get('all-mentee', 'AdminController@menteeList')->name('admin.menteeList');
    Route::post('mentor-profile-edit', 'AdminController@mentorProfileEditSave')->name('admin.mentorList');
    Route::get('add-edit-mentee/{menteeId}', 'AdminController@addMentee')->name('admin.menteeList');
	Route::get('add-edit-mentor/{mentorId}', 'AdminController@addMentor')->name('admin.mentorList');
    Route::post('mentee-profile-edit', 'AdminController@menteeProfileEditSave')->name('admin.menteeList');
    Route::get('delete-mentor', 'AdminController@deleteMentor');
    Route::get('delete-mentee', 'AdminController@deleteMentee');
    Route::get('all-skill/{id}', 'AdminController@addSkill')->name('admin.allSyllabus');
    Route::post('mentor-profile-edit', 'AdminController@mentorProfileEditSave');
    Route::get('all-syllabus/{id?}', 'AdminController@allSyllabus')->name('admin.allSyllabus');
    Route::get('purchase-courses', 'AdminController@purchaseCourses')->name('admin.allSyllabusP');
    Route::get('changes-status/{courseId}', 'AdminController@changesStatus');
	Route::get('changes-status-skill/{courseId}', 'AdminController@changesStatusCourse');
	Route::get('changes-status-rate/{id}', 'AdminController@changesStatusRate');


    Route::get('review', 'AdminController@rattingManage')->name('admin.ratting');
	Route::get('contact-us', 'AdminController@contactUs')->name('admin.contactUs');


});

Route::group(['prefix' => 'dashboard', 'middleware' => ['dashboard']], function () {
    Route::get('/', 'MentorController@dashboard')->name('dashboard');
    Route::get('Syllabus', 'MentorController@syllabus')->name('mentor.syllabus');
    Route::get('add-syllabus/{skillId?}', 'MentorController@addSyllabus')->name('mentor.addSyllabus');
    Route::get('get-category', 'MentorController@getCategory');
    Route::post('addEditSkill/{skillId?}', 'MentorController@addEditSkill');
    Route::get('delete-syllabus/{skillId}', 'MentorController@deleteSyllabus');
    Route::get('mentor-profile-edit', 'MentorController@mentorProfileEdit')->name('mentor-profile-edit');
    Route::get('mentor-profile', 'MentorController@mentorProfile')->name('mentor-profile');
    Route::post('mentor-profile-edit', 'MentorController@mentorProfileEditSave');
    Route::get('password-setting', 'MentorController@passwordSetting')->name('password-setting');
    Route::post('password-setting', 'MentorController@passwordSettingPost')->name('password-setting');
    Route::get('chat-bot/{courseId?}', 'MentorController@chatBot')->name('mentor.chatbot');
	Route::post('chat-bot/send-message', 'MentorController@sendMessage');
	Route::post('saveToken', 'MentorController@saveToken');
	Route::get('mail-box', 'MentorController@mailbox')->name('mentor.mailbox');
	Route::post('mail-box', 'MentorController@mailboxReplay')->name('mentor.mailbox');

});

Route::group(['prefix' => 'student-dashboard', 'middleware' => ['studentDashboard']], function () {
    Route::get('/', 'MenteeController@dashboard')->name('dashboard');
    Route::get('mentee-profile', 'MenteeController@menteeProfile');
    Route::get('my-course', 'MenteeController@myCourse')->name('mentee.mycourse');
	Route::get('messages', 'MenteeController@message')->name('mentor.chatbot');
	Route::get('mail-box', 'MenteeController@mailbox')->name('mentee.mailbox');
	Route::get('my-course/start-chat/{courseId}', 'MenteeController@message')->name('mentor.chatbot');
    Route::get('mentee-profile-edit', 'MenteeController@menteeProfileEdit')->name('mentee-profile-edit');
    Route::post('mentee-profile-edit', 'MenteeController@menteeProfileEditSave');
    Route::get('password-setting', 'MenteeController@passwordSetting')->name('password-setting');
    Route::post('password-setting', 'MenteeController@passwordSettingPost')->name('password-setting');
    Route::post('book-course-payment', 'MenteeController@addCourse');
	Route::post('confirm-payment', 'MenteeController@confirmPayment');
	Route::post('chat-bot/send-message', 'MenteeController@sendMessage');
	Route::post('saveToken', 'MenteeController@saveToken');
	Route::post('course-ratting', 'MenteeController@courseRatting');
});


Route::get('/logout', 'AuthController@logout');
Route::post('/signup', 'AuthController@postRegister');
Route::post('/signin', 'AuthController@postLogin');
Route::get('apisuggestion', 'HomeController@apiSuggestion');
Route::post('forgotPassword', 'AuthController@forgotPasswordPost');
Route::post('resetPassword', 'AuthController@resetPasswordPost');
Route::post('send-message', 'HomeController@sendMessage');








//Auth::routes();
