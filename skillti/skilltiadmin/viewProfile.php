<?php
	include('adminheader.php'); 
	protect_admin_page();
	$UserID = $_GET['id'];
	
	$Query = "select * from users where UserID='$UserID'";
	$Result =  $dbObject->query($Query);
	$Row = mysqli_fetch_assoc($Result);
	
	if($Row['RoleID']==1) { 
		$Role = 'Admin';
	} else if($Row['RoleID']==2) {
		$Role = 'Mentor';
	} else if($Row['RoleID']==2) {
		$Role = 'Mentee';
	}
	
?>
   <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper"><!--User Profile Starts-->
<!--Basic User Details Starts-->
<section id="user-profile">
    <div class="row">
        <div class="col-12">
            <div class="card profile-with-cover">
                <div class="card-img-top img-fluid bg-cover height-300" style="background: url('app-assets/img/banner11.jpg') 50%;"></div>
                <div class="media profil-cover-details row">
                    <div class="col-5">
                        <div class="align-self-start halfway-fab pl-3 pt-2">
                            <div class="text-left">
                                <h3 class="card-title white"><?php echo $Row['FirstName'].' '.$Row['LastName'];?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="align-self-center halfway-fab text-center">
                            <a class="profile-image">
                                <img src="../assets/profile-photo/<?php echo $Row['profilePic'];?>" class="rounded-circle img-border gradient-summer width-100" style="width:120px !important;height:120px" alt="Card image">
                            </a>
                        </div>
                    </div>
                    <div class="col-5">                        
                    </div>
                    <div class="profile-cover-buttons">
                        <div class="media-body halfway-fab align-self-end">
                            <div class="text-right d-none d-sm-none d-md-none d-lg-block">
                                <button type="button" class="btn btn-success btn-raised mr-3"><i class="fa fa-dashcube"></i> Message</button>
                            </div>
                          </div>
                    </div>
                </div>
                <div class="profile-section">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 ">
                            <ul class="profile-menu no-list-style">
                                <li>
                                    <a class="primary font-medium-2 font-weight-600"><?php echo $Row['EmailID'];?></a>
                                </li>
                                <li>
                                    <a class="primary font-medium-2 font-weight-600">ACTIVE</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-2 col-md-2 text-center">
                            <span class="font-medium-2 text-uppercase"><?php echo $Row['FirstName'].' '.$Row['LastName'];?></span>
                            <p class="grey font-small-2"><?php echo $Role;?></p>
                        </div>
                        <div class="col-lg-5 col-md-5">
                            <ul class="profile-menu no-list-style">
                                <li>
                                    <a class="primary font-medium-2 font-weight-600"><?php echo $Row['Country'];?></a>
                                </li>
                                <li>
                                    <a class="primary font-medium-2 font-weight-600"><?php echo $Row['Phone'];?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Basic User Details Ends-->

<!--About section starts-->
<section id="about">
    <div class="row">
        <div class="col-12">
            <div class="content-header">About</div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Personal Information</h5>
                </div>
                <div class="card-body">
                    <div class="card-block">
					<?php if($Row['RoleID']==2) { ?>
                        <div class="mb-3">
                            <span class="text-bold-500 primary">About Him:</span>
                            <span class="display-block overflow-hidden"><?php echo $Row['UserAbout'];?>
                            </span>
                        </div>
                        <hr>
					<?php } ?>
                        <div class="row">
						<?php if($Row['RoleID']==2) { ?>
                            <div class="col-12 col-md-6 col-lg-4">
                                <ul class="no-list-style">
                                    <li class="mb-2">
										<span class="text-bold-500 primary"><a><i class="icon-present font-small-3"></i> Qualification:</a></span>
										<span class="display-block overflow-hidden"><?php echo $Row['Qualification'];?></span>
									</li>
						
                                    <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-map-pin font-small-3"></i> Expertise:</a></span>
                                        <span class="display-block overflow-hidden"><?php echo $Row['Expertise'];?></span>
                                    </li>
                                    <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-globe font-small-3"></i> Verification Document:</a></span>
                                        <span class="display-block overflow-hidden"><a style="color:rgba(0, 0, 0, .87);" href="../assets/profile-doc/<?php echo $Row['VarificationDoc'];?>" ><?php echo $Row['VarificationDoc'];?></a></span>
                                    </li>
									<li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-mail font-small-3"></i> State:</a></span>
                                        <a class="display-block overflow-hidden"><?php echo $Row['State'];?></a>
                                    </li>
                                    
                                  
									
                                </ul>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <ul class="no-list-style">
                                    
                                    <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-user font-small-3"></i> Working With:</a></span>
                                        <span class="display-block overflow-hidden"><?php echo $Row['WorkingWith'];?></span>
                                    </li>
									<li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-mail font-small-3"></i> Experience:</a></span>
                                        <a class="display-block overflow-hidden"><?php echo $Row['Experience'];?></a>
                                    </li>
									 <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-globe font-small-3"></i> Address:</a></span>
                                        <span class="display-block overflow-hidden"><?php echo $Row['Address1'];?></span>
                                    </li>
									<li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-monitor font-small-3"></i> Zip:</a></span>
                                        <a class="display-block overflow-hidden"><?php echo $Row['Zip'];?></a>
                                    </li>
                                    
                                    
                                </ul>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <ul class="no-list-style">
                                  <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-monitor font-small-3"></i> Certified:</a></span>
                                        <a class="display-block overflow-hidden"><?php echo $Row['Certified'];?></a>
                                    </li>
									<li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-smartphone font-small-3"></i> Name of Certificate:</a></span>
                                        <span class="display-block overflow-hidden"><?php echo $Row['CertificateName'];?></span>
                                    </li>
									<li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-user font-small-3"></i> City:</a></span>
                                        <span class="display-block overflow-hidden"><?php echo $Row['City'];?></span>
                                    </li>
                                </ul>
                            </div>
							<hr>
						<?php } else if($Row['RoleID']==3) { ?>
							<div class="col-12 col-md-6 col-lg-4">
                                <ul class="no-list-style">
                                    
                                    <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-map-pin font-small-3"></i> Qualification:</a></span>
                                        <span class="display-block overflow-hidden"><?php echo $Row['Qualification'];?></span>
                                    </li>
                                    <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-map-pin font-small-3"></i> Interest To Learn:</a></span>
                                        <span class="display-block overflow-hidden"><?php echo $Row['InterestToLearn'];?></span>
                                    </li>
                                   
									
                                </ul>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <ul class="no-list-style">
                                     <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-globe font-small-3"></i> Address:</a></span>
                                        <span class="display-block overflow-hidden"><?php echo $Row['Address1'];?></span>
                                    </li>
                                    <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-user font-small-3"></i> City:</a></span>
                                        <span class="display-block overflow-hidden"><?php echo $Row['City'];?></span>
                                    </li>
									
                                </ul>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <ul class="no-list-style">
									<li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-mail font-small-3"></i> State:</a></span>
                                        <a class="display-block overflow-hidden"><?php echo $Row['State'];?></a>
                                    </li>
                                    
                                  <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-monitor font-small-3"></i> Zip:</a></span>
                                        <a class="display-block overflow-hidden"><?php echo $Row['Zip'];?></a>
                                    </li>
									</ul>
                            </div>
                        <?php } ?>
						
						</div>
                        <hr>
                        <div class="mt-3">
                            <span class="text-bold-500 primary">Hobbies:</span>
                            <span class="display-block overflow-hidden">I like to ride the cycle to work, swimming, listning music and 
                                working out. I also like reading magazines, go to museums, watching good movies and eat spicy food while 
                                it’s raining outside.Twin siblings Dipper and Mabel Pines spend the summer at their uncle's tourist trap 
                                in the enigmatic town of Gravity Falls.A mysterious Hollywood stuntman and mechanic moonlights as a 
                                getaway driver.Scuba Diving, Skiing, Surfing, Photography, Long drive.
                            </span>
                        </div>
                        <div class="mt-2 overflow-hidden">
                            <span class="mr-3 mt-2 text-center float-left width-100"> <i class="icon-plane danger font-large-2"></i> <div class="mt-2">Travelling</div></span>
                            <span class="mr-3 mt-2 text-center float-left width-100"> <i class="icon-speedometer danger font-large-2"></i> <div class="mt-2">Driving</div></span>
                            <span class="mr-3 mt-2 text-center float-left width-100"> <i class="icon-camera danger font-large-2"></i> <div class="mt-2">Photography</div></span>
                            <span class="mr-3 mt-2 text-center float-left width-100"> <i class="icon-game-controller danger font-large-2"></i> <div class="mt-2">Gaming</div></span>
                            <span class="mr-3 mt-2 text-center float-left width-100"> <i class="icon-music-tone-alt danger font-large-2"></i> <div class="mt-2">Music</div></span>
                            <span class="mr-3 mt-2 text-center float-left width-100"> <i class="ft-monitor danger font-large-2"></i> <div class="mt-2">Surfing</div></span>
                            <span class="mr-3 mt-2 text-center float-left width-100"> <i class="ft-pie-chart danger font-large-2"></i> <div class="mt-2">Foodie</div></span>
                            <span class="mr-3 mt-2 text-center float-left width-100"> <i class="ft-tv danger font-large-2"></i> <div class="mt-2">TV</div></span>
                            <span class="mr-3 mt-2 text-center float-left width-100"> <i class="icon-basket-loaded danger font-large-2"></i> <div class="mt-2">Shopping</div></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Educational Information</h5>
                </div>
                <div class="card-body">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                <ul class="no-list-style">
                                    <li class="mb-2">
                                        <span class="primary text-bold-500"><a><i class="ft-home font-small-3"></i> Broklin Institute</a></span>
                                        <span class="grey line-height-0 display-block mb-2 font-small-2">2008 - 2009</span>
                                        <span class="line-height-2 display-block overflow-hidden">Professor: Leonardo Stagg. Six months of best Developing tools course.</span>
                                    </li>
                                    <li class="mb-2">
                                        <span class="primary text-bold-500"><a><i class="ft-home font-small-3"></i> The Ninja College </a></span>
                                        <span class="grey line-height-0 display-block mb-2 font-small-2">2012 - 2013</span>
                                        <span class="line-height-2 display-block overflow-hidden">Professor: Steve Ustreil. Gave me the best educational information for Ninja.</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                <ul class="no-list-style">
                                    <li class="mb-2">
                                        <span class="primary text-bold-500"><a><i class="ft-home font-small-3"></i> Ninja Developer</a></span>
                                        <span class="grey line-height-0 display-block mb-2 font-small-2">2009-2011</span>
                                        <span class="line-height-2 display-block overflow-hidden">Ninja Developer for the “PIXINVENT” creative studio. </span>
                                    </li>
                                    <li class="mb-2">
                                        <span class="primary text-bold-500"><a><i class="ft-home font-small-3"></i> Senior Ninja Developer</a></span>
                                        <span class="grey line-height-0 display-block mb-2 font-small-2">2014-Now</span>
                                        <span class="line-height-2 display-block overflow-hidden">Senior Ninja Developer for the “PIXINVENT” creative studio.</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--About section ends-->

	</div>
        </div>
		
<?php include('adminfooter.php'); ?>
<style>

</style>