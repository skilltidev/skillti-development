@extends('header.dashboard_header')
@section('headtitle', "Dashboard")
@section('headdesc', "")
@section('maincontent')
    <div class="mdk-drawer-layout__content page">

        <div class="container-fluid page__container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">All Syllabus</li>
            </ol>

            <div class="d-flex flex-column flex-sm-row flex-wrap mb-headings align-items-start align-items-sm-center">
                <div class="flex mb-2 mb-sm-0">
                    <h1 class="h2">Manage Courses</h1>
                </div>
                <a href="{{url('dashboard/add-syllabus')}}" class="btn btn-success btn-sm">Add course</a>
            </div>

            @if(!$skills)
            <div class="alert alert-light alert-dismissible border-1 border-left-3 border-left-warning" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="text-black-70">Ohh no! No courses to display. Add some courses.</div>
            </div>
            @else
            <div class="row">
                @foreach($skills as $skill)
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-column flex-sm-row">
                                <a class="avatar avatar-lg avatar-4by3 mb-3 w-xs-plus-down-100 mr-sm-3">
                                    <img src="{{asset('admin-assets/cat-image/'.$skill->logo)}}" alt="{{$skill->catname}}" class="avatar-img rounded">
                                </a>
                                <div class="flex" style="min-width: 200px;">
                                    <h5 class="card-title text-base m-0"><strong>{{$skill->catName}}</strong></h5>
                                    <h4 class="card-title mb-1">{{$skill->subCatName}}</h4>
                                    <p class="text-black-70">{!! substr(strip_tags($skill->description), 0, 60 ); !!}{{(strlen(strip_tags($skill->description)) > 60)?"...":""}}</p>
                                    <div class="d-flex align-items-end">
                                        <div class="d-flex flex flex-column mr-3">
                                            <div class="d-flex align-items-center py-1 border-bottom">
                                                <small class="text-black-70 mr-2">Course Fee: &nbsp;{!! get_price_html($skill->coursefee) !!}</small>
                                            </div>
                                            <div class="d-flex align-items-center py-1">
                                                @foreach(json_decode($skill->skills) as $k => $skil)
                                                    @if($k < 4)
                                                        <small class="text-muted mr-2">{{$skil}}</small>
                                                    @endif
                                                        @if($k == 4)
                                                            <small class="text-muted mr-2">...</small>
                                                        @endif
                                                @endforeach
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card__options dropdown right-0 pr-2">
                            <a href="#" class="dropdown-toggle text-muted" data-caret="false" data-toggle="dropdown">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="{{url('dashboard/add-syllabus/'.custom_encode($skill->id))}}">Edit course</a>
                                <div class="dropdown-divider"></div>
                                <a onclick="javascript: return confirm('Are you sure! to delete this course?');" class="dropdown-item text-danger" href="{{url('dashboard/delete-syllabus/'.custom_encode($skill->id))}}">Delete course</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
        </div>

    </div>
@endsection
@section('menu')
    @include('dashboard.menu')
@endsection
@section('scripting')
@endsection
@section('footer')
    @include('footer.footer_dashboard')
@endsection
