@extends('header.student_header')
@section('headtitle', "Dashboard")
@section('headdesc', "")
@section('maincontent')
    <div class="mdk-drawer-layout__content page">
        <div class="container-fluid page__container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">Mailbox</li>
            </ol>
            <div class="media mb-headings align-items-center">
                <div class="media-body">
                    <h1 class="h2">Mailbox</h1>
                </div>
            </div>
            @if(!empty($messages) && count($messages) > 0)
                @foreach($messages as $mail)
                    <div class="media">
                        <div class="media-left text-center">
                            <img src="{{profile_image(asset('profile-photo/'.Auth::user()->profilePic))}}" alt="" class="rounded-circle" width="40">
                        </div>
                        <div class="media-body">
                            <div class="card">
                                <div class="card-body">
                                    <p><a href="javascript:void(0)">{{Auth::user()->firstName." ".Auth::user()->lastName}} </a> <small class="text-muted">{!! posted_ago($mail->created_at) !!}</small></p>
                                    <p>{{$mail->message}}</p>
                                </div>
                            </div>
                            @if(!empty($mail->replay) && count($mail->replay) > 0)
                            @foreach($mail->replay as $replay)
                            <div class="media">
                                <div class="media-left text-center">
                                    <img src="{{profile_image(asset('profile-photo/'.$replay->profilePic))}}" alt="" class="rounded-circle" width="40">
                                </div>
                                <div class="media-body">
                                    <div class="card">
                                        <div class="card-body">
                                            <p><a href="instructor-profile.html">{{$replay->mentorName}}</a> <small class="text-muted">{!! posted_ago($replay->created_at) !!}</small></p>
                                            <p>{{$replay->message}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                @endforeach
            @else
            <div class="alert alert-light alert-dismissible border-1 border-left-3 border-left-warning" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="text-black-70">Ohh no! No Message to display.</div>
            </div>
            @endif
        </div>
    </div>
@endsection
@section('menu')
    @include('student.menu')
@endsection
@section('scripting')
@endsection
@section('footer')
    @include('footer.footer_dashboard')
@endsection