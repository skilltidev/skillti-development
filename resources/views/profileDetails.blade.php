@extends('header.home_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")
@section('maincontent')
    <style>
        .rating-header {
            margin-top: -10px;
            margin-bottom: 10px;
        }

        .heading.text-center {
            margin-bottom: 30px;
        }

        .star {
            width: 50%;
            float: left;
        }

        .numbr {
            width: 10%;
            float: left;
            padding-top: 13px;
        }

        .form-group {
            margin-bottom: 30px;
        }

        #review {
            margin-top: 15px;
        }

        .post-btn {
            font-size: 17px;
            font-weight: 600;
            text-align: left;
            margin-top: 5px;
        }

        .user_p {
            width: 45px !important;
            height: 45px !important;
            border: none !important;
        }

        ul.rating_user h3 {
            line-height: 26px;
        }
    </style>
    <section class="mid_content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="profile_list profile_details">
                        <ul>
                            <li>
                                <img src="{{profile_image(asset('profile-photo/'.$mentor->profilePic))}}" alt="">
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-star"></i> {{$mentor->avgRatting}} Instructor Rating</li>
                                    <li><i class="fa fa-comment"></i> {{$mentor->ratting}} Reviews</li>
                                    <li><i class="fa fa-user"></i> {{$mentor->students}} Students</li>
                                    <li><i class="fa fa-video-camera"></i> {{$mentor->course}} Courses</li>
                                    <li><i class="fa fa-globe"></i> {{$mentor->state.", ".$mentor->country}}</li>
                                    <li>&nbsp;</li>
                                    <li>
                                        @if((Auth::check() && Auth::user()->roleId == 3))
                                        <a class="text-center see_all send-message" href="#">Send Message</a>
                                        @else
                                        <li><a class="text-center see_all" href="{{url('signin?redirect=profile-detail/'.custom_encode($mentor->id).'#msgForm')}}">Send Message</a></li>
                                        @endif
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <div class="profile_list_bx">
                                    <h4><a>{{$mentor->firstName." ".$mentor->lastName}}</a></h4>
                                    <h4>{{$mentor->tagline}}</h4>
                                    <div class="bio_disc">
                                        {{$mentor->about}}
                                    </div>
                                    <div class="grey_listing">
                                        {!! $mentor->description !!}
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            @if(!empty($courses) && count($courses) > 0)
            <div class="row">
                <h2>
                    <center>Courses</center>
                </h2>
                <div class="col-xs-12 col-sm-12">
                    <div class="profile_list">
                        @foreach($courses as $course)
                            {{--{{debug($mentor)}}--}}
                            <ul id="book-course-{{custom_encode($course->id)}}">
                                <li><img src="{{profile_image(asset('admin-assets/cat-image/'.$course->logo))}}" alt="{{$course->catname}}"></li>
                                <li>
                                    <div class="profile_list_bx">
                                        @if($course->subCatName != '')
                                            <h4>{{$course->subCatName}}</h4>
                                        @endif
                                        @if(strip_tags($course->shortBio) != '')
                                            <h4>{{$course->shortBio}}</h4>
                                        @endif
                                        <div class="bio_disc">{!! $course->description !!}</div>
                                        <div class="pills">
                                            <ul class="list-inline">
                                                <li class="head_main">Skills</li>
                                                @foreach(json_decode($course->skills) as $k => $cat)
                                                    <li>{{$cat}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="test">
                                            <ul class="list-inline">
                                                <li class="head_main">Course Fee :</li>
                                                <li>{!! get_price_html($course->coursefee) !!}</li>
                                                @if (Auth::check() && Auth::user()->roleId == 3)
                                                    <li><a class="book-course" data-id="{{custom_encode($course->id)}}" href="{{url('profile-detail/'.custom_encode($mentor->id).'#book-course-'.custom_encode($course->id))}}">Learn this course</a></li>
                                                @else
                                                    <li><a href="{{url('signin?redirect=profile-detail/'.custom_encode($mentor->id).'#book-course-'.custom_encode($course->id))}}">Learn this course</a></li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
            @if(!empty($rattings) && count($rattings) > 0)
                <div class="row">
                    <h2>
                        <center>Ratings & Review</center>
                    </h2>
                    <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                        <div class="message"></div>
                        @foreach($rattings as $ratting)
                            <div class="rating_section">
                                <div class="row">
                                    <div class="col-sm-11 col-xs-11">
                                        <ul class="rating_user">
                                            <li>
                                                <div><img class="user_p" src="{{profile_image(asset('profile-photo/'.$ratting->profilePic))}}"></div>
                                            </li>
                                            <li>
                                                <h4><b>{{$ratting->name}}</b> <font style="font-style: italic;color: #aaa;padding-right: 10px;float:right"><i class="fa fa-clock-o" aria-hidden="true"></i> Posted: {{date('M d, Y', strtotime($ratting->created_at))}}</font></h4>
                                                <h3>{{$ratting->comment}}</h3>
                                                <br>
												<?php $rate = $ratting->rating; ?>
                                                <div class="rating_details">
                                                    <div class="rating_star_s">
                                                        @for($i=0; $i < 5; $i++)
                                                            @if($rate > $i)
                                                                @if(is_float($rate) && ($i+1) > $rate)
                                                                    <i class="fa fa-star-half-o"></i>
                                                                @else
                                                                    <i class="fa fa-star"></i>
                                                                @endif
                                                            @else
                                                                <i class="fa fa-star-o"></i>
                                                            @endif
                                                        @endfor
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            @endif
            @if (Auth::check() && Auth::user()->roleId == 3)
            <div class="user_signup jsmsg" style="background:#fff;border: transparent;" id="msgForm">
                <div class="custom_form">
                    <h3 class="text-center" style="padding:0 0 8px 0">Message to {{$mentor->firstName." ".$mentor->lastName}}</h3>
                    @if(Session::get('message'))
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Success!</strong> {{ Session::get('message') }}{{ Session::forget('message') }}
                        </div>
                    @endif
                    @if(Session::get('errorMessage'))
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Message !</strong> {{ Session::get('errorMessage') }}{{ Session::forget('errorMessage') }}
                        </div>
                    @endif
                    <form method="post" action="{{url('send-message')}}">
                        @csrf
                        <input type="hidden" name="mentorId" value="{{custom_encode($mentor->id)}}">
                        <div class="form-group">
                            <textarea maxlength="500" rows="5" class="form-control" name="msgtext" placeholder="Type Message" required="" value="{{old('msgtext')}}"></textarea>
                            @if ($errors->has('msgtext'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('msgtext') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" name="msgSubmit" class="blue_btn custom_btn">Send</button>
                        </div>
                    </form>
                </div>
            </div>
            @endif
        </div>
    </section>
@endsection
@section('scripting')
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script>
        $(document).ready(function () {
            $('.send-message').click(function(e){
                e.preventDefault();
                $('html, body').animate({scrollTop: $("#msgForm").offset().top-140}, 2000);
            });
        });
        $('.book-course').click(function (e) {
            e.preventDefault();
            var self = $(this);
            self.attr('disabled', 'disabled');
            $.ajax({
                url: "{{url('student-dashboard/book-course-payment')}}",
                type: "POST",
                dataType: 'JSON',
                data: {courseId: self.data('id'), "_token": "{{ csrf_token() }}"},
                success: function (response) {
                    if (response.error == false) {
                        razer_pay(response.message);
                    } else {
                        var message = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + response.message + '</div>';
                        $('.message').html(message);
                        $('html, body').animate({scrollTop: $(".message").offset().top - 100}, 1000);
                        self.removeAttr('disabled');
                    }
                }
            });
        });

        function razer_pay(data) {
            var options = {
                "key": "{{ env('RAZZOR_PAY_KEY', 'rzp_test_TyMscKTEEZKLY6') }}",
                "amount": parseInt(data.paymentAmount) * 100,
                "name": data.packageName,
                "description": "No of tickets (" + data.packageCount + ")",
                "image": "{{asset('images/skillti_logo_4_update-fav_icon.png')}}",
                "handler": function (payment_response) {
                    complete_payment(payment_response);
                },
                "prefill": {
                    "name": data.name,
                    "email": data.email
                },
                "notes": {
                    "courseId": data.courseId,
                    "userId": data.userId
                },
                "theme": {
                    "color": "#00b2ff"
                }
            };
            var rzp1 = new Razorpay(options);
            rzp1.open();
        }

        function complete_payment(payment_response) {
            $('.ajax-loader').fadeIn(1000);
            var rsvp = 0;
            if ($('#rsvp').prop("checked") == true) {
                rsvp = 1;
            }
            $.ajax({
                url: "{{url('student-dashboard/confirm-payment')}}",
                type: "POST",
                dataType: 'JSON',
                data: {payment_response, "_token": "{{ csrf_token() }}", rsvp: rsvp},
                success: function (response) {
                    if (response.error == false) {
                        window.location.href = "{{url('student-dashboard/my-course')}}";
                    } else {
                        var message = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + response.message + '</div>';
                        $('.message').html(message);
                        $('html, body').animate({scrollTop: $(".message").offset().top - 100}, 1000);
                    }
                    $('.ajax-loader').fadeOut(1000);
                }
            });
        }
    </script>
@endsection

@section('footer')
    @include('footer.footer_home')
@endsection
