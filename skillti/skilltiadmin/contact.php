
<?php
	include('adminheader.php'); 
	protect_admin_page();
	
	$Query = "select * from contactus";
	$Result = $dbObject->query($Query);
	$i=1;
	
?>

  <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper"><!-- HTML (DOM) sourced data -->
			<section id="html">
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body ">
							
							
								<form class="form px-3 mt-2">
									<div class="form-body">
										<h4 class="form-section"><i class="fa fa-phone-square"></i> Contact Us Detail</h4>
									</div>
								</form>
								<div class="card-block card-dashboard">
								
								<table class="table table-striped table-bordered sourced-data">
										<thead>
											<tr>
												<th>SNO</th>
												<th>Name</th>
												<th>Email</th>
												<th>Subject</th>
												<th>Message</th>
											</tr>
										</thead>
										<tbody>
										<?php while($Row = mysqli_fetch_assoc($Result)) { ?>
											<tr >
												<td><?php echo $i;?></td>
												<td><?php echo $Row['Name'];?></td>
												<td><?php echo $Row['Email'];?></td>
												<td><?php echo $Row['Subject'];?></td>
												<td><?php echo $Row['Message'];?></td>
													
											</tr>
									<?php $i++; } ?>
											
										</tbody>
										
									</table>
								</div>
							
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--/ HTML (DOM) sourced data -->
			</div>
        </div>

   

<?php include('adminfooter.php'); ?>
