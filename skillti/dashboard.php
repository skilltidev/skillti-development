<?php 
	include('template/header.php'); 
	protect_admin_page();
	$UserID = $_SESSION['USER_ID'];
	$UserRole = $_SESSION['USER_ROLE'];
	if($UserRole == 2) {
	$MQuery = "select * from messagedata where RecieverID='$UserID'";
	$MResult = $dbObject->query($MQuery);
	$MRowCount = mysqli_num_rows($MResult);
	
	$SQuery = "select * from syllabus where UserID='$UserID'";
	$SResult = $dbObject->query($SQuery);
	$SRowCount = mysqli_num_rows($SResult);

?>


<section class="mid_content dashboard_content">
		<div class="container-fluid pl-0">
			<div class="row">
				<div class="col-xs-12 col-sm-3">
				<?php include ('template/sidebar.php');?>
				</div>
				<div class="col-xs-12 col-sm-9 right_side">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="dash_bx">
								<ul class="list-inline">
									<li class="pull-left">
										<p><?php echo $MRowCount;?></p>
										<small>No. of Mentee</small>
									</li>
									<li class="pull-right">
										<i class="fa fa-bullhorn"></i>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="dash_bx">
								<ul class="list-inline">
									<li class="pull-left">
										<p>0</p>
										<small>No. of Unread Message</small>
									</li>
									<li class="pull-right">
										<i class="fa fa-bullhorn"></i>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="dash_bx">
								<ul class="list-inline">
									<li class="pull-left">
										<p>$0</p>
										<small>Earned this  month</small>
									</li>
									<li class="pull-right">
										<i class="fa fa-bullhorn"></i>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="dash_bx">
								<ul class="list-inline">
									<li class="pull-left">
										<p><?php echo $SRowCount;?></p>
										<small>No. of syllabus</small>
									</li>
									<li class="pull-right">
										<i class="fa fa-bullhorn"></i>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<?php } else if($UserRole == 3) { ?>
<section class="mid_content dashboard_content">
		<div class="container-fluid pl-0">
			<div class="row">
				<div class="col-xs-12 col-sm-3">
				<?php include ('template/sidebar.php');?>
				</div>
				<div class="col-xs-12 col-sm-9 right_side">
				<p>Welcome Mentee</p>
				</div>
			</div>
		</div>
	</section>
<?php } ?>


<?php include('template/footer.php'); ?>