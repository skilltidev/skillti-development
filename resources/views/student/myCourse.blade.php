@extends('header.student_header')
@section('headtitle', "Dashboard")
@section('headdesc', "")
@section('maincontent')
    <div class="mdk-drawer-layout__content page">
        <div class="container-fluid page__container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">All Courses</li>
            </ol>

            <div class="d-flex flex-column flex-sm-row flex-wrap mb-headings align-items-start align-items-sm-center">
                <div class="flex mb-2 mb-sm-0">
                    <h1 class="h2">My Courses</h1>
                </div>
                <a target="_blank" href="{{url('all-categories')}}" class="btn btn-success btn-sm">Add Course</a>
            </div>

            @if(count($coureses) == 0)
            <div class="alert alert-light alert-dismissible border-1 border-left-3 border-left-warning" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="text-black-70">Ohh no! No courses to display. Add some courses.</div>
            </div>
            @else
                @foreach($coureses as $j => $skill)
                    <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-column flex-sm-row">
                                <a class="avatar avatar-lg avatar-4by3 mb-3 w-xs-plus-down-100 mr-sm-3">
                                    <img src="{{asset('admin-assets/cat-image/'.$skill->logo)}}" alt="{{$skill->catname}}" class="avatar-img rounded">
                                </a>
                                <div class="flex" style="min-width: 200px;">
                                    <h5 class="card-title text-base m-0"><strong>{{$skill->catName}}</strong></h5>
                                    <h4 class="card-title mb-1">{{$skill->subCatName}}</h4>
                                    <p class="text-black-70">{!! $skill->shortBio !!}</p>
                                    {!! $skill->description !!}
                                    <div class="d-flex align-items-end">
                                        <div class="d-flex flex flex-column mr-3">
                                            <div class="d-flex align-items-center py-1">
                                                <small class="text-black-70 mr-2">Course Status: &nbsp;{!! ($skill->courseStatus)?"<label class='text text-bold text-success'><strong>Complete</strong></label>":"<label class='text text-bold text-warning'><strong>In Proccess</strong></label>" !!} </small>
                                            </div>
                                            <div class="d-flex align-items-center py-1 border-bottom">
                                                @foreach(json_decode($skill->skills) as $k => $skil)
                                                <small class="text-muted mr-2">{{$skil}}</small>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-end" style="margin-top: 10px;">
                                        <div class="d-flex flex flex-column mr-3">
                                            <form action="{{url('student-dashboard/course-ratting')}}" method="post">
                                                @csrf
                                                <input type="hidden" name="courseId" value="{{custom_encode($skill->courseId)}}">
                                                <input type="hidden" name="mentorId" value="{{custom_encode($skill->mentorId)}}">
                                                <div class="form-group row">
                                                    <div class="col-sm-6 col-md-6">
                                                        <div class="input-group">
                                                            <input required name="message" placeholder="Give your feedback.." value="{{$skill->ratting->comment}}" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3">
                                                        <div class="input-group">
                                                            <div class="rater" id="rater{{($j)}}" data-rating="{{$skill->ratting->rating}}"></div>
                                                            <input type="hidden" class="rate" name="rate" value="{{$skill->ratting->rating}}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3">
                                                        <div class="input-group">
                                                            <button type="submit" class="btn btn-outline-primary btn-rounded .btn-sm">Submit</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card__options right-0 pr-2">
                            <a class="dropdown-item text-info" href="{{url('student-dashboard/my-course/start-chat/'.custom_encode($skill->id))}}">Send Message</a>
                        </div>
                    </div>
                </div>
                    </div>
                @endforeach

            @endif
        </div>

    </div>
@endsection
@section('menu')
    @include('student.menu')
@endsection
@section('scripting')
    <script src="https://laravel.skillti.in/js/rater-js-master/index.js?v=2"></script>
    <script>
        $(document).ready(function () {
            $('.rater').each(function (i, val) {
                raterJs({
                    starSize:33,
                    element:document.querySelector("#rater"+i),
                    rateCallback:function rateCallback(rating, done) {
                        this.setRating(rating);
                        done();
                        console.log(rating);
                        $("#rater"+i).siblings('.rate').val(rating);
                    }
                });
            });
        });
    </script>
@endsection
@section('footer')
    @include('footer.footer_dashboard')
@endsection
