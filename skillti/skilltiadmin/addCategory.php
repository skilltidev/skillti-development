<?php
	include('adminheader.php'); 
	protect_admin_page();
	
	$UserID = $_SESSION['USER_ID'];
	$CatID = $_GET['catid'];
	
	$Query2 = "select * from category where CatID='$CatID'";
	$Result2 = $dbObject->query($Query2);
	$Row2 = mysqli_fetch_assoc($Result2);
	
	
	if(empty($CatID)) {
		if(isset($_POST['addCat'])) {
		   $catName = $_POST['catName'];
		   $parent = $_POST['parent'];
		   $catImage = $_FILES["catImage"]["name"];
		   $catImage_temp_name = $_FILES["catImage"]["tmp_name"];
		   $target_path = "app-assets/cat-image/".$catImage;
		   move_uploaded_file($catImage_temp_name, $target_path);
		   
		   $description = $_POST['description'];
		   
		   $Query = "insert into category (CatName,Parent,Description,CatImage,Status) values ('$catName','$parent','$description','$catImage','ACTIVE')";
		   $Insert = $dbObject->query($Query);
		   if($Insert) {
			   $_SESSION['SESS_MSG'] = "Category has been added.";
		   }
		}
	} else {
		if(isset($_POST['Update'])) {
		   $catName = $_POST['catName'];
		   $parent = $_POST['parent'];
		   $catImage = $_FILES["catImage"]["name"];
		   $catImage_temp_name = $_FILES["catImage"]["tmp_name"];
		   $target_path = "app-assets/cat-image/".$catImage;
		   move_uploaded_file($catImage_temp_name, $target_path);
		   
		   $status = $_POST['status'];
		   $description = $_POST['description'];
		   if(empty($catImage)) {
				$Query = "update category set CatName='".$catName."',Parent='".$parent."',Description='".$description."',Status='".$status."' where CatID='".$CatID."'";
		   } else { 
				$Query = "update category set CatName='".$catName."',Parent='".$parent."',Description='".$description."',CatImage='".$catImage."',Status='".$status."' where CatID='".$CatID."'";
		   }
		   $Update = $dbObject->query($Query);
		   if($Update == true) {
			   header("location: addCategory?catid=".$CatID."");
				session_start();
				$_SESSION['SESS_MSG'] = "Category has been updated";
				//exit;
				
		   }
		}
	}
	
	
?>

<div class="main-panel">
    <div class="main-content">
    <div class="content-wrapper"><!-- Striped row layout section start -->
        <section id="striped-row-form-layouts">
        <div class="row">
        	    <div class="col-md-12">
        	        <div class="card">
        	            <div class="card-body">
        	                <div class="px-3 mt-2">
        	                    <form class="form form-horizontal striped-rows form-bordered" action="" method="post" enctype="multipart/form-data">
        	                    	<div class="form-body">
									<?php include("../include/error.msg.inc.php"); ?>
									<?php if(empty($CatID)) { ?>
        	                    		<h4 class="form-section"><i class="fa fa-plus"></i> Add New Category</h4>
									<?php } else { ?>
										<h4 class="form-section"><i class="fa fa-pencil-square-o"></i> Edit Category</h4>
									<?php } ?>
        
        		                        <div class="form-group row">
        									<label class="col-md-3 label-control" for="projectinput5">Category Name</label>
        									<div class="col-md-9">
        		                            <input type="text" class="form-control" placeholder="Category Name" value="<?php echo $Row2['CatName'];?>" name="catName" required />
        		                            </div>
        		                        </div>
        
        		                        <div class="form-group row">
        		                        	<label class="col-md-3 label-control" >Parent Category</label>
        		                        	<div class="col-md-9">
        			                            <select  name="parent" id="parent" class="form-control" required>
        			                                <option value="0" selected="selected" >Select Parent</option>
													<?php
														$CQuery = "select * from category where Status ='ACTIVE' and Parent=0";
														$CResult = $dbObject->query($CQuery);
														while($CRow = mysqli_fetch_assoc($CResult)) {
													?>
        			                                <option value="<?php echo $CRow['CatID'];?>"><?php echo $CRow['CatName'];?></option>
        			                               <?php } ?>
        			                            </select>
        		                            </div>
        		                        </div>
        
        		                       	<div class="form-group row">
        									<label class="col-md-3 label-control">Category Image</label>
        									<div class="col-md-9">
        										<label id="projectinput8" class="file center-block">
        											<input type="file" id="file" name="catImage">
        											<span class="file-custom"></span>
        										</label>
												<?php if(!empty($CatID)) { ?>
												<img style="width:70px;height:70px;position:absolute;top:3px;" src="app-assets/cat-image/<?php echo $Row2['CatImage'];?>">
												<?php } ?>
        									</div>
        								</div>
        
        								<div class="form-group row last">
        									<label class="col-md-3 label-control" for="projectinput9">Category Description</label>
        									<div class="col-md-9">
        										<textarea id="projectinput9" rows="5" class="form-control" name="description" placeholder="Category Description"><?php echo $Row2['Description'];?></textarea>
												
        									</div>
        								</div>
										<?php if(!empty($CatID)) { ?>
										<div class="form-group row last">
        									<label class="col-md-3 label-control" for="projectinput9">Category Status</label>
        									<div class="col-md-9">
        										<select  name="status" class="form-control" id="status" >
        			                                <option value="ACTIVE">ACTIVE</option>
        			                                <option value="INACTIVE">INACTIVE</option>
        			                             </select>
											</div>
        								</div>
										<?php } if(empty($CatID)) { ?>
										<div class="form-actions text-center">
											<a href="allCategory"> <button type="button" class="btn btn-raised btn-warning mr-1">
												<i class="ft-x"></i> Cancel
											</button></a>
											<button type="submit" name="addCat" class="btn btn-raised btn-primary">
												<i class="fa fa-check-square-o"></i> Save
											</button>
										</div>
										<?php } else { ?>
										<div class="form-actions text-center">
											<a href="allCategory"> <button type="button" class="btn btn-raised btn-warning mr-1">
												<i class="ft-x"></i> Cancel
											</button></a>
											<button type="submit" name="Update" class="btn btn-raised btn-primary">
												<i class="fa fa-check-square-o"></i> Update
											</button>
										</div>
										<?php } ?>
												
        							</div>
        
        	                        
        	                    </form>
        	                </div>
        	            </div>
        	        </div>
        	    </div>
        	</div>
        
        </section>
    </div>
    </div>    

        
		
		
<?php include('adminfooter.php'); ?>
<?php if(!empty($CatID)) { ?>
<script language="javascript">
	var parent = '<?php echo $Row2['Parent'];?>';
	document.getElementById("parent").value = parent;
	
	var Status = '<?php echo $Row2['Status'];?>';
	document.getElementById("status").value = Status;
</script>
<?php } ?>