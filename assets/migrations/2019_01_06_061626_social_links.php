<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SocialLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('socialLinks', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('userId')->unsigned();
		    $table->string('facebook')->nullable();
		    $table->string('twitter')->nullable();
		    $table->string('linkedIn')->nullable();
		    $table->string('github')->nullable();
		    $table->string('other')->nullable();
		    $table->timestamps();
	    });
	    Schema::table('socialLinks', function($table) {
		    $table->foreign('userId')->references('id')->on('socialLinks');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('socialLinks');
    }
}
