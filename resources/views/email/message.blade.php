<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<div class="emailer" style="max-width: 500px; width: 100%; margin: 0 auto; border: 1px solid #ccc; padding: 0 0 20px; box-shadow: 0 1px 2px rgba(0,0,0,0.5), 0 5px 2px rgba(0,0,0,0.2);font-size:16px;line-height:24px">
    <h1 style=" padding: 20px 0; margin:0; width: auto;  font-size: 20px; text-align: center; border-bottom: 1px solid #ccc; background: #f6f9fc; ">SKILLTI APPLICATION </h1>
    <p style="margin-left:20px;"><b>Dear {{$mentorData->firstName." ".$mentorData->lastName}},</b></p>
    <p style="margin: 20px 0;padding: 0 20px">You have new messages from <b>{{Auth::user()->firstName." ".Auth::user()->lastName}}</b></p>
    <p style="margin-left:20px;"><label><b>Message :</b> {{$inputs['msgtext']}}</label></p>
    <p style="margin: 20px 0;padding: 0 20px">Press Below to reply </p>
    <p style="margin: 30px 0;padding: 0 20px"><a href="{{url('dashboard/mailbox')}}">Dashboard</a>.</p>
</div>
</body>
</html>