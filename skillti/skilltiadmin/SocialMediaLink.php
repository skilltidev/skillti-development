
<?php
	include('adminheader.php'); 
	protect_admin_page();
	
	$Query = "select * from socialmedialink";
	$Result = $dbObject->query($Query);
	$i=1;
	
?>
<style>
    .table td {
            word-break: break-all;
    }
</style>
  <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper"><!-- HTML (DOM) sourced data -->
			<section id="html">
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body ">
							
							
								<form class="form px-3 mt-2">
									<div class="form-body">
										<h4 class="form-section"><i class="fa fa-medium"></i> All Social Media Linking</h4>
									</div>
								</form>
								<div class="card-block card-dashboard">
								
								<table class="table table-striped table-bordered sourced-data">
										<thead>
											<tr>
												<th>SNO</th>
												<th>User</th>
												<th>Facebook</th>
												<th>Twitter</th>
												<th>LinkedIn</th>
												<th>Github</th>
												<th>Other</th>
											</tr>
										</thead>
										<tbody>
										<?php while($Row = mysqli_fetch_assoc($Result)) {  
										        $userID = $Row['UserID'];
										        $UQuery = "select EmailID from users where UserID='$userID'";
        										$UResult = $dbObject->query($UQuery);
        										$URow = mysqli_fetch_assoc($UResult);
										?>
											<tr >
												<td><?php echo $i;?></td>
												<td><?php echo $URow['EmailID'];?></td>
												<td><?php echo $Row['Facebook'];?></td>
												<td><?php echo $Row['Twitter'];?></td>
												<td><?php echo $Row['LinkedIn'];?></td>
												<td ><?php echo $Row['Github'];?></td>
												<td ><?php echo $Row['Other'];?></td>
												
											</tr>
									<?php $i++; } ?>
											
										</tbody>
										
									</table>
								</div>
							
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--/ HTML (DOM) sourced data -->
			</div>
        </div>

   

<?php include('adminfooter.php'); ?>
