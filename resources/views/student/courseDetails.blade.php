@extends('header.student_header')
@section('headtitle', "Dashboard")
@section('headdesc', "")
@section('maincontent')
    <div class="mdk-drawer-layout__content page">
        <div class="container-fluid page__container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">View Details</li>
            </ol>

            @if(!$coureses)
            <div class="alert alert-light alert-dismissible border-1 border-left-3 border-left-warning" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="text-black-70">Ohh no! No courses-details to display.</div>
            </div>
            @else
            <div class="row">
                <div class="container page__container">
                    <div class="card text-white text-center">
                        <div class="card-body">
                            <h1 class="h2 mb-0">web-development</h1><div class="avatar" style="float: left;">
                                <img src="http://localhost/skillti-dev/public/profile-photo/User-dummy.png" alt="Course Image" class="avatar-img rounded">
                            </div>
                        </div>
                    </div>

                    <div class="text-center">
                        <p class="lead text-muted mb-0">Sub Course </p>
                        <p><span class="badge badge-light ">STUDENT</span></p>
                    </div>


                    <div class="row">
                        <div class="col-md-6 offset-md-3 text-center">
                            <h4 class="mb-0">Skill</h4>
                            <span class="badge badge-pill badge-primary">Primary</span>
                            <span class="badge badge-pill badge-primary">Primary</span>
                            <span class="badge badge-pill badge-primary">Primary</span>
                            <span class="badge badge-pill badge-primary">Primary</span>
                            <span class="badge badge-pill badge-primary">Primary</span>
                        </div>
                    </div>
                    <hr>
                    <div class="card-group">
                        <div class="card">
                            <div class="card-body">
                                This is Sonu Tyagi, CEO of skiillti. Would love to make this world more accessible to technology and mentors. Our aim to disrupt learning tradition. Team skillti & i will make this happen. Im B.tech in Electronics and communication engineering with certification in Associate level software business analyst. Worked with various projects and clients. Domain Expertise •	VoIP and Telecom •	BFSI domain • property and casualty domain • E-commerce & Marketplace
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                This is Sonu Tyagi, CEO of skiillti. Would love to make this world more accessible to technology and mentors. Our aim to disrupt learning tradition. Team skillti & i will make this happen. Im B.tech in Electronics and communication engineering with certification in Associate level software business analyst. Worked with various projects and clients. Domain Expertise •	VoIP and Telecom •	BFSI domain • property and casualty domain • E-commerce & Marketplace
                            </div>
                        </div>
                    </div>

                    <div class="card-columns card-header">
                        <div class="card">
                            <div class="card-header">
                                <div class="media align-items-center">

                                </div>
                            </div>
                            <ul class="list-group list-group-fit">
                                <li class="list-group-item">
                                    <a href="fixed-student-view-course.html"><span class="float-right "><i class="material-icons md-18"></i></span>Node : Laralipsum </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="fixed-student-view-course.html"><span class="float-right"><i class="material-icons md-18"></i></span>Node : Laralipsum </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="fixed-student-view-course.html"><span class="float-right"><i class="material-icons md-18"></i></span>Node : Laralipsum </a>
                                </li>
                            </ul>

                        </div>
                        <div class="card">
                            <div class="card-header">
                                <div class="media align-items-center">

                                </div>
                            </div>
                            <ul class="list-group list-group-fit">
                                <li class="list-group-item">
                                    <a href="fixed-student-view-course.html"><span class="float-right"><i class="material-icons md-18"></i></span>Node : Laralipsum </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="fixed-student-view-course.html"><span class="float-right"><i class="material-icons md-18"></i></span>Node : Laralipsum </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="fixed-student-view-course.html"><span class="float-right"><i class="material-icons md-18"></i></span>Node : Laralipsum </a>
                                </li>

                            </ul>
                        </div>

                    </div>
                    <hr>
                    <div class="app-messages__container d-flex flex-column h-100 pb-4">

                        <div class="flex pt-4" style="position: relative;" data-perfect-scrollbar>
                            <div class="container-fluid page__container">
                                <div class="jumbotron">
                                    <div class="d-flex align-items-center">
                                        <div class="mr-3">
                                            <div class="avatar">
                                                <img src="http://localhost/skillti-dev/public/profile-photo/User-dummy.png" alt="people" class="avatar-img rounded-circle">
                                            </div>
                                        </div>
                                        <div class="flex">
                                            <h4 class="mb-0">Michelle Smith</h4>
                                            <p class="text-muted mb-0">Personal Development Teacher since 2014</p>
                                        </div>
                                    </div>
                                </div>
                                <ul class="d-flex flex-column list-unstyled" id="messages">


                                    <li class="message d-inline-flex">
                                        <div class="message__aside">
                                            <a href="instructor-profile.html" class="avatar">
                                                <img src="assets/images/people/110/guy-6.jpg" alt="people" class="avatar-img rounded-circle">
                                            </a>
                                        </div>
                                        <div class="message__body card">
                                            <div class="card-body">
                                                <div class="d-flex align-items-center">
                                                    <div class="flex mr-3">
                                                        <a href="instructor-profile.html" class="text-body"><strong>Laza Bogdan</strong></a>
                                                    </div>
                                                    <div>
                                                        <small class="text-muted">1 hour ago</small>
                                                    </div>
                                                </div>
                                                <span class="text-black-70">Coming along nicely, we&#39;ve got a draft for the client design completed, take a look! 🤓</span>

                                                <a href="#" class="media align-items-center mt-2 text-decoration-0 bg-white px-3">
                                                            <span class="avatar mr-2">
                                                                <span class="avatar-title rounded-circle">
                                                                    <i class="material-icons font-size-24pt">attach_file</i>
                                                                </span>
                                                            </span>
                                                    <span class="media-body" style="line-height: 1.5">
                                                                <span class="text-primary">draft.sketch</span><br>
                                                                <span class="text-muted">5 MB</span>
                                                            </span>
                                                </a>

                                            </div>
                                        </div>
                                    </li>

                                    <li class="message d-inline-flex">
                                        <div class="message__aside">
                                            <a href="instructor-profile.html" class="avatar">
                                                <img src="assets/images/people/110/woman-5.jpg" alt="people" class="avatar-img rounded-circle">
                                            </a>
                                        </div>
                                        <div class="message__body card">
                                            <div class="card-body">
                                                <div class="d-flex align-items-center">
                                                    <div class="flex mr-3">
                                                        <a href="instructor-profile.html" class="text-body"><strong>Michelle</strong></a>
                                                    </div>
                                                    <div>
                                                        <small class="text-muted">5 minutes ago</small>
                                                    </div>
                                                </div>
                                                <span class="text-black-70">Clients loved the new design.</span>

                                            </div>
                                        </div>
                                    </li>



                                    <li class="message d-inline-flex">
                                        <div class="message__aside">
                                            <a href="instructor-profile.html" class="avatar">
                                                <img src="assets/images/people/110/guy-6.jpg" alt="people" class="avatar-img rounded-circle">
                                            </a>
                                        </div>
                                        <div class="message__body card">
                                            <div class="card-body">
                                                <div class="d-flex align-items-center">
                                                    <div class="flex mr-3">
                                                        <a href="instructor-profile.html" class="text-body"><strong>Laza Bogdan</strong></a>
                                                    </div>
                                                    <div>
                                                        <small class="text-muted">just now</small>
                                                    </div>
                                                </div>
                                                <span class="text-black-70">Glad it all worked out 😉</span>

                                            </div>
                                        </div>
                                    </li>


                                </ul>
                            </div>
                        </div>
                        <div class="container-fluid page__container">
                            <form action="#" id="message-reply">
                                <div class="input-group input-group-merge">
                                    <input type="text" class="form-control form-control-appended" autofocus="" required="" placeholder="Type message">
                                    <div class="input-group-append">
                                        <div class="input-group-text pr-2">
                                            <button class="btn btn-flush" type="button"><i class="material-icons">tag_faces</i></button>
                                        </div>
                                        <div class="input-group-text pl-0">
                                            <div class="custom-file custom-file-naked d-flex" style="width: 24px; overflow: hidden;">
                                                <input type="file" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" style="color: inherit;" for="customFile">
                                                    <i class="material-icons">attach_file</i>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            @endif
        </div>

    </div>
@endsection
@section('menu')
    @include('student.menu')
@endsection
<script id="template-message" type="text/x-jsrender">
        <li class="message d-inline-flex">
    <div class="message__aside">
      <a href="instructor-profile.html" class="avatar">
        <img src="" alt="people" class="avatar-img rounded-circle">
      </a>
    </div>
    <div class="message__body card">
      <div class="card-body">
        <div class="d-flex align-items-center">
          <div class="flex mr-3">
            <a href="instructor-profile.html" class="text-body"><strong></strong></a>
          </div>
          <div>
            <small class="text-muted"></small>
          </div>
        </div>
        <span class="text-black-70"></span>
                     <a href="#" class="media align-items-center mt-2 text-decoration-0 bg-white px-3">
                  <span class="avatar mr-2">
                    <span class="avatar-title rounded-circle">
                      <i class="material-icons font-size-24pt">attach_file</i>
                    </span>
                  </span>
                  <span class="media-body" style="line-height: 1.5">
                    <span class="text-primary"></span><br>
            <span class="text-muted"></span>
          </span>
        </a>
   </div>
              </div>
            </li>


            </script>

@section('scripting')
@endsection
@section('footer')
    @include('footer.footer_dashboard')
@endsection
