<?php
    include_once("config.php");
	
	class ss_API{
		public $message='';
		public $code=null;
		public $connect=null;
          
		function __construct($Db=null){
			$this->connect=$Db;
		}
	 
/*this is a function to login a registered user.*/
public function Adminlogin($username,$password){				
    $fields=array("UserID","FirstName","LastName","RoleID","EmailID","LastLoggedIn","Password,IsFirstTimeLogin");
    $where = "EmailID = '$username' and Password = '$password'";
    $userinformation=$this->connect->select(USER,$fields,$where);
	return $userinformation;
}

/* this is a function to send a recovery password on the mail of the user.if he forgot his old password.*/
public function forgetPassword($tblname,$email){
	global $dbObject;
    $sentmail = '';
	$email_en= base64_encode($email); 
    $fields=array("EmailID,Password,UniqueKey");
    $where = "EmailID = '$email' " ;
    $userinformation = $this->connect->select($tblname,$fields,$where);
	if(checkExists($userinformation)){
		$key = randomPassword(20);
		$Query = "update users set UniqueKey = '$key' where EmailID = '".$email."'";
        $Result =  $dbObject->query($Query);
		
	if($Result) {
			$fromEmail = "info@trakpros.com";
			    $to = $email;  
                $headers = "From:TrakPros Team <".$fromEmail.">\r\n";
        		$headers .= 'MIME-Version: 1.0' . "\r\n";
        		$headers .= "Content-type: text/html; charset=iso-8859-1";
				$subject = "Trakpros Reset Password";
				$body = '<p style="margin-left:20px;"></p>';
				$body .= '<div class="emailer" style="max-width: 500px; width: 100%; margin: 0 auto; border: 1px solid #ccc; padding: 0 0 20px; box-shadow: 0 1px 2px rgba(0,0,0,0.5), 0 5px 2px rgba(0,0,0,0.2);font-size:16px;line-height:24px">
		<h1 style=" padding: 20px 0; margin:0; width: auto;  font-size: 20px; text-align: center; border-bottom: 1px solid #ccc; background: #f6f9fc; ">TRAKPROS </h1>
		<p style="margin: 30px 0;padding: 0 20px">Our TRAKPROS system recently received a request for a reset password.</p>
		<p style="margin: 30px 0;padding: 0 20px">To change your password, click the link below. </p>
		<a class="btn" href="https://trakpros.com/dashboard/newPassword?key='.$key.'&email='.$email_en.'"   style="background: #666ee8;display: block;padding: 10px;border-radius: 4px;max-width: 300px; display: block; margin: 0 auto;text-align: center; color: #fff;text-transform: uppercase;
						box-shadow: 0 2px 2px rgba(0,0,0,0.5);text-decoration: none; transition: all .5s; ">Reset your password</a>
		<p style="margin: 30px 0;padding: 0 20px">We will be here to help you with any step along the way. You can find answers to most questions and get in touch with us on our <a href="#">support site</a>.</p>';
				
		
				$mail = mail($to, $subject, $body, $headers);
				if(!$mail)
				{
				   $_SESSION['SESS_MSG'] = "Cannot send password to your e-mail address due to some error";
				} else {

				$_SESSION['SESS_MSG'] = "Your Username and Password has been sent to your email address.";
				}
		}
		
		
    }
    else {
		$_SESSION['SESS_MSG'] =  "your email not found in our database";
    }   
}


}
	
?>