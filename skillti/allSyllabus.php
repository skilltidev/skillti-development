<?php 
	include('template/header.php'); 
	protect_admin_page();
	$userID = $_SESSION['USER_ID'];
	
?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
<style>
    table.dataTable thead .sorting:after {
    display:none;
}
table.dataTable thead .sorting_asc::after {
     display:none;
}
</style>

<section class="mid_content dashboard_content">
	<div class="container-fluid pl-0">
		<div class="row">
			<div class="col-xs-12 col-sm-3">
			<?php include ('template/sidebar.php');?>
			</div>
			<div class="col-xs-12 col-sm-9 right_side">
			<div class="col-xs-12 col-sm-12">
				<div class="custom_form">
				
				<table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>S.No.</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Other Sub Category</th>
                            <th>Published Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
					
					<?php
						$Query = "select * from syllabus where UserID='$userID'";
						$Result = $dbObject->query($Query);
						$i=1;
						while($Row = mysqli_fetch_assoc($Result)) { 
						$CatID = $Row['Category'];
						$SubCatID = $Row['SubCategory'];
										
						$CQuery = "select CatName from category where CatID='$CatID'";
						$CResult = $dbObject->query($CQuery);
						$CRow = mysqli_fetch_assoc($CResult);
						
						$SCQuery = "select CatName from category where CatID='$SubCatID'";
						$SCResult = $dbObject->query($SCQuery);
						$SCRow = mysqli_fetch_assoc($SCResult);
					?>
                        <tr>
                            <td><?php echo$i;?></td>
                            <td><?php echo $CRow['CatName'];?></td>
							<td><?php echo $SCRow['CatName'];?></td>
							<td><?php echo $SCRow['OtherSubCat'];?></td>
							<td><?php echo date('d M, Y', strtotime($Row['CreatedDate']));?></td>
                           	<td class="text-center"><a title="Edit" href="addSyllabus?edit=<?php echo $Row['ID'];?>"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size: 20px !important;"></i></a>
							</td>
                        </tr>
					<?php $i++; } ?>	
						
                    </tbody>
                   </table>
				
				</div>
			</div>
		</div>
	</div>
</section>



<?php include('template/footer.php'); ?>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>

