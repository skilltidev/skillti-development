<?php include('../include/config.php');?>
<?php include('../include/setting.php');?>

<!DOCTYPE html>
<html lang="en" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <?php $pageName = explode(".",$page) ?>
    <title><?php echo ucwords($pageName[0]);?>- SkillTI Application</title>
    
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <?php if($pageName[0] == 'ViewProfile') { ?>
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/prism.min.css">
    <?php } else { ?>
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/chartist.min.css">
	<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
	<?php } ?>
    <!-- END VENDOR CSS-->
    <!-- BEGIN APEX CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END APEX CSS-->
  
  </head>
  <body data-col="2-columns" class=" 2-columns ">
   <div class="wrapper nav-collapsed menu-collapsed">

	<div data-active-color="white" data-background-color="purple-bliss" data-image="app-assets/img/nav-bg.jpg" class="app-sidebar">
        <!-- main menu header-->
        <!-- Sidebar Header starts-->
        <div class="sidebar-header">
          <div class="logo clearfix"><a href="index.html" class="logo-text float-left">
              <div class="logo-img"><img src="../assets/images/skillti_logo_4_update-fav_icon.png"/></div><span class="text align-middle">SKILLTI</span></a><a id="sidebarToggle" href="javascript:;" class="nav-toggle d-none d-sm-none d-md-none d-lg-block"><i data-toggle="collapsed" class="ft-toggle-left toggle-icon"></i></a><a id="sidebarClose" href="javascript:;" class="nav-close d-block d-md-block d-lg-none d-xl-none"><i class="ft-x"></i></a></div>
        </div>
        <!-- Sidebar Header Ends-->
        <!-- / main menu header-->
        <!-- main menu content-->
        <div class="sidebar-content">
          <div class="nav-container">
            <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
			<li class="nav-item active">
				<a href="dashboard"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span></a>
            </li>
              <li class="has-sub nav-item"><a href="javascript:void(0)"><i class="fa fa-users"></i><span data-i18n="" class="menu-title">User Management</span></a>
                <ul class="menu-content">
                  <li class=""><a href="addNew" class="menu-item"><i class="fa fa-user-plus" aria-hidden="true"></i> Add New</a>
                  </li>
                  <li><a href="allMentor" class="menu-item"><i class="fa fa-list" aria-hidden="true"></i> All Mentors</a>
                  </li>
				  <li><a href="allMentee" class="menu-item"><i class="fa fa-list" aria-hidden="true"></i> All Mentee</a>
				  </li>
                </ul>
              </li>
			   <li class="has-sub nav-item"><a href="javascript:void(0)"><i class="fa fa-sitemap"></i><span data-i18n="" class="menu-title">Categories</span></a>
                <ul class="menu-content">
                  <li class=""><a href="allCategory" class="menu-item"><i class="fa fa-th-list" aria-hidden="true"></i> All Category</a>
                  </li>
                  <li><a href="addCategory" class="menu-item"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
                  </li>
				</ul>
              </li>
              <li class="nav-item"><a href="allSyllabus"><i class="fa fa-crosshairs"></i><span data-i18n="" class="menu-title">All Syllabus</span></a>
               
              </li>
              
              <li class="nav-item"><a href="SocialMediaLink"><i class="fa fa-medium"></i><span data-i18n="" class="menu-title">Social Media Linking</span></a>
               
              </li>
              <li class="nav-item"><a href="contact"><i class="fa fa-phone-square"></i><span data-i18n="" class="menu-title">Contact Us Detail</span></a>
               
              </li>
              <li class="nav-item"><a href="review"><i class="fa fa-star"></i><span data-i18n="" class="menu-title">Rating Management</span></a>
               
              </li>
			 
                </ul>
              </li>
              
            </ul>
          </div>
        </div>
        <!-- main menu content-->
        <div class="sidebar-background"></div>
        <!-- main menu footer-->
        <!-- include includes/menu-footer-->
        <!-- main menu footer-->
      </div>
      <!-- / main menu-->


      <!-- Navbar (Header) Starts-->
      <nav class="navbar navbar-expand-lg navbar-light bg-faded">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-left"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            <form role="search" class="navbar-form navbar-right mt-1">
              <img style="height: 55px;" src="app-assets/img/logo.png">
            </form>
          </div>
          <div class="navbar-container">
            <div id="navbarSupportedContent" class="collapse navbar-collapse">
              <ul class="navbar-nav">
                <li class="nav-item "><p class="username" style="color: #000 !important;"><?php echo $_SESSION['USER_FULL_NAME'];?></p></li>
                
                
                <li class="dropdown nav-item"><a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle"><i class="ft-user font-medium-3 blue-grey darken-4"></i>
                    <p class="d-none">User Settings</p></a>
                  <div ngbdropdownmenu="" aria-labelledby="dropdownBasic3" class="dropdown-menu dropdown-menu-right">
				  					  <a href="" class="dropdown-item py-1"><i class="ft-edit mr-2"></i><span>My Profile</span></a>
					  <a href="" class="dropdown-item py-1"><i class="ft-settings mr-2"></i><span>Reset Password</span></a>
				  				   
					  <a href="logout" class="dropdown-item py-1"><i class="ft-power mr-2"></i><span>Logout</span></a>
					    
					</div>
                </li>
               
              </ul>
            </div>
          </div>
        </div>
      </nav>
      <!-- Navbar (Header) Ends-->
