<?php include('template/header.php');
unset($_SESSION['BACK_URL']);

$UserID = $_GET['id'];
$SID = $_SESSION['USER_ID'];
$cat = ucwords($_GET['cat']);
$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]#msgForm";
$url2 = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]#review";

$CQuery = "select CatID from category where CatName='$cat'";
$CResult = $dbObject->query($CQuery);
$CRow = mysqli_fetch_assoc($CResult);
$SCatID = $CRow['CatID'];

    $SQuery = "select * from syllabus where UserID='$UserID' and SubCategory='$SCatID' or Category='$SCatID'";
	$SResult =  $dbObject->query($SQuery);
	$SRow = mysqli_fetch_assoc($SResult);

    $Query = "select * from users where UserID='$UserID'";
	$Result =  $dbObject->query($Query);
	$Row = mysqli_fetch_assoc($Result);
	//echo "<pre />";print_r($Row);

    $SYllQuery = "select * from syllabus where UserID='$UserID'";
    $SYllResult =  $dbObject->query($SYllQuery);
	$SYllRow = mysqli_num_rows($SYllResult);
	
    
     if(isset($_POST['msgSubmit'])) {
         
         
       $SenderID = $_SESSION['USER_ID'];
       $RecieverID = $_GET['id'];
       if($SenderID == $RecieverID) {
           $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]#msgForm";
           print "<script>setTimeout(\"location.href = '".$actual_link."';\",0);</script>";
	        session_start();
            $_SESSION['SESS_MSG'] = "You can't message to yourself.";
       }  else { 
       $msgtext = $_POST['msgtext'];
       
       $MQuery = "insert into messagedata (SenderID,RecieverID,Message,Status) values ('$SenderID','$RecieverID','$msgtext','1')";
       $Result = $dbObject->query($MQuery);
       if($Result) {
           
           $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]#msgForm";
           print "<script>setTimeout(\"location.href = '".$actual_link."';\",0);</script>";
	        session_start();
            $_SESSION['SESS_MSG'] = "Your message has been sent.";
            print "<script>setTimeout(\"location.href = '".$base_url."messages';\",1000);</script>";
       }
       }
   
   }
   
   
	if(isset($_POST['PostComment'])) {
       $UserID = $_GET['id'];
       $SenderID = $SID;
       $rating = $_POST['selected_rating'];
       $review = $_POST['review'];
	   
	   if($UserID == $SenderID) {
	       $ms="You can't post review yourself.";
	   } else {
	   
	   $RQuery = "insert into reviews (UserID,SenderID,Rating,Review,Status) values ('$UserID','$SenderID','$rating','$review','draft')";
       $RResult = $dbObject->query($RQuery);
       if($RResult) {
			print "<script>setTimeout(\"location.href = '".$url2."';\",10);</script>";
	        session_start();
            $msg = "Your review will be published within 24 hours.";
	   }
	   }
       
       
   }
   
   $RAteq = "SELECT AVG(Rating) as rate FROM reviews WHERE UserID = '$UserID' and Status='publish'";
   $AvResult = $dbObject->query($RAteq);
   $AvRow = mysqli_fetch_assoc($AvResult);
   $AVRate = $AvRow['rate'];
   
   
 ?>
<style>
    .rating-header {
    margin-top: -10px;
    margin-bottom: 10px;
}
.heading.text-center {
    margin-bottom: 30px;
}
.star{
    width:50%;
    float:left;
}
.numbr{
     width:10%;
    float:left;
    padding-top: 13px;
}
.form-group {
    margin-bottom: 30px;
}
#review {
    margin-top:15px;
}
.post-btn {
    font-size: 17px;
    font-weight: 600;
    text-align: left;
    margin-top: 5px;
}
.user_p {
    width: 45px !important;
    height: 45px !important;
    border: none !important;
}
ul.rating_user h3 {
   line-height: 26px;
}
</style>
<section class="mid_content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="profile_list profile_details">
						<ul>
							<li>
								<img src="assets/profile-photo/<?php echo $Row['profilePic'];?>" alt="">
								<ul class="list-unstyled">
								    
									<li><i class="fa fa-star"></i> <?php echo number_format((float)$AVRate, 1, '.', '');?> Instructor Rating</li>
									<li><i class="fa fa-comment"></i> 111,207 Reviews</li>
									<li><i class="fa fa-user"></i> 111,207 Students</li>
									<li><i class="fa fa-video-camera"></i> <?php echo $SYllRow;?> Courses</li>
									<li><i class="fa fa-globe"></i> <?php echo $Row['Country'];?></li>
								</ul>
								
							</li>
							<li>
								<div class="profile_list_bx">
									<h4><a ><?php echo $Row['FirstName']." ".$Row['LastName'];?></a></h4>
									<h4><?php echo $Row['UserTagline'];?></h4>
									<!--<div class="speciality">
										<i class="fa fa-star"></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									</div>-->
									<div class="bio_disc">
										<span class="more"><?php echo $Row['UserAbout'];?></span>
									</div>
									<div class="grey_listing">
										<h3>What will I Learn?</h3>
										<ul>
											<li><?php echo $SRow['KeyPoint1'];?></li>
											<li><?php echo $SRow['KeyPoint2'];?></li>
											<li><?php echo $SRow['KeyPoint3'];?></li>
											<li><?php echo $SRow['KeyPoint4'];?></li>
											<li><?php echo $SRow['KeyPoint5'];?></li>
											
											<?php if($SRow['KeyPoint6'] != '') { ?>
											<li><?php echo $SRow['KeyPoint6'];?></li>
											<?php } ?>
											
											<?php if($SRow['KeyPoint7'] != '') { ?>
											<li><?php echo $SRow['KeyPoint7'];?></li>
											<?php } ?>
											
											<?php if($SRow['KeyPoint8'] != '') { ?>
											<li><?php echo $SRow['KeyPoint8'];?></li>
											<?php } ?>
											
											<?php if($SRow['KeyPoint9'] != '') { ?>
											<li><?php echo $SRow['KeyPoint9'];?></li>
											<?php } ?>
											
											<?php if($SRow['KeyPoint10'] != '') { ?>
											<li><?php echo $SRow['KeyPoint10'];?></li>
											<?php } ?>
										
										</ul>
										<hr style="border-top: 1px solid #807e7e !important;">
										<h3>Description</h3>
										<p><?php echo $SRow['Description'];?></p>
									</div>
									<?php if($_SESSION['USER_ID'] == '') { ?>
									<div class="msgclass">
									    <form action="signin" method="post">
									        <input type="hidden" name="url" value="<?php echo $url;?>" />
									        <input type="submit" class="see_all " name="message" value="Message to <?php echo $Row['FirstName']." ".$Row['LastName'];?>" />
									   </form>
									   <br>
									</div>
									<?php } else { ?>
								    <div class="user_signup text-center jsmsg" style="background:#fff;border: transparent;" id="msgForm">
										
									    <div class="custom_form" >
									        <?php include("include/error.msg.inc.php"); ?>
									        <h3 style="padding:0 0 8px 0">Message to <?php echo $Row['FirstName']." ".$Row['LastName'];?></h3>
									        <form method="post" action="">
                        						<div class="form-group">
                        							<textarea class="form-control" name="msgtext" placeholder="Type Message" required=""></textarea>
                                                </div>
                                                <div class="form-group text-center">
                                                    <button type="submit" name="msgSubmit" class="blue_btn custom_btn">Send</button>
                                                </div>
                        					</form>
                        				</div>
									</div>
									<?php } ?>
									
									<?php
										$RRQuery = "select * from reviews where UserID='$UserID' and Status='publish'";
										$RRResult =  $dbObject->query($RRQuery);
										while($RRRow = mysqli_fetch_assoc($RRResult)) {
										$SenderID = $RRRow['SenderID'];
										$datetime = $RRRow['CreatedDate'];
										
										$UsrQuery = "select * from users where UserID='$SenderID'";
										$UsrResult =  $dbObject->query($UsrQuery);
										$UsrRow = mysqli_fetch_assoc($UsrResult);
										
									
									?>
									<div class="rating_section">
										<div class="row">
											<div class="col-sm-12 col-xs-12">
												<ul class="rating_user">
													<li><div ><img class="user_p" src="assets/profile-photo/<?php echo $UsrRow['profilePic'];?>" /></div></li>
													<li>
														<h4><b><?php echo $UsrRow['FirstName'].' '.$UsrRow['LastName'];?></b>  <font style="font-style: italic;color: #aaa;padding-right: 10px;float:right"><i class="fa fa-clock-o" aria-hidden="true"></i> Posted: <?php echo date('M j, Y',strtotime($datetime));?></font></h4>
														<h3><?php echo $RRRow['Review'];?></h3>
														<br>
														<div class="rating_details">
															<div class="rating_star_s">
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star-half-o"></i>
																<i class="fa fa-star-o"></i>
															</div>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<?php } ?>
									
									<div class="user_signup" id="review">
									<?php if($SID == '') { ?>
									    	<div class="row">
                                			<div class="col-xs-8">
                                				<div class="text-center">
                                				    <p class="post-btn">Our team is happy to answer your sales questions.</p>
                                				</div>
                                			</div>
                                			<div class="col-xs-4">
                                			    <form action="signin" method="post">
        									        <input type="hidden" name="url" value="<?php echo $url2;?>" />
        									        <button type="submit" class="blue_btn custom_btn " name="message" >Post Comment</button>
        									   </form>
                                			</div>
                                			
                                			</div>
									<?php } else { ?>
									
									<div class="row">
                                			<div class="col-xs-12">
                                				<div class="heading text-center">
                                				    <?php if(!empty($msg)) { ?>
                                				    <p style="color:red;text-align:center;"><?php echo $msg;?></p>
                                				    <?php } ?>
                                				    <h2>Post a comment</h2>
                                					<p>Our team is happy to answer your sales questions.</p>
                                				</div>
                                			</div>
                                			<div class="col-xs-10 col-xs-offset-1">
                                			  
                                                <div class="custom_form">
                                                    
                                					 <form method="post" action="">
                                						<div class="form-group">
                                							<label>Your Rating</label>
                                							<div class="star">
                                    							<input type="hidden" id="selected_rating" name="selected_rating" value="" required >
                                                                
                                                        	    <button type="button" class="btnrating btn btn-default btn-lg" data-attr="1" id="rating-star-1">
                                                        	        <i class="fa fa-star" aria-hidden="true"></i>
                                                        	    </button>
                                                        	    <button type="button" class="btnrating btn btn-default btn-lg" data-attr="2" id="rating-star-2">
                                                        	        <i class="fa fa-star" aria-hidden="true"></i>
                                                        	    </button>
                                                        	    <button type="button" class="btnrating btn btn-default btn-lg" data-attr="3" id="rating-star-3">
                                                        	        <i class="fa fa-star" aria-hidden="true"></i>
                                                        	    </button>
                                                        	    <button type="button" class="btnrating btn btn-default btn-lg" data-attr="4" id="rating-star-4">
                                                        	        <i class="fa fa-star" aria-hidden="true"></i>
                                                        	    </button>
                                                        	    <button type="button" class="btnrating btn btn-default btn-lg" data-attr="5" id="rating-star-5">
                                                        	        <i class="fa fa-star" aria-hidden="true"></i>
                                                        	    </button>
                                                    	    </div>
                                                    	    <div class="numbr" >
                                                    	        <h2 class="bold rating-header" style="">
                                                    	    <span class="selected-rating">0</span><small> / 5</small>
                                                    	    </h2>
                                                    	    </div>
                                                        </div>
                                                        <div class="form-group">
                                							<label>Your Comment</label>
                                                            <textarea class="form-control" name="review" placeholder="Your review" style="height: 80px;" required ></textarea>
                                                        </div>
                                                        <div class="form-group text-center" style="margin-bottom:15px">
                                                            <button type="submit"  name="PostComment" class="blue_btn custom_btn">Post Comment</button>
                                                        </div>
                                                    </form>
                                				</div>
                                			</div>
                                			
                                		</div>
								<?php } ?>
										
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php include('template/footer.php'); ?>




<script>
    	jQuery(document).ready(function($){
	    
	$(".btnrating").on('click',(function(e) {
	
	var previous_value = $("#selected_rating").val();
	
	var selected_value = $(this).attr("data-attr");
	$("#selected_rating").val(selected_value);
	
	$(".selected-rating").empty();
	$(".selected-rating").html(selected_value);
	
	for (i = 1; i <= selected_value; ++i) {
	$("#rating-star-"+i).toggleClass('btn-warning');
	$("#rating-star-"+i).toggleClass('btn-default');
	}
	
	for (ix = 1; ix <= previous_value; ++ix) {
	$("#rating-star-"+ix).toggleClass('btn-warning');
	$("#rating-star-"+ix).toggleClass('btn-default');
	}
	
	}));
	
	
	
		
});

</script>
