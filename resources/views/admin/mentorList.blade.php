@extends('header.admin_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")
<style>
    .modal-backdrop {
        position: relative!important;
</style>
@section('maincontent')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper"><!-- HTML (DOM) sourced data -->
                <section id="html">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body ">
                                    <form class="form px-3 mt-2">
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="fa fa-th-list"></i> All Mentor List

                                            </h4>
                                        </div>
                                    </form>
                                    <div class="card-block card-dashboard table-responsive">
                                        <table class="table table-striped table-bordered sourced-data">
                                            <thead>
                                            <tr>
                                                <th>SNO</th>
                                                <th>Img</th>
                                                <th class="nowrap">Name</th>
                                                <th>E-mail</th>
                                                <th>Qualification</th>
                                                <th>Experience</th>
                                               <th>Country</th>
                                               <th>Password</th>
                                                <th>Status</th>
                                                <th>Action</th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(!empty($mentorList))
                                                @foreach($mentorList as $k => $mentor)
                                                    <tr>
                                                        <td>{{($k + 1)}}</td>
                                                        <td class="text-center">
                                                            <img alt="{{$mentor->profilePic}}" style="width:50px;height:50px"
                                                                 src="{!! profile_image(asset('profile-photo/'.$mentor->profilePic)) !!}"/>
                                                        </td>
                                                        <td class="nowrap">{{$mentor->firstName .' '.  $mentor->lastName}}</td>
                                                        <td>{{$mentor->email}}</td>
                                                       {{-- <td>{{($mentor->gender==1)?'Male':'Female'}}</td>--}}
                                                        <td>{{$mentor->qualification}}</td>
                                                      {{--  <td>{{$mentor->expertise}}</td>--}}
                                                        <td>{{$mentor->experience}} Years</td>
                                                       {{-- <td>{{$mentor->avgRatting}}</td>--}}
                                                        <td>{{$mentor->country}}</td>
                                                        <td>{{$mentor->ppassword}}</td>
                                                    {{-- <td>{{substr($mentor->addressOne, 0, 30)}}</td>--}}
                                                        <td class="text-center">{{($mentor->status == 1)?'Active':'InActive'}}</td>
                                                        <td class="text-center">
                                                            <a title="Edit"
                                                               href="{{url('admin/add-edit-mentor/'.custom_encode($mentor->id))}}">
                                                                <i class="fa fa-pencil-square-o" aria-hidden="true"
                                                                   style="font-size: 20px !important;"></i>
                                                            </a>&nbsp;
                                                            <a title="Course"
                                                               href="{{url('admin/all-syllabus/'.custom_encode($mentor->id))}}">
                                                                <i class="fa fa-eye" aria-hidden="true"
                                                                   style="font-size: 20px !important;"></i>
                                                            </a>
                                                        </td>


                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ HTML (DOM) sourced data -->
            </div>
        </div>
    </div>



    <!-- Modal -->

@endsection
@section('scripting')
    <script>
        $('.delete').click(function () {
            if (confirm("Are you sure!")) {
                var el = this;
                var id = this.id;
                var deleteid = id;
                $.ajax({
                    url: '{{url('admin/delete-mentor')}}',
                    type: 'GET',
                    data: {id: deleteid},
                    postType: "JSON",
                    success: function (response) {
                        if (response.error == false) {
                            $(el).closest('tr').css('background', 'tomato');
                            $(el).closest('tr').fadeOut(800, function () {
                                $(this).remove();
                            });
                        }
                    }
                });
            }
        });
</script>
@endsection
@section('footer')
    @include('footer.footer_admin')
@endsection
