@extends('header.admin_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")
<style>
    .modal-backdrop {
        position: relative !important;
</style>
@section('maincontent')
<div class="main-panel">
    <div class="main-content">
        <div class="content-wrapper"><!-- HTML (DOM) sourced data -->
            <section id="html">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body ">
                                <form class="form px-3 mt-2">
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="fa fa-th-list"></i> Skill Details

                                        </h4>
                                    </div>
                                </form>
                                <div class="card-block card-dashboard table-responsive">
                                    <table class="table table-striped table-bordered sourced-data">
                                        @if(!empty($skillList))
                                        <tbody>
                                       <tr>
                                            <th style="width: 15%">Profile Pic</th>
                                            <td><img alt="{{$skillList->profilePic}}" style="width:50px;height:50px"
                                                     src="{!! profile_image(asset('profile-photo/'.$skillList->profilePic)) !!}"/>
                                            </td>

                                        </tr>
                                       <tr>
                                            <th style="width: 15%">Name  </th>
                                            <td>{{$skillList->username}}</td>

                                        </tr>
                                        <tr>
                                            <th style="width: 15%">Category  </th>
                                            <td>{{$skillList->catname}}</td>

                                        </tr>
                                        <tr>
                                            <th style="width: 15%">Sub-Category  </th>
                                            <td>{{$skillList->subcatname}}</td>

                                        </tr>
                                        <tr>
                                            <th style="width: 15%">Skill  </th>
                                            <td>
                                                @foreach(json_decode($skillList->skills, true) as $skill)
                                                    <span class="text text-bold">{{$skill}},</span>&nbsp;&nbsp;
                                                @endforeach
                                            </td>

                                        </tr>
                                       <tr>
                                           <th style="width: 15%">Course Fee  </th>
                                           <td>{{$skillList->coursefee}}</td>

                                       </tr>

                                       <tr>
                                           <th style="width: 15%">Avg Ratting  </th>
                                           <td>
	                                           <?php $rates = $skillList->avgRatting; ?>
                                               <div class="rating_details">
                                                   <div class="rating_star_s">
                                                       @for($i=0; $i < 5; $i++)
                                                           @if($rates > $i)
                                                               @if(is_float($rates) && ($i+1) > $rates)
                                                                   <i class="fa fa-star-half-o"></i>
                                                               @else
                                                                   <i class="fa fa-star"></i>
                                                               @endif
                                                           @else
                                                               <i class="fa fa-star-o"></i>
                                                           @endif
                                                       @endfor
                                                   </div>
                                               </div>
                                           </td>

                                       </tr>
                                       <tr>
                                           <th style="width: 15%">Short Bio  </th>
                                           <td>{{$skillList->shortBio}}</td>

                                       </tr>
                                       <tr>
                                           <th style="width: 15%">Description  </th>
                                           <td>{!! $skillList->description !!}</td>

                                       </tr>


                                        </tbody>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ HTML (DOM) sourced data -->
        </div>
    </div>
</div>



<!-- Modal -->

@endsection
@section('scripting')
@endsection
@section('footer')
@include('footer.footer_admin')
@endsection
