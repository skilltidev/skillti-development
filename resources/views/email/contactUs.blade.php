<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div class="emailer" style="max-width: 500px; width: 100%; margin: 0 auto; border: 1px solid #ccc; padding: 0 0 20px; box-shadow: 0 1px 2px rgba(0,0,0,0.5), 0 5px 2px rgba(0,0,0,0.2);font-size:16px;line-height:24px">
            <h1 style=" padding: 20px 0; margin:0; width: auto;  font-size: 20px; text-align: center; border-bottom: 1px solid #ccc; background: #f6f9fc; ">SKILLTI APPLICATION </h1>
            <p style="margin-left:20px;"><b>Dear {{$input['name']}},</b></p>
            <p style="margin-left:20px;"><label>Email : <b>{{$input['email']}}</b></label></p>
            <p style="margin-left:20px;"><label>Subject : <b>{{$input['subject']}}</b></label></p>
            <p style="margin-left:20px;"><label>Message : <b>{{$input['comment']}}</b></label></p>
            <p style="margin-left:20px;"><label>Thank you for contacting us, We will get in touch with you shortly.</label></p>
            <p style='margin-left:20px;'>Best Regards, </p><p style='margin-left:20px;'>SkillTI Team</p></br>
            <p style="margin: 30px 0;padding: 0 20px">We will be here to help you with any step along the way. You can find answers to most questions and get in touch with us on our <a href="#">support site</a>.</p>
        </div>
    </body>
</html>