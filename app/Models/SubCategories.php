<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategories extends Model
{
	/**
	 * Indicates mysql connection index
	 * @var string
	 */
	protected $connection = 'mysql';
	/**
	 * Indicates mysql table name
	 * @var string
	 */
	protected $table = 'subCategories';
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;

	public static function subCategoryDetails($catId = 0) {
		if($catId == 0) {
			$subcategory = SubCategories::select('subCategories.*', 'categories.name as categoryName', 'categories.description as ctegoryDesc')
				->join('categories', 'categories.id', 'subCategories.catId')
				->get();
		}else{
			$subcategory = SubCategories::select('subCategories.*', 'categories.name as categoryName', 'categories.description as ctegoryDesc')
				->join('categories', 'categories.id', 'subCategories.catId')
				->where('categories.id', $catId)
				->get();
		}
		return $subcategory;
	}
}
