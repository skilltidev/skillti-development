<?php if ( ! defined('DOCROOT_PATH')) exit('No direct script access allowed');
class ss_db extends mysqli 
{	
	public $conn = NULL;
	public $connect = NULL;
	private $onError = "";
	private $queryfail = "Cannot continue with the operation.";	
	
	public function __Construct() 
	{
		$this->conn = mysqli_connect (DB_HOST, DB_USER, DB_PASS, DB_NAME);
		mysqli_set_charset($this->conn,"utf8");
		
		$this->onError = IS_LOCAL;
        if (mysqli_connect_error()) {
            die(($this->onError)?('Connect Error (' . mysqli_connect_errno() . ')'.mysqli_connect_error()):$this->queryfail);
        }
    }
	
	function getDbConnection(){
		return $this->conn;
	}
	
	
   public function insert_id(){
   return $this->conn->insert_id;  
}
	
	public function connect_close()
	{
		mysqli_close($this->conn);
	}	
	
	/******************************************
	Function Name : formatColumnValue
	Return type : Associative array
	Comments : Add slashes in column values. It takes one argument as a parameter. Used in insert() and update().
	User Instruction : $objDb->formatColumnValue($columnVals)
	******************************************/
	function formatColumnValue($columnVals,$quote = TRUE)
	{
	  if(is_array($columnVals))
	  {
		   foreach($columnVals  as $key => $val)
		   {
			    if(is_array($val)){
					$val_format = array();
					foreach($val as $value){
						if($quote === TRUE)
							$val_format[] = "'".mysqli_real_escape_string($this->conn,$value)."'";
						else
							$val_format[] = mysqli_real_escape_string($this->conn,$value);
					}
					$columnVals[$key] = $val_format;
			    }
				else{
					if($quote === TRUE)
						$columnVals[$key] = "'".mysqli_real_escape_string($this->conn,$val)."'";
					else
						$columnVals[$key] = mysqli_real_escape_string($this->conn,$val);
				}
			    
		   } 
	  }
	  else
	  {
			if($quote === TRUE)
				$columnVals = "'".mysqli_real_escape_string($this->conn,$columnVals)."'";
			else
				$columnVals = mysqli_real_escape_string($this->conn,$columnVals);
	  }
	  
	  return $columnVals;
	}
	
	
	
	/******************************************

	Function Name : insert
	Return type : Integer
	Comments : Used to insert values in a table. It takes table name and assoc column array as a parameter.
	User Instruction : $objDb->insert($tableName, $arrColumns)

	******************************************/
	function insert($tableName, $arrColumns)
	{
		$columnsKeys = join(", ", array_keys($arrColumns));
		$values = join(", ", array_values($arrColumns));
		$sql = "INSERT INTO " . $tableName . " ($columnsKeys) VALUES ($values)";
		$query = $this->query($sql);
		if($query)
			return mysqli_insert_id($this->conn);
		else 
			return false;
	}
	

	/******************************************

	Function Name : update
	Return type : Integer
	Comments : Used to update values in a table. It takes table name,  assoc column array and where condition as a parameter.
	User Instruction : $objDb->update($tableName, $arrColumns, $where)

	******************************************/

	function update($tableName, $arrColumns, $where,$affected_rows = FALSE)
	{
		$arrStuff = array();
		if(!(is_array($arrColumns)))
			$this->notify('function :  update() <br> Error: Passed argument should be an array!', false, true);
		if($where=='')
			$this->notify('function :  update() <br> Error:  where condition is missing !', false, true);
		
		foreach($arrColumns as $key => $val)
			$arrStuff[] = "$key = ".$val."";
		
		$stuff = implode(", ", $arrStuff);
	        $sqlUpdate = 'UPDATE ' . $tableName . ' SET '. $stuff .' WHERE  ' . $where;
		$qrystatus = $this->query($sqlUpdate);
		if($affected_rows === TRUE)
		{
			if($this->conn->affected_rows>0)
				return 1;
			else
				return 0;
		}
		else if($this->conn->affected_rows>0 || ($this->conn->affected_rows==0 && $qrystatus))
			return 1;
		else
			return 0;
	}
	
	/******************************************
	Function Name : delete
	Return type : Integer value
	Comments : Used to delete records from a table. It takes table name and where condition as a parameter.
	******************************************/	
	function delete($tableName, $where)
	{
		if($where=='')
		{
			$this->notify('function :  delete() <br> Error: where condition is missing !', false, true);
		}
		$query="DELETE FROM " . $tableName . " WHERE " . $where;
		$varDel=$this->query($query);
		return  $this->conn->affected_rows;
	}	
	
	
	/******************************************

	Function Name : select
	Return type : Associative array
	Comments : Used to select values from a table. It takes table name and assoc column array, where, order by and limit as a parameter.
	User Instruction : $objDb->formatDbValue($tableName, $arrColumns='', $where='', $orderBy='' , $limit='')
	
	******************************************/	
	function select($tableName,$arrColumns='', $where='', $orderBy='' , $limit='',$resulttype = 'object',$distinct = FALSE)
	{	
		if(!(is_array($arrColumns)) )
			$this->notify('function :  select() <br> Error: Passed argument should be an array!', false, true);
		if(is_array($arrColumns)) 
			$arrColumns = implode(',',$arrColumns);			
		if($arrColumns=='*')
			 $this->notify('function :  select() <br> Error: Passed argument should be a column, no astric(*)!', false, true);
		if($where=='')
			$where = ' 1 ';
		if($orderBy!='')
			$orderBy = ' ORDER BY ' .	$orderBy;
		if($limit!='')
			$limit = ' LIMIT '.$limit;
		if($distinct===TRUE)
			$distinct = ' DISTINCT ';
		else
			$distinct = '';
			
         $query = 'SELECT '.$distinct.' '. $arrColumns . ' FROM '.$tableName . ' WHERE '. $where . $orderBy . $limit;  
	   
		if($resulttype == 'object'){
			return $this->getObjectResult($query);
		}
		else{
			return $this->getArrayResult($query);
		}
	}
	/******************************************

	Function Name : getArrayResult
	Return type : Associative array	
	Comments : Generate assoc array from record set. Used in selecting values from database. It takes query as a parameter.
	User Instruction : $objDb->getArrayResult($columnVals)

	******************************************/
	function getArrayResult($sql)
	{		
		$resultProvider =array();
	      $res = $this->query($sql);
		
		if($res)
		{
			while ($row = mysqli_fetch_assoc($res))
				$resultProvider[] = $row;
		}				
		return $resultProvider;  
	}
	
	function getObjectResult($sql)
	{		
		$resultProvider = array();
		$res = $this->query($sql);
		
		if($res)
		{
			while ($row = mysqli_fetch_object($res))
				$resultProvider[] = $row;
		}				
		return $resultProvider;  
	}
	
	/******************************************

	Function Name : query
	Return type : Record set
	Comments : Execute mysql query. It takes one argument as a parameter. If error redirect to notify page
	Calling function : getArrayResult();
	User instruction :$objDb->query($sql);

	******************************************/

	public function query($sql)
	{
		$result = mysqli_query($this->conn, $sql) or die(($this->onError)?mysqli_error($this->conn):$this->queryfail);
               // $result = mysqli_fetch_array($result);
		// return mysqli_insert_id($this->conn);
		return $result;

	}
	
	/******************************************
	Function Name : getNumRows
	Return type : Number
	Comments : Number of records in  a table . It takes three argument as a parameter.
	User Instruction : $objDb->getNumRows($argTableName, $argClmn, $argWhr='1')

	******************************************/	
	function getNumRows($argTableName, $argClmn, $argWhr = ''){	
		if($argWhr=="")
			$varWhr = ' 1 ';
		else
			$varWhr = $argWhr;
		
		$sqlNum = 'SELECT count('.$argClmn.') as numrows FROM ' . $argTableName .' Where '.$varWhr ;
		$varResult = mysqli_query($this->conn, $sqlNum) or die(($this->onError)?mysqli_error($this->conn):$this->queryfail);
		$resutlNum = mysqli_fetch_assoc($varResult);
		return $resutlNum['numrows'];
	}
	
	/******************************************

	Function Name : notify
	Return type : none
	Comments : Display error message to user .There is  three parameter error message , redirect and show last query.
	User Instruction : $objDb->notify($errMsg, $redirect = false, $showQuery = true)

	********************************************/	
	function notify($errMsg, $redirect = false, $showQuery = true)
	{	
		switch($this->onError)
		{
			case 1://1 will be when localhost
				if($showQuery)
					$errMsg = $errMsg . "<br/><br/>" . mysqli_error($this->conn);
				else
					$errMsg = $errMsg . "<br/><br/>";
				echo $errMsg;
				break;
			default :
				break;
		}
	}	
	
}//class ends
?>