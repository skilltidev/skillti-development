<div class="mdk-drawer js-mdk-drawer" id="default-drawer">
    <div class="mdk-drawer__content ">
        <div class="sidebar sidebar-left sidebar-dark bg-dark o-hidden" data-perfect-scrollbar>
            <div class="sidebar-p-y">
                <div class="sidebar-heading">APPLICATION</div>
                <ul class="sidebar-menu sm-active-button-bg">
                    @if(Auth::user()->roleId == 3)
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'dashboard')?'active':''}}">
                        <a class="sidebar-menu-button" href="{{url('student-dashboard')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">account_box</i> Student Dashboard
                        </a>
                    </li>
                    @elseif(Auth::User()->roleId == 2)
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'dashboard')?'active':''}}">
                        <a class="sidebar-menu-button" href="{{url('dashboard')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">school</i> Instructor Dashboard
                        </a>
                    </li>
                    @endif
                </ul>
                <ul class="sidebar-menu sm-active-button-bg">
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'mentee.mycourse')?'active':''}}">
                        <a class="sidebar-menu-button" href="{{url('student-dashboard/my-course')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">import_contacts</i> My Courses
                        </a>
                    </li>
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'mentor.chatbot')?'active':''}}">
                        <a class="sidebar-menu-button" href="{{url('student-dashboard/messages')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">comment</i>Messages
                        </a>
                    </li>
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'mentee.mailbox')?'active':''}}">
                        <a class="sidebar-menu-button" href="{{url('student-dashboard/mail-box')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">mail_outline</i>Mailbox
                        </a>
                    </li>
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'mentee-profile-edit')?'active':''}}">
                        <a class="sidebar-menu-button" href="{{url('student-dashboard/mentee-profile-edit')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">account_box</i> Account Settings
                        </a>
                    </li>
                </ul>
                <div class="sidebar-heading">Settings</div>
                <ul class="sidebar-menu">
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'password-setting')?'active':''}}">
                        <a class="sidebar-menu-button" href="{{url('student-dashboard/password-setting')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">settings</i> Password Setting
                        </a>
                    </li>
                    <li class="sidebar-menu-item">
                        <a class="sidebar-menu-button" href="{{url('logout')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">lock_open</i> Logout
                        </a>
                    </li>
                </ul>
            </div>
            </div>
        </div>
    </div>
</div>