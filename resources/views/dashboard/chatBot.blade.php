@extends('header.dashboard_header')
@section('headtitle', "Dashboard")
@section('headdesc', "")
@section('maincontent')
    <div style="display: none;" id="Notpermission" class="mdk-drawer-layout__content page">
        <div data-push data-responsive-width="768px" class="mdk-drawer-layout js-mdk-drawer-layout">
            <div class="mdk-drawer-layout__content">
                <div class="row" id="chatNotification" style="margin: 50px; font-family: Arial; font-size: 16px; color: rgb(95, 95, 95); text-align: center;" data-gr-c-s-loaded="true">
                    <div id="notificationPermision" style="display: none;">
                        <p style="margin-top:10px; font-size:24px;">Notification Permission</p>
                        <p style="line-height:1.5;">Provide notification permission for chat with students <a id="permissionGrant" href="javascript:void(0)">Click here</a> to provide permission</p>
                    </div>
                    <div id="notificationPermisionError" style="">
                        <p style="margin-top:10px; font-size:24px;">Notification Permission denied</p>
                        <p style="line-height:1.5;">You are denied the notification permission, please allow this manualy for chat with mentee. If you are using crome broser then <a target="_blank" href="https://support.google.com/chrome/answer/3220216?co=GENIE.Platform%3DDesktop&amp;hl=en">Click here</a> to know more how to enable this. for mozilla firefox user
                            <a target="_blank" href="https://support.mozilla.org/en-US/kb/push-notifications-firefox#w_upgraded-notifications">Click here</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="permission" class="mdk-drawer-layout__content page">
        <div data-push data-responsive-width="768px" class="mdk-drawer-layout js-mdk-drawer-layout">
            <div class="mdk-drawer-layout__content">
                <div class="app-messages__container d-flex flex-column h-100 pb-4">
                    <div class="navbar navbar-light bg-white navbar-expand-sm navbar-shadow z-1">
                        <div class="container-fluid flex-wrap px-sm-0">
                            <div class="nav py-2">
                                <div class="nav-item align-items-center mr-3">
                                    <div class="mr-3">
                                        <div class="avatar avatar-online avatar-sm">
                                            <img src="{!! profile_image(asset('profile-photo'.Auth::user()->profilePic)) !!}" alt="people" class="avatar-img rounded-circle">
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column" style="max-width: 200px; font-size: 15px">
                                        <strong class="text-body">{{Auth::user()->firstName." ".Auth::user()->lastName}}</strong>
                                        <span class="text-muted text-ellipsis">{{(count($chattings) > 0)?count($chattings):'No any'}} Recent Chattings</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex pt-4" style="position: relative;" data-perfect-scrollbar>
                        <div class="container-fluid page__container">
                            {{--<div class="jumbotron">
                                <div class="d-flex align-items-center">
                                    <div class="mr-3">
                                        <div class="avatar avatar-xl">
                                            <img src="{{asset('')}}/assets/images/people/110/woman-5.jpg" alt="people" class="avatar-img rounded-circle">
                                        </div>
                                    </div>
                                    <div class="flex">
                                        <h4 class="mb-0">Michelle Smith</h4>
                                        <p class="text-muted mb-0">Personal Development Teacher since 2014</p>
                                    </div>
                                </div>
                            </div>--}}
                            <ul class="d-flex flex-column list-unstyled" id="messages">
                                @foreach($chattings as $c => $chat)
                                @if($chat->sender == 2)
                                <li class="message sender d-inline-flex">
                                    <div class="message__aside">
                                        <a href="javascript:void(0)" class="avatar">
                                            <img src="{!! profile_image(asset('profile-photo/'.Auth::user()->profilePic)) !!}" alt="people" class="avatar-img rounded-circle">
                                        </a>
                                    </div>
                                    <div class="message__body card">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center">
                                                <div class="flex mr-3">
                                                    <a href="javascript:void(0)" class="text-body"><strong>{{Auth::user()->firstName." ".Auth::user()->lastName}}</strong></a>
                                                </div>
                                                <div>
                                                    <small class="text-muted">{!! posted_ago($chat->created_at) !!}</small>
                                                </div>
                                            </div>
                                            <span class="text-black-70">{!! $chat->message !!}</span>
                                            @if($chat->attachment != '')
                                            <a target="_blank" href="{{asset('chatt/'.$chat->attachment)}}" class="media align-items-center mt-2 text-decoration-0 bg-white px-3">
                                                <span class="avatar mr-2">
                                                    <span class="avatar-title rounded-circle">
                                                        <i class="material-icons font-size-24pt">attach_file</i>
                                                    </span>
                                                </span>
                                                <span class="media-body" style="line-height: 1.5">
                                                    <span class="text-primary">{{$chat->attachment}}</span><br>
                                                </span>
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                </li>
                                @endif
                                @if($chat->sender == 3)
                                <li class="message d-inline-flex">
                                    <div class="message__aside">
                                        <a href="javascript:void(0)" class="avatar">
                                            <img src="{!! profile_image(asset('profile-photo/'.$chat->profilePic)) !!}" alt="people" class="avatar-img rounded-circle">
                                        </a>
                                    </div>
                                    <div class="message__body card">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center">
                                                <div class="flex mr-3">
                                                    <a href="javascript:void(0)" class="text-body"><strong>{{$chat->firstName." ".$chat->lastName}}</strong></a>
                                                </div>
                                                <div>
                                                    <small class="text-muted">{!! posted_ago($chat->created_at) !!}</small>
                                                </div>
                                            </div>
                                            <span class="text-black-70">{!! $chat->message !!}</span>
                                            @if($chat->attachment != '')
                                                <a target="_blank" href="{{asset('chatt/'.$chat->attachment)}}" class="media align-items-center mt-2 text-decoration-0 bg-white px-3">
                                        <span class="avatar mr-2">
                                            <span class="avatar-title rounded-circle">
                                                <i class="material-icons font-size-24pt">attach_file</i>
                                            </span>
                                        </span>
                                                    <span class="media-body" style="line-height: 1.5">
                                            <span class="text-primary">{{$chat->attachment}}</span><br>
                                        </span>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </li>
                                @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @if($courseId)
                    <div class="container-fluid page__container">
                        <form action="{{url('dashboard/chat-bot/send-message')}}" method="post" enctype="multipart/form-data" id="message-reply">
                            @csrf
                            <input type="hidden" name="courseId" id="courseId" value="{{custom_encode($courseId)}}">
                            <div class="input-group input-group-merge">
                                <input type="text" name="message" class="form-control form-control-appended" autofocus="" required="" placeholder="Type message">
                                <div class="input-group-append">
                                    <div class="input-group-text pl-0">
                                        <div class="custom-file custom-file-naked d-flex" style="width: 24px; overflow: hidden;">
                                            <input type="file" name="file" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" style="color: inherit;" for="customFile">
                                                <i class="material-icons">attach_file</i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group-text pl-0">
                                        <button class="btn btn-flush" id="send" type="submit"><i class="material-icons">send</i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    @endif
                </div>
            </div>
            <div class="mdk-drawer js-mdk-drawer" data-align="end" id="messages-drawer">
                <div class="mdk-drawer__content top-0">
                    <div class="sidebar sidebar-right sidebar-light bg-white o-hidden">
                        <div class="d-flex flex-column h-100">
                            <div class="flex" data-perfect-scrollbar>
                                <div class="sidebar-heading">&nbsp;</div>
                                <div class="sidebar-heading d-flex">
                                    <div class="flex text-muted">{{(count($coureses) > 0)?count($coureses):"No any"}} Courses</div>
                                </div>
                                <ul class="list-group list-group-fit mb-3">
                                    @foreach($coureses as $k => $course)
                                        <li class="list-group-item px-4 py-3 {!! ($course->id == $courseId)?"bg-light":"" !!}">
                                            <a href="{{url('dashboard/chat-bot/'.custom_encode($course->id))}}" class="d-flex align-items-center position-relative">
                                            <span class="avatar avatar-sm {!! ($course->courseStatus == 0)?"avatar-online":"avatar-offline" !!} mr-3 flex-shrink-0">
                                                <img src="{!! profile_image(asset('profile-photo/'.$course->profilePic)) !!}" alt="Avatar" class="avatar-img rounded-circle">
                                            </span>
                                            <span class="flex d-flex flex-column" style="max-width: 175px;">
                                                <strong class="text-body">{{$course->firstName." ".$course->lastName}}</strong>
                                                <small><strong>Course: </strong> &nbsp;{{$course->subCatName}}</small>
                                            </span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('menu')
    @include('dashboard.menu')
@endsection
@section('scripting')
    <script id="template-message" type="text/javascript">
        $(document).on('submit', '#message-reply', function (e) {
            e.preventDefault();
            $('input[name=message], input[name=file], #send').attr('disabled', 'disabled');
            form = $(this)[0];
            var form_data = new FormData();
            if($('input[type=file]')[0].files[0]) {
                form_data.append('file', $('input[type=file]')[0].files[0]);
            }
            form_data.append("_token", '{{csrf_token()}}');
            form_data.append("courseId", '{{custom_encode($courseId)}}');
            form_data.append("message", $('input[name=message]').val());
            $.ajax({
                url: form.action,
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (response) {
                    if(response != '') {
                        $('#messages').append(response);
                        scroll();
                    }
                    form.reset();
                    $('input[name=message], input[name=file], #send').removeAttr('disabled');
                    $('input[name=message]').focus();
                }
            });
        });
    </script>
@endsection
@section('footer')
    @include('footer.footer_dashboard')
@endsection
