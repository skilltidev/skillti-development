<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Chattings;
use App\Models\Course;
use App\Models\Messages;
use App\Models\Skills;
use App\Models\SocialLinks;
use App\Models\SubCategories;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth,Hash,DB;
use Validator;


class MentorController extends Controller
{
	public function dashboard() {
		$skills = Skills::where('userId', Auth::user()->id)->where('status', 1)->count();
		$students = Course::where('mentorId', Auth::user()->id)->groupBy('menteeId')->count();
		$transaction = Course::where('mentorId', Auth::user()->id)->where('paymentStatus', 1)->sum('paymentAmount');
		$mailbox = Messages::where('mentorId',  Auth::user()->id)->where('sender', 3)->where('isRead', 0)->count();
		$data = compact('skills', 'students', 'transaction', 'mailbox');
		return view('dashboard.dashboard', $data);
	}

	public function addSyllabus(Request $request) {
		$skillId = $request->skillId;
		$skills = array();
		if($skillId != '') {
			$skills = Skills::where('id', custom_decode($skillId))->first();
			//debug($skills, false);
		}
		$category = Categories::where('status', 1)->get();
		return view('dashboard.addSyllabus', compact('category', 'skills'));
	}

	public function getCategory(Request $request) {
		$category = SubCategories::where('status', 1)->where('catId', $request->category)->get();
		return view('dashboard.subCategoryList', compact('category'));
	}

	public function addEditSkill(Request $request) {
		$input = $request->input();
		$skills = count($input['skill']);
		$rules = array(
			'category' => 'required|numeric',
			'subCategory' => 'required_with:category|numeric',
			'fee' => 'required|numeric',
			'shortBio' => 'required|max:150',
			'discription' => 'required',
			'skill.*' => 'required',
		);
		$v = Validator::make($input, $rules);
		if($v->passes()) {
			if(isset($input['id']) && $input['id'] != '') {
				$skil = Skills::where('id', custom_decode($input['id']))->first();
			}else{
				$skil = new Skills();
			}
			$skil->userId = Auth::user()->id;
			$skil->catId = $input['category'];
			$skil->subCatId = $input['subCategory'];
			$skil->coursefee = $input['fee'];
			$skil->skills = json_encode($input['skill']);
			$skil->description = $input['discription'];
            $skil->shortBio = $input['shortBio'];

			if($skil->save()) {
				if(isset($input['id']) && $input['id'] != '') {
					return back()->with('message', 'Course successfully updated.');
				}else{
					return back()->with('message', 'Course successfully added.');
				}
			}else{
				return back()->withInput()->with('errorMessage', 'Something went wrong please try again!');
			}
		}else{
			return back()->withInput()->withErrors($v);
		}
	}

	public function syllabus(Request $request) {
		$skills = Skills::getAllSkills(Auth::user()->id);
		return view('dashboard.syllabus', compact('skills'));
	}

	public function deleteSyllabus(Request $request) {
		$skillId = $request->skillId;
		$courese = Skills::where('id', custom_decode($skillId))->first();
		if($courese->delete()) {
			return back()->with('message', 'Course successfully deleted.');
		}else{
			return back()->with('message', 'Something went wrong please try again!');
		}
	}

	public function mentorProfileEdit(Request $request) {
		$profile = User::select('users.*', 'socialLinks.facebook', 'socialLinks.twitter', 'socialLinks.linkedIn', 'socialLinks.github', 'socialLinks.other')
			->leftjoin('socialLinks', 'socialLinks.userId', 'users.id')
			->where('users.id', Auth::user()->id)->first();
		if(isset($profile->ppassword)) {
			unset($profile->ppassword);
		}
		return view('dashboard.mentorProfileSeting', compact('profile'));
	}

	public function mentorProfile(Request $request) {
		$profile = Auth::user();
		$course = Skills::getAllSkills(Auth::user()->id);
		return view('dashboard.mentorProfile', compact('profile', 'course'));
	}

	public function mentorProfileEditSave(Request $request) {
		$inputs = $request->input();
		$this->validate($request, array(
			'firstName' => 'required|max:100',
			'lastName' => 'required|max:100',
			'profilePic' => 'image|mimes:jpeg,png,jpg|max:2048',
			'documents' => 'mimes:pdf|max:4096',
		));
		//debug($inputs, false);
		$image = $request->file('profilePic');
		$document = $request->file('documents');

		$inputs['profilePic'] = '';
		$inputs['varificationDoc'] = '';

		$profile = User::where('id', Auth::user()->id)->first();
		$social = SocialLinks::where('userId', Auth::user()->id)->first();
		if(empty($social)) {
			$social = new SocialLinks();
		}
		if(!empty($social)) {
			$social->userId = Auth::user()->id;
			$social->save();
		}

		if($inputs['facebook'] != '') {
			$social->facebook = $inputs['facebook'];
			$social->save();
		}
		if($inputs['twitter'] != '') {
			$social->twitter = $inputs['twitter'];
			$social->save();
		}
		if($inputs['linkedIn'] != '') {
			$social->linkedIn = $inputs['linkedIn'];
			$social->save();
		}
		if($inputs['github'] != '') {
			$social->github = $inputs['github'];
			$social->save();
		}
		if($inputs['other'] != '') {
			$social->other = $inputs['other'];
			$social->save();
		}
		if(!empty($social)) {
			$social->userId = Auth::user()->id;
			$social->save();
		}

		if($inputs['firstName'] != '') {
			$profile->firstName = $inputs['firstName'];
		}

		if (!empty($image)) {
			$inputs['imagename'] = time().str_replace(' ', '-', $profile->firstName).".".$image->getClientOriginalExtension();
			$destinationPath = public_path('/profile-photo');
			$image->move($destinationPath, $inputs['imagename']);
			$inputs['profilePic'] = $inputs['imagename'];
		}

		if($inputs['profilePic'] != '') {
			$profile->profilePic = $inputs['profilePic'];
		}

		if (!empty($document)) {
			$inputs['imagename'] = time().str_replace(' ', '-', $profile->firstName).".".$document->getClientOriginalExtension();
			$destinationPath = public_path('/profile-doc');
			$document->move($destinationPath, $inputs['imagename']);
			$inputs['varificationDoc'] = $inputs['imagename'];
		}

		if($inputs['varificationDoc'] != '') {
			$profile->varificationDoc = $inputs['varificationDoc'];
		}

		if($inputs['lastName'] != '') {
			$profile->lastName = $inputs['lastName'];
		}
		if($inputs['qualification'] != '') {
			$profile->qualification = $inputs['qualification'];
		}
		if($inputs['exprience'] != '') {
			$profile->experience = $inputs['exprience'];
		}
		if($inputs['tagLine']) {
			$profile->tagline = $inputs['tagLine'];
		}
		if($inputs['about'] != '') {
			$profile->about = $inputs['about'];
		}

		if($inputs['discription'] != '') {
			$profile->description = $inputs['discription'];
		}
		if($inputs['addressOne'] != '') {
			$profile->addressOne = $inputs['addressOne'];
		}
		if($inputs['addressTwo'] != '') {
			$profile->addressTwo = $inputs['addressTwo'];
		}
		if($inputs['country'] != '') {
			$profile->country = $inputs['country'];
		}
		if($inputs['state'] != '') {
			$profile->state = $inputs['state'];
		}
		if($inputs['city'] != '') {
			$profile->city = $inputs['city'];
		}
		if($inputs['zip'] != '') {
			$profile->zip = $inputs['zip'];
		}


		if($profile->save()) {
			return back()->with('message', 'Profile successfully update.');
		}else{
			return Redirect::back()->withInput()->with('errorMessage', 'Something went wrong please try again!');
		}
	}

	public function passwordSetting() {
		return view('dashboard.mentorPassword');
	}

	public function passwordSettingPost(Request $request) {
		$inputs = $request->input();
		$this->validate($request, array(
			'oldPassword' =>  [function ($attribute, $value, $fail) {
				if(Auth::user()->ppassword != $value) {
					$fail('Old password wrong or not matched!');
				}
			}],
			'password' =>  ['required',
				'min:6',
				'regex:/^([a-zA-Z0-9@*#]{6,15})$/',
				'confirmed'
			],
		));
		$ppassword = $inputs['password'];
		$password = Hash::make($ppassword);
		$user = User::where('id', Auth::user()->id)->first();
		$user->password = $password;
		$user->ppassword = $ppassword;
		if ($user->save()) {
			Auth::logout();
			return Redirect::to('signin')->with('errorMessage', 'Your password have been changed login again with new password!');
		} else {
			return back()->with('errorMessage', 'Sum error found please try again..')->withInput();
		}
	}

	/*************caht start here*********/

    public function chatBot(Request $request) {
	    $userId = Auth::user()->id;
	    $courseId = custom_decode($request->courseId);

	    $coureses = Course::select('course.courseStatus', 'users.profilePic', 'users.firstName', 'users.lastName', 'course.id', 'course.courseId', 'categories.name as catName', 'categories.logo', 'subCategories.name as subCatName')
		    ->join('skills', 'skills.id', 'course.courseId')
		    ->join('categories', 'categories.id', 'skills.catId')
		    ->join('subCategories', 'subCategories.id', 'skills.subCatId')
		    ->join('users', 'users.id', 'course.menteeId')
		    ->where('course.mentorId', $userId)
		    ->orderBy('course.courseStatus')
		    ->get();

    	$chattings = Chattings::select('chattings.message', 'chattings.sender', 'chattings.attachment', 'chattings.created_at', 'users.firstName', 'users.lastName', 'users.profilePic')
		    ->leftJoin('users', 'users.id', 'chattings.menteeId')
		    ->where('chattings.mentorId', Auth::user()->id)
		    ->where('chattings.status', 1)
		    ->orderBy('chattings.id', 'desc')
		    ->when($courseId, function ($q, $courseId) {
			    return $q->where('chattings.courseId', $courseId);
		    })
		    ->limit(50)
		    ->get();
    	if(!empty($chattings) && count($chattings) > 0) {
    		$chattings = $chattings->reverse();
	    }
        return view('dashboard.chatBot', compact('coureses', 'chattings', 'courseId'));
    }

	public function saveToken(Request $request) {
		$inputs = $request->input();
		$user = User::where('id', Auth::user()->id)->first();
		$user->fcm_tocken = $inputs['token'];
		if($user->save()) {
			echo 'true'; die;
		}
		echo 'false'; die;
	}

    public function sendMessage(Request $request) {
    	$inputs = $request->input();
    	$courseId = custom_decode($inputs['courseId']);
    	$message = $inputs['message'];
	    $file = $request->file('file');
	    $inputs['attachment'] = '';
	    if (!empty($file)) {
		    $inputs['imagename'] = time().$file->getClientOriginalName();
		    $destinationPath = public_path('/chatt');
		    $file->move($destinationPath, $inputs['imagename']);
		    $inputs['attachment'] = $inputs['imagename'];
	    }
    	$course = Course::select('mentorId', 'menteeId')->where('id', $courseId)->first();
    	if(!empty($course)) {
		    $chatting = new Chattings();
		    $chatting->mentorId = Auth::user()->id;
		    $chatting->courseId = $courseId;
		    $chatting->menteeId = $course->menteeId;
		    $chatting->message = $message;
		    $chatting->attachment = $inputs['attachment'];
		    $chatting->sender = 2;
		    $chatting->status = 1;
		    if($chatting->save()) {
		    	$mentee = User::select('fcm_tocken', 'id')->where('id', $chatting->menteeId)->first();
		    	$senderName = Auth::user()->firstName." ".Auth::user()->lastName;
		    	if(!empty($mentee) && $mentee->fcm_tocken != '') {
				    $payload = [
					    'registration_ids' 	=> [$mentee->fcm_tocken],
					    'data'				=> [
						    'title' => $senderName. " Send a message",
						    'body' => $chatting->message,
						    'icon' => asset('images/skillti_logo_4_update-fav_icon.png'),
						    'image' => asset('images/skillti_update.png'),
						    'attachment' => asset('chatt/'.$chatting->attachment),
						    'attachmentName' => $chatting->attachment,
						    'courseId' => $chatting->courseId,
						    'menteeId' => $chatting->menteeId,
						    'mentorId' => $chatting->mentorId,
						    'sender' => 2,
						    'sendorNeme' => $senderName,
						    'profilePic' => asset('profile-photo/'.Auth::user()->profilePic),
						    'date' => posted_ago($chatting->created_at),
					    ],
					    'notification' => [
						    'click_action' => url('student-dashboard/my-course/start-chat/'.custom_encode($chatting->courseId)),
						    'title' => $senderName. " Send a message",
						    'body' => $chatting->message,
						    'icon' => asset('images/skillti_logo_4_update-fav_icon.png'),
						    'image' => asset('images/skillti_update.png'),
					    ]
				    ];
				    $response = $this->fcmMessage($payload);
				    //debug($response);
			    }
			    return view('dashboard.message', compact('chatting'));
		    }
	    }
    	return "";
    }

    private function fcmMessage($payload) {
	    $curl = curl_init();
	    curl_setopt_array($curl, array(
		    CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_CUSTOMREQUEST => "POST",
		    CURLOPT_POSTFIELDS => json_encode( $payload ),
		    CURLOPT_HTTPHEADER => [
			    'Authorization: Key=' . env('SERVER_API_KEY'),
			    'Content-Type: Application/json'
		    ]
	    ));
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);
	    if ($err) {
		    return false;
	    } else {
		    return $response;
	    }
    }
	/*************caht start here*********/

	public function mailbox() {
		$mails = Messages::select(DB::raw('CONCAT(users.firstName, " ", users.lastName) AS menteeName'), 'users.profilePic', 'messages.*')
			->join('users', 'users.id', 'messages.menteeId')
			->where('sender', 3)
			->orderBy('id', 'desc')
			->limit(50)->get();
		if(!empty($mails) && count($mails) > 0) {
			foreach ($mails as $key => $mail) {
				$replay = Messages::select('messages.*')
					->where('sender', 2)
					->where('messageId', $mail->id)
					->get();
				if(!empty($replay) && count($replay) > 0) {
					$mail->replay = $replay;
				}else{
					$mail->replay = array();
				}
			}
		}
		$messages = $mails;
		return view('dashboard.mailbox', compact( 'messages'));
	}

	public function mailboxReplay(Request $request) {
		$inputs = $request->input();
		$this->validate($request, array(
			'replay' =>  'required|min:150|max:500',
		));
		$message = Messages::where('id', custom_decode($inputs['messageId']))->first();
		if (!empty($message)) {
			$message->isRead = 1;
			$message->save();
			$replay = new Messages();
			$replay->mentorId = $message->mentorId;
			$replay->menteeId = $message->menteeId;
			$replay->message = $inputs['replay'];
			$replay->sender = 2;
			$replay->messageId = custom_decode($inputs['messageId']);
			$replay->save();
			return back()->with('messsage', 'Your replay successfully sent.');
		} else {
			return back()->with('errorMessage', 'Sum error found please try again..')->withInput();
		}
	}

}
