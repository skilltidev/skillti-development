@extends('header.admin_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")
<style>
    .modal-backdrop {
        position: relative !important;
</style>
@section('maincontent')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper"><!-- HTML (DOM) sourced data -->
                <section id="html">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body ">
                                    <form class="form px-3 mt-2">
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="fa fa-th-list"></i>Mentee Reviews</h4>
                                        </div>
                                    </form>
                                    <div class="card-block card-dashboard table-responsive">
                                        <table class="table table-striped table-bordered sourced-data">
                                            <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Mentor</th>
                                                <th>Mentee</th>
                                                <th>Rate</th>
                                                <th>Comment</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($rattings))
                                                @foreach($rattings as $k => $rate)
                                                    <tr>
                                                        <td>{{($k + 1)}}</td>
                                                        <td  class="nowrap">
                                                            <a target="_blank" href="{{url('admin/add-edit-mentor/'.custom_encode($rate->mentorId))}}">{{$rate->mentorName}}</a>
                                                        </td>
                                                        <td  class="nowrap">
                                                            <a target="_blank" href="{{url('admin/add-edit-mentee/'.custom_encode($rate->menteeId))}}">{{$rate->menteeName}}</a>
                                                        </td>
                                                        <td class="nowrap">
                                                            <?php $rates = $rate->rating; ?>
                                                            <div class="rating_details">
                                                                <div class="rating_star_s">
                                                                    @for($i=0; $i < 5; $i++)
                                                                        @if($rates > $i)
                                                                            @if(is_float($rates) && ($i+1) > $rates)
                                                                                <i class="fa fa-star-half-o"></i>
                                                                            @else
                                                                                <i class="fa fa-star"></i>
                                                                            @endif
                                                                        @else
                                                                            <i class="fa fa-star-o"></i>
                                                                        @endif
                                                                    @endfor
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            {{$rate->comment}}
                                                        </td>
                                                        <td class="nowrap">{{format_date($rate->created_at)}}</td>
                                                        <td class="nowrap"><a onclick="return confirm('Do you want to change course sattus?');" id="changeStatus" href="{{url('admin/changes-status-rate/'.custom_encode($rate->id))}}">{{($rate->status == 1)?'Active':'InActive'}}</a></td>
                                                        <td class="text-center nowrap">
                                                            @if($rate->skillId != '')
                                                            <a target="_blank" title="All purched courses" href="{{url('admin/all-skill/'.custom_encode($rate->skillId))}}">
                                                                <i class="fa fa-eye" aria-hidden="true"
                                                                   style="font-size: 20px !important;"></i>
                                                            </a>
                                                            @else
                                                            - -
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ HTML (DOM) sourced data -->
            </div>
        </div>
    </div>



    <!-- Modal -->

@endsection
@section('scripting')
@endsection
@section('footer')
    @include('footer.footer_admin')
@endsection
