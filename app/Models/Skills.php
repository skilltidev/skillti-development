<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Skills extends Model
{
	/**
	 * Indicates mysql connection index
	 * @var string
	 */
	protected $connection = 'mysql';
	/**
	 * Indicates mysql table name
	 * @var string
	 */
	protected $table = 'skills';
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;

	public static function getAllSkills($userId) {
		$skills = Skills::select('skills.*', 'categories.name as catName', 'categories.logo', 'categories.link as catlink', 'subCategories.name as subCatName', 'subCategories.link as subCatlink')
			->join('categories', 'categories.id', 'skills.catId')
			->join('subCategories', 'subCategories.id', 'skills.subCatId')
			->where('skills.userId', $userId)
			->get();
		if(!empty($skills) && count($skills) > 0) {
			return $skills;
		}
		return false;
	}
}
