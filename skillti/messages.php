<?php 
	include('template/header.php'); 
	protect_admin_page();
	$userID = $_SESSION['USER_ID'];
	$RoleID = $_SESSION['USER_ROLE'];
	$name = $_SESSION['USER_FULL_NAME'];
	
?>
<style>
    #chalm li:first-child {
        background-color: #f5f5f5;
            border: none !important;
    }
    
</style>

<section class="mid_content dashboard_content">
	<div class="container-fluid pl-0">
		<div class="row">
			<div class="col-xs-12 col-sm-3">
			<?php include ('template/sidebar.php');?>
			</div>
			
			<div class="col-sm-9 col-xs-12 pl-0">
			<div class="row">
			    <div id="needRef">
				<div class="col-xs-12 col-sm-4">
					<!-- required for floating -->
					<!-- Nav tabs -->
					<ul class="nav nav-tabs tabs-left message_sec" id="chalm">
					    <?php
    					    if($RoleID == 2) {
        						//$Query = "select * from messagedata where MID in (Select Max(MID) from messagedata where SenderID = '$userID' group by RecieverID ) order by MID desc";
        						//$Query = "select * from messagedata where MID in (Select MID from messagedata where RecieverID = '34' group by SenderID order by MID DESC)";
        						$Query = "Select * from messagedata 
                                        where MID in (
                                        Select Max(MID) from 
                                        (
                                         Select Max(MID) as MID,RecieverID as receiverID,SenderID as senderID from messagedata where RecieverID = '$userID' group by RecieverID,SenderID
                                        	Union All 
                                        	Select Max(MID) as MID,SenderID as receiverID,RecieverID as receiverID from messagedata where SenderID = '$userID' group by SenderID,RecieverID
                                        
                                        ) v1 
                                        group by v1.senderID,v1.receiverID
                                            ) order by MID desc";
    					    } else if($RoleID == 3) {
    					        $Query = "Select * from messagedata 
                                        where MID in (
                                        Select Max(MID) from 
                                        (
                                         Select Max(MID) as MID,RecieverID as receiverID,SenderID as senderID from messagedata where RecieverID = '$userID' group by RecieverID,SenderID
                                        	Union All 
                                        	Select Max(MID) as MID,SenderID as receiverID,RecieverID as receiverID from messagedata where SenderID = '$userID' group by SenderID,RecieverID
                                        
                                        ) v1 
                                        group by v1.senderID,v1.receiverID
                                            ) order by MID desc";
    					    }
    						$Result = $dbObject->query($Query);
    						$i = 1;
    						while($Row=mysqli_fetch_assoc($Result)) {
    						    if($RoleID == 3) {
    						        
    						       // $UserID = $Row['RecieverID'];
    						        if($Row['RecieverID'] == $userID) {
    						            $UserID = $Row['SenderID']; 
    						        } else {
    						            $UserID = $Row['RecieverID']; 
    						        }
    						        
    						    } else if($RoleID == 2) {
    						        if($Row['RecieverID'] == $userID) {
    						            $UserID = $Row['SenderID']; 
    						        } else {
    						            $UserID = $Row['RecieverID']; 
    						        }
    						    
    						    }
    						    $UQuery = "select FirstName,LastName,profilePic from users where UserID='$UserID'";
    						    $UResult = $dbObject->query($UQuery);
    						    $URow=mysqli_fetch_assoc($UResult);
    						    if($i == 1) { 
    							
    					?>
						<li >
							<a href="#?>" data-toggle="tab" class="mentor"  id="<?php echo $UserID;?>">
								<div class="msg_display">
									<div class="user_name_dis"> <img src="assets/profile-photo/<?php echo $URow['profilePic'];?>" alt=""> </div>
									<div class="msg_disc">
										<div class="msg_disc_head">
											<div class="pull-left">
												<h4><?php echo $URow['FirstName']." ".$URow['LastName'];?></h4>
											</div>
											<div class="pull-right">
												<h5>4:14 AM</h5>
											</div>
										</div>
										<h3><?php echo substr($Row['Message'], 0, 35);?>...</h3>
									</div>
								</div>
							</a>
						</li>
						<?php } else { ?>
						<li >
							<a href="#" data-toggle="tab" class="mentor" id="<?php echo $UserID;?>">
								<div class="msg_display">
									<div class="user_name_dis"> <img src="assets/profile-photo/<?php echo $URow['profilePic'];?>" alt=""> </div>
									<div class="msg_disc">
										<div class="msg_disc_head">
											<div class="pull-left">
												<h4><?php echo $URow['FirstName']." ".$URow['LastName'];?></h4>
											</div>
											<div class="pull-right">
												<h5>4:14 AM</h5>
											</div>
										</div>
										<h3><?php echo substr($Row['Message'], 0, 35);?>...</h3>
									</div>
								</div>
							</a>
						</li>
						<?php } $i++; } ?>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-8 pl-0">
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="ChatWindow">
							<div class="message_container test2" id="msgsdiv">
								<div class="msg_display height_auto grey_bg">
								    <?php
								    if($RoleID == 2) {
                						//$Query2 = "select SenderID from messagedata where MID in (Select Max(MID) from messagedata where RecieverID = '$userID' group by RecieverID ) order by MID desc";
            					        $Query2 = "Select RecieverID,SenderID from messagedata 
                                        where MID in (
                                        Select Max(MID) from 
                                        (
                                         Select Max(MID) as MID,RecieverID as receiverID,SenderID as senderID from messagedata where RecieverID = '$userID' group by RecieverID,SenderID
                                        	Union All 
                                        	Select Max(MID) as MID,SenderID as receiverID,RecieverID as receiverID from messagedata where SenderID = '$userID' group by SenderID,RecieverID
                                        
                                        ) v1 
                                        group by v1.senderID,v1.receiverID
                                            ) order by MID desc";
            					   
            					    } else if($RoleID == 3) {
            					        $Query2 = "select RecieverID from messagedata where MID in (Select Max(MID) from messagedata where SenderID = '$userID' group by RecieverID ) order by MID desc";
            					    }
								        	$Result2 = $dbObject->query($Query2);
                    						$Row2 = mysqli_fetch_assoc($Result2);
                    						if($RoleID == 2) {
                    						    //$RUserID = $Row2['RecieverID'];
                    						    if($Row2['RecieverID'] == $userID) {
                						            $RUserID = $Row2['SenderID']; 
                						        } else {
                						            $RUserID = $Row2['RecieverID']; 
                						        }
                    						} else if($RoleID == 3) {
                    						    $RUserID = $Row2['RecieverID'];
                    						}
                						
                						$UQuery2 = "select FirstName,LastName,profilePic from users where UserID='$RUserID'";
            						    $UResult2 = $dbObject->query($UQuery2);
            						    $URow2 = mysqli_fetch_assoc($UResult2);
								    ?>
										<div class="user_name_dis uidForAjx test" id="<?php echo $RUserID;?>"> <img src="assets/profile-photo/<?php echo $URow2['profilePic'];?>" alt=""> </div>
										<div class="msg_disc">
											<div class="msg_disc_head">
												<div class="pull-left">
													<h4><?php echo $URow2['FirstName']." ".$URow2['LastName'];?></h4>
												</div>								
											</div>
										</div>
									</div>
								<div class="msg_display first" id="scrolltoBot">
									<?php
									if($RoleID == 2) {
    								$RecQuery = "select * from messagedata where RecieverID='$userID' and SenderID='$RUserID' or RecieverID='$RUserID' and SenderID='$userID' order by MID ASC";
    									} else if($RoleID == 3) {
    									$RecQuery = "select * from messagedata where SenderID='$userID' and RecieverID='$RUserID' or SenderID='$RUserID' and RecieverID='$userID' order by MID ASC";
    									}
    									$RecResult = $dbObject->query($RecQuery);
    									while($RecRow=mysqli_fetch_assoc($RecResult)) {
									    if($RecRow['RecieverID'] == $userID) {
									?>
									<div class="msg_display_top sender">
										<div class="msg_disc">
											<div class="msg_disc_head pull-left">
												<h5><?php echo $RecRow['Message'];?></h5>
											</div>
										</div>
									</div>
									<?php } else { ?>
									<div class="msg_display_top user_own">
										<div class="msg_disc">
											<div class="msg_disc_head pull-right">
												<h5><?php echo $RecRow['Message'];?></h5>
											</div>
										</div>
									</div>
									<?php } } ?>
									
								</div>
								</div>
							
						</div>
						
						<div class="tab-pane active" id="NewChatWindow" style="display:none;">
							    
						</div>
						
							<div class="message_type">
                		<?php
                			if(isset($_POST['msgSubmit'])) {
                			   $SenderID = $_SESSION['USER_ID'];
                			   $RecieverID = $RUserID;
                			   $msgtext = $_POST['msgtext'];
                			   $fname = $URow2['FirstName'];
                			   $lname = $URow2['LastName'];
                			   
                			   $MQuery = "insert into messagedata (SenderID,RecieverID,Message,Status) values ('$SenderID','$RecieverID','$msgtext','1')";
                			   $Result = $dbObject->query($MQuery);
                			   if($Result) {
                			       
                			       	$fromEmail = "info@skillti.com";
                                	$to = 'jasbeer.singh1990@gmail.com';  
                                	$headers = "From: <".$fromEmail.">\r\n";
                                	$headers .= 'MIME-Version: 1.0' . "\r\n";
                                	$headers .= "Content-type: text/html; charset=iso-8859-1";
                                	$subject = "New Message Notification";
                                    $body = '<p style="margin-left:20px;"><b>Dear'.$fname.' '.$lname.',</b></p>
                                    		 <p style="margin-left:20px;">User '.$name.' has sent you a messagefrom the website</p>';	
                                    $body .= '<p style="margin-left:20px;"><label>Message : <b>'.substr($msgtext, 0, 15).'</b></label></p>';
                                    $body .= '<p style="margin-left:20px;"><a href="'.$base_url.'messages">Click here to see</a></p>';
                                    $body .= "<p style='margin-left:20px;font-weight:bold;'>Best Regards, </p><p style='margin-left:20px;font-weight:bold;'>SkillTI Team</p></br>";
                                    $body .= "<p style='font-size:10px;'>Please consider your environmental responsibility before printing this e-mail</p>";
                                    
                                    $mail = mail($to, $subject, $body, $headers);                                                                                          
                			       if($mail==true) {
                				        print "<script>setTimeout(\"location.href = '".$base_url."messages';\",0);</script>";
                			       }
                			   }
                			}
                		
                		?>
                		<form id="chatForms">
                			<input type="text" name="msgtext" id="msgtext" value="" required />
                			<button type="button" name="msgSubmit" id="sendMsg"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</button>
                		</form>
                		</div>
					</div>
				</div>
				</div>
				
			</div>
			</div>
			</div>
			
			
	</div>
</section>



<?php include('template/footer.php'); ?>
<script>

    $(function () {
        var wtf = $('#scrolltoBot');
        var height = wtf[0].scrollHeight;
        wtf.scrollTop(height);
    });

    $( document ).ready(function() {
	$('.mentor').click(function(){
	    
	
		var RecieverID = this.id;
		var SenderID = "<?php echo $userID;?>";
		
		$.ajax({url: 'messageByAjax.php',
			type: "POST",
			data: {'RecieverID' :RecieverID,'SenderID':SenderID},
			dataType : "html",

			success: function(response){
			 
				 if(response) {   
				    $("#ChatWindow").removeClass("active ");
				    $(".test").removeClass("uidForAjx");
				    //$(".first").removeAttr("id");
				    $("#NewChatWindow").show();
				    $("#NewChatWindow").addClass("active");
				    $("#NewChatWindow").html(response);
				    $(".test2").removeAttr("id");
				    
				 }
			}
		});
	});
	
	
	
	$('#sendMsg').click(function(){
	    var msgtext = $("#msgtext").val();
    	var RecieverID = $(".uidForAjx").attr('id');
    	var SenderID = "<?php echo $userID;?>";
    	
    	$.ajax({
    	    url: 'addMsg.php',
			type: "POST",
			data: {'RecieverID' :RecieverID,'SenderID':SenderID,'msgtext':msgtext},
			
			success: function(data){
			     if(data) {   
				    //$("#msgsdiv").load(location.href + " #msgsdiv");
				    //$("#msgtext").reset();
				    //$("#chalm").load(" #chalm");
				    location.reload();
				   // $( "#msgsdiv" ).load(location.href + " #msgsdiv");
				    $(function () {
                    var wtf = $('#scrolltoBot');
                    var height = wtf[0].scrollHeight;
                    wtf.scrollTop(height);
                });
				 }
				
			}
		});
    	
	});

});
    
</script>

