@extends('header.student_header')
@section('headtitle', "Dashboard")
@section('headdesc', "")
@section('maincontent')
    <div class="mdk-drawer-layout__content page" style="transform: translate3d(0px, 0px, 0px);">

        <div class="container-fluid page__container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('student-dashboard')}}">Home</a></li>
                <li class="breadcrumb-item active">Profile</li>
            </ol>

            <div class="text-center">
                <img style="width: 125px; height: 130px;"  src="{!! profile_image(asset('profile-photo/'.$profile->profilePic)) !!}" alt="profile pic" class="rounded-circle">
                <h1 class="h2 mb-0 mt-1">{{$profile->firstName." ".$profile->lastName}}</h1>
                <p class="lead text-muted mb-0">{{$profile->state}}, {{$profile->country}}</p>
             {{--   <div class="badge badge-primary ">INSTRUCTOR</div>--}}
              {{--  <hr>--}}
              {{--  <h5 class="text-muted mb-1">Instructor Rating</h5>--}}
            {{--    <div class="rating">
                    @for($i=0; $i < 5; $i++)
                    <i class="material-icons {{($i < $profile->avgRatting)?"text-success":"text-muted-light"}}">{{($i < $profile->avgRatting)?"star":"star_border"}}</i>
                    @endfor
                </div>--}}
            </div>
            <hr>
            {{--<h4>My Courses  {{$profile->firstName." ".$profile->lastName}}</h4>--}}
            <div class="card-columns">
                @if(!empty($course) && count($course) > 0)
                    @foreach($course as $skill)
                <div class="card">
                    <div class="card-header">
                        <div class="media align-items-center">
                            <div class="media-left">
                                <img src="{{asset('admin-assets/cat-image/'.$skill->logo)}}" alt="Course logo" width="100" class="rounded">
                            </div>
                            <div class="media-body">
                                <h5 class="card-title text-base m-0"><strong>{{$skill->catName}}</strong></h5>
                                <h4 class="card-title mb-1">{{$skill->subCatName}}</h4>
                                <div class="rating">
                                    @for($i=0; $i < 5; $i++)
                                        <i class="material-icons">{{($i < $skill->avgRatting)?"star":"star_border"}}</i>
                                    @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection
@section('menu')
    @include('student.menu')
@endsection
@section('scripting')

@endsection
@section('footer')
    @include('footer.footer_dashboard')
@endsection
