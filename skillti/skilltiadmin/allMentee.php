
<?php
	include('adminheader.php'); 
	protect_admin_page();
	
	$USerID = $_GET['id'];
	$UpdQuery = "update users set Status='INACTIVE' where UserID='".$USerID."'";
	$UpdResult = $dbObject->query($UpdQuery);
	if($UpdResult == true) {
	    header("location: allMentee");
	   
	    
	}
?>

  <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper"><!-- HTML (DOM) sourced data -->
			<section id="html">
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body ">
								<form class="form px-3 mt-2">
								    <?php include("../include/error.msg.inc.php"); ?>
									<div class="form-body">
										<h4 class="form-section"><i class="ft-user"></i> All Mentors List</h4>
									</div>
								</form>
								<div class="card-block card-dashboard">
								
								<table class="table table-striped table-bordered sourced-data">
										<thead>
											<tr>
												<th>SNO</th>
												<th>Name</th>
												<th>Email ID</th>
												<th>Phone</th>
												<th>Country</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										<?php
										$Query = "select * from users where RoleID=3 and Status='ACTIVE'";
										$Result = $dbObject->query($Query);
										$i=1;
										while($Row = mysqli_fetch_assoc($Result)) { 
										?>
											<tr>
												<td><?php echo $i;?></td>
												<td><?php echo $Row['FirstName'].' '.$Row['LastName'];?></td>
												<td><?php echo $Row['EmailID'];?></td>
												<td><?php echo $Row['Phone'];?></td>
												<td><?php echo $Row['Country'];?></td>
												<td><?php echo $Row['Status'];?></td>
											<td class="text-center"><a title="View Profile" href="viewProfile?id=<?php echo $Row['UserID'];?>"><i class="fa fa-eye" aria-hidden="true" style="font-size: 20px !important;"></i></a> 
												&nbsp;&nbsp;<a title="Deactivate User" href="?id=<?php echo $Row['UserID'];?>" onclick="return confirm('Are you sure to De-Activate this user?');"><i class="fa fa-times" aria-hidden="true" style="font-size: 20px !important;color:red;"></i></a></td>
												
											</tr>
										<?php $i++; } ?>
											
										</tbody>
										
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--/ HTML (DOM) sourced data -->
			</div>
        </div>

   

<?php include('adminfooter.php'); ?>