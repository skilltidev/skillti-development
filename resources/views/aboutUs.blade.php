@extends('header.home_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")

@section('maincontent')
    <style>
        .why-stripe .bullet-points li {
            color: #000;
        }
        .why-stripe .intro-text {
            color: #000;
        }

    </style>
    <section class="mid_content">
        <!--<div class="gradient-background" style="background-image:url('assets/images/map.PNG');"></div>-->
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="heading text-center">
                        <h2>About Us</h2>
                    </div>
                    <div class="profile_list">
                        <p>Skillti is an online education company with intensive mentor-led programs for aspiring beginners
                            from any field. We facilitate and promotes Mentoring, Consulting, Advising and other related
                            activities worldwide. By using our platform anyone who wants to achieve success in their
                            respective endeavours will be getting surely.
                        </p>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="profile_list">
                        <h4><strong>About Skillti</strong></h4>
                        <p>A platform which is designed to help the individuals to accelerate their success. The individuals
                            will be connected to the experts for their respective field. Using Skillti the individuals will be
                            connected to the expert and get consultancy, advice, guidance, coaching ao any of the support
                            they ask from the experts. We combine skilled experts and project-based curriculum to offer any
                            individuals around the world a more accessible and effective alternative to traditional degrees.
                            Anybody can join our platform whether be it a mentee who wants to learn or a mentor who
                            wants to spread their knowledge among others. We will make sure that you learn cutting edge
                            skills, coordinate with peers and find new chances.</p>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="profile_list">
                        <h4><strong>Why Skillti?</strong></h4>
                        <p>Connect With Mentors</p>
                        <p>Contact with the experts to increase your knowledge.</p>
                        <br>
                        <h4><strong>Learn by Doing</strong></h4>
                        <p>Solve a set for your practice to achieve your goals.</p>
                        <br>
                        <h4><strong>Be Future-Ready</strong></h4>
                        <p>Be ready with the emerging trends and topics taught by experts.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripting')
@endsection

@section('footer')
    @include('footer.footer_home')
@endsection
