<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth,Hash;
use Illuminate\Support\Facades\Mail;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Mockery\Exception;
use App\User;

class AuthController extends Controller
{
    public function signin(Request $request)
    {
        return view('auth.signIn');
    }

    public function signup()
    {
        return view('auth.signUp');
    }


    public function postLogin(Request $request)
    {
        $input = $request->input();
        $rules = array('username' => 'required', 'password' => 'required');
        $v = Validator::make($input, $rules);

        if ($v->fails()) {
            return Redirect::back()->withErrors($v);
        } else {
            $credentials = array('email' => $input['username'], 'password' => $input['password'], 'status' => 1);
            if (Auth::attempt($credentials)) {
                $redirect = back()->getTargetUrl();
                $userData = Auth::user();
                if ($userData->roleId === 1) {
                    return Redirect::to('admin');
                } elseif ($userData->roleId === 2) {
                    return Redirect::to('dashboard');
                } elseif ($userData->roleId === 3) {
                    if (strpos($redirect, 'redirect') !== false) {
                        $redired = substr($redirect, strpos($redirect, "redirect") + 9);
                        return Redirect::to('/'.$redired);
                    }else{
                        return Redirect::to('student-dashboard');
                    }
                }
            } else {
                return Redirect::back()->withInput()->with('errorMessage', 'Wrong username/password combination.');
            }
        }
    }

    public function forgotPasswordPost(Request $request) {
	    $input = $request->input();
	    $rules = array('email' => 'required|email');
	    $v = Validator::make($input, $rules);
	    if ($v->fails()) {
		    return Redirect::to('forgotPassword')->withErrors($v);
	    } else {
		    $users = \App\Models\User::where('email', $input['email'])->first();
		    if(!empty($users)) {
		    	$users->remember_token = $input['_token'];
		    	$users->save();
			    try{
				    Mail::send('email.passwordReset', ['confirmation_code' => $input['_token'], 'email' => $input['email']], function ($m) use ($users) {
					    $m->to($users->email, $users->name)
						    ->subject('Skillti password reset link');
				    });
			    }catch (\Exception $e){
				    return Redirect::back()->with('message', $e->getMessage());
			    }
			    return Redirect::back()->with('message', 'Password reset link sent to your email.');
		    }else{
			    return Redirect::back()->withInput()->with('errorMessage', 'Email is not registered!');
		    }
	    }
    }

    public function resetPassword(Request $request) {
    	$token = $request->token;
    	$email = $request->email;
    	$user = \App\Models\User::where('remember_token', $token)->where('email', $email)->first();
    	if(!empty($user)) {
		    return view('auth.resetPassword', compact('email', 'token'));
	    }else{
		    return Redirect::to('forgotPassword')->with('errorMessage', 'Token expire or not vailid!');
	    }
    }

    public function resetPasswordPost(Request $request) {
	    $input = $request->input();
	    $rules = array(
		    'password' => ['required',
			    'min:6',
			    'regex:/^([a-zA-Z0-9@*#]{6,15})$/',
			    'confirmed']
	    );
	    $v = Validator::make($input, $rules);
	    if ($v->passes()) {
		    $user = \App\Models\User::where('remember_token', $input['token'])->where('email', $input['email'])->first();
		    if(!empty($user)) {
			    $user->ppassword = $input['password'];
			    $user->remember_token = '';
			    $user->password = Hash::make($input['password']);
			    $user->save();
			    return redirect(route('users.login'))->with('message', 'Your password successfully reset.');
		    }else{
			    return Redirect::to('forgotPassword')->with('errorMessage', 'Token expire or not vailid!');
		    }
	    }else{
		    return Redirect::back()->withInput()->withErrors($v);
	    }
    }

    public function postRegister(Request $request)
    {
        $input = $request->input();
        $rules = array(
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|unique:users|email',
            'password' => ['required',
                'min:6',
                'regex:/^([a-zA-Z0-9@*#]{6,15})$/',
                'confirmed'],
            'role' => [function ($attribute, $value, $fail) {
                if ($value < 2 && $value > 3) {
                    $fail('Please select given role only!');
                }
            }],
            'country' => 'required'
        );
        $v = Validator::make($input, $rules);

        if ($v->passes()) {
            $password = $input['password'];
            $password = Hash::make($password);
            $user = new User();
            $user->firstName = $input['fname'];
            $user->lastName = $input['lname'];
            $user->name = $input['fname'] . " " . $input['lname'];
            $user->email = $input['email'];
            $user->roleId = $input['role'];
            $user->country = $input['country'];
            $user->password = $password;
            $user->ppassword = $input['password'];
            $user->remember_token = $input['_token'];
            if ($user->save()) {
            	try{
		            Mail::send('email.verify', ['confirmation_code' => $input['_token'], 'email' => $input['email'], 'password' => $input['password']], function ($m) use ($user) {
			            $m->to($user->email, $user->name)
				            ->subject('SkillTI Welcome Email');
		            });
	            }catch (\Exception $e){
		            return redirect(route('users.register'))->with('message', 'Thanks for sign up! After verification via mail you will able to login.');
	            }
                return redirect(route('users.register'))->with('message', 'Thanks for sign up! Please check your email.');
            } else {
                return redirect(route('users.register'))->with('errorMessage', 'Sum error found please try again..')->withInput();
            }
        } else {
            return redirect('signup')->withInput()->withErrors($v);
        }
    }


	public function logout()
	{
		Auth::logout();
		return Redirect::to('/');
	}

	public function verify(Request $request) {
		$token = $request->tocken;
		if(!empty($token)) {
			$user = \App\Models\User::where('remember_token', $token)->first();
			if(!empty($user)) {
				$user->remember_token = '';
				$user->status = 1;
				$user->save();
				$user->email_verified_at = $user->updated_at;
				$user->save();
				return redirect('signin')->with('message', 'Your email have been successfully verified, now you able to login!');
			}
		}
		return redirect('signin')->with('errorMessage', 'Tocken expired or something else please try again!');
	}
}
