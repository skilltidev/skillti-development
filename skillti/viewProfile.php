<?php 
	include('template/header.php'); 
	protect_admin_page();
	
	$UserRole = $_SESSION['USER_ROLE'];
	if($UserRole == 2) {
	    $UserRol="Mentor";
	    
	} else if($UserRole == 3) { 
	    $UserRol="Mentee";
	    
	}
	$UserEmail = $_SESSION['USER_NAME'];
	$Query = "select * from users where EmailID='$UserEmail' and RoleID='$UserRole'";
	$Result =  $dbObject->query($Query);
	$Row = mysqli_fetch_assoc($Result);
	
	if($UserRole == 2) {

?>
<section class="mid_content dashboard_content">
		<div class="container-fluid pl-0">
			<div class="row">
				<div class="col-xs-12 col-sm-3">
				<?php include ('template/sidebar.php');?>
				</div>
				<div class="col-xs-12 col-sm-9 right_side">
				<div class="">
				  <div class="panel panel-info">
                    <div class="panel-heading">
                      <h3 class="panel-title"><?php echo $Row['FirstName'].' '.$Row['LastName'];?></h3>
                    </div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center"> 
						<div class="img-circle">
						<?php if($Row['profilePic']=='') { ?>
							<img alt="User Pic" src="assets/profile-photo/User-dummy.png" class="img-circle img-responsive">
						<?php } else { ?>
							<img alt="User Pic" src="assets/profile-photo/<?php echo $Row['profilePic'];?>" class="img-circle img-responsive">
						<?php } ?>
						</div>
						</div>
                        
                       <div class=" col-md-9 col-lg-9 "> 
                          <table class="table table-user-information">
                            <tbody>
                              <tr>
                                 <td>User Role:</td>
                                <td><?php echo $UserRol;?></td>
                              </tr>
                              <tr>
                                <td>EmailID:</td>
                                <td><?php echo $Row['EmailID'];?></td>
                              </tr>
							  <tr>
                                <td>Phone Number</td>
                                <td><?php echo $Row['Phone'];?></td>
                              </tr>
                              <tr>
                                <td>Qualification:</td>
                                <td><?php echo $Row['Qualification'];?></td>
                              </tr>
                              <tr>
                                <td>Expertise</td>
                                <td><?php echo $Row['Expertise'];?></td>
                              </tr>
								<tr>
                                <td>Verification Doc</td>
                                <td><a href="assets/profile-doc/<?php echo $Row['VarificationDoc'];?>" ><?php echo $Row['VarificationDoc'];?></a></td>
                              </tr>
                                <tr>
                                <td>Working With</td>
                                <td><?php echo $Row['WorkingWith'];?></td>
                              </tr>
                                <tr>
                                <td>Experience</td>
                                <td><?php echo $Row['Experience'];?></td>
                              </tr>
                              <tr>
                                <td>Certified</td>
                                <td><?php echo $Row['Certified'];?></td>
                              </tr>
							  <tr>
                                <td>Name of Certificate</td>
                                <td><?php echo $Row['CertificateName'];?></td>
                              </tr>
							  <tr>
                                <td>About</td>
                                <td><?php echo $Row['UserAbout'];?></td>
                              </tr>
							  <tr>
                                <td>Tagline</td>
                                <td><?php echo $Row['UserTagline'];?></td>
                              </tr>
							  
                             <tr>
                                <td>Address</td>
                                <td><?php echo $Row['Address1'];?>, <?php echo $Row['Address2'];?></td>
                              </tr>
                             <tr>
                                <td>Country</td>
                                <td><?php echo $Row['Country'];?></td>
                              </tr>
                             <tr>
								<td>State</td>
								<td><?php echo $Row['State'];?></td>
							  </tr>
							 <tr>
								<td>City</td>
								<td><?php echo $Row['City'];?></td>
							  </tr>
							 <tr>
								<td>Zip</td>
								<td><?php echo $Row['Zip'];?></td>
							  </tr>
                            </tbody>
                          </table>
                          
                         </div>
                      </div>
                    </div>
                        <div class="panel-footer text-right">
						<a href="editProfile" class="blue_btn custom_btn"><i class="fa fa-pencil-square-o fa-lg fa-fw sidebar-icon"></i> Edit Profile</a>
					</div>
                    
                  </div>
               
				</div>
			</div>
		</div>
	</section>
	
<?php } else if($UserRole == 3) { ?>

<section class="mid_content dashboard_content">
	<div class="container-fluid pl-0">
		<div class="row">
			<div class="col-xs-12 col-sm-3">
			<?php include ('template/sidebar.php');?>
			</div>
			<div class="col-xs-12 col-sm-9 right_side">
			<div class="">
			  <div class="panel panel-info">
				<div class="panel-heading">
				  <h3 class="panel-title"><?php echo $Row['FirstName'].' '.$Row['LastName'];?></h3>
				</div>
				<div class="panel-body">
				  <div class="row">
					<div class="col-md-3 col-lg-3 " align="center"> 
					<div class="img-circle">
					<?php if($Row['profilePic']=='') { ?>
						<img alt="User Pic" src="assets/profile-photo/User-dummy.png" class="img-circle img-responsive">
					<?php } else { ?>
						<img alt="User Pic" src="assets/profile-photo/<?php echo $Row['profilePic'];?>" class="img-circle img-responsive">
					<?php } ?>
					</div>
					</div>
					
				   <div class=" col-md-9 col-lg-9 "> 
					  <table class="table table-user-information">
						<tbody>
						  <tr>
							 <td>User Role:</td>
							<td>Mentee</td>
						  </tr>
						  <tr>
							<td>EmailID:</td>
							<td><?php echo $Row['EmailID'];?></td>
						  </tr>
						  <tr>
							<td>Phone Number</td>
							<td><?php echo $Row['Phone'];?></td>
						  </tr>
						  <tr>
							<td>Qualification:</td>
							<td><?php echo $Row['Qualification'];?></td>
						  </tr>
						  <tr>
							<td>Interest To Learn</td>
							<td><?php echo $Row['InterestToLearn'];?></td>
						  </tr>
							
						 <tr>
							<td>Address</td>
							<td><?php echo $Row['Address1'];?>, <?php echo $Row['Address2'];?></td>
						  </tr>
						 <tr>
							<td>Country</td>
							<td><?php echo $Row['Country'];?></td>
						  </tr>
						 <tr>
							<td>State</td>
							<td><?php echo $Row['State'];?></td>
						  </tr>
						 <tr>
							<td>City</td>
							<td><?php echo $Row['City'];?></td>
						  </tr>
						 <tr>
							<td>Zip</td>
							<td><?php echo $Row['Zip'];?></td>
						  </tr>
						 
						</tbody>
					  </table>
					  
					 </div>
				  </div>
				</div>
					 <div class="panel-footer text-right">
						<a href="editProfile" class="blue_btn custom_btn"><i class="fa fa-pencil-square-o fa-lg fa-fw sidebar-icon"></i> Edit Profile</a>
					</div>
				
			  </div>
		   
			</div>
		</div>
	</div>
</section>
	
<?php } ?>
<style>
    .user-row {
    margin-bottom: 14px;
}

.user-row:last-child {
    margin-bottom: 0;
}

.dropdown-user {
    margin: 13px 0;
    padding: 5px;
    height: 100%;
}

.dropdown-user:hover {
    cursor: pointer;
}

.table-user-information > tbody > tr {
    border-top: 1px solid rgb(221, 221, 221);
}

.table-user-information > tbody > tr:first-child {
    border-top: 0;
}


.table-user-information > tbody > tr > td {
    border-top: 0;
}
.toppad
{margin-top:20px;
}
.img-circle {
	width:200px;
	height:200px;
	background-position: center center;
    object-fit: cover;
}

</style>

<?php include('template/footer.php'); ?>