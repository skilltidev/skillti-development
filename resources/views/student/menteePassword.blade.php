@extends('header.student_header')
@section('headtitle', "Dashboard")
@section('headdesc', "")
@section('maincontent')
    <div class="mdk-drawer-layout__content page">
        <div class="container-fluid page__container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">Password Setting</li>
            </ol>
            <h1 class="h2">Password Setting</h1>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            @if(Session::get('message'))
                                <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Success!</strong> {{ Session::get('message') }}{{ Session::forget('message') }}
                                </div>
                            @endif
                            @if(Session::get('errorMessage'))
                                <div class="alert alert-danger">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Message !</strong> {{ Session::get('errorMessage') }}{{ Session::forget('errorMessage') }}
                                </div>
                            @endif
                            <form id="form" action="{{url('student-dashboard/password-setting')}}" method="post" class="form-horizontal">
                                @csrf
                                <div class="form-group row">
                                    <label for="fee" class="col-sm-3 col-form-label form-label">Old Password</label>
                                    <div class="col-sm-8 col-md-8">
                                        <div class="input-group">
                                            <input required id="oldPassword" name="oldPassword" type="password" class="form-control" placeholder="Enter your old password" value="{{old('oldPassword')?old('oldPassword'):''}}">
                                        </div>
                                        @if ($errors->has('oldPassword'))
                                            <span style="display: block;" class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('oldPassword') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fee" class="col-sm-3 col-form-label form-label">New Password</label>
                                    <div class="col-sm-8 col-md-8">
                                        <div class="input-group">
                                            <input required id="password" name="password" type="password" class="form-control" placeholder="Password" value="{{old('password')?old('password'):''}}">
                                        </div>
                                        @if ($errors->has('password'))
                                            <span style="display: block;" class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fee" class="col-sm-3 col-form-label form-label">Confirm Password</label>
                                    <div class="col-sm-8 col-md-8">
                                        <div class="input-group">
                                            <input required id="password_confirmation" name="password_confirmation" type="text" class="form-control" placeholder="Confirm Password" value="{{old('password_confirmation')?old('password_confirmation'):''}}">
                                        </div>
                                        @if ($errors->has('password_confirmation'))
                                            <span style="display: block;" class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('menu')
    @include('student.menu')
@endsection
@section('scripting')

@endsection
@section('footer')
    @include('footer.footer_dashboard')
@endsection
