<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
	/**
	 * Indicates mysql connection index
	 * @var string
	 */
	protected $connection = 'mysql';
	/**
	 * Indicates mysql table name
	 * @var string
	 */
	protected $table = 'roles';
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;
}
