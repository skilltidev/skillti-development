<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="icon" href="{{asset('images/skillti_logo_4_update-fav_icon.png')}}" type="image/x-icon" />
    <title>@yield('headtitle')</title>
    <meta name="description" content="@yield('headdesc')">
    <link href="{{asset('css/font-awesome.min.css')}}" type="text/css" rel="stylesheet"/>
    <link href="{{asset('css/bootstrap.min.css')}}" type="text/css" rel="stylesheet"/>
    <link href="{{asset('css/style.css')}}" type="text/css" rel="stylesheet" />
    <link href="{{asset('css/media.css')}}" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
    <style>

    </style>
</head>
<body>
<header>
    <div class="top_header">
        <div class="container">
            <div class="row">
                <div class="col-xs-5 col-sm-3">
                    <div class="logo"  style="padding-top:6px;">
                        <a href="{{url('')}}"><img src="{{asset('images/skillti_update.png')}}" alt="SkillTI" ></a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6  mobile_search">
                    <div class="search_bx">
                        <form action="{{url('search')}}" method="get">
                            <div class="search_inr">
                                <input type="text" name="term" id="search" placeholder="Find Your Mentor,Technology...." autocomplete="off" />
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-7 col-sm-3 mobile_float">
                    <div class="action">
                        <ul class="list-inline">
                            @if(Auth::check())
                                <div class="dropdown dash_btn">
                                    <button class="dropdown-toggle" type="button" data-toggle="dropdown">
                                        <strong>{{Auth::user()->firstName." ".Auth::user()->lastName}}</strong>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
				                        @if(Auth::user()->roleId == 1)
                                        <li><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
				                        @elseif(Auth::user()->roleId == 2)
                                        <li><a href="{{url('dashboard')}}">Dashboard</a></li>
                                        <li><a href="{{url('dashboard/mentor-profile')}}">View Profile </a></li>
                                        @elseif(Auth::user()->roleId == 3)
                                            <li><a href="{{url('student-dashboard')}}">Dashboard</a></li>
                                            <li><a href="{{url('student-dashboard/mentee-profile-edit')}}">View Profile </a></li>
				                        @endif
                                        <li><a href="{{url('logout')}}">Log out</a></li>
                                    </ul>
                                </div>
                            @else
                            <li><a href="{{url('signin')}}">Sign In</a></li>
                            <li><a href="{{url('signup')}}">Sign Up</a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-inverse" id="my_menu">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                @foreach($menus as $cat)
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="{{url('category/'.$cat->link)}}">{{$cat->name}}</a>
                        <ul class="dropdown-menu">
                            @if(!empty($cat->subCategory))
                                @foreach($cat->subCategory as $subCat)
                                    <li><a href="{{url('category/'.$subCat->link)}}">{{$subCat->name}}</a></li>
                                @endforeach
                            @else
                                <li><a href="{{url('category/'.$cat->link)}}">{{$cat->name}}</a></li>
                            @endif
                        </ul>
                    </li>
                @endforeach
                    <li><a href="{{url('all-categories')}}">All Categories </a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
@yield('maincontent')
@yield('footer')
</body>
</html>
