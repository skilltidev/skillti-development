@extends('header.home_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")

@section('maincontent')
    <section class="mid_content">
        <div class="container">
            <div class="row">
                @if(isset($mentorsData) && count($mentorsData) == 0)
                    <div class="col-xs-12 col-sm-9">
                        <div class="profile_list">
                            <h3>Sorry, we couldn't find any results for "{{$searchKey}}"</h3><br>
                            <h4>Try adjusting your search. Here are some ideas: </h4><h4>
                                <ul style="line-height: 39px;color: #727272;margin-top: 20px;margin-left: -18px;">
                                    <li>Make sure all words are spelled correctly.</li>
                                    <li>Try different search terms.</li>
                                    <li>Try more general search terms.</li>
                                </ul>
                            </h4>
                        </div>
                    </div>
                @else
                    <div class="col-xs-10 col-sm-12">
                        <div class="profile_list">
                            <h1 class="text-center">Search Result for search key "{{$searchKey}}"</h1><br>
                            <br>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9  col-xs-offset-1 col-sm-offset-1">
                        <div class="profile_list">
                            @foreach($mentorsData as $mentor)
                                {{--{{debug($mentor)}}--}}
                                <ul>
                                    <li><img src="{{profile_image(asset('profile-photo/'.$mentor->profilePic))}}" alt="{{$mentor->profilePic}}"></li>
                                    <li>
                                        <div class="profile_list_bx">
                                            <h4><a href="{{url('profile-detail/'.custom_encode($mentor->userId))}}">{{$mentor->firstName." ".$mentor->lastName}}</a></h4>
                                            @if($mentor->tagline != '')
                                                <h4>{{$mentor->tagline}}</h4>
                                            @endif
                                            @if($mentor->shortBio != '')
                                                <div class="bio_disc">{{$mentor->shortBio}}</div>
                                            @endif
                                            <div class="pills">
                                                <ul class="list-inline">
                                                    <li class="head_main">Skills</li>
                                                    @foreach(json_decode($mentor->skills) as $k => $cat)
                                                        @if($k < 5)
                                                            <li>{{$cat}}</li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <div class="test">
                                                <ul class="list-inline">
                                                    <li class="head_main">Location : </li>
                                                    <li>{{$mentor->state.", ".$mentor->country}}</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('scripting')
@endsection

@section('footer')
    @include('footer.footer_home')
@endsection
