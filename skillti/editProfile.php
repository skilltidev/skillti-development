<?php 
	include('template/header.php'); 
	protect_admin_page();
	
    $UserRole = $_SESSION['USER_ROLE'];
	$UserEmail = $_SESSION['USER_NAME'];
	$Query = "select * from users where EmailID='$UserEmail' and RoleID='$UserRole'";
	$Result =  $dbObject->query($Query);
	$Row = mysqli_fetch_assoc($Result);
	
	if($UserRole == 2) {
	if(isset($_POST['UpdateProfile'])) {
		$fname = $_POST['fname'];
		$lname = $_POST['lname'];
		$phone = $_POST['phone'];
		
		$profilePicName = $_FILES["profilePic"]["name"];
		$profile_temp_name = $_FILES["profilePic"]["tmp_name"];
		$target_path = "assets/profile-photo/".$profilePicName;
		move_uploaded_file($profile_temp_name, $target_path);


		$qualification = $_POST['qualification'];
		$expertise = str_replace("'", "", $_POST['expertise']);
		$country = $_POST['country'];
		$address1 = str_replace("'", "", $_POST['address1']);
		$address2 = str_replace("'", "", $_POST['address2']);
		$City = $_POST['city'];
		$State = $_POST['state'];
		$Zip = $_POST['zip'];
		
		$verifyDocName = $_FILES['verifyDoc']["name"];
		$verify_temp_name = $_FILES["verifyDoc"]["tmp_name"];
		$target_path2 = "assets/profile-doc/".$verifyDocName;
		move_uploaded_file($verify_temp_name, $target_path2);
		
		
		$NameOfDoc = str_replace("'", "", $_POST['NameOfDoc']);
		$UserAbout = str_replace("'", "", $_POST['UserAbout']);
		$UserTagline = str_replace("'", "", $_POST['UserTagline']);
		
		$workingWith = str_replace("'", "", $_POST['workingWith']);
		$experince = str_replace("'", "", $_POST['experince']);
		$Certified = $_POST['Certified'];
		
		if(empty($profilePicName) && empty($verifyDocName)) {
			$UpdQuery = "update users set FirstName='".$fname."', LastName='".$lname."', Phone='".$phone."', Qualification='".$qualification."', 	Expertise='".$expertise."', Country='".$country."', Address1='".$address1."', Address2='".$address2."',WorkingWith='".$workingWith."', Experience='".$experince."', City='".$City."', State='".$State."', Zip='".$Zip."', Certified='".$Certified."', CertificateName='".$NameOfDoc."', UserAbout='".$UserAbout."', UserTagline='".$UserTagline."' where EmailID='$UserEmail'";
		} else if(empty($profilePicName)) {
		$UpdQuery = "update users set FirstName='".$fname."', LastName='".$lname."', Phone='".$phone."', Qualification='".$qualification."', 	Expertise='".$expertise."', Country='".$country."', Address1='".$address1."', Address2='".$address2."', VarificationDoc='".$verifyDocName."', WorkingWith='".$workingWith."', Experience='".$experince."', City='".$City."', State='".$State."', Zip='".$Zip."', Certified='".$Certified."', CertificateName='".$NameOfDoc."', UserAbout='".$UserAbout."', UserTagline='".$UserTagline."' where EmailID='$UserEmail'";
		} else if(empty($verifyDocName)) {
			$UpdQuery = "update users set FirstName='".$fname."', LastName='".$lname."', Phone='".$phone."', profilePic='".$profilePicName."', Qualification='".$qualification."', 	Expertise='".$expertise."', Country='".$country."', Address1='".$address1."', Address2='".$address2."', WorkingWith='".$workingWith."', Experience='".$experince."', City='".$City."', State='".$State."', Zip='".$Zip."', Certified='".$Certified."', CertificateName='".$NameOfDoc."', UserAbout='".$UserAbout."', UserTagline='".$UserTagline."' where EmailID='$UserEmail'";
		} else { 
			$UpdQuery = "update users set FirstName='".$fname."', LastName='".$lname."', Phone='".$phone."', profilePic='".$profilePicName."', Qualification='".$qualification."', 	Expertise='".$expertise."', Country='".$country."', Address1='".$address1."', Address2='".$address2."', VarificationDoc='".$verifyDocName."', WorkingWith='".$workingWith."', Experience='".$experince."', City='".$City."', State='".$State."', Zip='".$Zip."', Certified='".$Certified."', CertificateName='".$NameOfDoc."', UserAbout='".$UserAbout."', UserTagline='".$UserTagline."' where EmailID='$UserEmail'";
		}
		$UpdResult =  $dbObject->query($UpdQuery);
		if($UpdResult == true) {
		//	header("location: editProfile");
	    	print "<script>setTimeout(\"location.href = '".$base_url."editProfile';\",500);</script>";
	        session_start();
            $_SESSION['SESS_MSG'] = "Your profile has been updated";
            
		}
		
		
		
		
		
	}

?>


<section class="mid_content dashboard_content">
	<div class="container-fluid pl-0">
		<div class="row">
			<div class="col-xs-12 col-sm-3">
			<?php include ('template/sidebar.php');?>
			</div>
			<div class="col-xs-12 col-sm-9 right_side">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<div class="custom_form">
				<?php include("include/error.msg.inc.php"); ?>
					 <form method="post" action="" enctype="multipart/form-data" >
						<div class="form-group">
						  <label for="">First Name</label>
						  <input type="text" class="form-control" placeholder="Jhon" value="<?php echo $Row['FirstName'];?>" name="fname" required />
						</div>
						 <div class="form-group">
						  <label for="">Last Name</label>
						  <input type="text" class="form-control"  placeholder="Doe" value="<?php echo $Row['LastName'];?>" name="lname" required />
						</div>
						 <div class="form-group">
						  <label for="">Profile Pic</label>
						  
						  <input type="file" class="form-control" name="profilePic" style="width: 69%;float:left;">
						  <div style="width:7%;float:left;padding: 0 5px;"><img <?php if($Row['profilePic']=='') { ?> src="assets/profile-photo/User-dummy.png"<?php } else { ?>src="assets/profile-photo/<?php echo $Row['profilePic'];?>"<?php } ?> style="width: 45px;height: 45px;border-radius: 50%;background-position: center center;object-fit: cover;"/></div>
						</div>
						<div class="form-group">
						  <label for="">Email</label>
						  <input type="email" class="form-control"  placeholder="test@ymail.com" value="<?php echo $Row['EmailID'];?>" readonly name="email">
						</div>
						 <div class="form-group">
						  <label for="">Phone</label>
						  <input type="text" class="form-control"  placeholder="Phone Number" value="<?php echo $Row['Phone'];?>"  name="phone">
						</div>
						 <div class="form-group">
						  <label for="">Qualification</label>
							 <select class="form-control" name="qualification" id="qualification" required>
								<option value="">Select Qualification</option>
								<option value="BA">BA</option>
								<option value="B-Tech">B-Tech</option>
								<option value="B.Com">B.Com</option>
							 </select>
						</div>
						 <div class="form-group">
						  <label for="">Expertise</label>
						  <input type="text" class="form-control" value="<?php echo $Row['Expertise'];?>" placeholder="Python, Php, React" name="expertise" required />
						</div>
						 <div class="form-group">
						  <label for="">Country</label>
						
						<?php include('template/country.php');?>
						</div>
						<div class="form-group">
						  <label for="">Address</label>
						  <input type="text" class="form-control" value="<?php echo $Row['Address1'];?>" placeholder="Address Line 1" name="address1" required />
						  <input type="text" class="form-control" value="<?php echo $Row['Address2'];?>" placeholder="Address Line 2" name="address2" >
						</div>
						 <div class="form-group">
						  <label for="">City</label>
						  <input type="text" class="form-control" value="<?php echo $Row['City'];?>" placeholder="City" name="city" required />
						</div>
						 <div class="form-group">
						  <label for="">State</label>
						  <input type="text" class="form-control" value="<?php echo $Row['State'];?>" placeholder="State" name="state" required />
						</div>
						<div class="form-group">
						  <label for="">Zip</label>
						  <input type="text" class="form-control" value="<?php echo $Row['Zip'];?>" placeholder="Zip" name="zip" required />
						</div>
						<div class="form-group">
						  <label for="">Upload <span>(Address Varification Document)</span> </label>
						  <input type="file" class="form-control" name="verifyDoc">
						</div>
						<div class="form-group">
						  <label for="">Working With <span>(Company Name)</span> </label>
						  <input type="text" class="form-control" value="<?php echo $Row['WorkingWith'];?>" placeholder="google.com" name="workingWith">
						</div>
						 <div class="form-group">
						  <label for="">Experience  <span>(in Years)</span> </label>
						  <input type="text" class="form-control" value="<?php echo $Row['Experience'];?>" placeholder="Eg - (3 Years)" name="experince" required />
						</div> 
						 <div class="form-group">
						  <label for="">Certified </label>
						 <input type="radio" value="Yes" <?php if($Row['Certified']=='Yes') { echo "checked"; }?> name="Certified">&nbsp; Yes
						    <input type="radio" value="No" <?php if($Row['Certified']=='No') { echo "checked"; }?> style="margin-left: 20px;" name="Certified">&nbsp; No
							
						
						  </ul>
						</div>
						<div class="form-group">
						  <label for="">Name of Cerficate</label>
							 <select class="form-control" name="NameOfDoc" id="NameOfDoc" >
								<option value="">Select Certificate</option>
								<option value="Certificate1">Certificate1</option>
								<option value="Certificate2">Certificate2</option>
								<option value="Certificate3">Certificate3</option>
							 </select>
						</div>
						<div class="form-group">
						  <label for="">About </label>
						  <textarea class="form-control" placeholder="About your profile" name="UserAbout" ><?php echo $Row['UserAbout'];?></textarea>
						</div> 
						<div class="form-group">
						  <label for="">Tagline </label>
						  <input type="text" class="form-control" value="<?php echo $Row['UserTagline'];?>" placeholder="Tagline" name="UserTagline"  />
						</div> 
						<div class="text-right"><button type="submit" name="UpdateProfile" class="blue_btn custom_btn">Update</button></div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>


<?php 
	} else if($UserRole == 3) { 
	if(isset($_POST['UpdateProfile2'])) {
		$fname = $_POST['fname'];
		$lname = $_POST['lname'];
		$phone = $_POST['phone'];
		
		$profilePicName = $_FILES["profilePic"]["name"];
		$profile_temp_name = $_FILES["profilePic"]["tmp_name"];
		$target_path = "assets/profile-photo/".$profilePicName;
		move_uploaded_file($profile_temp_name, $target_path);


		$qualification = $_POST['qualification'];
		$intToLearn = $_POST['intToLearn'];
		$country = $_POST['country'];
		$address1 = $_POST['address1'];
		$address2 = $_POST['address2'];
		$City = $_POST['city'];
		$State = $_POST['state'];
		$Zip = $_POST['zip'];
		
		
		
		if(empty($profilePicName)) {
			$UpdQuery = "update users set FirstName='".$fname."', LastName='".$lname."', Phone='".$phone."', Qualification='".$qualification."', 	InterestToLearn='".$intToLearn."', Country='".$country."', Address1='".$address1."', Address2='".$address2."', City='".$City."', State='".$State."', Zip='".$Zip."' where EmailID='$UserEmail'";
		} else { 
			$UpdQuery = "update users set FirstName='".$fname."', LastName='".$lname."', Phone='".$phone."', profilePic='".$profilePicName."', Qualification='".$qualification."', 	InterestToLearn='".$intToLearn."', Country='".$country."', Address1='".$address1."', Address2='".$address2."', City='".$City."', State='".$State."', Zip='".$Zip."' where EmailID='$UserEmail'";
		}
		
		$UpdResult =  $dbObject->query($UpdQuery);
		if($UpdResult == true) {
			print "<script>setTimeout(\"location.href = '".$base_url."editProfile';\",500);</script>";
	        session_start();
            $_SESSION['SESS_MSG'] = "Your profile has been updated";
		}
		
		
		
		
		
	}


?>

<section class="mid_content dashboard_content">
		<div class="container-fluid pl-0">
			<div class="row">
				<div class="col-xs-12 col-sm-3">
				<?php include ('template/sidebar.php');?>
				</div>
				<div class="col-xs-12 col-sm-9 right_side">
				<div class="col-xs-12 col-sm-8 col-sm-offset-2">
					<div class="custom_form">
					<?php include("include/error.msg.inc.php"); ?>
						 <form method="post" action="" enctype="multipart/form-data" >
							<div class="form-group">
							  <label for="">First Name</label>
							  <input type="text" class="form-control" placeholder="Jhon" value="<?php echo $Row['FirstName'];?>" name="fname" required />
							</div>
							 <div class="form-group">
							  <label for="">Last Name</label>
							  <input type="text" class="form-control"  placeholder="Doe" value="<?php echo $Row['LastName'];?>" name="lname" required />
							</div>
							 <div class="form-group">
							  <label for="">Profile Pic</label>
							  
							  <input type="file" class="form-control" name="profilePic" style="width: 69%;float:left;">
							  <div style="width:7%;float:left;padding: 0 5px;">
							  <img 	<?php if($Row['profilePic']=='') { ?> src="assets/profile-photo/User-dummy.png" <?php } else { ?> src="assets/profile-photo/<?php echo $Row['profilePic'];?>" <?php } ?> style="width: 45px;height: 45px;border-radius: 50%;background-position: center center;object-fit: cover;"/></div>
							</div>
							<div class="form-group">
							  <label for="">Email</label>
							  <input type="email" class="form-control"  placeholder="test@ymail.com" value="<?php echo $Row['EmailID'];?>" readonly name="email">
							</div>
							<div class="form-group">
							  <label for="">Phone</label>
							  <input type="text" class="form-control"  placeholder="Phone Number" value="<?php echo $Row['Phone'];?>"  name="phone">
							</div>
							 <div class="form-group">
							  <label for="">Qualification</label>
								 <select class="form-control" name="qualification" id="qualification" required>
									<option value="">Select Qualification</option>
									<option value="BA">BA</option>
									<option value="B-Tech">B-Tech</option>
									<option value="B.Com">B.Com</option>
								 </select>
							</div>
							<div class="form-group">
							  <label for="">Interested to learn?</label>
								 <select class="form-control" name="intToLearn" id="intToLearn" required>
									<option value="">Select Option</option>
									<option value="BA">BA</option>
									<option value="B-Tech">B-Tech</option>
									<option value="B.Com">B.Com</option>
								 </select>
							</div>
							 <div class="form-group">
							  <label for="">Country</label>
								<?php include('template/country.php');?>
							 </div>
							<div class="form-group">
							  <label for="">Address</label>
							  <input type="text" class="form-control" value="<?php echo $Row['Address1'];?>" placeholder="Address Line 1" name="address1" required />
							  <input type="text" class="form-control" value="<?php echo $Row['Address2'];?>" placeholder="Address Line 2" name="address2" >
							</div>
							 <div class="form-group">
							  <label for="">City</label>
							  <input type="text" class="form-control" value="<?php echo $Row['City'];?>" placeholder="City" name="city" required />
							</div>
							 <div class="form-group">
							  <label for="">State</label>
							  <input type="text" class="form-control" value="<?php echo $Row['State'];?>" placeholder="State" name="state" required />
							</div>
							<div class="form-group">
							  <label for="">Zip</label>
							  <input type="text" class="form-control" value="<?php echo $Row['Zip'];?>" placeholder="Zip" name="zip" required />
							</div>
							
							
							<div class="text-right"><button type="submit" name="UpdateProfile2" class="blue_btn custom_btn">Update</button></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php } ?>



<?php include('template/footer.php'); ?>
<script language="javascript">
	var qualification = '<?php echo $Row['Qualification'];?>';
    document.getElementById("qualification").value = qualification;
    
    var country = '<?php echo $Row['Country'];?>';
    document.getElementById("country").value = country;
    
	var NameOfDoc = '<?php echo $Row['CertificateName'];?>';
    document.getElementById("NameOfDoc").value = NameOfDoc;
    
    var intToLearn = '<?php echo $Row['InterestToLearn'];?>';
    document.getElementById("intToLearn").value = intToLearn;
    
    
</script>