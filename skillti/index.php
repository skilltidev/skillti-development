<?php include('template/header.php'); ?>
<style>
    .see_all:hover {
        background: #fff;
        color: #00b2ff;
        border: solid 1.5px #00b2ff;
    }
</style>	
	<section class="banner">
		<img src="assets/images/banner2.jpg" alt="" />
		<div class="banner_caption">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<h1>Quicken your personal and expert achievement with the help of Skillti!</h1>
						<p>We at Skillti, will help you out in evolving your career. A learning solution which inspires you to create your future. With this stage, tutors and mentees are given a chance to connect and help one another.</p>
						<a href="signup" class="custom_btn">Get Started</a>
						<!--<a href="#" class="custom_btn"> Adipiscing </a>-->
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="hire_for">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-lg-3">
					<h2>Perfect guidance for all your career need.</h2>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<h3>Learn from certified mentors!</h3>
					<p>With 100% experienced experts who will help you out.</p>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<h3>Minimal Learning Charge!</h3>
					<p>Money is our least most priority.</p>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<h3>Your money is secure with us!</h3>
					<p>Keep the burden of money aside.</p>
				</div>
			</div>
		</div>
	</section>
	
	<section class="custome_section hire_exp">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="section_heading">Learn something new, Organized, Online programs for career minded with occupied lives.</h2>
				</div>
				
				<?php
				    $Query = "Select * from ( Select SubCategory,count(SubCategory) as counts from syllabus group by SubCategory ) V1 Order by counts desc limit 8";
				    $Result = $dbObject->query($Query);
				    
				    while($Row = mysqli_fetch_assoc($Result)) {
				    $SubCatID = $Row['SubCategory'];
				    $CQuery = "select * from category where CatID='$SubCatID'";
				    $CResult = $dbObject->query($CQuery);
				    $CRow = mysqli_fetch_assoc($CResult)
				
				?>
				<div class="col-xs-12 col-sm-4 col-lg-3">
				    <a style="color:black;" href="category?cat=<?php echo strtolower($CRow['CatName']);?>">
					<div class="expertise">
						<img src="skilltiadmin/app-assets/cat-image/<?php echo $CRow['CatImage'];?>" alt="" >
						<h3><?php echo $CRow['CatName'];?></h3>
					
					</div>
					</a>
				</div>
			<?php } ?>
				<div class="col-xs-12 col-sm-12 text-center">
					<a class="see_all " href="ViewAllCategory">See All Categories</a>
				</div>
			</div>
		</div>
	</section>
	
	
	<section class="custome_section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="section_heading">How It Works</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-3">
					<div class="hw_works">
						<img src="assets/images/hire.png" alt="" >
						<h3>Learning Requiremt</h3>
						<p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard. </p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3">
					<div class="hw_works">
						<img src="assets/images/find.png" alt="" >
						<h3>Find a mentor</h3>
						<p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard. </p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3">
					<div class="hw_works">
						<img src="assets/images/work.png" alt="" >
						<h3>Start Learning</h3>
						<p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard. </p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3">
					<div class="hw_works">
						<img src="assets/images/pay.png" alt="" >
						<h3>Be ready for industry</h3>
						<p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard. </p>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="custome_section bg_grey">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="training">
						<h3>Ipsum dolor smit</h3>
						<p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard. </p>
						<a href="#" class="custom_btn">Start Teaching</a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="training">
						<h3>Ipsum dolor smit</h3>
						<p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard. </p>
						<a href="#" class="custom_btn">Start Learning</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	

	
<?php include('template/footer.php'); ?>