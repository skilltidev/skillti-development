<?php

namespace App\Http\Middleware;

use App\Models\Messages;
use Closure;
use Auth, DB;

class DashboardMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    if (Auth::check()) {
		    $userData = Auth::user();
		    if($userData->roleId == 1) {
			    return redirect('/signin')->with('errorMessage', 'You are not authorised for admin access.');
		    }
		    if($userData->roleId == 3) {
			    return redirect('/signin')->with('errorMessage', 'You are not authorised for mentee access.');
		    }
	    }else{
		    return redirect('/signin')->with('errorMessage', 'Please login for acceess admin dashboard.');
	    }
        /*$unreadMessages = Messages::select(DB::raw('CONCAT(users.firstName, " ", users.lastName) AS menteeName'), 'messages.*')
            ->join('users', 'users.id', 'messages.menteeId')
            ->where('mentorId', Auth::user()->id)
            ->where('sender', 3)
            ->where('isRead', 0)->get();
debug($unreadMessages, false);
        view()->share('unreadMessages', $unreadMessages);*/
	    return $next($request);
    }
}
