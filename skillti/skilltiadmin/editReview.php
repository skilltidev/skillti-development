<?php
	include('adminheader.php'); 
	protect_admin_page();
	
	$eid = $_GET['eid'];
	
	$Query = "select * from reviews where RID='$eid'";
	$Result = $dbObject->query($Query);
	$Row = mysqli_fetch_assoc($Result);
	
	$userID = $Row['UserID'];
    $UQuery = "select EmailID from users where UserID='$userID'";
	$UResult = $dbObject->query($UQuery);
	$URow = mysqli_fetch_assoc($UResult);
	
	$SenderID = $Row['SenderID'];
    $SQuery = "select EmailID from users where UserID='$SenderID'";
	$SResult = $dbObject->query($SQuery);
	$SRow = mysqli_fetch_assoc($SResult);
	
	
	
		if(isset($_POST['Update'])) {
		   
		   $status = $_POST['status'];
		   $Query = "update reviews set Status='".$status."' where RID='".$eid."'";
		   $Update = $dbObject->query($Query);
		   if($Update == true) {
		       print "<script>setTimeout(\"location.href = 'editReview?eid=".$eid."';\",500);</script>";
    	        session_start();
                $_SESSION['SESS_MSG'] = "Review has been updated";
			   
			
		   }
		}
	
	
	
?>

<div class="main-panel">
    <div class="main-content">
    <div class="content-wrapper"><!-- Striped row layout section start -->
        <section id="striped-row-form-layouts">
        <div class="row">
        	    <div class="col-md-12">
        	        <div class="card">
        	            <div class="card-body">
        	                <div class="px-3 mt-2">
        	                    <form class="form form-horizontal striped-rows form-bordered" action="" method="post" enctype="multipart/form-data">
        	                    	<div class="form-body">
									<?php include("../include/error.msg.inc.php"); ?>
									
										<h4 class="form-section"><i class="fa fa-pencil-square-o"></i> Approved/Disapproved Review</h4>
									
        
        		                        <div class="form-group row">
        									<label class="col-md-3 label-control" >Mentor Email</label>
        									<div class="col-md-7">
        		                            <input type="text" class="form-control" value="<?php echo $URow['EmailID'];?>" readonly  />
        		                            </div>
        		                        </div>
										
										<div class="form-group row">
        									<label class="col-md-3 label-control" >Mentee Email</label>
        									<div class="col-md-7">
        		                            <input type="text" class="form-control" value="<?php echo $SRow['EmailID'];?>" readonly />
        		                            </div>
        		                        </div>
        
        		                        
										<div class="form-group row last">
        									<label class="col-md-3 label-control" >Review Status</label>
        									<div class="col-md-7">
        										<select  name="status" class="form-control" id="status" >
        			                                <option value="publish">Publish</option>
        			                                <option value="draft">Draft</option>
        			                             </select>
											</div>
        								</div>
										
										<div class="form-actions text-center">
											<a href="review"> <button type="button" class="btn btn-raised btn-warning mr-1">
												<i class="ft-x"></i> Cancel
											</button></a>
											<button type="submit" name="Update" class="btn btn-raised btn-primary">
												<i class="fa fa-check-square-o"></i> Update
											</button>
										</div>
										
												
        							</div>
        
        	                        
        	                    </form>
        	                </div>
        	            </div>
        	        </div>
        	    </div>
        	</div>
        
        </section>
    </div>
    </div>    

        
		
		
<?php include('adminfooter.php'); ?>
<script language="javascript">
	
	var Status = '<?php echo $Row['Status'];?>';
	document.getElementById("status").value = Status;
</script>