-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 19, 2019 at 12:02 AM
-- Server version: 5.7.25-0ubuntu0.16.04.2
-- PHP Version: 7.2.13-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skillti_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menuOrder` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `logo`, `link`, `menuOrder`, `status`, `created_at`, `updated_at`) VALUES
(4, 'Software Development', 'Software Development', 'software_dovelopment.png', 'software-development', 1, 1, '2019-01-13 00:22:18', '2019-03-16 18:31:44'),
(5, 'IT Ops', 'IT Operations', 'IT_OPS.png', 'it-ops', 1, 1, '2019-01-13 00:24:46', '2019-03-16 18:32:03'),
(6, 'Data Science', 'Data Science', 'data_science.png', 'data-science', 4, 1, '2019-01-13 00:25:39', '2019-03-16 18:33:07'),
(7, 'Cyber Security', 'Information & Cyber Security', 'cyber_security.png', 'cyber-security', 1, 1, '2019-01-13 00:26:15', '2019-03-16 18:36:03'),
(8, 'Business Professionals', 'Business Professionals', 'business_proffesionals.png', 'business-professionals', 1, 1, '2019-01-13 00:26:53', '2019-03-11 13:18:19'),
(9, 'Digital Marketing', 'Digital Marketing', 'web_skillti.png', 'digital-marketing', 6, 1, '2019-01-13 00:27:53', '2019-03-16 18:34:17'),
(10, 'Artificial Intelligence', 'Artificial Intelligence', 'data_science.png', 'artificial-intelligence', 5, 1, '2019-01-13 00:29:03', '2019-03-16 18:33:49'),
(11, 'Blockchain', 'Blockchain', 'me.png', 'blockchain', 3, 1, '2019-01-13 00:31:26', '2019-03-16 18:32:37'),
(12, 'MySQL', 'MySQL', 'MySQL_Skillti.png', 'mysql', 0, 1, '2019-01-13 00:31:52', '2019-01-14 11:58:36'),
(13, 'Others', 'Others', 'catImg.PNG', 'others', 0, 1, '2019-01-13 01:16:39', '2019-01-14 11:58:36');

-- --------------------------------------------------------

--
-- Table structure for table `chattings`
--

CREATE TABLE `chattings` (
  `id` int(11) NOT NULL,
  `mentorId` int(11) NOT NULL,
  `courseId` int(11) DEFAULT NULL,
  `menteeId` int(11) NOT NULL,
  `message` text CHARACTER SET utf8mb4 NOT NULL,
  `attachment` varchar(2000) NOT NULL DEFAULT '',
  `sender` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contactUs`
--

CREATE TABLE `contactUs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `courseId` int(11) NOT NULL,
  `mentorId` int(11) NOT NULL,
  `menteeId` int(11) NOT NULL,
  `courseDuration` varchar(10) NOT NULL,
  `paymentStatus` tinyint(4) NOT NULL DEFAULT '0',
  `courseStatus` tinyint(4) NOT NULL DEFAULT '0',
  `paymentid` varchar(100) NOT NULL,
  `paymentAmount` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `mentorId` int(11) NOT NULL,
  `menteeId` int(11) NOT NULL,
  `message` varchar(1000) CHARACTER SET utf8mb4 NOT NULL,
  `sender` tinyint(4) DEFAULT NULL,
  `isRead` tinyint(4) NOT NULL DEFAULT '0',
  `messageId` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_01_05_185542_contact_us', 1),
(4, '2019_01_06_054947_roles', 1),
(5, '2019_01_06_061626_social_links', 1),
(6, '2019_01_06_062156_categories', 1),
(7, '2019_01_06_062932_sub_categories', 1),
(8, '2019_01_06_105750_skills', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ratting`
--

CREATE TABLE `ratting` (
  `id` int(11) NOT NULL,
  `mentorId` int(11) NOT NULL,
  `menteeId` int(11) NOT NULL,
  `rating` tinyint(4) NOT NULL,
  `comment` varchar(200) NOT NULL,
  `courseId` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Super admin credentials', 1, '2019-01-07 18:30:00', NULL),
(2, 'Mentor', 'Teacher front end user', 1, '2019-01-07 18:30:00', NULL),
(3, 'Mentee', 'Student front end user', 1, '2019-01-07 18:30:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `id` int(10) NOT NULL,
  `userId` int(10) NOT NULL,
  `catId` int(10) NOT NULL,
  `subCatId` int(10) NOT NULL,
  `skills` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `shortBio` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `courseType` text COLLATE utf8mb4_unicode_ci,
  `coursefee` float NOT NULL DEFAULT '0',
  `coursefeeType` text COLLATE utf8mb4_unicode_ci,
  `avgRatting` tinyint(4) NOT NULL DEFAULT '3',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `socialLinks`
--

CREATE TABLE `socialLinks` (
  `id` int(10) NOT NULL,
  `userId` int(10) NOT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedIn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `github` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subCategories`
--

CREATE TABLE `subCategories` (
  `id` int(10) NOT NULL,
  `catId` int(10) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `logo` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isHome` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subCategories`
--

INSERT INTO `subCategories` (`id`, `catId`, `name`, `description`, `link`, `logo`, `isHome`, `status`, `created_at`, `updated_at`) VALUES
(6, 4, 'Web Development', 'Web Development', 'web-development', '1552760501web-development.png', 1, 1, '2019-01-13 00:51:48', '2019-03-16 18:21:41'),
(7, 4, 'Mobile Development', 'Mobile Development', 'mobile-development', NULL, 0, 1, '2019-01-13 00:52:37', '2019-01-14 11:58:36'),
(8, 4, 'JavaScript', 'JavaScript', 'javascript', NULL, 0, 1, '2019-01-13 00:52:59', '2019-01-14 11:58:36'),
(9, 4, 'C#', 'ASP .Net', 'c', NULL, 0, 1, '2019-01-13 00:53:31', '2019-01-14 11:58:36'),
(10, 4, 'Python', 'Python', 'python', '1552759652python.png', 1, 1, '2019-01-13 00:53:57', '2019-03-16 18:07:32'),
(11, 4, 'Node.js', 'Node.js', 'nodejs', '1552758231nodejs.png', 1, 1, '2019-01-13 00:54:14', '2019-03-16 17:43:51'),
(12, 4, 'html', 'html', 'html', NULL, 0, 1, '2019-01-13 00:54:46', '2019-01-14 11:58:36'),
(13, 5, 'IT Certifications', 'IT Certifications', 'it-certifications', NULL, 0, 1, '2019-01-13 00:56:39', '2019-01-14 11:58:36'),
(14, 8, 'Business Intelligence', 'Business Intelligence', 'business-intelligence', NULL, 0, 1, '2019-01-13 00:57:34', '2019-01-14 11:58:36'),
(15, 7, 'Security Certifications', 'Security Certifications', 'security-certifications', '1552758358security-certifications.png', 1, 1, '2019-01-13 00:58:05', '2019-03-16 17:45:58'),
(16, 7, 'Security Fundamentals', 'Security Fundamentals', 'security-fundamentals', NULL, 0, 1, '2019-01-13 00:58:42', '2019-01-14 11:58:37'),
(17, 7, 'Security Auditing', 'Security Auditing', 'security-auditing', NULL, 0, 1, '2019-01-13 00:59:03', '2019-01-14 11:58:37'),
(18, 7, 'Penetration Testing', 'Penetration Testing', 'penetration-testing', NULL, 0, 1, '2019-01-13 00:59:36', '2019-01-14 11:58:37'),
(19, 7, 'Digital Forensics', 'Digital Forensics', 'digital-forensics', NULL, 0, 1, '2019-01-13 01:00:00', '2019-01-14 11:58:37'),
(20, 7, 'Malware Analysis', 'Malware Analysis', 'malware-analysis', NULL, 0, 1, '2019-01-13 01:00:22', '2019-01-14 11:58:37'),
(21, 8, 'Fundamentals of IT Ops', 'Fundamentals of IT Ops', 'fundamentals-of-it-ops', NULL, 0, 1, '2019-01-13 01:02:54', '2019-01-14 11:58:37'),
(22, 8, 'Scrum Framework', 'Scrum Framework', 'scrum-framework', NULL, 0, 1, '2019-01-13 01:03:13', '2019-01-14 11:58:37'),
(23, 8, 'PRINCE2Â®', 'PRINCE2Â®', 'prince2ar', NULL, 0, 1, '2019-01-13 01:03:33', '2019-01-14 11:58:37'),
(24, 8, 'Project Manager', 'Project Manager', 'project-manager', NULL, 0, 1, '2019-01-13 01:03:58', '2019-01-14 11:58:37'),
(25, 8, 'Risk Analysis', 'Risk Analysis', 'risk-analysis', NULL, 0, 1, '2019-01-13 01:04:25', '2019-01-14 11:58:37'),
(26, 5, 'Security Database', 'Security Database', 'security-database', NULL, 0, 1, '2019-01-13 01:04:53', '2019-01-14 11:58:37'),
(27, 5, 'Administration', 'Administration', 'administration', NULL, 0, 1, '2019-01-13 01:05:13', '2019-01-14 11:58:37'),
(28, 5, 'Security Database', 'Security Database', 'security-database', NULL, 0, 1, '2019-01-13 01:05:37', '2019-01-14 11:58:37'),
(29, 5, 'Virtualization', 'Virtualization', 'virtualization', NULL, 0, 1, '2019-01-13 01:06:41', '2019-01-14 11:58:37'),
(30, 5, 'IT Networking', 'IT Networking', 'it-networking', NULL, 0, 1, '2019-01-13 01:07:05', '2019-01-14 11:58:37'),
(31, 5, 'Servers', 'Servers', 'servers', NULL, 0, 1, '2019-01-13 01:07:35', '2019-01-14 11:58:37'),
(32, 9, 'SMO', 'SMO', 'smo', NULL, 0, 1, '2019-01-13 01:08:18', '2019-01-14 11:58:37'),
(33, 9, 'SEO', 'SEO', 'seo', NULL, 0, 1, '2019-01-13 01:08:47', '2019-01-14 11:58:37'),
(34, 9, 'Google Analytics', 'Google Analytics', 'google-analytics', '1552759806google-analytics.png', 1, 1, '2019-01-13 01:09:09', '2019-03-16 18:10:06'),
(35, 9, 'Pay Per Clicks', 'Pay Per Clicks', 'pay-per-clicks', NULL, 0, 1, '2019-01-13 01:09:36', '2019-01-14 11:58:37'),
(36, 9, 'Google Shopping', 'Google Shopping', 'google-shopping', NULL, 0, 1, '2019-01-13 01:09:57', '2019-01-14 11:58:37'),
(37, 9, 'Web Analytics', 'Web Analytics', 'web-analytics', NULL, 0, 1, '2019-01-13 01:10:20', '2019-01-14 11:58:37'),
(38, 8, 'BD', 'Business Development', 'bd', NULL, 0, 1, '2019-01-13 01:11:01', '2019-01-14 11:58:37'),
(39, 6, 'Data Dashboard', 'Data Dashboard', 'data-dashboard', NULL, 0, 1, '2019-01-13 01:12:52', '2019-01-14 11:58:37'),
(40, 6, 'Oracle', 'Oracle', 'oracle', NULL, 0, 1, '2019-01-13 01:13:25', '2019-01-14 11:58:37'),
(41, 6, 'SQL', 'SQL', 'sql', '1552759740sql.png', 1, 1, '2019-01-13 01:13:46', '2019-03-16 18:09:00'),
(42, 6, 'SQL Server', 'SQL Server', 'sql-server', NULL, 0, 1, '2019-01-13 01:14:06', '2019-01-14 11:58:37'),
(43, 11, 'Network and protocols', 'Network and protocols', 'network-and-protocols', NULL, 0, 1, '2019-01-13 01:14:40', '2019-01-14 11:58:37'),
(44, 13, 'Administration', 'Administration', 'administration', NULL, 0, 1, '2019-01-13 01:18:25', '2019-01-14 11:58:37'),
(45, 13, 'Servers', 'Servers', 'servers', NULL, 0, 1, '2019-01-13 01:20:29', '2019-01-14 11:58:37'),
(46, 13, 'IT Networking', 'IT Networking', 'it-networking', NULL, 0, 1, '2019-01-13 01:25:35', '2019-01-14 11:58:37'),
(47, 13, 'Servers', 'Servers', 'servers', NULL, 0, 1, '2019-01-13 01:26:25', '2019-01-14 11:58:37'),
(48, 13, 'Microsoft Office', 'Microsoft Office', 'microsoft-office', NULL, 0, 1, '2019-01-13 01:27:00', '2019-01-14 11:58:37'),
(49, 13, 'Windows Server', 'Windows Server', 'windows-server', NULL, 0, 1, '2019-01-13 01:27:35', '2019-01-14 11:58:37'),
(50, 13, 'Windows Server', 'Windows Server', 'windows-server', NULL, 0, 1, '2019-01-13 01:28:09', '2019-01-14 11:58:37'),
(51, 13, 'Microsoft Windows OS', 'Microsoft Windows OS', 'microsoft-windows-os', NULL, 0, 1, '2019-01-13 01:28:45', '2019-01-14 11:58:37'),
(52, 4, 'React', 'React', 'react', '1552758990react.png', 1, 1, '2019-01-13 01:29:23', '2019-03-16 17:56:30'),
(53, 13, 'Cloud Computing', 'Cloud Computing', 'cloud-computing', '1552758719cloud-computing.png', 1, 1, '2019-01-13 01:29:58', '2019-03-16 17:51:59'),
(54, 13, 'Docker', 'Docker', 'docker', NULL, 0, 1, '2019-01-13 01:30:37', '2019-01-14 11:58:37'),
(58, 13, 'SQL', 'SQL', 'sql', NULL, 0, 1, '2019-01-13 01:32:53', '2019-01-14 11:58:37'),
(59, 8, 'aditya kumar', 'sfdSAF', 'aditya-kumar', '1552447377aditya-kumar.png', 0, 0, '2019-03-11 13:58:45', '2019-03-16 17:45:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT '1',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `ppassword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expertise` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addressOne` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addressTwo` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `varificationDoc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `experience` float NOT NULL DEFAULT '0',
  `certified` tinyint(4) NOT NULL DEFAULT '0',
  `about` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `tagline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `interestToLearn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `msgurl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profilePic` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isFirstTimeLogin` tinyint(4) NOT NULL DEFAULT '1',
  `lastLoggedIn` datetime DEFAULT NULL,
  `roleId` int(10) UNSIGNED NOT NULL,
  `avgRatting` tinyint(4) NOT NULL DEFAULT '4',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `fcm_tocken` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chattings`
--
ALTER TABLE `chattings`
  ADD KEY `id` (`id`),
  ADD KEY `mentorId` (`mentorId`),
  ADD KEY `courseId` (`courseId`),
  ADD KEY `menteeId` (`menteeId`);

--
-- Indexes for table `contactUs`
--
ALTER TABLE `contactUs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `montorId` (`mentorId`),
  ADD KEY `menteeId` (`menteeId`),
  ADD KEY `courseId` (`courseId`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mentorId` (`mentorId`),
  ADD KEY `menteeId` (`menteeId`),
  ADD KEY `messageId` (`messageId`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `ratting`
--
ALTER TABLE `ratting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mentorId` (`mentorId`),
  ADD KEY `menteeId` (`menteeId`),
  ADD KEY `courseId` (`courseId`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `skills_catid_foreign` (`catId`),
  ADD KEY `skills_subcatid_foreign` (`subCatId`),
  ADD KEY `skills_userid_foreign` (`userId`);

--
-- Indexes for table `socialLinks`
--
ALTER TABLE `socialLinks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sociallinks_userid_foreign` (`userId`);

--
-- Indexes for table `subCategories`
--
ALTER TABLE `subCategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catId` (`catId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_roleid_foreign` (`roleId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `chattings`
--
ALTER TABLE `chattings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contactUs`
--
ALTER TABLE `contactUs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ratting`
--
ALTER TABLE `ratting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `socialLinks`
--
ALTER TABLE `socialLinks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subCategories`
--
ALTER TABLE `subCategories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `chattings`
--
ALTER TABLE `chattings`
  ADD CONSTRAINT `chattings_ibfk_1` FOREIGN KEY (`mentorId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `chattings_ibfk_2` FOREIGN KEY (`menteeId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `chattings_ibfk_3` FOREIGN KEY (`courseId`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `course`
--
ALTER TABLE `course`
  ADD CONSTRAINT `course_ibfk_1` FOREIGN KEY (`courseId`) REFERENCES `skills` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `course_ibfk_2` FOREIGN KEY (`mentorId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `course_ibfk_3` FOREIGN KEY (`menteeId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`mentorId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`menteeId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `messages_ibfk_3` FOREIGN KEY (`messageId`) REFERENCES `messages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ratting`
--
ALTER TABLE `ratting`
  ADD CONSTRAINT `ratting_ibfk_1` FOREIGN KEY (`mentorId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `ratting_ibfk_2` FOREIGN KEY (`menteeId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `ratting_ibfk_3` FOREIGN KEY (`courseId`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `skills`
--
ALTER TABLE `skills`
  ADD CONSTRAINT `skills_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `skills_ibfk_2` FOREIGN KEY (`catId`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `skills_ibfk_3` FOREIGN KEY (`subCatId`) REFERENCES `subCategories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `socialLinks`
--
ALTER TABLE `socialLinks`
  ADD CONSTRAINT `socialLinks_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `subCategories`
--
ALTER TABLE `subCategories`
  ADD CONSTRAINT `subCategories_ibfk_1` FOREIGN KEY (`catId`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
