@extends('header.admin_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")

@section('maincontent')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper">
                <section id="striped-row-form-layouts">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    @if(Session::get('message'))
                                        <div class="alert alert-success">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>Success!</strong> {{ Session::get('message') }}{{ Session::forget('message') }}
                                        </div>
                                    @endif
                                    @if(Session::get('errorMessage'))
                                        <div class="alert alert-danger">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>Message !</strong> {{ Session::get('errorMessage') }}{{ Session::forget('errorMessage') }}
                                        </div>
                                    @endif
                                    <div class="px-3 mt-2">
                                        <form class="form form-horizontal  form-bordered" action="{{url('admin/add-edit-category')}}" method="post" enctype="multipart/form-data">
                                            <div class="form-body">
                                                @if(empty($categoryInfo))
                                                <h4 class="form-section"><i class="fa fa-plus"></i> Add New Category</h4>
                                                @else
                                                <input type="hidden" name="id" id="id" value="{{$categoryInfo->id}}">
                                                <h4 class="form-section"><i class="fa fa-pencil-square-o"></i> Edit Category</h4>
                                                @endif
                                                @csrf
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Category Name</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" placeholder="Category Name" value="{{isset($categoryInfo->name)?$categoryInfo->name:old('name')}}" name="name" required />
                                                        @if ($errors->has('name'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('name') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control">Category Image</label>
                                                    <div class="col-md-9">
                                                        <label id="projectinput8" class="file center-block">
                                                            <input value="{{old('catImage')?old('catImage'):''}}" type="file" id="catImage" name="catImage">
                                                            <span class="file-custom"></span>
                                                            @if ($errors->has('catImage'))
                                                                <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('catImage') }}</strong>
                                                            </span>
                                                            @endif
                                                        </label>
                                                        @if(isset($categoryInfo->logo))
                                                        <img style="width:70px;height:70px;position:absolute;top:3px;" src="{{($categoryInfo->logo != '')?asset('admin-assets/cat-image/'.$categoryInfo->logo):asset('admin-assets/cat-image/catImg.PNG')}}">
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput9">Category Description</label>
                                                    <div class="col-md-9">
                                                        <textarea id="projectinput9" rows="5" class="form-control" name="description" placeholder="Category Description">{{isset($categoryInfo->description)?$categoryInfo->description:old('description')}}</textarea>
                                                        @if ($errors->has('description'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('description') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group row last">
                                                    <label class="col-md-3 label-control" for="projectinput9">Menu Order</label>
                                                    <div class="col-md-9">
                                                        <select  name="menuOrder" class="form-control" id="menuOrder" >
                                                            @for($i=0; $i<8; $i++)
                                                            <option {{((isset($categoryInfo->menuOrder) && $categoryInfo->menuOrder == $i) || old('menuOrder') == $i)?"selected":''}} value="{{$i}}">{{$i}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row last">
                                                    <label class="col-md-3 label-control" for="projectinput9">Category Status</label>
                                                    <div class="col-md-9">
                                                        <select  name="status" class="form-control" id="status" >
                                                            <?php $status = isset($categoryInfo->status)?$categoryInfo->status:old('status') ?>
                                                            <option {{($status == 1)?"selected":''}} value="1">Active</option>
                                                            <option {{($status == 0)?"selected":''}} value="0">InActive</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-actions text-right">
                                                    <a href="{{url('admin/all-categories')}}"> <button type="button" class="btn btn-raised btn-warning mr-1">
                                                            <i class="ft-x"></i> Cancel
                                                        </button>
                                                    </a>
                                                    @if(empty($categoryInfo))
                                                    <button type="submit" name="addCat" class="btn btn-raised btn-primary">
                                                        <i class="fa fa-check-square-o"></i> Save
                                                    </button>
                                                    @else
                                                    <button type="submit" name="Update" class="btn btn-raised btn-primary">
                                                        <i class="fa fa-check-square-o"></i> Update
                                                    </button>
                                                    @endif
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('scripting')
@endsection
@section('footer')
    @include('footer.footer_admin')
@endsection
