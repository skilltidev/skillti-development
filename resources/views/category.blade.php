@extends('header.home_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")

@section('maincontent')
    <section class="mid_content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <h1 class="section_heading"><u>All Categories</u></h1>
                </div>
            </div>
        </div>
        <div class="container">

                @if(!empty($categories))
                    <?php $i = 1; ?>
                    @foreach($categories as $cat)
                        @if(!empty($cat->subCategory))
                            @if(($i%4) == 0)
                            <div class="row">
                            @endif
                        <div class="col-xs-12 col-sm-3">
                            <div class="f_links category_links">
                                <h3>{{$cat->name}}</h3>
                                <ul>
                                    @foreach($cat->subCategory as $subcat)
                                    <li><a href="{{url('category/'.$subcat->link)}}">{{$subcat->name}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                                @if(($i%4) == 0)
                                    </div>
                                        @endif
                            <?php $i = $i + 1; ?>
                        @endif
                    @endforeach
                @else
                    <div class="row">
                    <div class="col-md-12">
                        <h1>No Category Data Found !</h1>
                    </div>
                    </div>
                @endif

        </div>
    </section>
@endsection

@section('scripting')
@endsection

@section('footer')
    @include('footer.footer_home')
@endsection
