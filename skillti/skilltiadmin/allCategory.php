
<?php
	include('adminheader.php'); 
	protect_admin_page();
	
	
?>

  <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper"><!-- HTML (DOM) sourced data -->
			<section id="html">
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body ">
								<form class="form px-3 mt-2">
									<div class="form-body">
										<h4 class="form-section"><i class="fa fa-th-list"></i> All Category List</h4>
									</div>
								</form>
								<div class="card-block card-dashboard">
								
								<table class="table table-striped table-bordered sourced-data">
										<thead>
											<tr>
												<th>SNO</th>
												<th>Name</th>
											<!--	<th>User</th>-->
												<th>Parent</th>
												<th>Image</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										<?php
										$Query = "select * from category";
										$Result = $dbObject->query($Query);
										$i=1;
										while($Row = mysqli_fetch_assoc($Result)) { 
										$userID = $Row['UserID'];
										$Parent = $Row['Parent'];
										
										$ChkQuery = "select us.FirstName,us.LastName,rl.RoleName from users us inner join roles rl on us.RoleID=rl.RoleID where us.UserID='$userID'";
										$ChkResult = $dbObject->query($ChkQuery);
										$ChkRow = mysqli_fetch_assoc($ChkResult);
										
										$Query2 = "select CatName from category where CatID='$Parent'";
										$Result2 = $dbObject->query($Query2);
										$Row2 = mysqli_fetch_assoc($Result2);
										?>
											<tr >
												<td><?php echo $i;?></td>
												<td><?php echo $Row['CatName'];?></td>
												<!--<td><?php echo $ChkRow['FirstName'].' '.$ChkRow['LastName'].'('.$ChkRow['RoleName'].')';?></td>-->
												<td><?php echo $Row2['CatName'];?></td>
												<td class="text-center">
												<?php if($Row['CatImage']=='') { ?>
													<img style="width:50px;height:50px" src="app-assets/cat-image/catImg.PNG" />
												<?php } else { ?>
													<img style="width:50px;height:50px" src="app-assets/cat-image/<?php echo $Row['CatImage'];?>" />
												<?php } ?>
												</td>
												<td class="text-center"><?php echo $Row['Status'];?></td>
												<td class="text-center"><a title="Edit" href="addCategory?catid=<?php echo $Row['CatID'];?>"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size: 20px !important;"></i></a> 
												&nbsp;&nbsp;<a title="Delete" id="<?php echo $Row['CatID'];?>" class="delete"><i class="fa fa-times" aria-hidden="true" style="font-size: 20px !important;color:red;"></i></a></td>
												
											</tr>
										<?php $i++; } ?>
											
										</tbody>
										
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--/ HTML (DOM) sourced data -->
			</div>
        </div>

   

<?php include('adminfooter.php'); ?>
<script>
   
 $('.delete').click(function(){
  var el = this;
  var id = this.id;
  
  var deleteid = id;
  
  // AJAX Request
  $.ajax({
   url: 'deleteCat.php',
   type: 'POST',
   data: { id:deleteid },
   success: function(response){

    $(el).closest('tr').css('background','tomato');
    $(el).closest('tr').fadeOut(800, function(){ 
     $(this).remove();
    });

   }
  });

 });


</script>