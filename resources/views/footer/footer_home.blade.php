<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="f_about f_links">
                    <h3>Who we are</h3>
                    <p style="text-align: justify;">Skillti is an online education company with intensive mentor-led programs for aspiring beginners from any field. We facilitate and promotes Mentoring, Consulting, Advising and other related activities worldwide. By using our platform anyone who wants to achieve success in their respective endeavours will be getting surely.</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8">
                <div class="row">
                    <div class="col-xs-12 col-sm-3">
                        <div class="f_links">
                            <h3>Company</h3>
                            <ul>
                                <li><a href="{{url('about-us')}}">About Us </a></li>
                                {{--<li><a href="#">Our Team  </a></li>--}}
                                <li><a href="{{url('contact-us')}}">Contact Us  </a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <div class="f_links">
                            <h3>Get Started</h3>
                            <ul>
                                <li><a href="{{url('/signup')}}">Become a Mentor </a></li>
                                <li><a href="{{url('/signup')}}">Become a Mentee  </a></li>
                                <li><a href="{{url('business')}}">Become a Recruiter</a></li>

                            </ul>
                        </div>
                    </div>
                    {{--<div class="col-xs-12 col-sm-3">
                        <div class="f_links">
                            <h3>Policies</h3>
                            <ul>
                                <li><a href="#">Privacy Policy </a></li>
                                <li><a href="#">Refund Policy  </a></li>
                                <li><a href="#">Term of Use  </a></li>

                            </ul>
                        </div>
                    </div>--}}
                    <div class="col-xs-12 col-sm-3">
                        <div class="f_links">
                            <h3>Explore More</h3>
                            <ul>
                                <li><a href="{{url('business')}}">Skillti Business</a></li>
                                <li><a href="{{url('jobs')}}">Skillti Jobs  </a></li>
                                <li><a href="{{url('projects')}}">Skillti Projects  </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <ul class="social_media">
                        <li><a href="https://www.facebook.com/SkilltiOfficial" target="_blank" rel="nofollow"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="https://twitter.com/skilltiofficial" target="_blank" rel="nofollow"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/skillti" target="_blank" rel="nofollow"><i class="fa fa-linkedin" aria-hidden="true" target="_blank" rel="nofollow"></i></a></li>
                      {{--  <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>--}}
                        <li><a href="https://www.instagram.com/skilltiofficial"  target="_blank" rel="nofollow"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-xs-12 text-right">
                    <div class="copyright_text">
                        <p>Copyright 2018 Skillti.com, All Rights Reserved.  </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="{{asset('js/jquery.min.3.3.1.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
<script src="{{asset('js/local.js')}}"></script>
@yield('scripting')
<script>
    $("#search_toggle").click(function(){
        $(".mobile_search").slideToggle();
    });
    $(document).ready(function(){
        $("#searcha").autocomplete({
            source:'{{url('apisuggestion')}}',
            minLength:1
        });
        $("#search").autocomplete({
            source: '{{url('apisuggestion')}}',
            select: function( event, ui ) {
                $( "#search" ).val( ui.item.label);
                window.location.href = ui.item.value;
                return false;
            },
            focus: function(event, ui) {
                $( "#search" ).val( ui.item.label);
                //window.location.href = ui.item.value;
                return false;
            }
        });
    });
</script>



