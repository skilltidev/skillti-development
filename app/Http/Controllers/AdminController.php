<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\ContactUs;
use App\Models\Course;
use App\Models\Ratting;
use App\Models\Skills;
use App\Models\SocialLinks;
use App\Models\SubCategories;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use DB;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    public function dashboard() {
    	$totalMentor = User::where('roleId', 2)->count();
    	$totalMentee = User::where('roleId', 3)->count();
    	$totalSkills = Skills::count();
    	$totalCourse = Course::count();
    	$totalAmount = Course::where('paymentStatus', 1)->sum('paymentAmount');
    	$data = compact('totalMentor', 'totalMentee', 'totalSkills', 'totalCourse', 'totalAmount');
	    return view('admin.dashboard', $data);
    }

    public function categories() {
    	$categories = Categories::get();
	    return view('admin.categories', compact('categories'));
    }

    public function addCategories(Request $request) {
    	if($request->catId != '') {
		    $categoryInfo = Categories::where('id', custom_decode($request->catId))->first();
    	}else{
		    $categoryInfo = [];
	    }
	    return view('admin.addCategories', compact('categoryInfo'));
    }

    public function postCategoryInfo(Request $request) {
	    $input = $request->input();
	    if(isset($input['id']) && $input['id'] != '') {
		    $this->validate($request, array(
			    'name' => 'required|max:100',
			    'description' => 'required',
			    'catImage' => 'image|mimes:jpeg,png,jpg|max:2048',
			    'status' => 'required'
		    ));
	    }else{
		    $this->validate($request, array(
			    'name' => 'required|unique:categories|max:100',
			    'description' => 'required',
			    'catImage' => 'image|mimes:jpeg,png,jpg|max:2048',
			    'status' => 'required'
		    ));
	    }


	    $image = $request->file('catImage');
	    $input['logo'] = '';
	    if(isset($input['id']) && $input['id'] != '') {
	    	$categoryInfo = Categories::where('id', $input['id'])->first();
		    $input['logo'] = $categoryInfo->logo;
	    }else{
		    $categoryInfo = new Categories();
	    }

	    $categoryInfo->link = getFriendlyURL($input['name']);
	    if (!empty($image)) {
		    $input['imagename'] = time().$categoryInfo->link.".".$image->getClientOriginalExtension();//$image->getClientOriginalName();
		    $destinationPath = public_path('/admin-assets/cat-image');
		    $image->move($destinationPath, $input['imagename']);
		    $input['logo'] = $input['imagename'];
	    }
	    $categoryInfo->name = $input['name'];
	    $categoryInfo->description = $input['description'];
	    $categoryInfo->status = $input['status'];
	    $categoryInfo->menuOrder = $input['menuOrder'];
	    $categoryInfo->logo = $input['logo'];
	    if($categoryInfo->save()) {
	    	return redirect('admin/add-edit-category/'.custom_encode($categoryInfo->id))->with('message', 'Category successfully add/update.');
	    }else{
		    return Redirect::back()->withInput()->with('errorMessage', 'Something went wrong please try again!');
	    }
    }

    public function deleteCategories(Request $request) {
    	$category = Categories::where('id', custom_decode($request->id))->first();
    	if($category->delete()) {
    		$output  = ['error' => false];
	    }else{
		    $output  = ['error' => true];
	    }
    	return response()->json($output);
    }

	public function subCategories(Request $request) {
		$categories = Categories::get();
		$catId = $request->catId;
    	if($catId != '' && ucfirst($catId) != 'All') {
		    $subCategories = SubCategories::subCategoryDetails(custom_decode($catId));
	    }else{
		    $subCategories = SubCategories::subCategoryDetails();
	    }
		return view('admin.subCategories', compact('categories', 'subCategories', 'catId'));
	}

	public function addSubCategories(Request $request) {
		$categories = Categories::get();
		if($request->catId != '') {
			$categoryInfo = SubCategories::where('id', custom_decode($request->catId))->first();
		}else{
			$categoryInfo = [];
		}
		return view('admin.addSubCategories', compact('categoryInfo', 'categories'));
	}

	public function postSubCategoryInfo(Request $request) {
		$input = $request->input();
		if(isset($input['id']) && $input['id'] != '') {
			$this->validate($request, array(
				'name' => 'required|max:100',
				'name' => 'required|max:100',
				'description' => 'required',
				'status' => 'required',
				'catImage' => 'image|mimes:jpeg,png,jpg|max:2048',
			));
		}else{
			$this->validate($request, array(
				'categoryName' => 'required',
				'name' => 'required|unique:categories|max:100',
				'description' => 'required',
				'status' => 'required',
				'catImage' => 'image|mimes:jpeg,png,jpg|max:2048',
			));
		}

		$image = $request->file('catImage');
		$input['logo'] = '';

		if(isset($input['id']) && $input['id'] != '') {
			$categoryInfo = SubCategories::where('id', $input['id'])->first();
			$input['logo'] = $categoryInfo->logo;
		}else{
			$categoryInfo = new SubCategories();
		}
		$categoryInfo->link = getFriendlyURL($input['name']);
		if (!empty($image)) {
			$input['imagename'] = time().$categoryInfo->link.".".$image->getClientOriginalExtension();//$image->getClientOriginalName();
			$destinationPath = public_path('/admin-assets/cat-image');
			$image->move($destinationPath, $input['imagename']);
			$input['logo'] = $input['imagename'];
		}
		$categoryInfo->name = $input['name'];
		$categoryInfo->description = $input['description'];
		$categoryInfo->status = $input['status'];
		$categoryInfo->isHome = $input['isHome'];
		$categoryInfo->catId = $input['categoryName'];
		$categoryInfo->logo = $input['logo'];

		if($categoryInfo->save()) {
			return redirect('admin/add-edit-subCategory/'.custom_encode($categoryInfo->id))->with('message', 'Category successfully add/update.');
		}else{
			return Redirect::back()->withInput()->with('errorMessage', 'Something went wrong please try again!');
		}
	}



	public function deleteSubCategories(Request $request) {
		$category = SubCategories::where('id', custom_decode($request->id))->first();
		if($category->delete()) {
			$output  = ['error' => false];
		}else{
			$output  = ['error' => true];
		}
		return response()->json($output);
	}

	/****************mentor******************************/
    public function mentorList() {

       $mentorList =  User::where('roleId', 2)->get();
        return view('admin.mentorList', compact('mentorList'));
    }

    public function addMentor(Request $request) {
       if($request->mentorId != '') {
           $profile = User::select('users.*', 'socialLinks.facebook', 'socialLinks.twitter', 'socialLinks.linkedIn', 'socialLinks.github', 'socialLinks.other')
               ->leftjoin('socialLinks', 'socialLinks.userId', 'users.id')
               ->where('users.id', custom_decode($request->mentorId))->first();
        }else{
            $profile = new User();
        }

        return view('admin.mentorProfileSetting', compact('profile'));

    }

    public function mentorProfileEditSave(Request $request) {

        $inputs = $request->input();

        $this->validate($request, array(
            'firstName' => 'required|max:100',
            'lastName' => 'required|max:100',
            'profilePic' => 'image|mimes:jpeg,png,jpg|max:2048',
            'documents' => 'mimes:pdf|max:4096',
        ));
        //debug($inputs, false);

        $image = $request->file('profilePic');
        $document = $request->file('documents');

        $inputs['profilePic'] = '';
        $inputs['varificationDoc'] = '';

        $profile = User::where('id', $inputs['mentorId'])->first();
        $social = SocialLinks::where('userId', $inputs['mentorId'])->first();
        if(empty($social)) {
            $social = new SocialLinks();
        }
        if(!empty($social)) {
            $social->userId = $inputs['mentorId'];
            $social->save();
        }

        if($inputs['facebook'] != '') {
            $social->facebook = $inputs['facebook'];
            $social->save();
        }
        if($inputs['twitter'] != '') {
            $social->twitter = $inputs['twitter'];
            $social->save();
        }
        if($inputs['linkedIn'] != '') {
            $social->linkedIn = $inputs['linkedIn'];
            $social->save();
        }
        if($inputs['github'] != '') {
            $social->github = $inputs['github'];
            $social->save();
        }
        if($inputs['other'] != '') {
            $social->other = $inputs['other'];
            $social->save();
        }
        if(!empty($social)) {
            $social->userId = $inputs['mentorId'];
            $social->save();
        }

        if($inputs['firstName'] != '') {
            $profile->firstName = $inputs['firstName'];
        }
        if($inputs['lastName'] != '') {
            $profile->lastName = $inputs['lastName'];
        }

	    if (!empty($image)) {
		    $inputs['imagename'] = time().str_replace(' ', '-', $profile->firstName).".".$image->getClientOriginalExtension();
		    $destinationPath = public_path('/profile-photo');
		    $image->move($destinationPath, $inputs['imagename']);
		    $inputs['profilePic'] = $inputs['imagename'];
	    }

	    if($inputs['profilePic'] != '') {
		    $profile->profilePic = $inputs['profilePic'];
	    }

	    if (!empty($document)) {
		    $inputs['imagename'] = time().str_replace(' ', '-', $profile->firstName).".".$document->getClientOriginalExtension();
		    $destinationPath = public_path('/profile-doc');
		    $document->move($destinationPath, $inputs['imagename']);
		    $inputs['varificationDoc'] = $inputs['imagename'];
	    }

	    if($inputs['varificationDoc'] != '') {
		    $profile->varificationDoc = $inputs['varificationDoc'];
	    }

        if($inputs['qualification'] != '') {
            $profile->qualification = $inputs['qualification'];
        }
        if($inputs['exprience'] != '') {
            $profile->experience = $inputs['exprience'];
        }
        if($inputs['tagLine']) {
            $profile->tagline = $inputs['tagLine'];
        }
        if($inputs['about'] != '') {
            $profile->about = $inputs['about'];
        }

        if($inputs['discription'] != '') {
            $profile->description = $inputs['discription'];
        }
        if($inputs['addressOne'] != '') {
            $profile->addressOne = $inputs['addressOne'];
        }
        if($inputs['addressTwo'] != '') {
            $profile->addressTwo = $inputs['addressTwo'];
        }
        if($inputs['country'] != '') {
            $profile->country = $inputs['country'];
        }
        if($inputs['state'] != '') {
            $profile->state = $inputs['state'];
        }
        if($inputs['city'] != '') {
            $profile->city = $inputs['city'];
        }
        if($inputs['zip'] != '') {
            $profile->zip = $inputs['zip'];
        }
        if($inputs['avgRatting'] != '') {
            $profile->avgRatting = $inputs['avgRatting'];
        }
        if($inputs['status'] != '') {
            $profile->status = $inputs['status'];
        }
	    if($inputs['certified'] != '') {
		    $profile->certified = $inputs['certified'];
	    }




        if($profile->save()) {
            return back()->with('message', 'Profile successfully update.');
        }else{
            return Redirect::back()->withInput()->with('errorMessage', 'Something went wrong please try again!');
        }
    }

    /*public function allMentorCourse(Request $request){
       // echo custom_decode($request->mentorId); die;

        if($request->mentorId != '') {
            $mentorCourse = Skills::select('skills.skills', 'categories.name as catname' ,'subcategories.name as subcatname')
                ->leftjoin('categories', 'categories.id', 'skills.catId')
                ->leftjoin('subcategories', 'subcategories.id', 'skills.subCatId')
                ->where('skills.userId', custom_decode($request->mentorId))->first();
            debug($mentorCourse);
        }else{
            $mentorCourse = [];
        }
        return view('admin.allMentorCourse', compact('mentorCourse'));
    }*/


    public function deleteMentor(Request $request) {
        $mentor = User::where('id', custom_decode($request->id))->first();
        if($mentor->delete()) {
            $output  = ['error' => false];
        }else{
            $output  = ['error' => true];
        }
        return response()->json($output);
    }


    /****************mentee Profile******************************/
    public function menteeList() {

        $menteeList =  User::where('roleId', 3)->get();
        return view('admin.menteeList', compact('menteeList'));
    }



    public function addMentee(Request $request) {

        if($request->menteeId != '') {
            $profile = User::select('users.*', 'socialLinks.facebook', 'socialLinks.twitter', 'socialLinks.linkedIn', 'socialLinks.github', 'socialLinks.other')
                ->leftjoin('socialLinks', 'socialLinks.userId', 'users.id')
                ->where('users.id', custom_decode($request->menteeId))->first();
        }else{
            $profile = new User();
        }

        return view('admin.menteeProfileSetting', compact('profile'));

    }

    public function menteeProfileEditSave(Request $request) {

        $inputs = $request->input();

        $this->validate($request, array(
            'firstName' => 'required|max:100',
            'lastName' => 'required|max:100',
            'profilePic' => 'image|mimes:jpeg,png,jpg|max:2048',
            'documents' => 'mimes:pdf|max:4096',
        ));
        $image = $request->file('profilePic');
        $document = $request->file('documents');

        $inputs['profilePic'] = '';
        $inputs['varificationDoc'] = '';

        $profile = User::where('id', $inputs['menteeId'])->first();

        if($inputs['firstName'] != '') {
            $profile->firstName = $inputs['firstName'];
        }
        if($inputs['lastName'] != '') {
            $profile->lastName = $inputs['lastName'];
        }

	    if (!empty($image)) {
		    $inputs['imagename'] = time().str_replace(' ', '-', $profile->firstName).".".$image->getClientOriginalExtension();
		    $destinationPath = public_path('/profile-photo');
		    $image->move($destinationPath, $inputs['imagename']);
		    $inputs['profilePic'] = $inputs['imagename'];
	    }

	    if($inputs['profilePic'] != '') {
		    $profile->profilePic = $inputs['profilePic'];
	    }


        if($inputs['qualification'] != '') {
            $profile->qualification = $inputs['qualification'];
        }

        /*if($inputs['about'] != '') {
            $profile->about = $inputs['about'];
        }

        if($inputs['discription'] != '') {
            $profile->description = $inputs['discription'];
        }*/
        if($inputs['addressOne'] != '') {
            $profile->addressOne = $inputs['addressOne'];
        }
        if($inputs['addressTwo'] != '') {
            $profile->addressTwo = $inputs['addressTwo'];
        }
        if($inputs['country'] != '') {
            $profile->country = $inputs['country'];
        }
        if($inputs['state'] != '') {
            $profile->state = $inputs['state'];
        }
        if($inputs['city'] != '') {
            $profile->city = $inputs['city'];
        }
        if($inputs['zip'] != '') {
            $profile->zip = $inputs['zip'];
        }
        /*if($inputs['avgRatting'] != '') {
            $profile->avgRatting = $inputs['avgRatting'];
        }*/
        if($inputs['status'] != '') {
            $profile->status = $inputs['status'];
        }
        if($profile->save()) {
            return back()->with('message', 'Profile successfully update.');
        }else{
            return Redirect::back()->withInput()->with('errorMessage', 'Something went wrong please try again!');
        }
    }
    public function deleteMentee(Request $request) {
        $mentor = User::where('id', custom_decode($request->id))->first();
        if($mentor->delete()) {
            $output  = ['error' => false];
        }else{
            $output  = ['error' => true];
        }
        return response()->json($output);
    }
    /****************mentee Profile******end here************************/

    /*****************All Syllabus********************/

    public function allSyllabus(Request $request) {
         $mentorId = custom_decode($request->id);
        if($mentorId != '') {
            $skillList = Skills::select('skills.*', 'categories.name as catname', 'categories.logo as catimg',
                'subCategories.name as subcatname', 'users.firstName', 'users.lastName', DB::raw('CONCAT(firstName, " ", lastName) AS username'))
                ->leftjoin('categories', 'categories.id', 'skills.catId')
                ->leftjoin('subCategories', 'subCategories.id', 'skills.subCatId')
                ->leftjoin('users', 'users.id', 'skills.userId')
                ->where('skills.userId', $mentorId)->get();
        }else{

            $skillList = Skills::select('skills.*', 'categories.name as catname', 'categories.logo as catimg',
                'subCategories.name as subcatname' , DB::raw('CONCAT(firstName, " ", lastName) AS username'))
                ->leftjoin('categories', 'categories.id', 'skills.catId')
                ->leftjoin('users', 'users.id', 'skills.userId')
                ->leftjoin('subCategories', 'subCategories.id', 'skills.subCatId')->get();
        }


        return view('admin.skillList', compact('skillList'));
    }

    public function addSkill(Request $request){
		$id = $request->id;
        $skillList = Skills::select('skills.*', 'categories.name as catname',
            'categories.logo as catimg', 'users.name as username', 'users.qualification', 'users.expertise', 'users.profilePic', 'users.avgRatting',
            'subCategories.name as subcatname')
            ->leftjoin('categories', 'categories.id', 'skills.catId')
            ->leftjoin('users', 'users.id', 'skills.userId')
            ->leftjoin('subCategories', 'subCategories.id', 'skills.subCatId')
	        ->where('skills.id', custom_decode($id))
	        ->first();

        return view('admin.addSkill', compact('skillList'));
    }

    public function purchaseCourses(Request $request) {

        $coursePurchase = Course::select('course.*', 'skills.skills', DB::raw("(select CONCAT(users.firstName, ' ', users.lastName) AS mentorname from users where users.id = course.mentorId) as mentorName"),
                DB::raw("(select CONCAT(users.firstName, ' ', users.lastName) AS menteeName from users where users.id = course.menteeId) as menteeName")
            )
            ->leftjoin('skills', 'skills.id', 'course.courseId')->get();
        return view('admin.purchaseCoursesList', compact('coursePurchase'));


    }

    public function changesStatus(Request $request){
	    $courseId= custom_decode($request->courseId);
	    $course = Course::where('id', $courseId)->first();
	    $course->courseStatus = ($course->courseStatus == 1)?0:1;
	    $course->save();
	    return back()->with('message', 'Course status successfully changed.');
    }

    public function changesStatusCourse(Request $request){
	    $courseId= custom_decode($request->courseId);
	    $course = Skills::where('id', $courseId)->first();
	    $course->status = ($course->status == 1)?0:1;
	    $course->save();
	    return back()->with('message', 'Course status successfully changed.');
    }

    public function changesStatusRate(Request $request) {
	    $id= custom_decode($request->id);
	    $rate = Ratting::where('id', $id)->first();

	    $rate->status = ($rate->status == 1)?0:1;
	    $rate->save();
	    //debug($rate);
	    return back()->with('message', 'Ratting status successfully changed.');
    }

    public function rattingManage() {
		$rattings = Ratting::select('ratting.*', DB::raw("(select CONCAT(users.firstName, ' ', users.lastName) AS mentorname from users where users.id = ratting.mentorId) as mentorName"),
			DB::raw("(select CONCAT(users.firstName, ' ', users.lastName) AS menteeName from users where users.id = ratting.menteeId) as menteeName"), 'course.courseId as skillId')
				->leftjoin('course', 'course.id', 'ratting.courseId')->get();
	    //debug($rattings);
	    return view('admin.rattings', compact('rattings'));
    }

    public function contactUs() {
    	$contacts = ContactUs::get();
    	//debug($contacts);
	    return view('admin.contactUs', compact('contacts'));
    }






}

