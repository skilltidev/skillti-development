importScripts('https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.9.1/firebase-messaging.js');
var config = {
    apiKey: "AIzaSyDuWXb_J60JkFp9OnaMbuK2a4Hds_UVKGg",
    authDomain: "skillti.firebaseapp.com",
    databaseURL: "https://skillti.firebaseio.com",
    projectId: "skillti",
    storageBucket: "skillti.appspot.com",
    messagingSenderId: "984583284362"
};
firebase.initializeApp(config);
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
    const notificationTitle = payload.data.title;
    const notificationOptions = {
        body: payload.data.body,
        icon: 'http://test.skillti.com/images/skillti_logo_4_update-fav_icon.png',
        image: 'http://test.skillti.com/images/skillti_update.png'
    };
    return self.registration.showNotification(notificationTitle, notificationOptions);
});