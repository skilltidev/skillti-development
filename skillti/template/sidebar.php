	<?php
	    $Role = $_SESSION['USER_ROLE'];
	    $UserID = $_SESSION['USER_ID'];
	  ?>
	
	<div class="nav-side-menu">
		<div class="menu-list">
  
			<ul id="menu-content" class="menu-content">
				<li>
				  <a href="dashboard"><i class="fa fa-dashboard fa-lg fa-fw sidebar-icon"></i> Dashboard</a>
				</li>
				<?php 
				 $query = "select * from messagedata where SenderID='$UserID' OR RecieverID='$UserID'";
				$Result = $dbObject->query($query);
				$RowCount = mysqli_num_rows($Result);
				if($RowCount > 0) {
				?>
				<li>
				  <a href="messages"><i class="fa fa-envelope-open fa-lg fa-fw sidebar-icon"></i> Message</a>
				</li>
				<?php } ?>
				<li  data-toggle="collapse" data-target="#manage" class="collapsed">
					<a href="javascript:void(0)"><i class="fa fa-puzzle-piece fa-lg fa-fw sidebar-icon"></i> My Profile <span class="arrow"></span></a>
				</li>
				<ul class="sub-menu collapse" id="manage">
					<li><a href="viewProfile"><i class="fa fa-angle-double-right"></i> View Profile</a></li>
					<li><a href="editProfile"><i class="fa fa-angle-double-right"></i> Edit Profile</a></li>
					
					<li><a href="changePassword"><i class="fa fa-angle-double-right"></i> Password & Security</a></li>
				</ul>
				
				<?php if($Role == 2) { ?>	
				<li  data-toggle="collapse" data-target="#manage2" class="collapsed">
					<a href="javascript:void(0)"><i class="fa fa-crosshairs fa-lg fa-fw sidebar-icon"></i> All Syllabus<span class="arrow"></span></a>
				</li>
				<ul class="sub-menu collapse" id="manage2">
					<li><a href="allSyllabus"><i class="fa fa-angle-double-right"></i> All Syllabus</a></li>
					<li><a href="addSyllabus"><i class="fa fa-angle-double-right"></i> Add New Syllabus</a></li>
				</ul>
				
			    
				<li  data-toggle="collapse" data-target="#maintenance" class="collapsed">
					<a href="SocialMediaLink"><i class="fa fa-cogs fa-lg fa-fw sidebar-icon"></i> Link your social media </a>
				</li>
			    <?php } ?>
				
				<li  data-toggle="collapse" data-target="#help" class="collapsed">
					<a href="#"><i class="fa fa-life-ring fa-lg fa-fw sidebar-icon"></i> Payment </a>
				</li>
				<?php if($Role == 2) { ?>
				<li  data-toggle="collapse" data-target="#help" class="collapsed">
					<a href="#"><i class="fa fa-life-ring fa-lg fa-fw sidebar-icon"></i> Paid from Skillti </a>
				</li>
			    <?php } ?>
			</ul>
	 </div>
</div>
