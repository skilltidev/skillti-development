@extends('header.dashboard_header')
@section('headtitle', "Dashboard")
@section('headdesc', "")
@section('maincontent')
    <div class="mdk-drawer-layout__content page">
        <div class="container-fluid page__container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">Add Syllabus</li>
            </ol>
            @if(isset($skills->catId))
            <h1 class="h2">Edit Course</h1>
            @else
            <h1 class="h2">Add New Course</h1>
            @endif
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            @if(Session::get('message'))
                                <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Success!</strong> {{ Session::get('message') }}{{ Session::forget('message') }}
                                </div>
                            @endif
                            @if(Session::get('errorMessage'))
                                <div class="alert alert-danger">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Message !</strong> {{ Session::get('errorMessage') }}{{ Session::forget('errorMessage') }}
                                </div>
                            @endif
                            <form id="form" action="{{url('dashboard/addEditSkill')}}" method="post" class="form-horizontal">
                                @csrf
                                @if(isset($skills->catId))
                                    <input type="hidden" name="id" id="id" value="{{custom_encode($skills->id)}}">
                                    <?php $selectSkill = json_decode($skills->skills); ?>
                                    <?php $count = old('skillCount')?old('skillCount'):count($selectSkill); ?>
                                @else
                                    <?php $count = old('skillCount')?old('skillCount'):1; ?>
                                @endif
                                <input type="hidden" name="skillCount" id="skillCount" value="{{$count}}">
                                <div class="form-group row">
                                    <label for="category" class="col-sm-3 col-form-label form-label">Category</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <?php $selectedCat = old('category')?old('category'):(isset($skills->catId)?$skills->catId:''); ?>
                                            <select required name="category" id="category" class="form-control custom-select">
                                                <option value="">Select Category</option>
                                                @foreach($category as $cat)
                                                <option {{($selectedCat == $cat->id)?"selected":""}} value="{{$cat->id}}">{{$cat->name}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('category'))
                                                <span style="display: block;" class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('category') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="subCategory" class="col-sm-3 col-form-label form-label">Sub Category</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <select name="subCategory" required disabled="" id="subCategory" class="form-control custom-select">
                                                <option value="">Select Sub-Category</option>
                                            </select>
                                            @if ($errors->has('subCategory'))
                                                <span style="display: block;" class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('subCategory') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fee" class="col-sm-3 col-form-label form-label">Course Fee</label>
                                    <div class="col-sm-8 col-md-8">
                                        <div class="input-group">
                                            <input required id="fee" name="fee" type="number" class="form-control" placeholder="Course Fee" value="{{old('fee')?old('fee'):(isset($skills->coursefee)?$skills->coursefee:'')}}">
                                        </div>
                                        @if ($errors->has('fee'))
                                            <span style="display: block;" class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('fee') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div id="skills">
                                    @for($i = 0; $i < $count; $i++)
                                    <div class="form-group row">
                                        <label for="skill" class="col-sm-3 col-form-label form-label">{{($i==0)?"Name of Skills":""}}</label>
                                        <div id="skill" class="col-sm-8 col-md-8">
                                            <div class="input-group">
                                                <input name="skill[]" type="text" class="form-control" placeholder="Skill Name" value="{{old('skill.'.$i)?old('skill.'.$i):(isset($selectSkill[$i])?$selectSkill[$i]:'')}}">
                                            </div>
                                            @if ($errors->has('skill.'.$i))
                                                <span style="display: block;" class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('skill.'.$i) }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        @if($i > 0)
                                        <div class="col-md-1">
                                            <div class="input-group" style="margin-top: 7px;">
                                                <a onclick="javascript:confirm('Are you sure?')" class="btn btn-outline-danger btn-sm"><i class="material-icons">delete_forever</i></a>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    @endfor
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="float-right">
                                            <a class="btn btn-outline-primary btn-sm" id="addSkill"><i class="material-icons md-18 text-muted">exposure_plus_1</i>&nbsp; Skill</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fee" class="col-sm-3 col-form-label form-label">Short Description</label>
                                    <div class="col-sm-8 col-md-8">
                                        <div class="input-group">
                                            <textarea required id="shortBio" name="shortBio" class="form-control" placeholder="Short Description" >{{old('shortBio')?old('shortBio'):(isset($skills->shortBio)?$skills->shortBio:'')}}</textarea>
                                        </div>
                                        @if ($errors->has('shortBio'))
                                            <span style="display: block;" class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('shortBio') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label for="description" class="col-sm-3 col-form-label form-label">Full Description</label>
                                    <div class="col-sm-8">
                                        <div id="description" name="description" class="input-group" style="height: 150px;" data-toggle="quill" data-quill-placeholder="Quill WYSIWYG editor">
                                            {!! old('discription')?old('discription'):(isset($skills->description)?$skills->description:'') !!}
                                        </div>
                                        @if ($errors->has('discription'))
                                            <span style="display: block;" class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('discription') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <textarea name="discription" id="discription" style="display: none;"></textarea>
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="addMoreskill" style="display: none;">
            <div class="form-group row">
                <label for="skill" class="col-sm-3 col-form-label form-label">&nbsp;</label>
                <div id="skill" class="col-sm-8 col-md-8">
                    <div class="input-group">
                        {{--<div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="material-icons md-18 text-muted">announcement</i>
                            </div>
                        </div>--}}
                        <input name="skill[]" type="text" class="form-control" placeholder="Skill Name" value="">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="input-group" style="margin-top: 7px;">
                        <a onclick="javascript:confirm('Are you sure?')" class="btn btn-outline-danger btn-sm"><i class="material-icons">delete_forever</i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('menu')
    @include('dashboard.menu')
@endsection
@section('scripting')
    <!-- Quill -->
    <script src="{{asset('assets/vendor/quill.min.js')}}"></script>
    {{--<script src="{{asset('assets/js/quill.js')}}"></script>--}}
    <script>
        var editor = new Quill('#description', {
            modules: {
                'toolbar': {},
            },
            theme: "snow",
            placeholder: Quill.DEFAULTS.placeholder,
            readOnly: Quill.DEFAULTS.readOnly,
            debug: Quill.DEFAULTS.debug,
            formats: Quill.DEFAULTS.formats
        });

        $('#form').submit(function (e) {
            $('#discription').val(document.querySelector(".ql-editor").innerHTML);
        });
    </script>
    <script>
        $(document).ready(function () {
            if($('#category').val() != '') {
                getSubCat($('#category').val());
            }
            $('#category').change(function () {
                var category = $(this).val();
                getSubCat(category);
            });
            $('#addSkill').click(function () {
                var skillCount = $('#skillCount').val();
               $('#skills').append($('#addMoreskill').html());
                $('#skillCount').val(parseInt(skillCount) + 1);
            });
        });
        $(document).on('click', '.btn-outline-danger', function () {
            var skillCount = $('#skillCount').val();
            $(this).parents('.form-group.row').remove();
            $('#skillCount').val(parseInt(skillCount) - 1);
        });
        function getSubCat(category) {
            $('#subCategory').attr('disabled', 'disabled');
            $.ajax({
                url: '{{url('dashboard/get-category')}}',
                dataType: 'html',
                data: { category : category },
                success: function(data) {
                    $('#subCategory').html( data );
                    $('#subCategory').removeAttr('disabled');
                    var selected = "{{old('subCategory')?old('subCategory'):(isset($skills->subCatId)?$skills->subCatId:'')}}";
                    $('#subCategory').val(selected).change();
                }
            });
        }
    </script>
@endsection
@section('footer')
    @include('footer.footer_dashboard')
@endsection
