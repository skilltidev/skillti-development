@extends('header.student_header')
@section('headtitle', "Dashboard")
@section('headdesc', "")
@section('maincontent')
    <div class="mdk-drawer-layout__content page">
        <div class="container-fluid page__container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="instructor-dashboard.html">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <h1 class="h2">Dashboard</h1>
            <div class="row">
                <div class="col-lg-7">
                    <div class="card">
                        <div class="card-header">
                            <div class="media align-items-center">
                                <div class="media-body">
                                    <h4 class="card-title">Courses</h4>
                                    <p class="card-subtitle">Your recent courses</p>
                                </div>
                                <div class="media-right">
                                    <a class="btn btn-sm btn-primary" href="{{url('student-dashboard/my-course')}}">My courses</a>
                                </div>
                            </div>
                        </div>
                        <ul class="list-group list-group-fit mb-0" style="z-index: initial;">
                            @foreach($coureses as $skill)
                            <li class="list-group-item" style="z-index: initial;">
                                <div class="d-flex align-items-center">
                                    <a href="javascript:void(0);" class="avatar avatar-4by3 mr-3">
                                        <img src="{{asset('admin-assets/cat-image/'.$skill->logo)}}" alt="course" class="avatar-img rounded">
                                    </a>
                                    <div class="flex">
                                        <a href="javascript:void(0);" class="text-body"><strong>{{$skill->catName}}</strong></a>
                                        <h4 class="card-title mb-1">{{$skill->subCatName}} &nbsp;</h4>
                                    </div>
                                    <div class="dropdown ml-3">
                                        <a href="#" class="dropdown-toggle text-muted" data-caret="false" data-toggle="dropdown">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="{{url('student-dashboard/my-course/start-chat/'.custom_encode($skill->id))}}">Messages</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <dev class="col-lg-5">
                    <div class="card">
                        <div class="card-header">
                            <div class="media align-items-center">
                                <div class="media-body">
                                    <h4 class="card-title">Latest message</h4>
                                    <p class="card-subtitle">Recent messages sent by mentor</p>
                                </div>
                            </div>
                        </div>
                        <ul class="list-group list-group-fit">
                            @foreach($chattings as $chat)
                            <li class="list-group-item forum-thread">
                                <div class="media align-items-center">
                                    <div class="media-left">
                                        <div class="forum-icon-wrapper">
                                            <a href="{{url('student-dashboard/my-course/start-chat/'.custom_encode($chat->courseId))}}" class="forum-thread-icon">
                                                <img src="{!! profile_image(asset('profile-photo/'.$chat->profilePic)) !!}" alt="profile" width="20" class="img-responsive">
                                            </a>
                                            <a href="{{url('student-dashboard/my-course/start-chat/'.custom_encode($chat->courseId))}}" class="forum-thread-user">
                                                <img src="{!! profile_image(asset('profile-photo/'.$chat->profilePic)) !!}" alt="profile" width="20" class="rounded-circle">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <div class="d-flex align-items-center">
                                            <a href="{{url('student-dashboard/my-course/start-chat/'.custom_encode($chat->courseId))}}" class="text-body"><strong>{{$chat->firstName." ".$chat->lastName}}</strong></a>
                                            <small class="ml-auto text-muted">{!! posted_ago($chat->created_at) !!}</small>
                                        </div>
                                        <small>{!! $chat->message !!}</small>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </dev>
            </div>
        </div>
    </div>
@endsection
@section('menu')
    @include('student.menu')
@endsection
@section('scripting')
    <script>
        (function () {
            'use strict';

            Charts.init()

            var earnings = []

            // Create a date range for the last 7 days
            var start = moment().subtract(7, 'days').format('YYYY-MM-DD') // 7 days ago
            var end = moment().format('YYYY-MM-DD') // today
            var range = moment.range(start, end)

            // Create the earnings graph data
            // Iterate the date range and assign a random ($) earnings value for each day
            range.by('days', function (moment) {
                earnings.push({
                    y: Math.floor(Math.random() * 300) + 10,
                    x: moment.toDate()
                })
            })

            var Earnings = function (id, type = 'roundedBar', options = {}) {
                options = Chart.helpers.merge({
                    barRoundness: 1.2,
                    scales: {
                        yAxes: [{
                            ticks: {
                                callback: function (a) {
                                    if (!(a % 10))
                                        return "$" + a
                                }
                            }
                        }],
                        xAxes: [{
                            offset: true,
                            ticks: {
                                padding: 10
                            },
                            maxBarThickness: 20,
                            gridLines: {
                                display: false
                            },
                            type: 'time',
                            time: {
                                unit: 'day'
                            }
                        }]
                    },
                    tooltips: {
                        callbacks: {
                            label: function (a, e) {
                                var t = e.datasets[a.datasetIndex].label || "",
                                    o = a.yLabel,
                                    r = "";
                                return 1 < e.datasets.length && (r += '<span class="popover-body-label mr-auto">' + t + "</span>"), r += '<span class="popover-body-value">$' + o + "</span>"
                            }
                        }
                    }
                }, options)

                var data = {
                    datasets: [{
                        label: "Earnings",
                        data: earnings
                    }]
                }

                Charts.create(id, type, options, data)
            }
            // Create Chart
            Earnings('#earningsChart')
        })()
    </script>
@endsection
@section('footer')
    @include('footer.footer_dashboard')
@endsection
