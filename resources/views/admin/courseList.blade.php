@extends('header.admin_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")
<style>
    .modal-backdrop {
        position: relative !important;
</style>
@section('maincontent')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper"><!-- HTML (DOM) sourced data -->
                <section id="html">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body ">
                                    <form class="form px-3 mt-2">
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="fa fa-th-list"></i> All Course List

                                            </h4>
                                        </div>
                                    </form>
                                    <div class="card-block card-dashboard table-responsive">
                                        <table class="table table-striped table-bordered sourced-data">
                                            <thead>
                                            <tr>
                                                <th>SNO</th>
                                                <th>Course</th>
                                                <th>Mentor Name</th>
                                                <th>Duration</th>
                                                <th>Payment Status</th>
                                                <th>Course Status</th>
                                              {{--  <th>Short Bio</th>--}}
                                             {{--   <th>Type</th>--}}
                                                <th>Fee</th>
                                                <th>Status</th>
                                                <th>Action</th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(!empty($skillList))
                                                @foreach($skillList as $k => $val)
                                                    <tr>
                                                        <td>{{($k + 1)}}</td>
                                                        <td class="text-center">
                                                            <img style="width:50px;height:50px" src="{{($val->catimg != '')?asset('admin-assets/cat-image/'.$val->catimg):asset('admin-assets/cat-image/catImg.PNG')}}" />
                                                        <td>{{$val->catname}}</td>
                                                        <td>{{$val->subcatname}}</td>
                                                        <td>{{$val->skills}}</td>
                                                      {{--  <td>{{$val->shortBio}}</td>--}}
                                                      {{--  <td>{{$val->courseType}}</td>--}}
                                                        <td>{{$val->coursefee}}</td>
                                                        <td class="text-center">{{($val->status == 1)?'Active':'InActive'}}</td>
                                                        <td class="text-center">
                                                            &nbsp;&nbsp; <a title="Delete"
                                                                            id="{{custom_encode($val->id)}}"
                                                                            class="delete">
                                                                <i class="fa fa-times" aria-hidden="true"
                                                                   style="font-size: 20px !important;color:red;"></i>
                                                            </a>
                                                        </td>


                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ HTML (DOM) sourced data -->
            </div>
        </div>
    </div>



    <!-- Modal -->

@endsection
@section('scripting')
    <script>
        $('.delete').click(function () {
            if (confirm("Are you sure!")) {
                var el = this;
                var id = this.id;
                var deleteid = id;
                $.ajax({
                    url: '{{url('admin/delete-mentee')}}',
                    type: 'GET',
                    data: {id: deleteid},
                    postType: "JSON",
                    success: function (response) {
                        if (response.error == false) {
                            $(el).closest('tr').css('background', 'tomato');
                            $(el).closest('tr').fadeOut(800, function () {
                                $(this).remove();
                            });
                        }
                    }
                });
            }
        });
    </script>
@endsection
@section('footer')
    @include('footer.footer_admin')
@endsection
