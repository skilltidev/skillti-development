@extends('header.dashboard_header')
@section('headtitle', "Dashboard")
@section('headdesc', "")
@section('maincontent')
    <div class="mdk-drawer-layout__content page">
        <div class="container-fluid page__container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="instructor-dashboard.html">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <h1 class="h2">Dashboard</h1>
            <div class="row">
                <div class="col-md-6">
                    <div class="card bg-primary text-white text-center">
                        <div class="card-header">
                            <h4 class="card-title">{{$skills}}. Course</h4>
                        </div>
                        <div class="card-body">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card bg-success text-white text-center">
                        <div class="card-header">
                            <h4 class="card-title">{{$students}}. Students</h4>
                        </div>
                        <div class="card-body">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card bg-dark text-white text-center">
                        <div class="card-header">
                            <h4 class="card-title">{!! get_price_html($transaction) !!} Transaction</h4>
                        </div>
                        <div class="card-body">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card bg-warning text-white text-center">
                        <div class="card-header">
                            <h4 class="card-title">{{$mailbox}}. Mailbox</h4>
                        </div>
                        <div class="card-body">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('menu')
    @include('dashboard.menu')
@endsection
@section('scripting')
    <script>
        (function () {
            'use strict';

            Charts.init()

            var earnings = []

            // Create a date range for the last 7 days
            var start = moment().subtract(7, 'days').format('YYYY-MM-DD') // 7 days ago
            var end = moment().format('YYYY-MM-DD') // today
            var range = moment.range(start, end)

            // Create the earnings graph data
            // Iterate the date range and assign a random ($) earnings value for each day
            range.by('days', function (moment) {
                earnings.push({
                    y: Math.floor(Math.random() * 300) + 10,
                    x: moment.toDate()
                })
            })

            var Earnings = function (id, type = 'roundedBar', options = {}) {
                options = Chart.helpers.merge({
                    barRoundness: 1.2,
                    scales: {
                        yAxes: [{
                            ticks: {
                                callback: function (a) {
                                    if (!(a % 10))
                                        return "$" + a
                                }
                            }
                        }],
                        xAxes: [{
                            offset: true,
                            ticks: {
                                padding: 10
                            },
                            maxBarThickness: 20,
                            gridLines: {
                                display: false
                            },
                            type: 'time',
                            time: {
                                unit: 'day'
                            }
                        }]
                    },
                    tooltips: {
                        callbacks: {
                            label: function (a, e) {
                                var t = e.datasets[a.datasetIndex].label || "",
                                    o = a.yLabel,
                                    r = "";
                                return 1 < e.datasets.length && (r += '<span class="popover-body-label mr-auto">' + t + "</span>"), r += '<span class="popover-body-value">$' + o + "</span>"
                            }
                        }
                    }
                }, options)

                var data = {
                    datasets: [{
                        label: "Earnings",
                        data: earnings
                    }]
                }

                Charts.create(id, type, options, data)
            }
            // Create Chart
            Earnings('#earningsChart')
        })()
    </script>
@endsection
@section('footer')
    @include('footer.footer_dashboard')
@endsection
