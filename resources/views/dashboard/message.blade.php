<li class="message sender d-inline-flex">
    <div class="message__aside">
        <a href="javascript:void(0)" class="avatar">
            <img src="{!! profile_image(asset('profile-photo/'.Auth::user()->profilePic)) !!}" alt="people" class="avatar-img rounded-circle">
        </a>
    </div>
    <div class="message__body card">
        <div class="card-body">
            <div class="d-flex align-items-center">
                <div class="flex mr-3">
                    <a href="javascript:void(0)" class="text-body"><strong>{{Auth::user()->firstName." ".Auth::user()->lastName}}</strong></a>
                </div>
                <div>
                    <small class="text-muted">{!! posted_ago($chatting->created_at) !!}</small>
                </div>
            </div>
            <span class="text-black-70">{!! $chatting->message !!}</span>
            @if($chatting->attachment != '')
                <a target="_blank" href="{{asset('chatt/'.$chatting->attachment)}}" class="media align-items-center mt-2 text-decoration-0 bg-white px-3">
                    <span class="avatar mr-2">
                        <span class="avatar-title rounded-circle">
                            <i class="material-icons font-size-24pt">attach_file</i>
                        </span>
                    </span>
                    <span class="media-body" style="line-height: 1.5">
                        <span class="text-primary">{{$chatting->attachment}}</span><br>
                    </span>
                </a>
            @endif
        </div>
    </div>
</li>