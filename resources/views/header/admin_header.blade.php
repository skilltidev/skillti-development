<!DOCTYPE html>
<html lang="en" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('headtitle')</title>
    <meta name="description" content="@yield('headdesc')">
    <link rel="icon" href="{{asset('images/skillti_logo_4_update-fav_icon.png')}}" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/fonts/feather/style.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/fonts/simple-line-icons/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/vendors/css/chartist.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/css/app.css')}}">

    <!-- Quill Theme -->
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">

    <style>
        .nowrap{
            white-space: nowrap;
        }
    </style>
</head>
<body data-col="2-columns" class=" 2-columns ">
<div class="wrapper nav-collapsed menu-collapsed">
    <div data-active-color="white" data-background-color="purple-bliss" data-image="app-assets/img/nav-bg.jpg" class="app-sidebar">
        <!-- main menu header-->
        <!-- Sidebar Header starts-->
        <div class="sidebar-header">
            <div class="logo clearfix">
                <a href="{{url('admin/dashboard')}}" class="logo-text float-left">
                    <div class="logo-img">
                        <img src="{{asset('images/skillti_logo_4_update-fav_icon.png')}}"/>
                    </div>
                    <span class="text align-middle">SKILLTI</span>
                </a>
                <a id="sidebarToggle" href="javascript:void(0);" class="nav-toggle d-none d-sm-none d-md-none d-lg-block">
                    <i data-toggle="collapsed" class="ft-toggle-left toggle-icon"></i>
                </a>
                <a id="sidebarClose" href="javascript:void(0);" class="nav-close d-block d-md-block d-lg-none d-xl-none">
                    <i class="ft-x"></i>
                </a>
            </div>
        </div>
        <!-- Sidebar Header Ends-->
        <!-- / main menu header-->
        <!-- main menu content-->
        <div class="sidebar-content">
            <div class="nav-container">
                <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
                    <li class="nav-item {{(Request::route()->getName() == 'admin.dashboard')?'active':''}}">
                        <a href="{{url('admin/dashboard')}}">
                            <i class="ft-home"></i>
                            <span data-i18n="" class="menu-title">Dashboard</span>
                        </a>
                    </li>
                    <li class="has-sub nav-item {{(Request::route()->getName() == 'admin.menteeList' || Request::route()->getName() == 'admin.mentorList')?'active':''}}">
                        <a href="javascript:void(0)">
                            <i class="fa fa-users"></i>
                            <span data-i18n="" class="menu-title">User Management</span>
                        </a>
                        <ul class="menu-content">
                            <li class="{{(Request::route()->getName() == 'admin.mentorList')?'active':''}}">
                                <a href="{{url('/admin/all-mentors')}}" class="menu-item"><i class="fa fa-list" aria-hidden="true"></i> All Mentors</a>
                            </li>
                            <li class="{{(Request::route()->getName() == 'admin.menteeList')?'active':''}}">
                                <a href="{{url('/admin/all-mentee')}}" class="menu-item"><i class="fa fa-list" aria-hidden="true"></i> All Mentee</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub nav-item {{(Request::route()->getName() == 'admin.categories' || Request::route()->getName() == 'admin.subCategories')?'active':''}}">
                        <a href="javascript:void(0)">
                            <i class="fa fa-sitemap"></i>
                            <span data-i18n="" class="menu-title">Categories</span>
                        </a>
                        <ul class="menu-content">
                            <li class="{{(Request::route()->getName() == 'admin.categories')?'active':''}}">
                                <a href="{{url('/admin/all-categories')}}" class="menu-item"><i class="fa fa-th-list" aria-hidden="true"></i> Category</a>
                            </li>
                            <li class="{{(Request::route()->getName() == 'admin.subCategories')?'active':''}}">
                                <a href="{{url('/admin/all-subCategories')}}" class="menu-item"><i class="fa fa-th-list" aria-hidden="true"></i>Sub Category</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item {{(Request::route()->getName() == 'admin.allSyllabus')?'active':''}}">
                        <a href="{{url('/admin/all-syllabus')}}">
                            <i class="fa fa-crosshairs"></i>
                            <span data-i18n="" class="menu-title">All Syllabus</span>
                        </a>
                    </li>

                    <li class="nav-item {{(Request::route()->getName() == 'admin.allSyllabusP')?'active':''}}">
                        <a href="{{url('/admin/purchase-courses')}}">
                            <i class="fa fa-crosshairs"></i>
                            <span data-i18n="" class="menu-title">All Course</span>
                        </a>
                    </li>
                    <li class="nav-item {{(Request::route()->getName() == 'admin.contactUs')?'active':''}}">
                        <a href="{{url('/admin/contact-us')}}">
                            <i class="fa fa-phone-square"></i>
                            <span data-i18n="" class="menu-title">Contact Us Detail</span>
                        </a>
                    </li>
                    <li class="nav-item {{(Request::route()->getName() == 'admin.ratting')?'active':''}}">
                        <a href="{{url('/admin/review')}}">
                            <i class="fa fa-star"></i>
                            <span data-i18n="" class="menu-title">Rating Management</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="sidebar-background"></div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light bg-faded">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-left">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <form role="search" class="navbar-form navbar-right mt-1">
                    <a href="{{url('admin/dashboard')}}">
                        <img style="height: 55px;" src="{{asset('admin-assets/img/logo.png')}}">
                    </a>
                </form>
            </div>
            <div class="navbar-container">
                <div id="navbarSupportedContent" class="collapse navbar-collapse">
                    <ul class="navbar-nav">
                        <li class="nav-item ">
                            <p class="username" style="color: #000 !important;">{{Auth::user()->name}}</p>
                        </li>
                        <li class="dropdown nav-item">
                            <a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle">
                                <i class="ft-user font-medium-3 blue-grey darken-4"></i>
                                <p class="d-none">User Settings</p>
                            </a>
                            <div ngbdropdownmenu="" aria-labelledby="dropdownBasic3" class="dropdown-menu dropdown-menu-right">
                                {{--<a href="" class="dropdown-item py-1">
                                    <i class="ft-edit mr-2"></i><span>My Profile</span>
                                </a>
                                <a href="" class="dropdown-item py-1">
                                    <i class="ft-settings mr-2"></i><span>Reset Password</span>
                                </a>--}}
                                <a href="{{url('logout')}}" class="dropdown-item py-1">
                                    <i class="ft-power mr-2"></i><span>Logout</span>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- Navbar (Header) Ends-->
@yield('maincontent')
    <footer class="footer footer-static footer-light">
        <p class="clearfix text-muted text-sm-center px-2"><span>Copyright  &copy; 2018 <a href="{{url('/')}}" id="pixinventLink" target="_blank" class="text-bold-800 primary darken-2">SKILLTI </a>, All rights reserved. </span></p>
    </footer>
</div>
@yield('footer')
</body>
</html>
