<?php

namespace App\Http\Middleware;

use App\Models\Categories;
use App\Models\SubCategories;
use Closure;

class HeaderMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$categories = Categories::where('status', 1)
		    ->where('menuOrder', '>', 0)
		    ->limit(7)
		    ->orderBy('menuOrder')->get();
    	if(!empty($categories)) {
    		foreach ($categories as $k => $cat) {
    			$subCategory = SubCategories::where('catId', $cat->id)->where('status', 1)->get();
    			$cat->subCategory = $subCategory;
		    }
	    }
	    view()->share('menus', $categories);
        return $next($request);
    }
}
