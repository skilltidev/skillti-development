@extends('header.home_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")

@section('maincontent')
    <style>
        .why-stripe .bullet-points li {
            color: #000;
        }
        .why-stripe .intro-text {
            color: #000;
        }

    </style>
    <section class="mid_content">
        <!--<div class="gradient-background" style="background-image:url('assets/images/map.PNG');"></div>-->
        <div class="container">
            <div class="col-xs-12">
                <div class="heading text-center">
                    <h2>coming soon</h2>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripting')
@endsection

@section('footer')
    @include('footer.footer_home')
@endsection
