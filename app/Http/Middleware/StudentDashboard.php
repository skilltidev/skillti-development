<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class StudentDashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    if (Auth::check()) {
		    $userData = Auth::user();
		    if($userData->roleId == 1) {
			    return redirect('/signin')->with('errorMessage', 'You are not authorised for admin access.');
		    }
		    if($userData->roleId == 2) {
			    return redirect('/signin')->with('errorMessage', 'You are not authorised for mentor access.');
		    }
	    }else{
		    return redirect('/signin')->with('errorMessage', 'Please login for acceess admin dashboard.');
	    }
	    return $next($request);
    }
}
