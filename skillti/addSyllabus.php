<?php 
	include('template/header.php'); 
	protect_admin_page();
	
    $UserID = $_SESSION['USER_ID'];
	$UserEmail = $_SESSION['USER_NAME'];
	
	$eid = $_GET['edit'];
	$UpdQuery = "select * from syllabus where ID='$eid' and UserID='$UserID'";
	$UpdResult = $dbObject->query($UpdQuery);
	$UpdRow = mysqli_fetch_assoc($UpdResult);
	$catID = $UpdRow['Category'];
	
	
	

	if(isset($_POST['AddSyllabus'])) {
		$cat = $_POST['cat'];
	    $subcat = $_POST['subcat'];
		$other = $_POST['other'];
		$key1 = str_replace("'", "", $_POST['key1']);
		$key2 = str_replace("'", "", $_POST['key2']);
		$key3 = str_replace("'", "", $_POST['key3']);
		$key4 = str_replace("'", "", $_POST['key4']);
		$key5 = str_replace("'", "", $_POST['key5']);
		
		$key6 = str_replace("'", "", $_POST['key6']);
		$key7 = str_replace("'", "", $_POST['key7']);
		$key8 = str_replace("'", "", $_POST['key8']);
		$key9 = str_replace("'", "", $_POST['key9']);
		$key10 = str_replace("'", "", $_POST['key10']);
		
		$description = str_replace("'", "", $_POST['description']);
		
		
		if($other != '') {
			$CQuery = "insert into category (UserID,CatName,Parent,Status) values ('$UserID','$other','$cat','INACTIVE')";
			$dbObject->query($CQuery);
		}
		
		$Query = "insert into syllabus (UserID,Category,SubCategory,OtherSubCat,KeyPoint1,KeyPoint2,KeyPoint3,KeyPoint4,KeyPoint5,KeyPoint6,KeyPoint7,KeyPoint8,KeyPoint9,KeyPoint10,Description) values ('$UserID','$cat','$subcat','$other','$key1','$key2','$key3','$key4','$key5','$key6','$key7','$key8','$key9','$key10','$description')";
		$Result = $dbObject->query($Query);
		if($Result == true) {
			$_SESSION['SESS_MSG'] = "Syllabus has beed added.";
         }
		
		
		
		
		
	}

	
	if(isset($_POST['UpdSyllabus'])) {
		$cat = $_POST['cat'];
		$subcat = $_POST['subcat'];
		$other = $_POST['other'];
		$key1 = str_replace("'", "", $_POST['key1']);
		$key2 = str_replace("'", "", $_POST['key2']);
		$key3 = str_replace("'", "", $_POST['key3']);
		$key4 = str_replace("'", "", $_POST['key4']);
		$key5 = str_replace("'", "", $_POST['key5']);
		
		$key6 = str_replace("'", "", $_POST['key6']);
		$key7 = str_replace("'", "", $_POST['key7']);
		$key8 = str_replace("'", "", $_POST['key8']);
		$key9 = str_replace("'", "", $_POST['key9']);
		$key10 = str_replace("'", "", $_POST['key10']);
		
		$description = str_replace("'", "", $_POST['description']);
		
		if($other != '') {
			$CQuery = "update category set CatName='".$other."', Parent='".$cat."', Status='INACTIVE' where CatID='".$subcat."' and UserID='".$UserID."'";
			$dbObject->query($CQuery);
		}
		
		$Query = "update syllabus set Category='".$cat."', SubCategory='".$subcat."', OtherSubCat='".$other."', KeyPoint1='".$key1."', KeyPoint2='".$key2."', KeyPoint3='".$key3."', KeyPoint4='".$key4."', KeyPoint5='".$key5."', KeyPoint6='".$key6."', KeyPoint7='".$key7."', KeyPoint8='".$key8."', KeyPoint9='".$key9."', KeyPoint10='".$key10."', Description='".$description."' where ID='".$eid."' and UserID='".$UserID."'";
		$Update = $dbObject->query($Query);
		if($Update == true) {
		    print "<script>setTimeout(\"location.href = '".$base_url."addSyllabus?edit=".$eid."';\",500);</script>";
	        session_start();
			$_SESSION['SESS_MSG'] = "Syllabus has beed updated.";
		}
	}
?>


<section class="mid_content dashboard_content">
	<div class="container-fluid pl-0">
		<div class="row">
			<div class="col-xs-12 col-sm-3">
			<?php include ('template/sidebar.php');?>
			</div>
			<div class="col-xs-12 col-sm-9 right_side">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<div class="custom_form">
				<?php include("include/error.msg.inc.php"); ?>
					 <form method="post" action="" enctype="multipart/form-data" >
						<div class="form-group">
						  <label for="">Category</label>
							 <select class="form-control" name="cat" id="cat" required>
							     <option value="">Select Category</option>
							     <?php
							        $Query = "select * from category where parent=0 and Status='ACTIVE'";
                                	$Result =  $dbObject->query($Query);
                                	while($Row = mysqli_fetch_assoc($Result)) {
                                ?>
								<option value="<?php echo $Row['CatID'];?>"><?php echo $Row['CatName'];?></option>
								<?php } ?>
							
							</select>
						</div>
						<div class="form-group" id="AddSubCat">
						    <label for="">Sub Category</label>
						    <?php if(!empty($eid)) { ?>
                        	 <select class="form-control" name="subcat" id="subcat" style="display:none;">
                        		<option value="">Select Sub Category</option>
                        	 </select>
                        	 
							<select class="form-control" name="subcat" id="subcat2" >
								<option value="">Select Sub Category</option>
								 <?php
									$Query = "select * from category where Parent='$catID' and Status='ACTIVE'";
									$Result =  $dbObject->query($Query);
									while($Row = mysqli_fetch_assoc($Result)) {
								?>
								<option value="<?php echo $Row['CatID'];?>"><?php echo $Row['CatName'];?></option>
								<?php } ?>
								<option value="999999">Other</option>
							</select>
							<?php } else { ?>
							<select class="form-control" name="subcat" id="subcat" style="">
                        		<option value="">Select Sub Category</option>
                        	 </select>
                        	 <?php } ?>
						</div>
						
						<div class="form-group" style="display:none;" id="other">
						  <label for="">Other Sub Category</label>
						  <input type="text" class="form-control attr" placeholder="Enter Sub Category" value="<?php echo $UpdRow['OtherSubCat'];?>" name="other" />
						</div>
						<div class="form-group">
						  <label for="">Key Point 1</label>
						  <input type="text" class="form-control" placeholder="Key Point 1" value="<?php echo $UpdRow['KeyPoint1'];?>" name="key1" required />
						</div>
						<div class="form-group">
						  <label for="">Key Point 2</label>
						  <input type="text" class="form-control" placeholder="Key Point 2" value="<?php echo $UpdRow['KeyPoint2'];?>" name="key2" required />
						</div>
						<div class="form-group">
						  <label for="">Key Point 3</label>
						  <input type="text" class="form-control" placeholder="Key Point 3" value="<?php echo $UpdRow['KeyPoint3'];?>" name="key3" required />
						</div>
						<div class="form-group">
						  <label for="">Key Point 4</label>
						  <input type="text" class="form-control" placeholder="Key Point 4" value="<?php echo $UpdRow['KeyPoint4'];?>" name="key4" required />
						</div>
						<div class="form-group">
						  <label for="">Key Point 5</label>
						  <input type="text" class="form-control" placeholder="Key Point 5" value="<?php echo $UpdRow['KeyPoint5'];?>" name="key5" required />
						</div>
						<div class="form-group">
						  <label for="">Key Point 6</label>
						  <input type="text" class="form-control" placeholder="Key Point 6" value="<?php echo $UpdRow['KeyPoint6'];?>" name="key6"  />
						</div>
						<div class="form-group">
						  <label for="">Key Point 7</label>
						  <input type="text" class="form-control" placeholder="Key Point 7" value="<?php echo $UpdRow['KeyPoint7'];?>" name="key7"  />
						</div>
						<div class="form-group">
						  <label for="">Key Point 8</label>
						  <input type="text" class="form-control" placeholder="Key Point 8" value="<?php echo $UpdRow['KeyPoint8'];?>" name="key8"  />
						</div>
						<div class="form-group">
						  <label for="">Key Point 9</label>
						  <input type="text" class="form-control" placeholder="Key Point 9" value="<?php echo $UpdRow['KeyPoint9'];?>" name="key9"  />
						</div>
						<div class="form-group">
						  <label for="">Key Point 10</label>
						  <input type="text" class="form-control" placeholder="Key Point 10" value="<?php echo $UpdRow['KeyPoint10'];?>" name="key10"  />
						</div>
						
						<div class="form-group">
						  <label for="">Description </label>
						  <textarea class="form-control" placeholder="About your profile" name="description" ><?php echo $UpdRow['Description'];?></textarea>
						</div>
						<?php if(empty($eid)) { ?>
						<div class="text-right"><button type="submit" name="AddSyllabus" class="blue_btn custom_btn">Add Syllabus</button></div>
						<?php } else { ?>
						<div class="text-right"><button type="submit" name="UpdSyllabus" class="blue_btn custom_btn">Update Syllabus</button></div>
						<?php } ?>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>




<?php include('template/footer.php'); ?>
<script>
$(document).ready(function(){
    $(document).on('change','#cat',function(){
        var Cid = $(this).val();
		
        $.ajax({
            url:'getSubCat.php',
            type:'post',
            data:{'Cid':Cid},
            success:function(data){
				$('#subcat2').hide();
				$('#subcat').show();
                $('#subcat').html(data);
            }
        })
    });
    
     $(document).on('change','#subcat',function(){
        var subcat = $(this).val();
        if(subcat == '999999') {
            $("#other").show();
            $('.attr').attr("required", "required");
        } else {
             $("#other").hide();
             $('.attr').removeAttr("required", "required");
        }
        
     });
	 $(document).on('change','#subcat2',function(){
        var subcat = $(this).val();
        if(subcat == '999999') {
            $("#other").show();
            $('.attr').attr("required", "required");
        } else {
             $("#other").hide();
             $('.attr').removeAttr("required", "required");
        }
        
     });
	 
	 
	});


	var cat = '<?php echo $UpdRow['Category'];?>';
    document.getElementById("cat").value = cat;
	
	var subcat = '<?php echo $UpdRow['SubCategory'];?>';
    document.getElementById("subcat2").value = subcat;
	
	if(subcat == '999999') {
            $("#other").show();
            $('.attr').attr("required", "required");
        } else {
             $("#other").hide();
             $('.attr').removeAttr("required", "required");
        }
	
	
</script>