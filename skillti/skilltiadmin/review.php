
<?php
	include('adminheader.php'); 
	protect_admin_page();
	
	$Query = "select * from reviews";
	$Result = $dbObject->query($Query);
	$i=1;
	
?>
<style>
    .rating_star_s i {
    color: #ff8d00;
}
</style>
  <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper"><!-- HTML (DOM) sourced data -->
			<section id="html">
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body ">
							
							
								<form class="form px-3 mt-2">
									<div class="form-body">
										<h4 class="form-section"><i class="fa fa-star"></i> All Reviews</h4>
									</div>
								</form>
								<div class="card-block card-dashboard">
								
								<table class="table table-striped table-bordered sourced-data">
										<thead>
											<tr>
												<th>SNO</th>
												<th>Mentor</th>
												<th>User</th>
												<th>Rating</th>
												<th>Review</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										<?php while($Row = mysqli_fetch_assoc($Result)) {  
										        $MID = $Row['UserID'];
										        $SID = $Row['SenderID'];
										        
										        $UQuery = "select EmailID from users where UserID='$MID'";
        										$UResult = $dbObject->query($UQuery);
        										$URow = mysqli_fetch_assoc($UResult);
        										
        										$SQuery = "select EmailID from users where UserID='$SID'";
        										$SResult = $dbObject->query($SQuery);
        										$SRow = mysqli_fetch_assoc($SResult);
										?>
											<tr >
												<td><?php echo $i;?></td>
												<td><?php echo $URow['EmailID'];?></td>
												<td><?php echo $SRow['EmailID'];?></td>
												<td class="text-center" style="width:140px;font-size: 20px;">
												<div class="rating_star_s">
													<?php $rating = $Row['Rating'];
													for($j=1;$j<=5;$j++) {
													    if($j <= $rating) { ?>
													<i class="fa fa-star"></i>
													<?php } else { ?>
													<i class="fa fa-star-o"></i>
													<?php } } ?>
													
												</div>
												</td>
												<td><?php echo $Row['Review'];?></td>
												<td ><?php echo $Row['Status'];?></td>
												<td class="text-center"><a title="Edit" href="editReview?eid=<?php echo $Row['RID'];?>"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size: 20px !important;"></i></a> 
												&nbsp;&nbsp;<a title="Delete" id="<?php echo $Row['RID'];?>" class="delete"><i class="fa fa-times" aria-hidden="true" style="font-size: 20px !important;color:red;"></i></a></td>
												
											</tr>
									<?php $i++; } ?>
											
										</tbody>
										
									</table>
								</div>
							
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--/ HTML (DOM) sourced data -->
			</div>
        </div>

   

<?php include('adminfooter.php'); ?>
<script>
    
    $('.delete').click(function(){
      var el = this;
      var rid = this.id;
      var rdeleteid = rid;
      
      // AJAX Request
      $.ajax({
       url: 'deleteCat.php',
       type: 'POST',
       data: { rid:rdeleteid },
       success: function(response){
    
        $(el).closest('tr').css('background','tomato');
        $(el).closest('tr').fadeOut(800, function(){ 
         $(this).remove();
        });
    
       }
      });
    
     });
    
</script>
