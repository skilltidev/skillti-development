@extends('header.admin_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")

@section('maincontent')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper"><!-- HTML (DOM) sourced data -->
                <section id="html">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body ">
                                    <form class="form px-3 mt-2">
                                        <div class="row form-section">
                                            <div class="col-md-7">
                                                <h4><i class="fa fa-th-list"></i> All Sub Category List</h4>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Filter: </label>
                                                <div class="pull-right">
                                                    <select onchange="location = this.options[this.selectedIndex].value;" style="margin-top: 10px;" class="form-control form-control-sm" name="category" id="category">
                                                        <option value="all">All Category</option>
                                                        @foreach($categories as $cate)
                                                            <option {{($catId == custom_encode($cate->id))?"selected":''}} value="{{url('admin/all-subCategories/'.custom_encode($cate->id))}}">{{$cate->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <a href="{{url('admin/add-edit-subCategory')}}" class="float-right"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="card-block card-dashboard table-responsive">
                                        <table class="table table-striped table-bordered sourced-data">
                                            <thead>
                                            <tr>
                                                <th>SNO</th>
                                                <th>Img</th>
                                                <th>Name</th>
                                                <th>Sub Category</th>
                                                <th class="text-center">IsHome</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($subCategories))
                                                @foreach($subCategories as $k => $category)
                                                    <tr>
                                                        <td>{{($k + 1)}}</td>
                                                        <td class="text-center">
                                                            @if($category->isHome)
                                                            <img style="width:50px;height:50px" src="{{($category->logo != '')?asset('admin-assets/cat-image/'.$category->logo):asset('admin-assets/cat-image/catImg.PNG')}}" />
                                                            @endif
                                                        </td>
                                                        <td>{{$category->name}}</td>
                                                        <td>{{substr($category->categoryName, 0, 20)}}</td>
                                                        <td class="text-center">{{($category->isHome == 1)?'Yes':''}}</td>
                                                        <td class="text-center">{{($category->status == 1)?'Active':'InActive'}}</td>
                                                        <td class="text-center">
                                                            <a title="Edit" href="{{url('admin/add-edit-subCategory/'.custom_encode($category->id))}}">
                                                                <i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size: 20px !important;"></i>
                                                            </a>
                                                        &nbsp;&nbsp; <a title="Delete" id="{{custom_encode($category->id)}}" class="delete">
                                                                <i class="fa fa-times" aria-hidden="true" style="font-size: 20px !important;color:red;"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ HTML (DOM) sourced data -->
            </div>
        </div>
    </div>
@endsection
@section('scripting')
    <script>
        $('.delete').click(function(){
            if(confirm("Are you sure!")) {
                var el = this;
                var id = this.id;
                var deleteid = id;
                $.ajax({
                    url: '{{url('admin/delete-subCategory')}}',
                    type: 'GET',
                    data: { id:deleteid },
                    postType: "JSON",
                    success: function(response){
                        if(response.error == false) {
                            $(el).closest('tr').css('background','tomato');
                            $(el).closest('tr').fadeOut(800, function(){
                                $(this).remove();
                            });
                        }
                    }
                });
            }
        });
    </script>
@endsection
@section('footer')
    @include('footer.footer_admin')
@endsection
