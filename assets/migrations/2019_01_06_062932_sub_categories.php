<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('subCategories', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('catId')->unsigned();
		    $table->string('name');
		    $table->string('description');
		    $table->tinyInteger('status')->default('0');
		    $table->timestamps();
	    });
	    Schema::table('subCategories', function($table) {
		    $table->foreign('catId')->references('id')->on('categories');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('subCategories');
    }
}
