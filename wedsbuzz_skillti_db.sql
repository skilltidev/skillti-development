-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 21, 2018 at 10:35 AM
-- Server version: 5.6.41
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wedsbuzz_skillti_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `CatID` int(255) NOT NULL,
  `CatName` varchar(100) NOT NULL,
  `Parent` int(255) NOT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `CatImage` varchar(150) DEFAULT NULL,
  `Status` varchar(15) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`CatID`, `CatName`, `Parent`, `Description`, `CatImage`, `Status`, `CreatedDate`, `ModifiedDate`) VALUES
(14, 'Software Development', 0, 'Software Development', '', 'ACTIVE', '2018-10-31 12:52:05', '2018-10-31 12:52:05'),
(64, 'IT Ops', 0, 'IT Operations', '', 'ACTIVE', '2018-11-12 19:40:43', '2018-11-12 19:40:43'),
(3, 'Data Science', 0, 'Data Professional', '', 'ACTIVE', '2018-10-30 13:02:09', '2018-10-30 13:02:09'),
(4, 'Cyber Security', 0, 'Information & Cyber Security', '', 'ACTIVE', '2018-10-30 14:37:32', '2018-10-30 14:37:32'),
(5, 'Business Professionals ', 0, 'Business Professionals ', '', 'ACTIVE', '2018-10-30 14:37:43', '2018-10-30 14:37:43'),
(6, 'Digital Marketing', 0, 'Digital Marketing', '', 'ACTIVE', '2018-10-30 14:38:00', '2018-10-30 14:38:00'),
(7, 'Artificial Intelligence ', 0, 'Artificial Intelligence ', '', 'INACTIVE', '2018-10-30 14:38:31', '2018-10-30 14:38:31'),
(8, 'Blockchain', 0, 'Blockchain Development ', '', 'ACTIVE', '2018-10-30 14:38:43', '2018-10-30 14:38:43'),
(18, 'Web Development', 14, 'Web Development', 'IT_OPS.png', 'ACTIVE', '2018-11-12 12:38:57', '2018-11-12 12:38:57'),
(19, 'IT Certifications', 2, 'IT Certifications ', '', 'INACTIVE', '2018-11-12 12:39:23', '2018-11-12 12:39:23'),
(20, 'Business Intelligence ', 3, 'Business Intelligence ', '', 'ACTIVE', '2018-11-12 12:39:37', '2018-11-12 12:39:37'),
(21, 'Security Certifications', 4, 'Security Certifications', 'cyber_security.png', 'ACTIVE', '2018-11-12 12:39:47', '2018-11-12 12:39:47'),
(22, 'Business Analysis', 5, 'Business Analysis', 'business_proffesionals.png', 'ACTIVE', '2018-11-12 12:40:02', '2018-11-12 12:40:02'),
(23, 'SMO', 6, 'SMO', '', 'ACTIVE', '2018-11-12 12:40:59', '2018-11-12 12:40:59'),
(24, 'BD', 5, NULL, NULL, 'INACTIVE', '2018-11-12 15:50:03', '2018-11-12 15:50:03'),
(72, 'html', 14, NULL, NULL, 'INACTIVE', '2018-11-15 15:37:24', '2018-11-15 15:37:24'),
(25, 'Mobile Development', 14, 'Mobile Development', '', 'ACTIVE', '2018-11-12 16:28:26', '2018-11-12 16:28:26'),
(26, 'JavaScript', 14, 'JavaScript', '', 'ACTIVE', '2018-11-12 16:28:42', '2018-11-12 16:28:42'),
(27, 'C#', 14, 'C#', '', 'ACTIVE', '2018-11-12 16:28:53', '2018-11-12 16:28:53'),
(28, 'Python', 14, 'Python', 'software_dovelopment.png', 'ACTIVE', '2018-11-12 16:29:14', '2018-11-12 16:29:14'),
(29, 'Node.js', 14, 'Node.js', 'software_dovelopment.png', 'ACTIVE', '2018-11-12 16:29:28', '2018-11-12 16:29:28'),
(43, 'Data Dashboard ', 3, '', '', 'ACTIVE', '2018-11-12 19:26:37', '2018-11-12 19:26:37'),
(31, 'Administration ', 2, '', '', 'ACTIVE', '2018-11-12 19:23:15', '2018-11-12 19:23:15'),
(33, 'Virtualization ', 2, '', '', 'ACTIVE', '2018-11-12 19:23:46', '2018-11-12 19:23:46'),
(34, 'IT Networking ', 2, '', '', 'ACTIVE', '2018-11-12 19:24:01', '2018-11-12 19:24:01'),
(35, 'Servers ', 2, '', '', 'ACTIVE', '2018-11-12 19:24:10', '2018-11-12 19:24:10'),
(36, 'Microsoft Office ', 2, '', '', 'ACTIVE', '2018-11-12 19:24:20', '2018-11-12 19:24:20'),
(37, 'Windows Server', 2, '', '', 'ACTIVE', '2018-11-12 19:24:28', '2018-11-12 19:24:28'),
(38, 'Windows Server ', 2, '', '', 'ACTIVE', '2018-11-12 19:24:46', '2018-11-12 19:24:46'),
(39, 'Microsoft Windows OS', 2, '', '', 'ACTIVE', '2018-11-12 19:24:56', '2018-11-12 19:24:56'),
(40, 'Linux', 2, '', '', 'ACTIVE', '2018-11-12 19:25:03', '2018-11-12 19:25:03'),
(41, 'Cloud Computing ', 2, '', '', 'ACTIVE', '2018-11-12 19:25:11', '2018-11-12 19:25:11'),
(42, 'Docker', 2, '', '', 'ACTIVE', '2018-11-12 19:25:20', '2018-11-12 19:25:20'),
(44, 'Oracle ', 3, '', 'data_science.png', 'ACTIVE', '2018-11-12 19:26:44', '2018-11-12 19:26:44'),
(45, 'MySQL', 0, '', 'data_science.png', 'ACTIVE', '2018-11-12 19:26:55', '2018-11-12 19:26:55'),
(46, 'SQL', 3, '', '', 'ACTIVE', '2018-11-12 19:27:03', '2018-11-12 19:27:03'),
(47, 'SQL Server', 3, '', '', 'ACTIVE', '2018-11-12 19:27:11', '2018-11-12 19:27:11'),
(48, 'Security Fundamentals ', 4, '', '', 'ACTIVE', '2018-11-12 19:27:26', '2018-11-12 19:27:26'),
(49, 'Security Auditing', 4, '', '', 'ACTIVE', '2018-11-12 19:27:34', '2018-11-12 19:27:34'),
(50, 'Penetration Testing ', 4, '', '', 'ACTIVE', '2018-11-12 19:27:43', '2018-11-12 19:27:43'),
(51, 'Digital Forensics ', 4, '', '', 'ACTIVE', '2018-11-12 19:27:51', '2018-11-12 19:27:51'),
(52, 'Malware Analysis', 4, '', '', 'ACTIVE', '2018-11-12 19:28:03', '2018-11-12 19:28:03'),
(53, 'SEO', 6, '', '', 'ACTIVE', '2018-11-12 19:28:40', '2018-11-12 19:28:40'),
(55, 'Google Analytics', 6, '', '', 'ACTIVE', '2018-11-12 19:29:03', '2018-11-12 19:29:03'),
(56, 'Pay Per Clicks ', 6, '', '', 'ACTIVE', '2018-11-12 19:29:59', '2018-11-12 19:29:59'),
(57, 'Google Shopping', 6, '', '', 'ACTIVE', '2018-11-12 19:30:09', '2018-11-12 19:30:09'),
(58, 'Web Analytics', 6, '', '', 'ACTIVE', '2018-11-12 19:30:33', '2018-11-12 19:30:33'),
(59, 'Fundamentals of IT Ops ', 5, '', '', 'ACTIVE', '2018-11-12 19:32:16', '2018-11-12 19:32:16'),
(60, 'Scrum Framework', 5, '', '', 'ACTIVE', '2018-11-12 19:32:35', '2018-11-12 19:32:35'),
(61, 'PRINCE2®', 5, '', '', 'ACTIVE', '2018-11-12 19:33:04', '2018-11-12 19:33:04'),
(62, 'Project Manager', 5, '', '', 'ACTIVE', '2018-11-12 19:33:13', '2018-11-12 19:33:13'),
(63, 'Risk Analysis ', 5, '', '', 'ACTIVE', '2018-11-12 19:33:28', '2018-11-12 19:33:28'),
(65, 'Security Database', 64, '', 'cyber_security.png', 'ACTIVE', '2018-11-12 19:41:08', '2018-11-12 19:41:08'),
(66, 'Administration ', 64, '', '', 'ACTIVE', '2018-11-12 19:41:21', '2018-11-12 19:41:21'),
(67, 'Security Database', 64, '', '', 'ACTIVE', '2018-11-12 19:41:33', '2018-11-12 19:41:33'),
(68, 'Virtualization ', 64, '', '', 'ACTIVE', '2018-11-12 19:41:44', '2018-11-12 19:41:44'),
(69, 'IT Networking ', 64, '', '', 'ACTIVE', '2018-11-12 19:41:57', '2018-11-12 19:41:57'),
(70, 'Servers ', 64, '', '', 'ACTIVE', '2018-11-12 19:42:23', '2018-11-12 19:42:23'),
(75, 'Network and protocols', 8, '', '', 'ACTIVE', '2018-11-26 19:58:18', '2018-11-26 19:58:18');

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE `configurations` (
  `Configurationsid` int(11) NOT NULL,
  `ConfigKey` varchar(45) DEFAULT NULL,
  `ConfigName` varchar(45) DEFAULT NULL,
  `ConfigValue` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE `contactus` (
  `CID` int(255) NOT NULL,
  `Name` varchar(150) NOT NULL,
  `Email` varchar(150) NOT NULL,
  `Subject` varchar(200) NOT NULL,
  `Message` varchar(500) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`CID`, `Name`, `Email`, `Subject`, `Message`, `CreatedDate`) VALUES
(1, 'Shashank', 'jasbeer.singh1990@gmail.com', 'Which Stripe Account Connected to my Plaid ', 'Which Stripe Account Connected to my Plaid ', '2018-12-15 19:20:03'),
(2, 'Shashank', 'jasbeer.singh1990@gmail.com', 'Test Enquiry', 'Test Enquiry', '2018-12-15 19:33:46'),
(3, 'Sonu', 'sonu.tyagi2229@gmail.com', 'Profile ', 'FYI', '2018-12-15 21:36:19'),
(4, 'Sonu Tyagi', 'sonu.tyagi2229@gmail.com', 'TEST tEST', 'tEST ', '2018-12-17 14:23:07'),
(5, 'jasbir', 'jasbeer.singh1990@gmail.com', 'Test Enquiry', 'Test Enquiry', '2018-12-17 18:49:58');

-- --------------------------------------------------------

--
-- Table structure for table `menumaster`
--

CREATE TABLE `menumaster` (
  `MenuMasterID` int(11) NOT NULL,
  `MenuName` varchar(45) DEFAULT NULL,
  `MenuDescription` varchar(200) DEFAULT NULL,
  `IsActive` tinyint(4) DEFAULT NULL,
  `Seq` int(11) DEFAULT NULL,
  `PagePath` varchar(255) DEFAULT NULL,
  `ParentId` int(11) DEFAULT NULL,
  `Icons` varchar(255) DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `ModifiedDate` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `messagedata`
--

CREATE TABLE `messagedata` (
  `MID` int(255) NOT NULL,
  `SenderID` int(255) NOT NULL,
  `RecieverID` int(255) NOT NULL,
  `Message` varchar(500) NOT NULL,
  `Status` int(10) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messagedata`
--

INSERT INTO `messagedata` (`MID`, `SenderID`, `RecieverID`, `Message`, `Status`, `CreatedDate`) VALUES
(1, 2, 34, 'Hi there', 1, '2018-12-13 10:38:10'),
(2, 34, 2, 'Hi', 1, '2018-12-13 16:09:47'),
(3, 34, 2, 'Hi', 1, '2018-12-13 16:09:52'),
(4, 34, 2, 'how are you?', 1, '2018-12-13 16:10:02'),
(5, 34, 2, 'how are you?', 1, '2018-12-13 16:10:03'),
(6, 34, 2, 'This is for test', 1, '2018-12-13 16:10:12'),
(7, 34, 2, 'Did you get my message?', 1, '2018-12-13 16:10:20'),
(8, 34, 2, 'hi', 1, '2018-12-13 16:10:26'),
(9, 34, 2, 'hi', 1, '2018-12-13 16:10:34'),
(10, 2, 34, 'Check again', 1, '2018-12-15 19:27:51'),
(11, 2, 34, 'Check again', 1, '2018-12-15 19:29:06'),
(12, 2, 34, 'Thanks For reply', 1, '2018-12-15 19:32:30'),
(13, 2, 34, 'Thanks For reply mentor bhai ', 1, '2018-12-15 19:37:27'),
(14, 2, 34, 'Check again', 1, '2018-12-15 19:49:01'),
(15, 2, 34, 'dfdfsdfsdfsdf', 1, '2018-12-19 15:01:07');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `RID` int(255) NOT NULL,
  `UserID` int(255) NOT NULL,
  `SenderID` int(255) NOT NULL,
  `Rating` int(10) NOT NULL,
  `Review` varchar(500) NOT NULL,
  `Status` varchar(15) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`RID`, `UserID`, `SenderID`, `Rating`, `Review`, `Status`, `CreatedDate`) VALUES
(1, 34, 2, 3, 'This is Sonu Tyagi, CEO of skiillti. Would love to make this world more accessible to technology and mentors. Our aim to disrupt learning tradition. Team skillti & i will make this happen.', 'publish', '2018-12-18 10:39:33'),
(2, 34, 2, 4, 'Skillti is an online education company with intensive mentor-led programs for aspiring beginners from any field.', 'publish', '2018-12-18 10:42:03'),
(3, 34, 2, 4, 'Skillti is an online education company with intensive mentor-led programs for aspiring beginners from any field.', 'draft', '2018-12-18 10:43:15');

-- --------------------------------------------------------

--
-- Table structure for table `rolemenu`
--

CREATE TABLE `rolemenu` (
  `RoleMenuID` int(11) NOT NULL,
  `RoleID` int(11) DEFAULT NULL,
  `MenuID` int(11) DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `ModifiedDate` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='	';

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `RoleID` int(11) NOT NULL,
  `RoleName` varchar(45) NOT NULL,
  `RoleDescription` varchar(100) DEFAULT NULL,
  `RoleType` varchar(20) NOT NULL,
  `IsActive` tinyint(4) DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `ModifiedDate` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`RoleID`, `RoleName`, `RoleDescription`, `RoleType`, `IsActive`, `CreatedBy`, `ModifiedBy`, `CreatedDate`, `ModifiedDate`) VALUES
(1, 'Admin', 'Admin', 'Application', 1, 1, 1, '2018-10-03 15:16:49', '2018-10-03 15:16:49'),
(2, 'Mentor', 'Mentor', 'Front-end User', 1, 1, 1, '2018-10-03 15:16:49', '2018-10-03 15:16:49'),
(3, 'Mentee', 'Mentee', 'Front-end User', 1, 1, 1, '2018-10-03 15:17:20', '2018-10-03 15:17:20');

-- --------------------------------------------------------

--
-- Table structure for table `socialmedialink`
--

CREATE TABLE `socialmedialink` (
  `SID` int(255) NOT NULL,
  `UserID` int(255) NOT NULL,
  `Facebook` varchar(250) NOT NULL,
  `Twitter` varchar(250) NOT NULL,
  `LinkedIn` varchar(250) NOT NULL,
  `Github` varchar(250) NOT NULL,
  `Other` varchar(250) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `socialmedialink`
--

INSERT INTO `socialmedialink` (`SID`, `UserID`, `Facebook`, `Twitter`, `LinkedIn`, `Github`, `Other`, `CreatedDate`) VALUES
(2, 34, 'https://www.facebook.com/tyagisonuagra', '', '', '', '', '2018-12-13 12:13:03');

-- --------------------------------------------------------

--
-- Table structure for table `syllabus`
--

CREATE TABLE `syllabus` (
  `ID` int(255) NOT NULL,
  `UserID` int(255) NOT NULL,
  `Category` int(10) NOT NULL,
  `SubCategory` int(10) NOT NULL,
  `OtherSubCat` varchar(500) NOT NULL,
  `KeyPoint1` varchar(500) NOT NULL,
  `KeyPoint2` varchar(500) NOT NULL,
  `KeyPoint3` varchar(500) NOT NULL,
  `KeyPoint4` varchar(500) NOT NULL,
  `KeyPoint5` varchar(500) NOT NULL,
  `KeyPoint6` varchar(500) NOT NULL,
  `KeyPoint7` varchar(500) NOT NULL,
  `KeyPoint8` varchar(500) NOT NULL,
  `KeyPoint9` varchar(500) NOT NULL,
  `KeyPoint10` varchar(500) NOT NULL,
  `Description` varchar(1000) NOT NULL,
  `ModifiedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `syllabus`
--

INSERT INTO `syllabus` (`ID`, `UserID`, `Category`, `SubCategory`, `OtherSubCat`, `KeyPoint1`, `KeyPoint2`, `KeyPoint3`, `KeyPoint4`, `KeyPoint5`, `KeyPoint6`, `KeyPoint7`, `KeyPoint8`, `KeyPoint9`, `KeyPoint10`, `Description`, `ModifiedDate`, `CreatedDate`) VALUES
(1, 3, 14, 18, 'Other Sub Category', 'This the key point detail22rte', 'This the key point detail22rtert', 'This the key point detail22erter', 'This the key point detail22ertert', 'This the key point detail22erte', 'This the key point detail22ertert', 'This the key point detail22ertert', 'This the key point detail22erter', 'This the key point detail22terter', 'This the key point detail22ertert', 'This the key point dscription 22ertert', '2018-11-12 15:43:41', '2018-11-12 15:43:41'),
(2, 3, 4, 21, '', 'This is another key details', 'This is another key details', 'This is another key details', 'This is another key details', 'This is another key details', '', '', '', '', '', 'This is another key details', '2018-11-12 15:44:35', '2018-11-12 15:44:35'),
(16, 34, 5, 22, '', 'Have a fundamental understanding of the Python programming language', 'Acquire the pre-requisite Python skills to move into specific branches - Machine Learning, Data Science, etc', 'Understand how to create your own Python programs', 'Understand both Python 2 and Python 3', 'Have the skills and understanding of Python to confidently apply for Python programming jobs', 'Add the Python Object-Oriented Programming (OOP) skills to your résumé', '', '', '', '', '', '2018-11-26 16:19:22', '2018-11-26 16:19:22'),
(5, 3, 14, 18, '', 'Make REAL web applications using cutting-edge technologies', 'Create a blog application from scratch using Express, MongoDB, and Semantic UI', 'Write your own browser-based game', 'Think like a developer. Become an expert at Googling code questions!', 'Write web apps with full authentication', 'Implement responsive navbars on websites', 'Write Javascript functions, and understand scope and higher order functions', 'Manipulate the DOM with vanilla JS', 'Translate between jQuery and vanillas JS', 'Use NodeJS to write server-side JavaScript', 'Hi! Welcome to the Web Developer Bootcamp, the only course you need to learn web development. There are a lot of options for online developer training, but this course is without a doubt the most comprehensive and effective on the market. ', '2018-11-13 21:02:27', '2018-11-13 21:02:27'),
(6, 34, 14, 28, '', 'Have a fundamental understanding of the Python programming language', 'Acquire the pre-requisite Python skills to move into specific branches - Machine Learning, Data Science, etc', 'Understand how to create your own Python programs', 'Understand both Python 2 and Python 3', 'Have the skills and understanding of Python to confidently apply for Python programming jobs', 'Add the Python Object-Oriented Programming (OOPS) skills to your resume', 'Learn Python from experienced professional software developers', 'Installing IntelliJ on Windows', 'Configuring IntelliJ and Pycharm on Mac', 'Printing the result of a calculation', 'Python is an interpreted, object-oriented programming language similar to PERL, that has gained popularity because of its clear syntax and readability. ... Python was created by Guido van Rossum, a former resident of the Netherlands, whose favorite comedy group at the time was Monty Pythons Flying Circus.', '2018-11-14 22:59:18', '2018-11-14 22:59:18'),
(10, 37, 14, 28, 'Other Sub Category', 'Go from a total beginner to an advanced JavaScript developer', 'Code 3 beautiful real-world apps with both ES5 and ES6+ (no boring toy apps)', 'JavaScript and programming fundamentals: variables, boolean logic, if/else, loops, functions, arrays, etc.', 'Complex features like the this keyword, function constructors, prototypal inheritance, first-class functions, closures', 'Asynchronous JavaScript: The event loop, promises, async/await, AJAX and APIs', 'Modern JavaScript for 2018: NPM, Webpack, Babel and ES6 modules', 'A true understanding of how JavaScript works behind the scenes', 'Whats new in ES6: arrow functions, classes, default and rest parameters, etc.', 'Practice your new skills with coding challenges (solutions included)', 'Organize and structure your code using JavaScript patterns like modules', 'Are you tired of wasting your time and money on random youtube videos or JavaScript courses that are either too simple, or too difficult to follows', '2018-11-15 15:10:59', '2018-11-15 15:10:59'),
(11, 38, 14, 28, '', 'Learn to use Python professionally, learning both Python 2 and Python 3', 'reate games with Python, like Tic Tac Toe and Blackjack', 'Learn advanced Python features, like the collections module and how to work with timestamps', 'Learn to use Object Oriented Programming with classes', 'Understand complex topics, like decorators', 'Understand how to use both the Jupyter Notebook and create .py files', 'Get an understanding of how to create GUIs in the Jupyter Notebook system', 'Build a complete understanding of Python from the ground up', 'Have a great intuition of many Machine Learning models', 'Create strong added value to your business', 'This is the most comprehensive, yet straight-forward, course for the Python programming language on Udemy! Whether you have never programmed before, already know basic syntax, or want to learn about the advanced features of Python, this course is for you! In this course we will teach you Python 3. ', '2018-11-15 15:20:58', '2018-11-15 15:20:58'),
(15, 34, 3, 45, '', 'Create your own database or interact with existing databases', 'Write complex SQL queries across multiple tables', 'Build a web app using NodeJS and MySQL', 'Model real-world data and generate reports using SQL', 'Answer company performance or sales questions using data', '', '', '', '', '', 'Become an In-demand SQL Master by creating complex databases and building reports through real-world projects', '2018-11-17 17:40:00', '2018-11-17 17:40:00'),
(17, 34, 3, 44, '', 'Have a fundamental understanding of the Python programming language', 'Acquire the pre-requisite Python skills to move into specific branches - Machine Learning, Data Science, etc', 'Understand how to create your own Python programs', 'Understand both Python 2 and Python 3', 'Have the skills and understanding of Python to confidently apply for Python programming jobs', 'Add the Python Object-Oriented Programming (OOP) skills to your résumé', '', '', '', '', '', '2018-11-26 16:19:47', '2018-11-26 16:19:47'),
(13, 40, 14, 29, '', 'Build, test, and launch Node apps', 'Create Express web servers and APIs', 'Store data with Mongoose and MongoDB', 'Use cutting-edge ES6/ES7 JavaScript', 'Deploy your Node apps to production', 'Create real-time web apps with SocketIO', 'Understand the Javascript and technical concepts behind NodeJS', 'Build a Web Server in Node and understand how it really works', 'Build a web application and API more easily using Express', 'Understand Buffers, Streams, and Pipes', 'Have you tried to learn Node before? You start a new course, and the instructor has you installing a bunch of libraries before you even know what Node is or how it works. You eventually get stuck and reach out to the instructor, but you get no reply. You then close the course and never open it again.', '2018-11-15 15:43:27', '2018-11-15 15:43:27'),
(14, 41, 64, 65, '', 'Web app coding', 'ethical hacking of web, webportal application', 'avenger advanced bypass techniques', 'web app designing ideas', 'Fully understand Cloud Security from a real-world viewpoint', 'Comprehend the industry security standards for both exam knowledge and implementation', 'Understand how compliance is viewed and dealt with in the cloud', 'Receive the hands-on experience needed to implement Cloud Security with practical implementations', 'Have a general working knowledge of what to audit in a cloud architecture. To know hands-on methods of auditing a cloud environment from the best practices viewpoint.', 'Gain the knowledge needed to pass the exam', 'The Cloud is being widely adopted today for a diverse set of reasons. However, many are finding that security in the cloud is a huge challenge - either because of implementation or governance. Yes, governance of security, related to your cloud vendor is a huge challenge. However, many global standards have been developed that provide a great baseline for cloud security along with governance', '2018-11-15 15:49:47', '2018-11-15 15:49:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `UserID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `FirstName` varchar(45) DEFAULT NULL,
  `LastName` varchar(45) DEFAULT NULL,
  `EmailID` varchar(45) DEFAULT NULL,
  `Password` varchar(45) DEFAULT NULL,
  `profilePic` varchar(100) DEFAULT NULL,
  `Qualification` varchar(100) DEFAULT NULL,
  `Expertise` varchar(250) DEFAULT NULL,
  `Address1` varchar(200) DEFAULT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `City` varchar(45) DEFAULT NULL,
  `State` varchar(45) DEFAULT NULL,
  `Country` varchar(45) DEFAULT NULL,
  `Zip` varchar(10) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `VarificationDoc` varchar(200) DEFAULT NULL,
  `WorkingWith` varchar(150) DEFAULT NULL,
  `Experience` varchar(100) DEFAULT NULL,
  `Certified` varchar(10) DEFAULT NULL,
  `CertificateName` varchar(100) DEFAULT NULL,
  `UserAbout` varchar(1000) DEFAULT NULL,
  `UserTagline` varchar(500) DEFAULT NULL,
  `InterestToLearn` varchar(100) DEFAULT NULL,
  `Status` varchar(10) DEFAULT NULL,
  `msgurl` varchar(200) NOT NULL,
  `IsFirstTimeLogin` tinyint(4) DEFAULT NULL,
  `UniqueKey` varchar(45) DEFAULT NULL,
  `ResetPasswordDate` datetime DEFAULT NULL,
  `LastLoggedIn` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `ModifiedDate` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='		';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `RoleID`, `FirstName`, `LastName`, `EmailID`, `Password`, `profilePic`, `Qualification`, `Expertise`, `Address1`, `Address2`, `City`, `State`, `Country`, `Zip`, `Phone`, `VarificationDoc`, `WorkingWith`, `Experience`, `Certified`, `CertificateName`, `UserAbout`, `UserTagline`, `InterestToLearn`, `Status`, `msgurl`, `IsFirstTimeLogin`, `UniqueKey`, `ResetPasswordDate`, `LastLoggedIn`, `CreatedDate`, `ModifiedDate`) VALUES
(1, 1, 'jasbir', 'singh', 'jasbeer.singh1990@gmail.com', '25d55ad283aa400af464c76d713c07ad', '', '', '', NULL, '', NULL, NULL, 'India', NULL, '9911947979', '', '', '', '', '', '', '', '', 'ACTIVE', '', NULL, 'RGND9E6F0Hpi5nOfbAvg', '2018-10-16 17:50:03', NULL, '2018-10-06 09:40:53', '2018-10-06 09:40:53'),
(2, 3, 'Jasbir', 'Singh', 'mentee@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'jassi_DP.jpg', 'B-Tech', '', '2176 Thomaston Avenue, Waterbury CT  06704', '', 'Noida', 'UP', 'India', '201301', '9911947979', '', '', '', '', '', '', '', 'B-Tech', 'ACTIVE', '', NULL, '3dcDLGFSNt5WZkOip7Co', NULL, NULL, '2018-10-06 09:53:46', '2018-10-06 09:53:46'),
(3, 2, 'Sonu', 'Tyagi', 'mentor@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'WIN_20171208_09_51_30_Pro.jpg', 'B-Tech', 'PHP,WordPress', 'Noida, Sec-63, C-Block', '', 'Noida', 'UP', 'India', '201301', '9911947979', 'Practice Areas.doc', 'skilltI - A Learning Marketplace', '3 Years', 'Yes', 'Certificate2', 'past can make their lives once again visible to us today.', 'This is my tagline', '', 'ACTIVE', '', NULL, 'LblOfNhrmYQ4vVkiH1Ig', '2018-11-13 23:09:41', NULL, '2018-10-06 09:55:03', '2018-10-06 09:55:03'),
(22, 1, 'Admin', NULL, 'admin@skillti.com', '25d55ad283aa400af464c76d713c07ad', 'avator1.png', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', 'ACTIVE', '', NULL, NULL, NULL, NULL, '2018-10-29 15:14:45', '2018-10-29 15:14:45'),
(32, 2, 'Jasbir', 'Singh', 'jasbir@gnxtsystems.com', '25d55ad283aa400af464c76d713c07ad', 'me.png', 'BA', 'php', 'Noida', '', 'Noida', 'UP', 'Azerbaijan', '201001', '9797979797', NULL, 'sdfsdfsdf', '3 years', 'Yes', 'Certificate1', 'About ', 'Taglinne', 'B-Tech', 'INACTIVE', '', NULL, 'F8LfATRSQ7ZMlBOI5hVc', '2018-11-14 18:02:24', NULL, '2018-11-14 11:26:52', '2018-11-14 11:26:52'),
(33, 3, 'Jasbir', 'Singh', 'jasbir@gnxtsystems.com', '25d55ad283aa400af464c76d713c07ad', 'WIN_20171208_09_51_30_Pro.jpg', 'BA', NULL, 'Noida', '', 'Noida', 'UP', 'Azerbaijan', '201001', '9797979797', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B-Tech', 'ACTIVE', 'https://www.jeetso.com/skillti/dev/detail?cat=python&m=sonu-tyagi&id=34#msgForm', NULL, 'F8LfATRSQ7ZMlBOI5hVc', '2018-11-14 18:02:24', NULL, '2018-11-14 17:57:18', '2018-11-14 17:57:18'),
(34, 2, 'Sonu', 'Tyagi', 'sonu.tyagi2229@gmail.com', '0608b2d7eb0aa38b7ac8ac08c9120068', 'Sonu-DP.jpg', 'B-Tech', 'Python', '9/63 - Palm Olympia ', 'Gaur City ', 'Greater Noida West', 'UP', 'India', '201308', '8800572549', NULL, 'skillti - A Learning Marketplace', '3 Years', 'Yes', 'Certificate1', 'This is Sonu Tyagi, CEO of skiillti. Would love to make this world more accessible  to technology and mentors. Our aim to disrupt learning tradition. Team skillti & i will make this happen. \r\nIm B.tech in Electronics and communication engineering with certification in Associate level software business analyst. Worked with various projects and clients.\r\n\r\n<b>Domain Expertise</b>\r\n\r\n•	VoIP and Telecom \r\n•	BFSI domain \r\n•	property and casualty domain \r\n•       E-commerce & Marketplace \r\n', '5 Years experience in Business and Management.Working with Skillti.', NULL, 'ACTIVE', '', NULL, 'YV5TH18xU3uiQLSaWIPc', '2018-11-14 20:09:24', NULL, '2018-11-14 19:03:54', '2018-11-14 19:03:54'),
(35, 2, 'Shashank Singh', 'Somal', 'itsmeshashank1994@gmail.com', 'ce781c88e1f5a9d19966080b053d55df', 'DSC_3477.JPG', 'B.Com', 'Java', '2176 Thomaston Avenue, Waterbury CT  06704', '', 'Noida', 'UP', 'India', '201301', '', 'CBSE - JOINT ENTRANCE EXAMINATION (MAIN) - 2018 (1).pdf', 'CSC', '2 years', 'Yes', 'Certificate1', 'Java basic', '', 'B-Tech', 'INACTIVE', '', NULL, 'yuPlKTsWxgnCXBVQNwM8', '2018-11-14 21:00:31', NULL, '2018-11-14 20:11:29', '2018-11-14 20:11:29'),
(36, 3, 'Shashank Singh', 'Somal', 'itsmeshashank1994@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'DSC_3477.JPG', 'B.Com', NULL, '2176 Thomaston Avenue, Waterbury CT  06704', '', 'Noida', 'UP', 'India', '201301', '97979797979', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B-Tech', 'ACTIVE', '', NULL, 'ti8YRmMZLKQ0IxelWFkv', NULL, NULL, '2018-11-14 22:35:12', '2018-11-14 22:35:12'),
(37, 2, 'test1', 'test1', 'test1@skillti.com', '25d55ad283aa400af464c76d713c07ad', 'jassi_DP.jpg', 'B-Tech', 'Python', '9/63, Palm Olympia Gaur City', '', 'Noida', 'Uttar Pradesh', 'India', '2013', '09761453003', NULL, 'skiltti.com', '3 years', 'Yes', 'Certificate1', 'This is Jasbir Singh, CTO of skiillti. Would love to make this world more accessible to technology and mentors. Our aim to disrupt learning tradition. Team skillti & i will make this happen.\r\nIm B.tech in Electronics and communication engineering with certification in Associate level software business analyst. Worked with various projects and clients.\r\n\r\nDomain Expertise. \r\n\r\n•	VoIP and Telecom\r\n•	BFSI domain\r\n•	property and casualty domain\r\n•       E-commerce & Marketplace \r\n', '5 Years experience in PHP and Programming ! Working with Skillti to change the world.', NULL, 'ACTIVE', '', NULL, 'imk9NcabJzBLCe4yZToM', NULL, NULL, '2018-11-15 13:06:06', '2018-11-15 13:06:06'),
(38, 2, 'Shovit ', 'Tyagi', 'test2@skillti.com', '25d55ad283aa400af464c76d713c07ad', 'Shovit_tyagi.jpg', 'B-Tech', 'python,java', '9/63, Palm Olympia Gaur City', '', 'Noida', 'Uttar Pradesh', 'Israel', '201308', '09711636280', NULL, 'skillti.com', '3 years', 'Yes', 'Certificate1', 'This is Shovit Tyagi, VP - Product of skillti. Would love to make this world more accessible to technology and mentors. Our aim to disrupt learning tradition. Team skillti & i will make this happen. Im B.tech in Electronics and communication engineering with certification in Associate level software business analyst. Worked with various projects and clients. Domain Expertise', '5 Years experience in Project Management & Product Management! Working with Skillti to change the world.', NULL, 'ACTIVE', '', NULL, '72XYpxkPCEf0ujDJg6wQ', NULL, NULL, '2018-11-15 13:07:31', '2018-11-15 13:07:31'),
(39, 2, 'Test', 'three', 'test3@skillti.com', '25d55ad283aa400af464c76d713c07ad', NULL, 'B.Com', 'html', '9/63, Palm Olympia Gaur City', '', 'Noida', 'Uttar Pradesh', 'Bahrain', '201308', '7993360207', NULL, 'skillti', '3 years', 'Yes', 'Certificate1', 'HTML stands for Hyper Text Markup Language\r\nHTML describes the structure of Web pages using markup\r\nHTML elements are the building blocks of HTML pages\r\nHTML elements are represented by tags\r\n', 'Advice is now provided in the HTML spec on how to mark up subheadings.', NULL, 'ACTIVE', '', NULL, 'cPD64hpFUrqVHtf3i5mb', NULL, NULL, '2018-11-15 13:10:43', '2018-11-15 13:10:43'),
(40, 2, 'test', 'four', 'test4@skillti.com', '25d55ad283aa400af464c76d713c07ad', NULL, 'BA', 'node.js', '9/63, Palm Olympia Gaur City', '', 'Noida', 'Uttar Pradesh', 'India', '281502', '8800572549', NULL, 'skiltti.com', '9 years', 'Yes', 'Certificate1', 'There are many third party tools available for profiling Node.js applications but, in many cases, the easiest option is to use the Node.js built in profiler. The built in profiler uses the profiler inside V8 which samples the stack at regular intervals during program execution', 'As an asynchronous event driven JavaScript runtime, Node is designed to build scalable network applications', NULL, 'ACTIVE', '', NULL, 't3KOV96iuBR1Ip8qzSvw', NULL, NULL, '2018-11-15 13:11:26', '2018-11-15 13:11:26'),
(41, 2, 'test', 'five', 'test5@skillti.com', '25d55ad283aa400af464c76d713c07ad', NULL, 'B.Com', 'security databse', '9/63, Palm Olympia Gaur City', '', 'Mathura', 'Uttar Pradesh', 'India', '281502', '09761453003', NULL, 'skiltti.com', '6 years', 'Yes', 'Certificate1', 'Database security refers to the collective measures used to protect and secure a database or database management software from illegitimate use and malicious threats and attacks.', 'Database security refers to the collective measures used to protect and secure a database ', NULL, 'ACTIVE', '', NULL, 'DtH1OTFsA7nuYgmb0vjk', NULL, NULL, '2018-11-15 13:12:10', '2018-11-15 13:12:10'),
(42, 2, 'test', 'six', 'test6@skillti.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Australia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '', NULL, 'rKCHMNdZAbpYnkFR9zth', NULL, NULL, '2018-11-15 13:12:45', '2018-11-15 13:12:45'),
(43, 2, 'test', 'seven', 'test7@skillti.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Argentina', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '', NULL, 'Mf6FshotGL0qlxzvKZuD', NULL, NULL, '2018-11-15 13:13:23', '2018-11-15 13:13:23'),
(44, 2, 'test', 'eight', 'test8@skillti.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'American Samoa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '', NULL, 'IwR6ux7cPmyDTGi5M1QW', NULL, NULL, '2018-11-15 13:14:01', '2018-11-15 13:14:01'),
(45, 2, 'test', 'nine', 'test9@skillti.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'American Samoa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '', NULL, 'VRcQDvKrHIC03LnjPxBy', NULL, NULL, '2018-11-15 13:14:33', '2018-11-15 13:14:33'),
(46, 2, 'test', 'ten', 'test10@skillti.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Azerbaijan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '', NULL, 'n2V03Z7hEdS8BLAsJ5qx', NULL, NULL, '2018-11-15 13:15:07', '2018-11-15 13:15:07'),
(47, 2, 'vineet', 'tyagi', '123vineettyagi@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '', NULL, '03RaetlvwfZOpSQzMXHy', NULL, NULL, '2018-11-21 10:44:35', '2018-11-21 10:44:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`CatID`);

--
-- Indexes for table `configurations`
--
ALTER TABLE `configurations`
  ADD PRIMARY KEY (`Configurationsid`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`CID`);

--
-- Indexes for table `menumaster`
--
ALTER TABLE `menumaster`
  ADD PRIMARY KEY (`MenuMasterID`);

--
-- Indexes for table `messagedata`
--
ALTER TABLE `messagedata`
  ADD PRIMARY KEY (`MID`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`RID`);

--
-- Indexes for table `rolemenu`
--
ALTER TABLE `rolemenu`
  ADD PRIMARY KEY (`RoleMenuID`),
  ADD KEY `plan_id_idx` (`RoleID`),
  ADD KEY `planmenu_plan_id_idx` (`RoleID`),
  ADD KEY `planmenu_menu_id_idx` (`MenuID`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`RoleID`);

--
-- Indexes for table `socialmedialink`
--
ALTER TABLE `socialmedialink`
  ADD PRIMARY KEY (`SID`);

--
-- Indexes for table `syllabus`
--
ALTER TABLE `syllabus`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserID`),
  ADD KEY `role_id_idx` (`RoleID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `CatID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `configurations`
--
ALTER TABLE `configurations`
  MODIFY `Configurationsid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `CID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `menumaster`
--
ALTER TABLE `menumaster`
  MODIFY `MenuMasterID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messagedata`
--
ALTER TABLE `messagedata`
  MODIFY `MID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `RID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `rolemenu`
--
ALTER TABLE `rolemenu`
  MODIFY `RoleMenuID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `RoleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `socialmedialink`
--
ALTER TABLE `socialmedialink`
  MODIFY `SID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `syllabus`
--
ALTER TABLE `syllabus`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
