
<!-- jQuery -->
<script src="{{asset('assets/vendor/jquery.min.js')}}"></script>

<!-- Bootstrap -->
<script src="{{asset('assets/vendor/popper.min.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap.min.js')}}"></script>

<!-- Perfect Scrollbar -->
<script src="{{asset('assets/vendor/perfect-scrollbar.min.js')}}"></script>

<!-- MDK -->
<script src="{{asset('assets/vendor/dom-factory.js')}}"></script>
<script src="{{asset('assets/vendor/material-design-kit.js')}}"></script>

<!-- App JS -->
<script src="{{asset('assets/js/app.js')}}"></script>

<!-- Highlight.js -->
<script src="{{asset('assets/js/hljs.js')}}"></script>

<!-- App Settings (safe to remove) -->
<script src="{{asset('assets/js/app-settings.js')}}"></script>

<!-- Global Settings -->
{{--
<script src="{{asset('assets/js/settings.js')}}"></script>
--}}

<!-- Moment.js -->
<script src="{{asset('assets/vendor/moment.min.js')}}"></script>
<script src="{{asset('assets/vendor/moment-range.min.js')}}"></script>

<!-- Chart.js -->
<script src="{{asset('assets/vendor/Chart.min.js')}}"></script>

<!-- UI Charts Page JS -->

<!-- List.js -->
<script src="{{asset('assets/vendor/list.min.js')}}"></script>
<script src="{{asset('assets/js/list.js')}}"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
<link rel="manifest" href="{{asset('manifest.json')}}">
<script>
    function scroll() {
        var messageBody = document.querySelector(".app-messages__container [data-perfect-scrollbar]");
        messageBody.scrollTop = messageBody.scrollHeight - messageBody.offsetHeight - 16
    }
    var config = {
        apiKey: "{{env('apiKey')}}",
        authDomain: "{{env('authDomain')}}",
        databaseURL: "{{env('databaseURL')}}",
        projectId: "{{env('projectId')}}",
        storageBucket: "{{env('storageBucket')}}",
        messagingSenderId: "{{env('messagingSenderId')}}"
    };
    firebase.initializeApp(config);
    const messaging = firebase.messaging();
    @if(Request::route()->getName() == 'mentor.chatbot')
    messaging.requestPermission().then(function () {
        getRegToken();
        $('#Notpermission').hide();
        $('#permission').show();
    }).catch(function (err) {
        console.log('error', err);
        $('#Notpermission').show();
        $('#permission').hide();
    });
    $(document).ready(function () {
        scroll();
    });
    @endif
    function getRegToken() {
        messaging.getToken().then(function (currentToken) {
            if (currentToken) {
                saveToken(currentToken);
                console.log('Token:', currentToken);
            }
        }).catch(function (err) {
            console.log('An error occurred while retrieving token. ', err);
        });
    }

    function setTokenSentToServer(sent) {
        window.localStorage.setItem('sentToServer', sent ? 1 : 0);
        console.log('token status', sent);
    }

    function saveToken(currentToken) {
        $.ajax({
            url: '{{(Auth::user()->roleId == 2)?url('dashboard/saveToken'):url('student-dashboard/saveToken')}}',
            method: 'post',
            data: {'token':currentToken, '_token': '{{csrf_token()}}'},
            dataType: 'JSON'
        }).done(function (result) {
            if(result.error == false) {
                setTokenSentToServer(true);
            }else{
                setTokenSentToServer(false);
            }
        })
    }

    messaging.onMessage(function (payload) {
        console.log('payload', payload);
        notificationTitle = payload.data.title;
        notificationOptions = {
            body: payload.data.body,
            icon: payload.data.icon,
            image: payload.data.image,
            actions: payload.notification.click_action
        };
        console.log('noti', notificationOptions);
        @if(Request::route()->getName() == 'mentor.chatbot')
        var message = '<li class="message d-inline-flex">\n' +
            '    <div class="message__aside">\n' +
            '        <a href="javascript:void(0)" class="avatar">\n' +
            '            <img src="'+ payload.data.profilePic +'" alt="people" class="avatar-img rounded-circle">\n' +
            '        </a>\n' +
            '    </div>\n' +
            '    <div class="message__body card">\n' +
            '        <div class="card-body">\n' +
            '            <div class="d-flex align-items-center">\n' +
            '                <div class="flex mr-3">\n' +
            '                    <a href="javascript:void(0)" class="text-body"><strong>'+ payload.data.sendorNeme +'</strong></a>\n' +
            '                </div>\n' +
            '                <div>\n' +
            '                    <small class="text-muted">'+ payload.data.date +'</small>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <span class="text-black-70">'+ payload.data.body +'</span>\n';
            if(payload.data.attachmentName != '' && payload.data.attachmentName != null) {
                message += ' <a target="_blank" href="' + payload.data.attachment + '" class="media align-items-center mt-2 text-decoration-0 bg-white px-3">\n' +
                    '                    <span class="avatar mr-2">\n' +
                    '                        <span class="avatar-title rounded-circle">\n' +
                    '                            <i class="material-icons font-size-24pt">attach_file</i>\n' +
                    '                        </span>\n' +
                    '                    </span>\n' +
                    '                    <span class="media-body" style="line-height: 1.5">\n' +
                    '                        <span class="text-primary">' + payload.data.attachmentName + '</span><br>\n' +
                    '                    </span>\n' +
                    '                </a>\n';
            }
            message +=' </div>\n' +
            '    </div>\n' +
            '</li>';
        if('{{$courseId}}' == payload.data.courseId) {
            if('{{Auth::user()->roleId}}' != payload.data.sender) {
                $('#messages').append(message);
                scroll();
            }
        }else{
            window.location.href = notificationOptions.actions;
            var notification = new Notification(notificationTitle, notificationOptions);
        }
        @else
            var notification = new Notification(notificationTitle, notificationOptions);
        @endif
    });
</script>
@yield('scripting')




