@extends('header.admin_header')
@section('headtitle', "A Learning Marketplace")
@section('headdesc', "A Learning Marketplace")
<style>
    .modal-backdrop {
        position: relative !important;
</style>
@section('maincontent')
    <div class="main-panel">
        <div class="main-content">
            <div class="content-wrapper"><!-- HTML (DOM) sourced data -->
                <section id="html">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body ">
                                    <form class="form px-3 mt-2">
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="fa fa-th-list"></i> Purchase Course List

                                            </h4>
                                        </div>
                                    </form>
                                    <div class="card-block card-dashboard table-responsive">
                                        <table class="table table-striped table-bordered sourced-data">
                                            <thead>
                                            <tr>
                                                <th>SNO</th>
                                                <th>Mentor</th>
                                                <th>Mentee</th>
                                                <th>Skills</th>
                                                <th>Duration</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Action</th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(!empty($coursePurchase))
                                                @foreach($coursePurchase as $k => $mentor)
                                                    <tr>
                                                        <td>{{($k + 1)}}</td>
                                                        <td  class="nowrap">
                                                            <a target="_blank" href="{{url('admin/add-edit-mentor/'.custom_encode($mentor->mentorId))}}">{{$mentor->mentorName}}</a>
                                                        </td>
                                                        <td  class="nowrap">
                                                            <a target="_blank" href="{{url('admin/add-edit-mentee/'.custom_encode($mentor->menteeId))}}">{{$mentor->menteeName}}</a>
                                                        </td>
                                                        <td>
                                                            @foreach(json_decode($mentor->skills, true) as $skill)
                                                                <span class="text text-bold">{{$skill}},</span>&nbsp;
                                                                &nbsp
                                                            @endforeach
                                                        </td>
                                                        <td>
                                                            @if($mentor->courseDuration == 'No limit')
                                                            N/A
                                                            @else
                                                            {{$mentor->courseDuration}} Years
                                                            @endif
                                                        </td>
                                                        <td class="nowrap">{{format_date($mentor->created_at)}}</td>
                                                        <td class="nowrap"><a onclick="return confirm('Do you want to change course sattus?');" id="changeStatus" href="{{url('admin/changes-status/'.custom_encode($mentor->id))}}">{{($mentor->courseStatus==1)?'Completed':'In Progress'}}</a></td>
                                                        <td class="text-center nowrap">
                                                            <a target="_blank" title="All purched courses" href="{{url('admin/all-skill/'.custom_encode($mentor->id))}}">
                                                                <i class="fa fa-eye" aria-hidden="true"
                                                                   style="font-size: 20px !important;"></i>
                                                            </a>
                                                            &nbsp;&nbsp
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ HTML (DOM) sourced data -->
            </div>
        </div>
    </div>



    <!-- Modal -->

@endsection
@section('scripting')
@endsection
@section('footer')
    @include('footer.footer_admin')
@endsection
