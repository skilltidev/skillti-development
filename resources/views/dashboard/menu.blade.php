<div class="mdk-drawer js-mdk-drawer" id="default-drawer">
    <div class="mdk-drawer__content ">
        <div class="sidebar sidebar-left sidebar-dark bg-dark o-hidden" data-perfect-scrollbar>
            <div class="sidebar-p-y">
                <div class="sidebar-heading">APPLICATION</div>
                <ul class="sidebar-menu sm-active-button-bg">
                    @if(Auth::user()->roleId == 3)
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'dashboard')?'active':''}}">
                        <a class="sidebar-menu-button" href="{{url('dashboard')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">account_box</i> Student Dashboard
                        </a>
                    </li>
                    @elseif(Auth::User()->roleId == 2)
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'dashboard')?'active':''}}">
                        <a class="sidebar-menu-button" href="{{url('dashboard')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">school</i> Instructor Dashboard
                        </a>
                    </li>
                    @endif
                </ul>
                <ul class="sidebar-menu sm-active-button-bg">
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'mentor.addSyllabus' || Request::route()->getName() == 'mentor.syllabus')?'active open':''}}">
                        <a class="sidebar-menu-button" data-toggle="collapse" href="#course">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">import_contacts</i> Course Manager
                            <span class="ml-auto sidebar-menu-toggle-icon"></span>
                        </a>
                        <ul class="sidebar-submenu sm-indent collapse {{(Request::route()->getName() == 'mentor.addSyllabus' || Request::route()->getName() == 'mentor.syllabus')?'show':''}}" id="course">
                            <li class="sidebar-menu-item {{(Request::route()->getName() == 'mentor.syllabus')?'active':''}}">
                                <a class="sidebar-menu-button" href="{{url('dashboard/Syllabus')}}">
                                    <span class="sidebar-menu-text">All Syllabus</span>
                                </a>
                            </li>
                            <li class="sidebar-menu-item {{(Request::route()->getName() == 'mentor.addSyllabus')?'active':''}}">
                                <a class="sidebar-menu-button" href="{{url('dashboard/add-syllabus')}}">
                                    <span class="sidebar-menu-text">Add New Syllabus</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    {{--chat message--}}
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'mentor.chatbot')?'active':''}}">
                        <a class="sidebar-menu-button" href="{{url('dashboard/chat-bot')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">comment</i>Messages
                        </a>
                    </li>
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'mentor.mailbox')?'active':''}}">
                        <a class="sidebar-menu-button" href="{{url('dashboard/mail-box')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">mail_outline</i>Mailbox
                        </a>
                    </li>
                    {{--chat message--}}
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'mentor-profile')?'active':''}}">
                        <a class="sidebar-menu-button" href="{{url('dashboard/mentor-profile')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">language</i> Public Profile
                        </a>
                    </li>
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'mentor-profile-edit')?'active':''}}">
                        <a class="sidebar-menu-button" href="{{url('dashboard/mentor-profile-edit')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">account_box</i> Account Settings
                        </a>
                    </li>
                </ul>
                <div class="sidebar-heading">Settings</div>
                <ul class="sidebar-menu">
                    <li class="sidebar-menu-item {{(Request::route()->getName() == 'password-setting')?'active':''}}">
                        <a class="sidebar-menu-button" href="{{url('dashboard/password-setting')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">settings</i> Password Setting
                        </a>
                    </li>
                    <li class="sidebar-menu-item">
                        <a class="sidebar-menu-button" href="{{url('logout')}}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">lock_open</i> Logout
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>